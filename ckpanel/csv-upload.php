
<html>
<head>
<title>CSV Upload | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php 
require_once 'header.php';
?>

<!--*****************************  HEADER  ************************************** -->

<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-form.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/upload_progress.js"></script>
<style>

table {
	background-color: transparent;
}
tbody {
	display: table-row-group;
	vertical-align: middle;
	border-color: inherit;
}
.header-cell .state-error  {
	text-align: center;
	font-weight: normal;
}
#map-columns {
	overflow: auto;
	padding-left:10px;
}
.below12 {
	margin-bottom: 12px !important;
}
#import-map th.map-activecol {
	border-top-color: #7FCE75;
}
#import-map .map-activecol {
	border-bottom: none;
	color: #484848;
	text-align: left;
	border-color: #7FCE75;
}
#import-map th {
	height: 130px;
	min-width: 200px;
	text-align: middle;
	vertical-align: middle;
	border-top: 2px solid #e0e0e0;
}
#import-map td, #import-map th {
	font-size: 12px;
	border-left: 2px solid #e0e0e0;
	border-right: 2px solid #e0e0e0;
	padding: 6px 6px;
}
.table-bordered, td, th {
	border-radius: 0!important;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
#import-map th.unnamed {
	border-top-color: #ee836e;
}
#import-map .unnamed {
	border-bottom: none;
	border-color: #ee836e;
}
select {
	padding: 3px 4px;
	height: 30px;
}
.form-control, select {
	border-radius: 0;
	-webkit-box-shadow: none!important;
	box-shadow: none!important;
	color: #858585;
	background-color: #fff;
	border: 1px solid #d5d5d5;
}
tr {
	display: table-row;
	vertical-align: inherit;
	border-color: inherit;
}
#import-map td.map-activecol {
	background-color: #ADE2A6;
}
#import-map td.unnamed {
	background-color: #fbe3e4;
}
#import-map td {
	border-top: 1px dotted #e0e0e0;
}
#import-map tbody tr:last-child .map-activecol {
	border-bottom-color: #7FCE75;
}
#import-map tbody tr:last-child td {
	border-bottom: 2px solid #e0e0e0;
}
#import-map tbody tr:last-child .unnamed {
	border-bottom-color: #ee836e;
}
#import-map tbody tr:last-child td {
	border-bottom: 2px solid #e0e0e0;
}

</style>

<script type="text/javascript">
	function validateExtension(){
		let res = ''			
		var allowedFiles = [".csv"];
		var fileUpload = document.getElementById("file");
		var feedback = document.getElementById("feedback");
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
		// console.log(fileUpload.value.toLowerCase())
		if (!regex.test(fileUpload.value.toLowerCase())) {
			feedback.innerHTML = "Please upload files having extension: <b>" + allowedFiles.join(', ') + "</b> only.";
			res =  false;
		}else{
			feedback.innerHTML = "";
			res =  true;
		}
		if(res === true){
			$("#next").html("<span class='fa fa-spinner fa-spin'></span> Uploading...")
		}
		return res;
	}    
</script>
<?php
//*********************** CHECK FOR SESSION ****************************//

function checkFields($field_array, $fields){
    $checkFields = '';
    if(is_array($field_array) > 0 && is_array($fields) && !empty($fields)){
        $uncheck_arr = array();
        foreach($field_array as $key=>$value){
            if(!in_array(trim($value), $fields)){
                array_push($uncheck_arr, $value);
            }
        }
        if(!empty($uncheck_arr)){
            $checkFields = implode(',',$uncheck_arr);
        }
		// echo "<pre>";print_r($uncheck_arr); die;
    }
    return $checkFields;
}

if(isset($_POST['submit']))
{
	// echo "<pre>";print_r($_POST);die;
	require 'report-upload.php';
	// die;
}

?>

    <h1 class="stats"><span class="fa fa-upload"></span> CSV Upload</h1><br/><br/><br/>
    	<div class="form-container">
    		<h3>*Upload reports into database</h3>
			<?php if(isset($_SESSION['error_x'])){ ?>
				<div class="alert alert-danger alert-dissmissable" style="width: 85%;">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<?=@$_SESSION['error_x'];?>
				</div>
			<?php unset($_SESSION['error_x']); } ?>					
			<div class="display_error" style="margin-top: 10px;"><?=@$display_error;?></div>

		
			<div align='center' id='output'>
				<?php if(!empty($success_msg)){  echo $success_msg; } ?>
				<?php if(!empty($error_msg)){  echo '<div class="alert alert-danger alert-form">'.@$error_msg.'</div>';  } ?>
			</div>
			<?php if(empty($success_msg)){ ?>
			<form id='form' class="form-horizontal"  action="<?=BASE_URL?>csv-upload" method='post' enctype='multipart/form-data'>
			<?php if(@$customized == 1){ ?>				
				<input type="hidden" name='flag'  value='0'/>
				<input type="hidden" name='csv_format'  value='0'/>
				<input type="hidden" name='customized'  value='1'/>
				<input type="hidden" name='file_type'  value='<?=@$file_type?>'/>
				<input type="hidden" name='file_name'  value='<?=@$file_name?>'/>
				<input type="hidden" name='org_file_name'  value='<?=@$org_file_name?>'/>
				<!-- <input type="hidden" name='table'  value='<?=@$table?>'/> -->
				<!-- <input type="hidden" name='category_id'  value='<?=@$category_id?>'/>
				<input type="hidden" name='publisher_id'  value='<?=@$publisher_id?>'/> -->
				<input type="hidden" name='org_file_header'  value='<?=json_encode(@$org_file_header)?>'/>
				<input type="hidden" name='file_header'  value='<?=json_encode(@$file_header)?>'/>
				<input type="hidden" name='sheetData'  value='<?=json_encode(@$sheetData)?>'/>
				<input type="hidden" name='columns' id='total_columns'  value='<?=count(@$file_header)?>'/>
				
				<?php 
					$current_header = @$file_header;					
					// echo '<pre>'; print_r($current_header);exit;
				?>
				<div class="below12 relative table-responsive" id="map-columns">
					<table id="import-map">
						<tbody>
							<tr>
								<?php
									if(is_array($current_header) && !empty($current_header)){
										$cls = array();
										$int = 0;
										// echo '<pre>'; print_r($current_header);
										foreach($current_header as $k => $h){
											// echo strtolower(@$GLOBALS['CUSTOM_CSV_REPORT_HEADER'][$h]).'=='.strtolower($h).'<br>';
											$cls[$k] = ((strtolower(@$GLOBALS['CUSTOM_CSV_REPORT_HEADER'][$h]) == strtolower($h)) ? 'map-activecol' : 'unnamed');
								?>
											<th class="header-cell <?=((strtolower(@$GLOBALS['CUSTOM_CSV_REPORT_HEADER'][$h]) == strtolower($h)) ? 'map-activecol' : 'unnamed')?> cust_hdr_cln_<?=$int?>" id="header-<?=$int?>">
												<?php
													$matched_h = 'skip'; $notmatched = $org_file_header[$k];
													if(strtolower(@$GLOBALS['CUSTOM_CSV_REPORT_HEADER'][$h]) == strtolower($h)){
														$matched_h = $h;
													}
													echo '<center class="field">'.form_dropdown('custom_header['.$k.']', $GLOBALS['CUSTOM_CSV_REPORT_HEADER'], $matched_h, 'class="required custom_header" id="col-name-'.$int.'" map="'.$h.'" required').'</center>';
													echo '<center><br />'.$notmatched.'</center>';
												?>
											</th>
								<?php
											$int++;
										}
									}
								?>
							</tr>
							<?php
							if(@$current_header){
								$int = 0;
								if(is_array(@$current_header) && !empty(@$current_header)){
									if(isset($sheetData) && !empty(@$sheetData)){
										foreach($sheetData as $k => $val){
							?>
								<tr>
									<?php
										foreach($val as $kk => $vv){
									?>
										<td class="<?=@$cls[$kk]?>  cust_hdr_cln_<?=@$int?> "><?=(is_string(@$vv) && strlen(@$vv) > 200) ?  substr(trim(@$vv),0,200) : @$vv ?></td>
									<?php $int++;}?>
								</tr>
							<?php
								$int = 0;
										}
									}
								}
							}
							?>
						</tbody>
					</table>
				</div>
				
				<div class="control-group">
					<label class="control-label"></label>
					<div class="controls">
						<button type="submit"class="btn btn-upload" name='submit'><span class="fa fa-upload"></span> Import CSV</button>
						<button class="btn btn-danger btn-lg" type="button" onclick="javascript:history.back();"><i class="fa fa-times"></i> Cancel</button>
					</div>
					<br />
				</div>
			<?php }else{ ?>			
					<input type="hidden" name='flag'  value='1'/>
					<input type="hidden" name='customized'  value='0'/>
					<input type="hidden" name='csv_format'  value='1'/>
					<!-- <div class="form-group">
						<div class="col-md-10">
							<label class="control-label">Report Category<span class="star">*</span> :</label>
							<select class="form-control" name="category_id" id="category" title="Report Category"  required>
								<option value="">Select Category </option>		
								<?php
									$ReportCategory = "select * from mr_sub_cat_1 order by sc1_name";
									$ReportCategoryQuery = $link->query($ReportCategory);
									if ($ReportCategoryQuery->num_rows > 0){
										while ($row = $ReportCategoryQuery->fetch_array()){   										
											$cat_id = $row['id'];
											$selected =  (isset($category_id) && @$category_id == $cat_id ) ? 'selected' : ''  ;                     
											echo "<option value='".$cat_id."' ".@$selected."   >".$row["sc1_name"]."</option>";                                
										}
									}                        
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-10">
							<label class="control-label">Publisher<span class="star">*</span> :</label>
							<select class="form-control" name="publisher_id" id="publisher" title="Report publisher"  required>
								<option value="">Select Publisher </option>
								<?php
									$ReportPublisher = "select * from mr_publisher order by publisher_name";
									$ReportPublisherQuery = $link->query($ReportPublisher);
									if ($ReportPublisherQuery->num_rows > 0){
										while ($row = $ReportPublisherQuery->fetch_array()){ 
											$pub_id = $row['id'];  
											$selected =  (isset($publisher_id) && @$publisher_id == $pub_id ) ? 'selected' : ''  ;                     
											echo '<option value="'. @$pub_id.'" '.@$selected.'>'.$row["publisher_name"].'</option>';                                
										}
									}
								?>
							</select>
						</div>
					</div> -->
					<div class="form-group">
						<div class="col-md-10">
							<label class="control-label">SELECT FILE <span class="star">*</span> :</label>
							<input type="file" id='file' accept=".csv" name="csv_file" class="form-control" required="" />
							<span id='feedback' class="text-danger"></span>
						</div>
					</div>
					<div>
						<!--<button type="submit" name="submit" class="btn btn-upload" onclick='upload_image();'><span class="fa fa-upload"></span> Upload</button>-->
						<button type="submit" onclick="return validateExtension();" id="next" name="submit" class="btn btn-upload"><span class="fa fa-upload"></span> Next</button>
						<a href="<?=BASE_URL?>example_csv.csv" download="" target='_blank'>Example CSV File</a>
					</div><br/>
					<div class="row">
						<div class="col-md-10">
							<div class="jumbotron">
								<div class="text-danger text-center"><strong>NOTE : Following category and publisher ids should exist in csv file</strong></div>
								<div class="alert alert-info">
									<details>
									<summary><strong>Categories <span class="fa fa-arrow-right"></span> : </strong></summary>
									<ul>
										<li><mark>Category ID => Category Name</mark></li>
									<?php
										$category_query=$link->query("select id,sc1_name from mr_sub_cat_1 order by id");
										while($row=$category_query->fetch_assoc()){
											echo '<li>'.$row['id']." => ".$row['sc1_name'].'</li>';
										}
									?>
									</ul>
									</details>
								</div>
								<div class="alert alert-info">
									<details>
									<summary><strong>Publishers <span class="fa fa-arrow-right"></span> : </strong></summary>
									<ul>
										<li><mark>Publisher ID => Publisher Name</mark></li>
									<?php
										$publisher_query=$link->query("select id,publisher_name from mr_publisher order by id");
										while($row=$publisher_query->fetch_assoc()){
											echo '<li>'.$row['id']." => ".$row['publisher_name'].'</li>';
										}
									?>
									</ul>
									</details>
								</div>
							</div>
						</div>
					</div>
			<?php } ?>
			</form>
			<?php } ?>
		</div><br/>

		<!-- <div class='progress' id="progress_div">
			<div class='bar' id='bar'></div>
			<div class='percent' id='percent'>0%</div>
		</div>
		<div id='output'></div> -->
		

<script>
	$(document).ready(function(){
		var selected_arr = new Array();
		$('.custom_header').on('change', function(){
			if($(this).val() != 'skip' && $(this).val() != ''){
				var xx = $(this).prop('id');
				var id = xx.split("-");
				var header = $(this).val();
				var total = Number($("#total_columns").val());
				//alert(id +'='+ header +'='+ total);
				for(var i =0; i <=total; i++){
					if($("#col-name-"+i).val() == header){  //  && i == id[2]
						//$('.display_error').show();
						//$('.display_error').html('<code>'+header+" is already used for other column!<code>");
						$("#col-name-"+i).prop('value', "skip");
						$(".cust_hdr_cln_"+i).removeClass('map-activecol').addClass('unnamed');
						selected_arr[i] = "skip";
						//return false;
					}
					$('.display_error').hide();
				}
				$(this).prop('value', header);
				$(this).parent('center').parents('th').removeClass('unnamed').addClass('map-activecol');
				$(".cust_hdr_cln_"+id[2]).removeClass('unnamed').addClass('map-activecol');
				selected_arr[id[2]] = $(this).val();
			}else{
				var xx = $(this).prop('id');
				var id = xx.split("-");
				$(".cust_hdr_cln_"+id[2]).removeClass('map-activecol').addClass('unnamed');
				$("#col-name-"+id[2]+'  option[value="skip"]').prop('selected', true);
			}
		});
	});
</script>

    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
