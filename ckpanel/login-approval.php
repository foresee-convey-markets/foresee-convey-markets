
<html>
<head>
<title>User Authentication | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>



    <h1 class="stats"><span class="fa fa-lock"></span> User Authentication</h1><br/><br/><br/>
    
    <div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
    				<th>Sr No</th>
    				<th>Name</th>
    				<th>Email</th>
    				<th>Role</th>
    				<th>Status</th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php
    			$getUsers=$link->query("select * from mr_login where user_type!='1'");
    			if($getUsers->num_rows > 0){
                    $i=1;
	    			while($row=$getUsers->fetch_assoc()){
                        $id = $row['id'];
                        $status = @$row['status'];
	    				echo "<tr><td>".$i++."</td>";
	    				echo "<td>".$row["name"]."</td>";
	    				echo "<td>".$row["email"]."</td>";
	    				echo "<td>".@$USER_TYPES[$row["user_type"]]."</td>";
	    				                          
                        $checked = @$status == '1' ? 'checked' : '';
                        echo getStatus($id, @$status, $STATUS, $checked, 6, 1);                        

                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='5' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO USERS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
