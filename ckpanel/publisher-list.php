
<html>
<head>
<title>Publisher List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>


    <h1 class="stats"><span class="fas fa-list-ol"></span> Publisher List</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.location.href='<?php echo BASE_URL.'publisher-add';?>'">
        <a href="<?php echo BASE_URL.'publisher-add';?>"><i class="fa fa-plus"></i> Add New Publisher</a> 
    </button>

    <?php if($_SESSION["user_type"]=='1'){  ?>
    <button  class='btn btn-danger helper-item' onclick="window.location.href='<?php echo BASE_URL.'publisher-list';?>'">
        <a href="<?php echo BASE_URL.'publisher-archive-list';?>"><i class="fa fa-archive"></i> Publisher Archive List</a> 
    </button>
    <?php } ?>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
    				<th>Publisher ID</th>
                    <th>Publisher</th>
                    <th>Publisher Code</th>

                <?php if($_SESSION["user_type"]=='1'){ ?>
                    <th>Status</th>
                    <th>Archive</th>
    				<th>Options</th>
                <?php } ?>

    			</tr>
    		</thead>
    		<tbody id='deleteCount'>
    			<?php
    			$getPublishers=$link->query("select * from mr_publisher order by id");
    			if($getPublishers->num_rows > 0){
	    			while($row=$getPublishers->fetch_assoc()){
                        $id = $row['id'];
                        $status = @$row['status'];
                        $archive_status = @$row['archive_status'];
	    				echo "<tr id='publisherDelete".$id."'><td>".$id."</td>";
                        echo "<td>".$row["publisher_name"]."</td>";
                        echo "<td>".$row["publisher_code"]."</td>";

                    if($_SESSION["user_type"]=='1'){                         
                        $checked = @$status == '1' ? 'checked' : '';
                        $archive_checked = @$archive_status == '1' ? 'checked' : '';
                        echo getStatus($id, @$status, $STATUS, $checked, 3, 1);                        
                        echo getArchiveStatus($id, @$archive_status, $ARCHIVE_STATUS, $archive_checked, 3, 2);                        

	    				echo "<td colspan='2'  align='center'><a href='publisher-edit?id=".$id."'><i class='far fa-edit text-primary'></i></a>

                            <button data-toggle='modal' data-target='#removeMe".$id."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                                <div id='removeMe".$id."' class='modal fade' role='dialog'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button class='btn btn-primary btn-confirm' type='submit' onclick='deleteData(".$id.", 3)' >Confirm</button>
                                                <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div></td>";
                        }
                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='6' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO PUBLISHERS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
    