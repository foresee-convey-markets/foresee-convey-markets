<?php require_once 'config.php';

  //************************************  LOGIN & REGISTER   **************************//

    if(isset($_POST['register'])){
        $name=mysqli_real_escape_string($link,$_POST["name"]);
        $email=mysqli_real_escape_string($link,$_POST["email"]);
        $password=md5(mysqli_real_escape_string($link,$_POST["password"]));
        $orig_password=mysqli_real_escape_string($link,$_POST["password"]);
        $user_type=mysqli_real_escape_string($link,$_POST["user_type"]);
        $date=date("Y-m-d H:i:s");
        if(!empty(@$name) && !empty(@$email) && !empty(@$orig_password) && !empty(@$user_type) ){

            $getRegistrationDetails=$link->query("select * from mr_login where email='".$email."' and password='".$password."'and user_type='".$user_type."' ");
        
            if(@$getRegistrationDetails && @$getRegistrationDetails->num_rows > 0){
                $getRegistrationVerificationDetails=$link->query("select * from mr_login where email='".$email."'and password='".$password."'and user_type='".$user_type."' and status='1'");
                if($getRegistrationVerificationDetails->num_rows > 0){
                    echo "<h4 class='alert alert-success text-center'>You have already registered with us ! Please login</h4> ";
                }else{
                    echo "<h4 class='alert alert-danger text-center'>We have received your registration request. Please wait till we verify the details !</h4> ";
                }
            }else{
                $getEmailAddress=$link->query("select * from mr_login where  email='".$email."'");

                if($getEmailAddress->num_rows > 0){
                    echo "<h4 class='alert alert-danger text-center'>Sorry ! This Email Address already exists !</h4> ";
                }else{
                    $insertData=$link->query("insert into mr_login(name,email,password,orig_password,user_type,created_at) values('".$name."','".$email."','".$password."','".$orig_password."','".$user_type."','".$date."') ");
                    if($insertData){
                        echo "<h4 class='alert alert-success text-center'>Registration Successful </h4> ";
                    }else{
                        echo "<h4 class='alert alert-danger text-center'>Error While Processing Your Request".mysqli_error($link)."</h4> ";
                    }
                }
            }
            
        }else{
            echo "<h4 class='alert alert-danger text-center'>All fields are required.</h4>";
        }
        

    }


    if(isset($_POST['login'])){
        // $user_type=mysqli_real_escape_string($link,$_POST["user_type"]);
        // echo '<pre>';print_r($_POST);die;
        $email=mysqli_real_escape_string($link,$_POST["email"]);
        $password=md5(mysqli_real_escape_string($link,$_POST["password"]));
        if(!empty(@$email) && !empty(@$password)){

            $getLoginDetails=$link->query("select * from mr_login where password='".$password."' and email='".$email."'");        

            if(@$getLoginDetails && @$getLoginDetails->num_rows > 0){
                while($row=$getLoginDetails->fetch_assoc()){
                    $login_email=$row["email"];
                    $login_password=$row["password"];
                    $login_name=$row["name"];                
                    $user_type=$row["user_type"];

                    if($login_email==$email && $login_password==$password){

                        $getLoginVerificationDetails=$link->query("select * from mr_login where password='".$password."' and email='".$email."' and status='1'");   

                        if($getLoginVerificationDetails->num_rows > 0){
                            $_SESSION["email"]=$login_email;
                            $_SESSION["name"]=$login_name;
                            $_SESSION["user_type"]=$user_type;

                            echo "<h4 class='alert alert-success text-center'>Login Successful.</h4>";
                        }else{
                            echo "<h4 class='alert alert-danger text-center'>Your registration request is pending. Please wait till we verify the details !</h4>"; 
                        }
                    }
                }

            }else{
                echo "<h4 class='alert alert-danger text-center'>Invalid Email Address or Password.</h4>";
            }
        }else{
            echo "<h4 class='alert alert-danger text-center'>All fields are required.</h4>";
        }
    }


// ***********************************  STATUS      ******************************************    
      

// ***********************************  STATUS TOGGLE  ***********************************    

    if(isset($_POST["table"]) && isset($_POST['status_value']) && isset($_POST['id']) && isset($_POST['to_be_changed']))
    {
        $table = mysqli_real_escape_string($link,$_POST["table"]);
        $id = mysqli_real_escape_string($link,$_POST["id"]);
        $to_be_changed = mysqli_real_escape_string($link,$_POST["to_be_changed"]);
        $status_value = mysqli_real_escape_string($link,$_POST["status_value"]);
                    
        $update_qyr=mysqli_query($link,"update ".@$STATUS_TABLES[$table]." set ".@$STATUS_TYPES[$to_be_changed]."=$status_value where id=$id");        
        if($update_qyr){
            echo "Success";
        }else{
            echo "Error";
        }  
    }


//*************************************   DELETE    **********************************************//


if(isset($_POST["type"]) && $_POST['type'] == 'delete' && isset($_POST["table"]) &&  isset($_POST['id']))
{
    $table = mysqli_real_escape_string($link,$_POST["table"]);
    $id = mysqli_real_escape_string($link,$_POST["id"]);
    $where = " where id = $id";
    if($table == '8'){
        $where = " where contact_id = $id";
    }
    $query=mysqli_query($link,"delete from ".@$STATUS_TABLES[$table].@$where);
    if($query){
        echo "Success";
    }else{
        echo 'Error';
    }    
}



//*************************** PROFILE PICTURE UPLOAD *********************************//



if(isset($_FILES["file"]["type"]))
{
    $validextensions = array("jpeg", "jpg", "png");
    $temporary = explode(".", $_FILES["file"]["name"]);
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
    ) && ($_FILES["file"]["size"] < 1000000)//Approx. 100kb files can be uploaded.
    && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0)
        {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        }
        else
        {
            // if (file_exists("assets/images/Profile/" . $_FILES["file"]["name"])) {
            //     echo $_FILES["file"]["name"] . " <span class='text-danger'><b>already exists.</b></span> ";
            // }
            // else
            // {
                $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                $file=$_FILES["file"]["name"];
                $id=$_POST["id"];
                $targetPath ="assets/images/Profile/".$_FILES['file']['name']; // Target path where file is to be stored
                if(move_uploaded_file($sourcePath,$targetPath) ){ // Moving Uploaded file

                    $sql=mysqli_query($link,"update mr_login set profile_image='".$file."' where id='$id' ");
            
                    if($sql===TRUE )
                    {
                        echo "<span class='text-success'>Profile Picture Uploaded Successfully...!!</span><meta http-equiv='refresh' content='1,url=".BASE_URL."profile '/>";
                    }
                }else{
                    echo "<span class='text-danger'>Error while updating profile pricture.<span>";

                }
                
            // }
        }
    }
    else
    {
    echo "<span class='text-danger'>Invalid file Size or Type<span>";
    }
}

?>

