
<html>
<head>
<title>Press Release List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>
<?php 

//********************  PAGINATION  ****************************//
$where = '';
$input_query = '';
if (isset($_GET['query']) && !empty($_GET['query'])) {
    $page_query = "&query=".trim($_GET['query']);
    $input_query = trim($_GET['query']);
    $where.= "title like '%".$input_query."%' ";
}    
if(!empty($where)){
    $where = " where ".$where;
}

$data = create_links($_GET, $link , 'mr_press', $where);
@$data['no_of_records_per_page'] = @$data['no_of_records_per_page'];

if(@$data['total_rows'] > 0 && @$data['pageno'] > @$data['total_pages']){
    $url = BASE_URL.'press-release-list'.@$data['last_page'];
    echo "<meta http-equiv='refresh' content='0, url=".@$url."'/>";
}
$input_placeholder = 'Search here for Press Title';

?>

    <h1 class="stats"><span class="fas fa-list-ol"></span> Press Release List <span class="text-danger">[</span><span class='count text-danger'><?=@@$data['total_rows']?></span><span class="text-danger">]</span></h1><br/><br/>


    <button  class='btn btn-primary helper-item' onclick="window.location.href='<?php echo BASE_URL.'press-release-add';?>'">
        <a href="<?php echo BASE_URL.'press-release-add';?>"><i class="fa fa-plus"></i> Add New PressRelease</a> 
    </button>
    <?php if($_SESSION["user_type"]=='1'){  ?>
    <button  class='btn btn-danger helper-item' onclick="window.location.href='<?php echo BASE_URL.'press-release-archive-list';?>'">
        <a href="<?php echo BASE_URL.'press-release-archive-list';?>"><i class="fa fa-archive"></i> PressRelease Archive List</a> 
    </button>
    <?php } ?>

    <div class="pagination-form">
        <?php require_once 'pagination-form.php' ?>
    </div>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
    				<th>ID</th>
    				<th>Press title</th>

                <?php if($_SESSION["user_type"]=='1'){ ?>
                    <th>Status</th>
                    <th>Archive</th>
    				<th>Options</th>
                <?php } ?>
    			</tr>
    		</thead>
    		<tbody id='deleteCount'>
    			<?php
    			$getPressReleases=$link->query("select * from mr_press ".@$where." order by id limit ". @$data['offset']. "," . @$data['no_of_records_per_page']);
    			if($getPressReleases->num_rows > 0){
	    			while($row=$getPressReleases->fetch_assoc()){
                        $id = $row['id'];
                        $status = @$row['status'];
                        $archive_status = @$row['archive_status'];
	    				echo "<tr id='pressDelete".$row['id']."'><td>".$id."</td>";
	    				echo "<td>".$row["title"]."</td>";

                    if($_SESSION["user_type"]=='1'){                         
                        $checked = @$status == '1' ? 'checked' : '';
                        $archive_checked = @$archive_status == '1' ? 'checked' : '';
                        echo getStatus($id, @$status, $STATUS, $checked, 2, 1);                        
                        echo getArchiveStatus($id, @$archive_status, $ARCHIVE_STATUS, $archive_checked, 2, 2);                        


	    				echo "<td colspan='2'  align='center'><a href='press-release-edit?id=".$id."'><i class='far fa-edit text-primary'></i></a>

                            <button data-toggle='modal' data-target='#removeMe".$id."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                                <div id='removeMe".$id."' class='modal fade' role='dialog'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button class='btn btn-primary btn-confirm' type='submit' onclick='deleteData(".$id.", 2)' >Confirm</button>
                                                <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div></td>";
                        }
                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='5' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO PRESS RELEASES FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/>
    <ul class="pagination">
        <?=pagination(@$data);?>
    </ul>

    <br/><br/>
    



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
    