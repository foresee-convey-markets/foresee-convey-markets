
<html>
<head>
<title>Global Checkout | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->



<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<link href="<?php echo BASE_URL;?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo BASE_URL;?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
		$(".datepicker1").datepicker({dateFormat:"yy-mm-dd",
            onSelect: function(datetext) {
                var d = new Date(); // for now

                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;

                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;

                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;

                datetext = datetext + " " + h + ":" + m + ":" + s;
             $('.datepicker1').val(datetext)   
            }
        });
});
</script>

<?php
if(isset($_GET['id'])){
	$id=$_GET["id"];
	$getLeadDetails=$link->query("select * from mr_form_contact where contact_id='$id'");
	$row=$getLeadDetails->fetch_assoc();
}

?>
<?php
    if(isset($_POST["submit"])){        
        $name=$_POST["name"];
        $email=$_POST["email"];
        $phone=$_POST["phone"];
        $company=$_POST["company"];
        $address=$_POST["address"];
        $price=$_POST["price"];
        $date=$_POST["date"];
        $expiry_date=($_POST["expiry_date"] && !empty($_POST['expiry_date'])) ? $_POST['expiry_date'] : NULL;
        $license=$_POST["license"];
        $title=$_POST["title"];
        $country=$_POST["country"];
        $zip=$_POST["zip"];
        
        $sql=mysqli_query($link,"insert into mr_global_checkout(name,email,title,price,company,phone,license,address,date,expiry_date,country,zip)  values('".$name."','".$email."','".$title."','".$price."','".$company."','".$phone."','".$license."','".$address."','".$date."','".$expiry_date."','".$country."','".$zip."')");
        
		if($sql===TRUE ){			
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Global Checkout Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'global-checkout-list "/>'; 

		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Global Checkout !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		} 
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add New Global Checkout</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <?php if(isset($_GET["id"])){ ?>
			<form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."?id=".$_GET["id"]; ?>" method='post' name="add_checkout" id="add_checkout" enctype="application/x-www-form-urlencoded" >
		<?php }else{ ?>
			<form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="add_checkout" id="add_checkout" enctype="application/x-www-form-urlencoded">
		<?php } ?>   
	    

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Name<span class="star">*</span> :</label>
		    		<input  name="name" class="form-control" required=""    value="<?php echo (isset($row["contact_person"])) ? $row["contact_person"] : ''; ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Email<span class="star">*</span> :</label>
		    		<input type="email" name="email" class="form-control" required=""  value="<?php echo (isset($row["contact_email"])) ? $row["contact_email"] : ''; ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Phone<span class="star">*</span> :</label>
		    		<input  name="phone" class="form-control" required="" value="<?php echo (isset($row["contact_phone"])) ? $row["contact_phone"] : ''; ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Company<span class="star">*</span> :</label>
		    		<input  name="company" class="form-control" required=""  value="<?php echo (isset($row["contact_company"])) ? $row["contact_company"] : ''; ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Address<span class="star">*</span> :</label>
		    		<input  name="address" class="form-control" required="" value="<?php echo (isset($row["contact_country"])) ? $row["contact_country"] : ''; ?>" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Zip<span class="star">*</span> :</label>
		    		<input  name="zip" class="form-control" required="" value="<?php echo (isset($row["zip"])) ? $row["zip"] : ''; ?>" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Country<span class="star">*</span> :</label>
		    		<select class="form-control selectpicker" style='z-index: 9999' data-live-search="true" data-actions-box="true" name="country" id='country'  title="Country"  required>
                        <?php echo getCountryDropdown(); ?>
                    </select>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control" required="" value="<?php echo (isset($row["contact_rep_title"])) ? $row["contact_rep_title"] : ''; ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Price<span class="star">*</span> :</label>
		    		<input type="number"  name="price" class="form-control" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">License<span class="star">*</span> :</label>
		    		<select class="form-control" name="license" id="license" title="License"  required>
                        <option value="">Select </option>
                        <option value="Single User License">Single User</option>
						<option value="Enterprise User License">Enterprise User</option>
						<option value="Corporate User License">Corporate User</option>	
						<option value="Global">Global</option>	
                    </select>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" autocomplete="off" required="" />
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Link Expiry Date :</label>
		    		<input  name="expiry_date" class="form-control datepicker1" autocomplete="off" />
		    	</div>
	    	</div>
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="far fa-edit"></span> Add</button>
		    </div>
	    </form>
		<?php } ?>
	</div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
