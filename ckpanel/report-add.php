
<html>
<head>
<title>Add Report | Foresee Convey Markets</title>


<script type="text/javascript">
$(document).ready(function(){
    $(".datepicker").datepicker({dateFormat:"yy-mm-dd"});
});

</script>

 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->

<?php
    if(isset($_POST["submit"]))
    {
        
        $category=$_POST["category"];
        $publisher=$_POST["publisher"];
        $title=addslashes($_POST["title"]);

        $url=generateReportURL($title);
        $pages=$_POST["page"];
        $content=addslashes($_POST["content"]);
        $description=addslashes($_POST["description"]);
        $toc=addslashes($_POST["toc"]);
        $tof=addslashes($_POST["tof"]);
        $status=$_POST["status"];
        $archive_status=$_POST["archive_status"];

        $published_date=$_POST["date"];
        $date=date("Y-m-d H:i:s");

		// REPORT LICENSES		
		$li_value1=$_POST["single_user"];
		$li_value2=null;
		$li_value3=null;
		if($_POST['enterprise_user']){			
			$li_value2=$_POST["enterprise_user"];
		}
		if($_POST['corporate_user']){			
			$li_value3=$_POST["corporate_user"];
		}
        
        $sql="insert ignore into mr_report (rep_sub_cat_1_id,publisher_id,rep_url,rep_title,rep_descrip,rep_page,rep_date,rep_toc_fig,rep_entry_date,status,archive_status)  
    	VALUES ('".$category."','".$publisher."','".$url."','".$title."','".$description."','".$pages."','".$date."','".$tof."','".$published_date."','".$status."','".$archive_status."')";
        
		if($link->query($sql)){
			$last_id = $link->insert_id;

			$sql="insert ignore into mr_report_metadata (rep_contents,rep_table_of_contents,meta_rep_id)  
			VALUES ('$content','$toc','$last_id')";
			$link->query($sql);	

			$li_key1="Single User License";
			if($li_value1 > 0){	
				$sql1="insert ignore into mr_report_license (li_pub_id,li_rep_id,li_key,li_value)
				VALUES ('$publisher','$last_id','$li_key1','$li_value1')";
				$link->query($sql1); 
			}
			$li_key2="Enterprise User License";
			if($li_value2 > 0){
				$sql2="insert ignore into mr_report_license (li_pub_id,li_rep_id,li_key,li_value)
				VALUES ('$publisher','$last_id','$li_key2','$li_value2')";
				$link->query($sql2);
			}
			$li_key3="Corporate User License";
			if($li_value3 > 0){
				$sql3="insert ignore into mr_report_license (li_pub_id,li_rep_id,li_key,li_value)
				VALUES ('$publisher','$last_id','$li_key3','$li_value3')";
				$link->query($sql3);
			}
			
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Report Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'report-list "/>'; 

		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Report !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add New Report</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="add_report" id="add_report">
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Category<span class="star">*</span> :</label>
		    		<select class="form-control" name="category" id="category" title="Report Category"  required>
                        <option value="">Select Category </option>		
                        <?php
	                        $ReportCategory = "select * from mr_sub_cat_1 order by sc1_name";
	                        $ReportCategoryQuery = $link->query($ReportCategory);
	                        if ($ReportCategoryQuery->num_rows > 0){
	                            while ($row = $ReportCategoryQuery->fetch_array()){
									$category_id = $row['id'];  
									$selected =  (isset($category) && @$category == $category_id ) ? 'selected' : ''  ;                     
									echo "<option value='".$category_id."' ".@$selected."  >".$row["sc1_name"]."</option>";                                
								}
	                        }                        
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher<span class="star">*</span> :</label>
		    		<select class="form-control" name="publisher" id="publisher" title="Report publisher"  required>
                        <option value="">Select Publisher </option>
                        <?php
	                        $ReportPublisher = "select * from mr_publisher order by publisher_name";
	                        $ReportPublisherQuery = $link->query($ReportPublisher);
	                        if ($ReportPublisherQuery->num_rows > 0){
	                            while ($row = $ReportPublisherQuery->fetch_array()){ 
									$publisher_id = $row['id'];  
									$selected =  (isset($publisher) && @$publisher == $publisher_id ) ? 'selected' : ''  ;                     
	                                echo '<option value="'. @$publisher_id.'" '.@$selected.'>'.$row["publisher_name"].'</option>';                                
	                            }
	                        }
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control"  value="<?=@$title?>" required="" />
		    	</div>
	    	</div>

	    	<!-- <div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" required="" />
		    	</div>
	    	</div> -->
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Description<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("description", @$description);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Contents<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("content", @$content);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Contents :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("toc", @$toc);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Figures:</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("tof", @$tof);
                    ?>  
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Pages<span class="star">*</span> :</label>
		    		<input  name="page" class="form-control" value="<?=@$pages?>" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Single User License<span class="star">*</span> :</label>
		    		<input  name="single_user" class="form-control" value="<?=@$li_value1?>" required="" />
		    	</div>
	    	</div>
	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Enterprise User License :</label>
		    		<input  name="enterprise_user" value="<?=@$li_value2?>" class="form-control" />
		    	</div>
	    	</div>
	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Corporate User License :</label>
		    		<input  name="corporate_user" value="<?=@$li_value3?>" class="form-control" />
		    	</div>
	    	</div>
	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Published Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" autocomplete="off" value="<?=@$published_date?>" required="" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?=@$status == '1' ? 'checked' : ''?> value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?=@$status == '0' ? 'checked' : ''?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive Status <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="1" <?=@$archive_status == '1' ? 'checked' : ''?> required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="0" <?=@$archive_status == '0' ? 'checked' : ''?> required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="far fa-edit"></span> Add</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->