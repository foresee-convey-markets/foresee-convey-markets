<?php

ini_set("memory_limit","1000M");
ini_set('max_execution_time', 800);
ini_set('post_max_size', '50M');
ini_set('upload_max_filesize', '50M');
        
ini_set("date.timezone", "Asia/Kolkata");
date_default_timezone_set('Asia/Kolkata');

$file_type = (isset($_FILES['csv_file'])) ? $_FILES['csv_file']['type'] : @$_POST['file_type'];
// echo "<pre>";print_r($file_type) ;exit;
if($file_type === 'application/vnd.ms-excel'){
      
    $filename_tmp = (isset($_POST['file_name'])) ? $_POST['file_name'] : @$_FILES['csv_file']['tmp_name'];
    $fp = shell_exec('perl -pe \'s/\r\n|\n|\r/\n/g\' ' . escapeshellarg($filename_tmp) . ' | wc -l');

    if($_POST['flag'] == 1){
        if(trim($fp) > 500002){
            $_SESSION['error_x'] = 'Oops sorry, Your CSV file contain <strong>'.trim($fp).'</strong> records. It must not contain more than <strong>500,000</strong> records.';
            echo "<meta http-equiv='refresh' content='0, url=".BASE_URL."lead-import'  />";exit;
        }
        
        // $csv_data = array('upload_data' => $this->upload->data());
        
        $csv_file=$_FILES["csv_file"]["name"];
        $dir="../uploads/";
        move_uploaded_file($filename_tmp,$dir.$csv_file);
        $org_file_name = $csv_file;
        $fileName  = realpath($org_file_name);
        $_SESSION['org_file_name'] = $org_file_name;
        $_SESSION['fileName'] = $fileName;
        chmod($fileName, 0777);
        
        // $csv_data = (isset($csv_data)) ? @$csv_data : $this->session->userdata('csv_data');
        $org_file_name = (isset($org_file_name)) ? @$org_file_name : $_SESSION['org_file_name'];
        $fileName  = (isset($fileName)) ? @$fileName :  $_SESSION['fileName'];
    }

    $file_name = (isset($_POST['file_name'])) ? $_POST['file_name'] : @$fileName;
    $org_file_name = (isset($_POST['org_file_name'])) ? $_POST['org_file_name'] : @$org_file_name;

    $field_name = 'Name,Email,Phone,Company,LeadType,LeadDate,ReportTitle';
    $field_array = explode(',',$field_name);
    try{
        $data['custom_header'] = $GLOBALS['CUSTOM_LEAD_HEADER'];
        
        if($_POST['csv_format'] == 1){ // Customized CSV Format
            $sheetData = array();
            //STARTs - read CSV
            $delimiter = ',';
            $header = NULL;
            $loop = 0;
            if(($handle = fopen($file_name, 'r')) !== false){
                while(($row = fgetcsv($handle, 5000, $delimiter)) !== false){
                    if($loop >= 6) continue;
                    if(!$header){
                        $header = $row;
                        $org_file_header = $row;
                    }else{
                        if(count($header)==count($row)){
                            $sheetData[] = $row;
                        }					
                    }
                    $loop++;
                }
                fclose($handle);
            }//end- if
            //ENDs - read CSV
            
            $customized = 1;
            $file_header = recognize(@$org_file_header);
            // echo "<pre>";print_r($file_header);exit;
            if(empty($file_header)){
                $_SESSION['error_x'] = 'Invalid CSV Format. Your CSV file must contain column with name "'.$field_name.'". Use CSV FORMAT : (<strong>'.$field_name.'</strong>)';
                if(file_exists($file_name)){
                    unlink($file_name);
                }
                echo "<meta http-equiv='refresh' content='0, url=".BASE_URL."lead-import'  />";exit;
            }else{
                unset($_SESSION['error_x']);
            }
        }else{
            
            if(!file_exists(@$file_name)){
                $_SESSION['error_x'] =  "Please try again. File doesn't exist";
                echo "<meta http-equiv='refresh' content='0, url=".BASE_URL."lead-import'  />";exit;
            }else{
                unset($_SESSION['error_x']);
            }
            
            $fields = $_POST['custom_header']; 
            $csv_header = array();
            if(!empty($fields)){
                foreach($fields as $k =>$v){
                    if(trim($v) == 'skip')
                        $fields[$k] = '';
                }
                //$fields = array_diff($fields, ['skip']);
                $csv_header = array_values($fields);
                $csv_header = array_filter($csv_header);
                // $csv_header = array_unique($csv_header);
            }
            $display_error = '';
            $checkFields = checkFields($field_array,$csv_header); 
            $customized = 1;
            $org_file_header = json_decode($_POST['org_file_header'], true);
            $file_header = recognize(@$org_file_header);
            $sheetData = json_decode($_POST['sheetData'], true);
            if(empty($csv_header)){
                $display_error = '<code>You have to select '.@$field_name.' columns. You have Skipped / ignored <strong>ALL</strong> the columns!</code>';
            }elseif(is_array($fields) && !empty($fields) && strlen($checkFields) > 0 ){
                $display_error = '<code>You have to select <strong>'.$checkFields.'</strong> column. You have Skipped / ignored <strong>'.$checkFields.'</strong> columns!</code>';
            }
            // echo "<pre>";echo $display_error;die;
            if($handle = fopen($file_name, 'r')){
                $file_headers = fgetcsv($handle, 5000, ',');
                // echo "<pre>";print_r($file_headers);exit;
                $valid_count = 0;
                $invalid_count = 0;
                $count = 0;  
                $sql_chunk_leads = '';	
                $total_count = 0;	
                while(($data = fgetcsv($handle, 1000, ',')) !== false){
                    $total_count++;
                }
                // echo $total_count;die;
                $handle = fopen($file_name, 'r');
                fgetcsv($handle, 5000, ',');
                $header = NULL;  

                $qyr_part_leads = "INSERT IGNORE INTO mr_form_contact (`contact_person`, `contact_email`,
                `contact_phone`, `contact_country`, `contact_company`, `contact_form_type`, `contact_datetime`,
                `contact_rep_title`, `is_followed`, `followup_id`, `interval_set_date`, `followup_interval`, `interval_type`, `followup_status`, `query_source`) VALUES";

                while(($data = fgetcsv($handle, 5000, ',')) !== false){
                    // $report_count++;
                    $csvdata = array();
		
                    if(!$header){
                        $header = (is_array($fields) && !empty($fields)) ? $fields + $data : $data;
                    }                    
                    // echo "<pre>";print_r($header);exit;

                    if(count($header)==count($data)){
                        $csvdata = array_combine($header, $data); unset($csvdata['']);
                        // echo "<pre>";print_r($csvdata);exit;
                
                        $rep_title= @$csvdata['ReportTitle'] ?  $csvdata['ReportTitle'] : '';                
                        $name = @$csvdata['Name'];
                        $email= @$csvdata['Email'];
                        $phone= @$csvdata['Phone'];
                        $lead_date= @$csvdata['LeadDate'];
                        $lead_entry_date= date("Y-m-d H:i:s",strtotime($lead_date));
                        $company= @$csvdata['Company'];
                        $country= @$csvdata['Country'];
                        $lead_type= @$csvdata['LeadType'];
                        $is_followed= (isset($csvdata['isFollowed']) && @$csvdata['isFollowed'] > 0) ? @$csvdata['isFollowed'] : 0;
                        $followup_id = (isset($csvdata['FollowupId']) && @$csvdata['FollowupId'] > 0) ? @$csvdata['FollowupId'] : 0;
                        $interval_set_date= (isset($csvdata['IntervalDate']) && !empty(@$csvdata['IntervalDate'])) ? @$csvdata['IntervalDate'] : null;
                        if(@$interval_set_date){
                            $interval_set_date= date("Y-m-d H:i:s",strtotime($interval_set_date));
                        }
                        $followup_status = 0;
                        if(@$is_followed > 0 && @$followup_id > 0){
                            $followup_status = 1;
                        }                        
                        $interval_type = (isset($csvdata['Interval']) && @$csvdata['Interval'] > 0) ? @$csvdata['Interval'] : 0;
                        $interval_value = (isset($csvdata['IntervalValue']) && @$csvdata['IntervalValue'] > 0) ? @$csvdata['IntervalValue'] : 0;
                        $web_source = 'admin';
                        if(empty($rep_title) && empty($name) && empty($email) && empty($company) && empty($phone) && empty($lead_date) && empty($lead_type)){
                            continue;
                        }else{       
                            $sql_chunk_leads .= sprintf("( '%s','%s','%s','%s','%s','%d','%s','%s','%d','%d','%s','%d','%d','%d','%s'),", addslashes(trim($name)), addslashes(trim($email)) , trim($phone)
                            , trim($country), trim($company), $lead_type, $lead_entry_date,addslashes(trim(@$rep_title)), @$is_followed, @$followup_id, @$interval_set_date, @$interval_value, @$interval_type, @$followup_status, @$web_source );
                               
                            $count++;
                            if($count == 100){
                                // echo "<pre>";print_r($link);die;
                                $sql_insert_leads = substr($sql_chunk_leads, 0,-1);
                                $lead_query = $link->query($qyr_part_leads.$sql_insert_leads);
                                $last_insert_id = $link->insert_id;
            
                                if(@$last_insert_id > 0){
                                    $valid_count+=$count;
                                }
                                else{ 
                                    $invalid_count+=$count; 
                                }
                                $count = 0; $sql_chunk_leads = '';
                            }
                        }
                    }
                }
                if(!empty($sql_chunk_leads)){
                    $sql_insert_leads = substr($sql_chunk_leads, 0,-1);
                    $lead_query = $link->query($qyr_part_leads.$sql_insert_leads);
                    $last_insert_id = $link->insert_id;

                    //echo $qyr_part.$sql_insert; exit;
                    
                    if(@$last_insert_id > 0){
                        $valid_count+=$count;
                    }
                    else{ 
                        $invalid_count+=$count; 
                    }
                    $count = 0; $sql_chunk_leads = '';
                }
                $upload_msg = '<span class="text-success">[Valid Leads : '.@$valid_count.'</span> , ';
                $upload_msg.= '<span class="text-danger">Invalid Leads : '.@$invalid_count.']</span> , ';
                $message="<h3 class='alert alert-success'>
                            <b>Leads have been imported successfully into database</b> ".@$upload_msg."<br/>
                        </h3>
                        <span class='text-danger'>*NOTE : Please Wait till page gets refreshed automatically after 10 seconds</span>
                        <meta http-equiv='refresh' content='10,url=".BASE_URL."lead-list?type=4' />";
            }else{
                $message='<div class="alert alert-danger alert-form">Unable to open file. Please try again !</div>';
            }
            fclose($handle);
            $file_dir='../uploads/'.$org_file_name;
            if(file_exists($file_dir)){unlink($file_dir);}            
        }
    }catch (Exception $e){
        $_SESSION['error_x'] ='File not found. Please try again.';
        if(file_exists($file_name)){unlink($file_name);}
    }
}else{
    $message='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Invalid File Format. File needs to be CSV Only</div>';
}