
<html>
<head>
<title>Global Checkout List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->


<?php 
    require_once 'header.php';
    require_once 'auth.php';

    $query='';
    if(@$_GET['query']){
        $query=$_GET['query'];
    }
?>
<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>
<?php

//********************  PAGINATION  ****************************//
$where = '';
$input_query = '';
if (isset($_GET['query']) && !empty($_GET['query'])) {
    $page_query = "&query=".trim($_GET['query']);
    $input_query = trim($_GET['query']);
    $where.= " name like '%".$input_query."%' or email like '%".$input_query."%' ";
}    
if(!empty($where)){
    $where = " where ".$where;
}

$data = create_links($_GET, $link , 'mr_global_checkout', $where);

if(@$data['total_rows'] > 0 && @$data['pageno'] > @$data['total_pages']){
    $url = BASE_URL.'global-checkout-list'.@$data['last_page'];
    echo "<meta http-equiv='refresh' content='0, url=".@$url."'/>";
}
$input_placeholder = 'Search here for Client Name, Client Email Address';

?>

    <h1 class="stats"><span class="fas fa-list-ol"></span> Global Checkout List <span class="text-danger">[</span><span class='count text-danger'><?=@$data['total_rows']?></span><span class="text-danger">]</span></h1><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.location.href='<?php echo BASE_URL.'global-checkout-add';?>'">
        <a href="<?php echo BASE_URL.'global-checkout-add';?>"><i class="fa fa-plus"></i> Add New Global Checkout</a> 
    </button>

    <div class="pagination-form">
        <?php require_once 'pagination-form.php' ?>
    </div>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
                    <th>Sr.No</th>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th style= "min-width:250px">Report Title</th>
                    <th style= "min-width:120px">Company</th>
                    <th>Report Price</th>
                    <th style= "min-width:120px">Report License</th>
                    <th style= "min-width:170px">Date</th>
                    <th style= "min-width:170px">Expiry Date</th>
                    <th>Link Status</th>
                    <th style= "min-width:120px">Checkout Link</th>
    				<th>Options</th>
    			</tr>
    		</thead>
    		<tbody id="deleteCount">
    			<?php
    			$getGlobalCheckouts=$link->query("select * from mr_global_checkout ".@$where." order by id desc limit ". @$data['offset']. "," . @$data['no_of_records_per_page']);
    			if($getGlobalCheckouts->num_rows > 0){
                    $i=1;
	    			while($row=$getGlobalCheckouts->fetch_assoc()){                        
                        $sr_no = ( @$data['no_of_records_per_page'] * (@$data['pageno'] == '1' ? 0 : @$data['pageno']-1) ) + $i;

                        $name="PRR".rand(1,1000);
                        $encrypted_id=encrypt_decrypt('encrypt',@$name.'-'.@$row['id']);
                        // $encrypted_id=str_ireplace(array(',','-','.',';',':','/','"',"'",')','(','{','}','[',']','!','@','#','$','%','^','&','*','&','*','+','_','=','|',' '),'',@$encrypted_id);
	    				$status=($row["status"] === '1')  ? '<b style="color:green">Active</b>' : '<b style="color:#f00">Expired</b>';
                        $onclick= ($row['status'] !== '1') ? 'return false;' : '';
                        $expiry_date = (@$row["expiry_date"] == '0000-00-00 00:00:00') ? '' : @$row['expiry_date'];
                        echo "<tr id='globalCheckoutDelete".$row['id']."'><td>".@$sr_no."</td>";
                        echo "<td>".$row["name"]."</td>";
                        echo "<td>".$row["email"]."</td>";
                        echo "<td>".$row["title"]."</td>";
                        echo "<td>".$row["company"]."</td>";
                        echo "<td>$".$row["price"]."</td>";
                        echo "<td>".$row["license"]."</td>";
                        echo "<td>".$row["date"]."</td>";
                        echo "<td>".@$expiry_date."</td>";
                        echo "<td>".@$status."</td>";
                        echo "<td><a href='../global-checkout/".@$encrypted_id."' onclick='".@$onclick."' target='_blank' class='btn-link checkout-link'>Checkout Link</a></td>";                      

	    				echo "<td colspan='2'  align='center'><a href='global-checkout-edit?id=".$row["id"]."'><i class='far fa-edit text-primary'></i></a>

                            <button data-toggle='modal' data-target='#removeMe".$row["id"]."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                                <div id='removeMe".$row["id"]."' class='modal fade' role='dialog'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button class='btn btn-primary btn-confirm' type='submit' onclick='deleteData(".$row["id"].", 9)' >Confirm</button>
                                                <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div></td>";
                        echo "</tr>";
                        $i++;
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='11' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO GLOBAL CHECKOUTS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>
    <ul class="pagination">
        <?=pagination(@$data);?>
    </ul>
    <br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
