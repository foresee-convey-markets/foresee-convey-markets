
<html>
<head>
<title>Publisher Archive List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>

    <h1 class="stats"><span class="fas fa-archive"></span> Publisher Archive List</h1><br/><br/><br/>

    <button  class='btn btn-success helper-item' onclick="window.location.href='<?php echo BASE_URL.'publisher-list';?>'">
        <a href="<?php echo BASE_URL.'publisher-list';?>"><i class="fa fa-list"></i> Publisher List</a> 
    </button>


	<div class="table-responsive">
    	<table class="table table-bordered table-hover text-center">
    		<thead class="text-primary">
    			<tr>
    				<th>Publisher ID</th>
    				<th>Publisher</th>
                    <th>Archive Status</th>
    			</tr>
    		</thead>
    		<tbody  id="archiveCount">
    			<?php
    			$getArchivedPublishers=$link->query("select id, archive_status, publisher_name from mr_publisher  where archive_status='1' order by id desc");
    			if($getArchivedPublishers->num_rows > 0){
                    $i=1;
	    			while($row=$getArchivedPublishers->fetch_assoc()){
                        $id = $row['id'];
                        $archive_status = @$row['archive_status'];                        
                        $archive_checked = @$archive_status == '1' ? 'checked' : '';

	    				echo "<tr id='unarchiveStat".$id."'><td>".@$id."</td>";
	    				echo "<td>".$row["publisher_name"]."</td>";                                                
                        echo getArchiveStatus($id, @$archive_status, $ARCHIVE_STATUS, $archive_checked, 3, 2);                        
                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px;background:#efefef'><td colspan='5' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NOTHING ARCHIVED  !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
<script type="text/javascript">

        function makeUnarchiveStatus(id)
            {
                let table = 'mr_publisher';
                let type = 'unarchive';
                const data = {
                    'table' : 'mr_publisher',
                    'type' : 'unarchive',
                    'to_be_changed' : 'archive_status',
                    'id' : id
                }
                $.ajax({
                    'type' : 'post',
                    'url' : 'ajaxoperations.php',
                    'username' : $_SESSION['email'],
                    'data' : data,
                    success : function(response){
                        if(response.trim()!==""){
                            document.getElementById("archiveCount").innerHTML=this.responseText;
                        }else{
                            $("#unarchiveStat"+id).css({display:'none'});
                        }
                    }
                })
                // var xhr=new XMLHttpRequest();
                // xhr.onreadystatechange=function()
                // {
                //     if(this.readyState===4 && this.status===200)
                //     {
                //         var response=this.responseText;
                //         //alert(response.trim());
                //         if(response.trim()!==""){
                //             document.getElementById("archiveCount").innerHTML=this.responseText;
                //         }else{
                //             $("#unarchiveStat"+id).css({display:'none'});
                //             //alert(" Return ID = "+id);
                //         }
                //     }
                // }
                // xhr.open("get","ajaxoperations.php?table =" + table + "&type =" + type + "&id= " + id,true);
                // xhr.send();
            }
    </script>