
<html>
<head>
<title>Add Publisher | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->

<?php
    if(isset($_POST["submit"]))
    {
        
        $publisher=$_POST["publisher"];
        $publisher_code=$_POST["publisher_code"];
		$url=generateReportURL($publisher);
        // $url=$_POST["url"];
        $status=$_POST["status"];        
        $archive_status=$_POST["archive_status"];        
        
        $sql=mysqli_query($link,"insert ignore into mr_publisher(publisher_name,publisher_code,publisher_url,status,archive_status)  values('".$publisher."','".$publisher_code."','".$url."','".$status."','".$archive_status."') ");
        
		if($sql===TRUE ){			
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Publisher Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'publisher-list "/>'; 

		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Publisher !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add New Publisher</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="add_publisher" id="add_publisher">
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher<span class="star">*</span> :</label>
		    		<input  name="publisher" value="<?=@$publisher?>" class="form-control" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher Code<span class="star">*</span> :</label>
		    		<input  name="publisher_code" value="<?=@$publisher_code?>"  class="form-control" required="" />
		    	</div>
	    	</div>

	    	<!-- <div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" value="<?=@$url?>"  required="" />
		    	</div>
	    	</div> -->

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1"  <?=@$status == '1' ? 'checked' : ''?>  required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" <?=@$status == '0' ? 'checked' : ''?>  required="" />Inactive</label>		    		
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="1" <?=@$archive_status == '1' ? 'checked' : ''?>  required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="0" <?=@$archive_status == '0' ? 'checked' : ''?> required="" />Inactive</label>		    		
		    	</div>
	    	</div>
	    	
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="far fa-edit"></span> Add</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->