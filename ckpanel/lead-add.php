
<html>
<head>
<title>Add Lead  | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
    if(!isset($_GET['type']) || empty($_GET['type']) || $_GET['type'] != '4' ){
        echo "<meta http-equiv='refresh' content='0,url=".BASE_URL."' />";
    }else{
        $contact_form_type = $_GET['type'];
    }
?>

<!--*****************************  HEADER  ************************************** -->

<?php
    if(isset($_POST["submit"])){
        $name=$_POST["name"];
        $email=$_POST["email"];
        $phone=$_POST["phone"];
        $company=$_POST["company"];
        $country=$_POST["country"];
        $form_type=$_POST["form_type"];
		$title=$_POST["title"];
        $date=$_POST["date"];    
        
        $sql=mysqli_query($link,"insert ignore into mr_form_contact set contact_form_type='".$form_type."', contact_person='".$name."',contact_email='".$email."',contact_phone='".$phone."',contact_company='".$company."',contact_country='".$country."', contact_rep_title='".$title."',contact_datetime='".$date."',query_source='admin' ");
         
		if($sql===TRUE ){			
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Lead Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'lead-list?type=4"/>'; 

		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Lead !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		} 
    }   
    
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add Lead</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#" ><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]).'?type=4'; ?>" method='post' name="add" id="add" enctype="application/x-www-form-urlencoded">
	    	
	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Name<span class="star">*</span> :</label>
		    		<input  name="name" class="form-control" required="" value='<?=@$name?>'  placeholder="Name"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Email<span class="star">*</span> :</label>
		    		<input type="email" name="email" class="form-control" value='<?=@$email?>'  required=""  placeholder="Email" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Phone<span class="star">*</span> :</label>
		    		<input  name="phone" class="form-control" value='<?=@$phone?>' required=""  placeholder="Phone"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Company<span class="star">*</span> :</label>
		    		<input  name="company" class="form-control" required="" value='<?=@$company?>' placeholder="Company"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Country :</label>
		    		<input  name="country" class="form-control" value='<?=@$country?>'   placeholder="Country" />
		    	</div>
	    	</div>

			<div class="form-group">
				<div class="col-md-6">
					<label class="control-label">Report title<span class="star">*</span> :</label>
					<input  name="title" class="form-control" required="" value='<?=@$title?>'    placeholder="Report Title" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6">
					<label class="control-label">Lead Type<span class="star">*</span> :</label>
					<select name="form_type" class="form-control" required  placeholder="Lead Type" >
						<option value=''>Select</option>
						<?php foreach($FORM_TYPES as $key => $value){ 
							if(!in_array($key,array(4,5,6,7) ) ) continue;	
								$selected =  (isset($form_type) && @$form_type == $key ) ? 'selected' : ''  ;                     
							?>
							<option value="<?=@$key?>" <?=@$selected?>><?=str_ireplace('Query','',@$value)?></option>
						<?php }?>
					</select>
				</div>
			</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" value='<?=@$date?>' autocomplete="off"  required="" />
		    	</div>
	    	</div>
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Add</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
