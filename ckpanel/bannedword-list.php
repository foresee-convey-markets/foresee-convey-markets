
<html>
<head>
<title>Banned Words List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>

<?php
//********************  PAGINATION  ****************************//
$where = '';
$input_query = '';
if (isset($_GET['query']) && !empty($_GET['query'])) {
    $page_query = "&query=".trim($_GET['query']);
    $input_query = trim($_GET['query']);
    $where.= "word like '%".$input_query."%' ";
}    
if(!empty($where)){
    $where = " where ".$where;
}

$data = create_links($_GET, $link , 'mr_bannedwords', $where);

if(@$data['total_rows'] > 0 && @$data['pageno'] > @$data['total_pages']){
    $url = BASE_URL.'bannedword-list'.@$data['last_page'];
    echo "<meta http-equiv='refresh' content='0, url=".@$url."'/>";
}
$input_placeholder = 'Search here for Banned Word';

?>
    <h1 class="stats"><span class="fas fa-list-ol"></span> Banned Words List <span class="text-danger">[</span><span class='count text-danger'><?=@@$data['total_rows']?></span><span class="text-danger">]</span></h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.location.href='<?php echo BASE_URL.'bannedword-add';?>'">
        <a href="<?php echo BASE_URL.'bannedword-add';?>"><i class="fa fa-plus"></i> Add New BannedWord</a> 
    </button>

    <div class="pagination-form">
        <?php require_once 'pagination-form.php' ?>
    </div>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover" style="text-align:center">
    		<thead class="text-primary">
    			<tr>
    				<th style="max-width:1px">Sr.No</th>
    				<th>Word</th>

                <?php if($_SESSION["user_type"]=='1'){ ?>
                    <th>Status</th>
    				<th>Options</th>
                <?php } ?>

    			</tr>
    		</thead>
    		<tbody id='deleteCount'>
    			<?php
    			$getBannedwords=$link->query("select * from mr_bannedwords ".@$where." order by word limit ". @$data['offset']. "," . @$data['no_of_records_per_page']);
    			if($getBannedwords->num_rows > 0){
                    $i=1;
	    			while($row=$getBannedwords->fetch_assoc()){
                        $id = $row['id'];
                        $status = @$row['status'];
                        $sr_no = ( @$data['no_of_records_per_page'] * (@$data['pageno'] == '1' ? 0 : @@$data['pageno']-1) ) + $i;
	    				echo "<tr id='categoryDelete".$id."'><td>".@$sr_no."</td>";
	    				echo "<td>".ucfirst($row["word"])."</td>";

                    if($_SESSION["user_type"]=='1'){                         
                        $checked = @$status == '1' ? 'checked' : '';
                        echo getStatus($id, @$status, $STATUS, $checked, 7, 1);
                        
	    				echo "<td colspan='2'  align='center'><a href='bannedword-edit?id=".$id."'><i class='far fa-edit text-primary'></i></a>

                            <button data-toggle='modal' data-target='#removeMe".$id."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                                <div id='removeMe".$id."' class='modal fade' role='dialog'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button class='btn btn-primary btn-confirm' type='submit' onclick='deleteData(".$id.", 7)' >Confirm</button>
                                                <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div></td>";
                        }
                        echo "</tr>";
                        $i++;
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='5' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO BANNEDWORDS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>
    <ul class="pagination">
        <?=pagination(@$data);?>
    </ul>

    <br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
    