
<html>
<head>
<title>Add Press Release | Foresee Convey Markets</title>


<script type="text/javascript">
$(document).ready(function(){
    $(".datepicker").datepicker({dateFormat:"yy-mm-dd"});
});

</script>

 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->

<?php
    if(isset($_POST["submit"]))
    {
        
        $category=$_POST["category"];
        $title=$_POST["title"];
        $url=generateReportURL($title);
        $description=$_POST["description"];
        $status=$_POST["status"];
        $archive_status=$_POST["archive_status"];
        $published_date=$_POST["date"];
        $date=date("Y-m-d H:i:s");
        $image=$_FILES["image"]["name"];
        $image_dir="../assets/images/press/";	
		move_uploaded_file($_FILES["image"]["tmp_name"], $image_dir.$image);
        
        
        $sql=mysqli_query($link,"insert ignore into mr_press(cat_id,title,url,content,status,archive_status,image,published_on,created_at)  values('".$category."','".$title."','".$url."','".$description."','".$status."','".$archive_status."','".$image."','".$published_date."','".$date."')");
        
		if($sql===TRUE ){			
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Press Release Added successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'press-release-list "/>'; 

		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Adding Press Release !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add New Press Release</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#" ><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="add_press" id="add_press" enctype='multipart/form-data'>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Press Release Category<span class="star">*</span> :</label>
		    		<select class="form-control" name="category" id="category" title="Press Release Category"  required>
                        <option value="">Select Category </option>
                        <?php
	                        $pressCategory = "select * from mr_sub_cat_1 order by sc1_name";
	                        $pressCategoryQuery = $link->query($pressCategory);
							if($pressCategoryQuery->num_rows > 0){
								while ($row = $pressCategoryQuery->fetch_array()){
									$category_id = $row['id'];
									$selected =  (isset($category) && @$category == $category_id ) ? 'selected' : ''  ;                     
									echo "<option value='".$category_id."' ".@$selected."  >".$row["sc1_name"]."</option>";                                
								}
							}
                        ?>
                    </select>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Press Title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control" value="<?=@$title?>" required="" />
		    	</div>
	    	</div>

	    	<!-- <div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Press URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" value="<?=@$url?>" required="" />
		    	</div>
	    	</div> -->

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Press Description<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("description",  @$description);
                    ?>  
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Press Image<span class="star">*</span> :</label>
		    		<input type="file" name="image" class="form-control" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Published Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker"  autocomplete="off" value="<?=@$published_date?>" required="" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1"  <?=@$status == '1' ? 'checked' : ''?> required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" <?=@$status == '0' ? 'checked' : ''?> required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="1" <?=@$archive_status == '1' ? 'checked' : ''?> required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="0" <?=@$archive_status == '0' ? 'checked' : ''?> required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="far fa-edit"></span> Add</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
