
<html>
<head>
<title>Edit Lead  | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
    if(!isset($_GET['type']) || empty($_GET['type']) || $_GET['type'] != '4'){
        // echo "<meta http-equiv='refresh' content='0,url=".BASE_URL."' />";
    }else{
        $contact_form_type = $_GET['type'];
    }
?>

<!--*****************************  HEADER  ************************************** -->

<?php 
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getLeadDetails=$link->query("select * from mr_form_contact where contact_id='$id'");
$row=$getLeadDetails->fetch_assoc();
?>
<?php
    if(isset($_POST["submit"])){
		if(!empty($_POST['title'])){        
			$contact_id=$_POST["id"];
			$name=$_POST["name"];
			$email=$_POST["email"];
			$phone=$_POST["phone"];
			$company=$_POST["company"];
			$country=$_POST["country"];
			$form_type=$_POST["form_type"];
			$title=$_POST["title"];        
			$date=$_POST["date"];        
			
			$sql=mysqli_query($link,"update mr_form_contact set contact_person='".$name."', contact_form_type='".$form_type."',contact_email='".$email."',contact_phone='".$phone."',contact_company='".$company."',contact_country='".$country."', contact_rep_title='".$title."',contact_datetime='".$date."' where contact_id='$contact_id' ");
			
			if($sql===TRUE ){
				$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Lead Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
				<meta http-equiv="refresh" content="2,url='.BASE_URL.'lead-list?type=4 "/>'; 
			}else{
				$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Lead !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
			}
		}else{				
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Report Title is required</div>';
		}
    }
    
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Edit Lead</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#" ><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="lead_edit" id="lead_edit" enctype='application/x-www-form-urlencoded'>
			<input type="hidden" name="type" value="<?=@$contact_form_type?>" />
			<input type="hidden" name="id" value="<?=@$id?>" />
	    	
	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Name<span class="star">*</span> :</label>
		    		<input  name="name" class="form-control" required=""   value="<?=@$name ? @$name :  ($row["contact_person"] ? $row["contact_person"] : ''); ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Email<span class="star">*</span> :</label>
		    		<input type="email" name="email" class="form-control" required=""  value="<?=@$email ? @$email :  ($row["contact_email"] ? $row["contact_email"] : ''); ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Phone<span class="star">*</span> :</label>
		    		<input  name="phone" class="form-control" required=""   value="<?=@$phone ? @$phone :  ($row["contact_phone"] ? $row["contact_phone"] : ''); ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Company<span class="star">*</span> :</label>
		    		<input  name="company" class="form-control" required=""  value="<?=@$company ? @$company :  ($row["contact_company"] ? $row["contact_company"] : ''); ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Country :</label>
		    		<input  name="country" class="form-control"   value="<?=@$country ? @$country :  ($row["contact_country"] ? $row["contact_country"] : ''); ?>" />
		    	</div>
	    	</div>

			<div class="form-group">
				<div class="col-md-6">
					<label class="control-label">Report title<span class="star">*</span> :</label>
					<input  name="title" class="form-control" required=""  value="<?=@$title ? @$title :  ($row["contact_rep_title"] ? $row["contact_rep_title"] : ''); ?>" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6">
					<label class="control-label">Lead Type<span class="star">*</span> :</label>
					<select name="form_type" class="form-control" required  placeholder="Lead Type" >
						<option value=''>Select</option>
						<?php foreach($FORM_TYPES as $key => $value){ 
							if(!in_array($key,array(4,5,6,7) ) ) continue;	
							$selected =  (isset($form_type) && @$form_type == $key ) ? 'selected' : ($key == $row['contact_form_type'] ? 'selected' : '')  ;
						?>
							<option value="<?=@$key?>" <?=@$selected?> ><?=str_ireplace('Query','',@$value)?></option>
						<?php } ?>
					</select>
				</div>
			</div>

	    	<div class="form-group">
	    		<div class="col-md-6">
		    		<label class="control-label">Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" required="" autocomplete="off" value="<?=@$date ? @$date :  ($row["contact_datetime"] ? $row["contact_datetime"] : ''); ?>" />
		    	</div>
	    	</div>
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
