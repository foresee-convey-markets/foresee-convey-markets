
<html>
<head>
<title>Edit Blog | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php 
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getBlogDetails=$link->query("select * from mr_blogs where id='$id'");
$row=$getBlogDetails->fetch_assoc();
?>
<?php
    if(isset($_POST["submit"])){
		$blog_id = $_POST['id'];       
        $category=$_POST["category"];
        $title=$_POST["title"];
        $url=$_POST["url"];
        $url=generateReportURL($url);
        $description=$_POST["description"];
        $status=$_POST["status"];
        $archive_status=$_POST["archive_status"];
        $published_date=$_POST["date"];
        $date=date("Y-m-d H:i:s");        
        
        if(!empty($_FILES['image']['name'])){
	        $image=$_FILES["image"]["name"];
	        $image_dir="../assets/images/blogs/";
	        unlink($image_dir.$row["blog_img"]);	
			move_uploaded_file($_FILES["image"]["tmp_name"], $image_dir.$image);
		}else{
			$image=$_POST["blog_image"];
		}
        
        $sql=mysqli_query($link,"update mr_blogs set blog_cat_id='".$category."',blog_title='".$title."',blog_url='".$url."',blog_content='".$description."',status='".$status."', archive_status='".$archive_status."',blog_img='".$image."',published_on='".$published_date."',created_at='".$date."' where id='$blog_id' ");
        
		if($sql===TRUE ){
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Blog Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'blog-list "/>'; 
		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Blog !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>
    <h1 class="stats"><span class="far fa-edit"></span> Edit Blog</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#" ><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="edit_blog" id="edit_blog" enctype='multipart/form-data'>
	    	<input type="hidden" name="blog_image" value="<?php echo $row["blog_img"] ; ?>">
			<input type="hidden" name="id" value="<?=@$id?>" />

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">blog Category<span class="star">*</span> :</label>
		    		<select class="form-control" name="category" id="category" title="blog Release Category"  required>
                        <option value="">Select Category </option>		
						<?php
	                        $BlogCategory = "select * from mr_sub_cat_1 order by sc1_name";
	                        $BlogCategoryQuery = $link->query($BlogCategory);
	                        if ($BlogCategoryQuery->num_rows > 0){
	                            while ($blog_cat = $BlogCategoryQuery->fetch_array()){
									$category_id = $blog_cat['id'];  
									$selected =  (isset($category) && @$category == $category_id ) ? 'selected' : ((@$category_id == @$row['blog_cat_id']) ? 'selected' : '') ;                     
	                                echo "<option value='".$category_id."' ".@$selected."  >".$blog_cat["sc1_name"]."</option>";                                
	                            }
	                        }
                        ?>
                    </select>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">blog Title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control" required=""  value="<?=@$title ? @$title :  ($row["blog_title"] ? $row["blog_title"] : ''); ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">blog URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" required=""  value="<?=@$url ? @$url :  ($row["blog_url"] ? $row["blog_url"] : ''); ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">blog Description<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("description", @$description ? @$description :  $row["blog_content"]);
                    ?>  
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">blog Image<span class="star">*</span> :</label><img  src="../assets/images/blogs/<?php echo $row["blog_img"]; ?>"  title="<?php echo $row["blog_img"] ; ?>" height="100px" width="200px" /><br/><br/>
		    		<input type="file" name="image" class="form-control" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Published Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" required="" autocomplete="off"  value="<?=@$published_date ? @$published_date :  ($row["published_on"] ? $row["published_on"] : ''); ?>" />
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1" required=""  <?= @$status && @$status == '1' ? 'checked' : ($row["status"]=='1' ? 'checked' : ''); ?> />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" required=""  <?= @$status && @$status == '0' ? 'checked' : ($row["status"]=='0' ? 'checked' : ''); ?> />Inactive</label>
		    		
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive Status <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" <?= @$archive_status && @$archive_status == '1' ? 'checked' : ($row["archive_status"]=='1' ? 'checked' : ''); ?>  value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" <?= @$archive_status && @$archive_status == '0' ? 'checked' : ($row["archive_status"]=='0' ? 'checked' : ''); ?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
