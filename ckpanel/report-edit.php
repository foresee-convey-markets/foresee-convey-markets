
<html>
<head>
<title>Edit  | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php 
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getReportDetails=$link->query("select * from mr_report left join mr_report_metadata on mr_report.id=mr_report_metadata.meta_rep_id  where mr_report.id='$id' ");
$row=$getReportDetails->fetch_assoc();
?>

<?php
    if(isset($_POST["submit"])){
        $rep_id = $_POST['id'];
        $category=$_POST["category"];
        $publisher=$_POST["publisher"];
		$title=addslashes($_POST["title"]);
        $url=$_POST["url"];
        $url=generateReportURL($url);
        $pages=$_POST["page"];
        $content=addslashes($_POST["content"]);
        $description=addslashes($_POST["description"]);
        $toc=addslashes($_POST["toc"]);
        $tof=addslashes($_POST["tof"]);


        $status=$_POST["status"];
        $archive_status=$_POST["archive_status"];

        $published_date=$_POST["date"];
        $date=date("Y-m-d H:i:s");

		// REPORT LICENSES		
		$li_value1=$_POST["single_user_price"];
		$li_value2=null;
		$li_value3=null;
		if(isset($_POST['enterprise_user_price'])){			
			$li_value2=$_POST["enterprise_user_price"];
		}
		if(isset($_POST['corporate_user_price'])){			
			$li_value3=$_POST["corporate_user_price"];
		}
        
        
        
        $sql="update mr_report set rep_sub_cat_1_id='".$category."',publisher_id='".$publisher."',rep_url='".$url."',rep_title='".$title."',rep_descrip='".$description."',rep_page='".$pages."',rep_date='".$date."',rep_toc_fig='".$tof."',rep_entry_date='".$published_date."',status='".$status."',archive_status='".$archive_status."' where id='$rep_id' ";
        
		if($link->query($sql)){

			$sql="update mr_report_metadata set rep_contents='$content',rep_table_of_contents='$toc' where meta_rep_id='$id')";
			$link->query($sql);	

			if($li_value1 > 0){	
				$sql1="update mr_report_license set li_value='$li_value1' where li_rep_id='$id' and li_key='Single User License' ";
				$link->query($sql1); 
			}
			if($li_value2 > 0){
				$sql2="update mr_report_license set li_value='$li_value2' where li_rep_id='$id' and li_key='Enterprise User License'";
				$link->query($sql2);
			}
			if($li_value3 > 0){
				$sql3="update mr_report_license set li_value='$li_value3' where li_rep_id='$id' and li_key='Corporate User License'";
				$link->query($sql3);
			}
			
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Report Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'report-list "/>'; 

		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Report !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Edit Report</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#" ><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="report_edit" id="report_edit" enctype='application/x-www-form-urlencoded'>
	    	
			<input type="hidden" name="id" value="<?=@$id?>" />
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Category<span class="star">*</span> :</label>
		    		<select class="form-control" name="category" id="category" title="Report Category"  required>
                        <option value="">Select Category </option>		
						<?php
	                        $ReportCategory = "select * from mr_sub_cat_1 order by sc1_name";
	                        $ReportCategoryQuery = $link->query($ReportCategory);
	                        if ($ReportCategoryQuery->num_rows > 0){
	                            while ($report_cat = $ReportCategoryQuery->fetch_array()){
									$category_id = $report_cat['id'];  
									$selected =  (isset($category) && @$category == $category_id ) ? 'selected' :  ((@$category_id == @$row['rep_sub_cat_1_id']) ? 'selected' : '') ;                     
									echo "<option value='".$category_id."' ".@$selected."  >".$report_cat["sc1_name"]."</option>";                                
								}
	                        }                        
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher<span class="star">*</span> :</label>
		    		<select class="form-control" name="publisher" id="publisher" title="Report publisher"  required>
                        <option value="">Select Publisher </option>	
                        <?php
	                        $ReportPublisher = "select * from mr_publisher order by publisher_name";
	                        $ReportPublisherQuery = $link->query($ReportPublisher);
	                        if ($ReportPublisherQuery->num_rows > 0){
	                            while ($report_pub = $ReportPublisherQuery->fetch_array()){ 
									$publisher_id = $report_pub['id'];  
									$selected =  (isset($publisher) && @$publisher == $publisher_id ) ? 'selected'  :  ((@$publisher_id == @$row['publisher_id']) ? 'selected' : '') ;                     
	                                echo '<option value="'. @$publisher_id.'"  '.@$selected.'>'.$report_pub["publisher_name"].'</option>';                                
	                            }
	                        }
                        ?>
                    </select>
		    	</div>
	    	</div>
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Title<span class="star">*</span> :</label>
		    		<input  name="title" class="form-control" required="" value="<?=@$title ? @$title :  ($row["rep_title"] ? $row["rep_title"] : ''); ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" required="" value="<?=@$url ? @$url :  ($row["rep_url"] ? $row["rep_url"] : ''); ?>" />
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Description<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("description", @$description ? @$description :  $row["rep_descrip"]);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Contents<span class="star">*</span> :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("content", @$content ? @$content :  $row["rep_contents"]);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Contents :</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("toc", @$toc ? @$toc :  $row["rep_table_of_contents"]);
                    ?>  
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Table Of Figures:</label>
		    		<?php
                        require_once 'ckeditor_old/ckeditor.php';
                        $sBasePath = "ckeditor/";
                        $CKEditor = new CKEditor();
                        $CKEditor->editor("tof", @$tof ? @$tof :  $row["rep_toc_fig"]);
                    ?>  
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Report Pages<span class="star">*</span> :</label>
		    		<input  name="page" class="form-control" required="" value="<?=@$pages ? @$pages :  ($row["rep_page"] ? $row["rep_page"] : ''); ?>"/>
		    	</div>
	    	</div>

	    	<?php
	    	$getReportLicense=$link->query("select * from mr_report_license where li_rep_id='$id' ");
	    	while($result=$getReportLicense->fetch_assoc()) { ?>
				<div class="form-group">
					<div class="col-md-10">
						<label class="control-label"><?php echo $result["li_key"]; ?> :</label>
						<input  name="<?php echo strtolower(str_replace(" ","_" , $result["li_key"] )); ?>" class="form-control" value="<?php echo $result["li_value"]; ?>" />
					</div>
				</div>
	    	<?php } ?>  	    	

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Published Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" autocomplete="off" required="" value="<?=@$published_date ? @$published_date :  ($row["rep_entry_date"] ? $row["rep_entry_date"] : ''); ?>"/>
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1" required=""   <?= @$status && @$status == '1' ? 'checked' : ($row["status"]=='1' ? 'checked' : ''); ?> />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" required=""   <?= @$status && @$status == '0' ? 'checked' : ($row["status"]=='0' ? 'checked' : ''); ?>/>Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive Status <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="1" required=""  <?= @$archive_status && @$archive_status == '1' ? 'checked' : ($row["archive_status"]=='1' ? 'checked' : ''); ?> />Active</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" value="0" required=""  <?= @$archive_status && @$archive_status == '0' ? 'checked' : ($row["archive_status"]=='0' ? 'checked' : ''); ?>/>Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->