<?php
require_once 'array.php';
function encrypt_decrypt($type,$text){

    $auth_iv = '1234567891011123';  // encryption/decryption iv (Initialization Vector) 
    $auth_key = "prospectresearchreports";    // encryption/decryption key 
    $ciphering = "AES-256-CBC";    // Store the cipher method 
    $iv_length = openssl_cipher_iv_length($ciphering);
    $options = 0;    // Store the cipher method 
    $key = hash('sha256', $auth_key);
    $iv = substr(hash('sha256', $auth_iv), 0, 16);
    $string='';
    if(@$type === 'encrypt'){
        $string=openssl_encrypt($text, $ciphering, 
            $key, $options, $iv); 
        $string = base64_encode($string);

    }elseif (@$type === 'decrypt') {
        $string=openssl_decrypt(base64_decode($text), $ciphering, 
            $key, $options, $iv); 
    }
    return $string;
}

// CSV UPLOAD FUNCTIONS


function get_shortDescription($rep_descrip, $limit = 50, $condi = 20){
	$cond = $limit+$condi; 
	$descrip = $rep_descrip;
	$sentence=$rep_descrip; 
	$words=count(explode(' ', $sentence));

	if($words > $cond){	
		$descrip=implode(' ', array_slice(explode(' ', $sentence), 0, $limit));
		$descrip .= " ...";
	}

	$descrip = str_replace('&Acirc;','',$descrip);
	$descrip=str_replace('&prime;','&lsquo;',$descrip);
	$descrip=str_replace('&Prime;','&rdquo;',$descrip);
	$descrip=str_replace('&bprime;','&rsquo;',$descrip);
	$descrip=str_replace('&ndash;','&dash;',$descrip);
	$descrip=str_replace('&mdash;','&dash;',$descrip);
	//$string=str_replace('’;',"'",$string);

	$specials = array('&Acirc;','&acirc;','&Aacute;','&Atilde;','&aacute;','&Ecirc;','&Egrave;','&Eacute;','“','�');
	$descrip=str_replace($specials,'',$descrip);
	return $descrip;	
}
function get_pure_words_url($string){
	$specials = array("Global","Africa","South & Southeast Asia","Asia-Pacific","North Africa",
	"Europe","Sub-Saharan Africa","North & Cental America","Antarctica","Caribbean Islands",
	"Asia","Mesoamerica","East Asia","North America","North Asia","Oceania","West & Central Asia",
	"South America","Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola",
	"Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia",
	"Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
	"Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", 
	"Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", 
	"Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", 
	"Central African Republic", "Chad", "Chile", "China", "Christmas Island", 
	"Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", 
	"Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", 
	"Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
	"Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", 
	"Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji",
	"Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia",
	"French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", 
	"Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau",
	"Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras",
	"Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", 
	"Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", 
	"Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao,
	People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia",
	"Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, 
	The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", 
	"Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", 
	"Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", 
	"Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", 
	"Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", 
	"Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", 
	"Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", 
	"Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", 
	"Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", 
	"Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", 
	"Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", 
	"South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", 
	"St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", 
	"Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", 
	"Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", 
	"Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", 
	"United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", 
	"Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", 
	"Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", 
	"Zambia", "Zimbabwe","Latin America",'Middle East','and','USA','&amp;','global',
	'2017','2022','(',')','-',',','Industry','Research','Report','Market','Company','Profile','Deals',
	'Ltd','Alliances',': ','Country','Sector',' in ','Prospects','Survey','Chinese','Regional',
	'Regional ','Production', 'Sales' ,'Consumption','Status','Prospects','Professional','Outlook',
	'By', 'Players', 'Regions', 'Product' ,'Types','Applications','Analysis',' to ');
	$string=str_replace($specials,'',$string);
	return $string;
}
function get_pure_words($string){
	$specials = array('(',')',"'");
	$string=str_replace($specials,'',$string);
	return $string;
}
function generateReportURL($report_title){
	$URL_CHAR_LIMIT = 100;
	$url = '';
	if(!empty(@$report_title)){
		$invalid_charactes = array('%5Cr','--','.','?',"'",'}','{','[',']','~','!','@','#','$','%','^','&','*','(',')','=','_','+','<','>','/',';',':','"',',',"'",'`','~');
		$url = str_replace(array('\r','\n','\r\n'),'',($report_title));
		$key = (explode('Market',$url)) ? explode('Market',$url) : array($url);
		$url = str_ireplace($invalid_charactes, '', $key[0]);
		$url = str_replace(" ","-",trim($url));
		$url=urlencode(str_replace(" ","-",trim($url)));
		// $unique_url=strtolower($url)."-market-".rand(0,111);
		$url=str_replace(array("/"," ","  ","&nbsp;"),"-",$url);
		$url = strlen($url) > $URL_CHAR_LIMIT ? substr($url,0,$URL_CHAR_LIMIT) : $url;
	}
	return urlencode(strtolower($url));
}
function checkDuplicateReport($report_title, $link){
	$is_duplicate_report = false;
	$getReport = $link->query("Select * from mr_report where rep_title='".$report_title."' ");
	if($getReport->num_rows > 0 ){
		$is_duplicate_report = true;
	}
	return $is_duplicate_report;
}
function pagination($data = array()){
	$pagination = '';
	if(!empty($data)){
		$pagination = "<li><a href='".@$data['first_page']."'>First</a></li>";

		$prev_cls = (@$data['pageno'] <= 1) ?   'disabled' : '';
		$prev_link = @$data['pageno'] <= 1 ? '#' :  @$data['prev_page'];
		$next_cls = (@$data['pageno'] >= @$data['total_pages']) ? 'disabled' : '';
		$next_link = (@$data['pageno'] >= @$data['total_pages']) ? '#' : @$data['next_page'];
		$last_cls = (@$data['pageno'] == @$data['total_pages']) ?   'disabled' : '';
		$last_link = (@$data['pageno'] == @$data['total_pages']) ? '#' : @$data['last_page'];

		$pagination.="<li class='".$prev_cls."'>";
		$pagination.="<a href='".@$prev_link."'>Prev</a>";
		$pagination.="</li>";
		$pagination.="<li class='".$next_cls."'>";
		$pagination.="<a href='".$next_link."'>Next</a>";
		$pagination.="</li>";
		$pagination.="<li class='".$last_cls."'>";
		$pagination.="<a href='".$last_link."'>Last</a>";
		$pagination.="</li>";
	}
	return $pagination;
}
function create_links($GET, $link, $table, $where = '', $contact_form_type = ''){
	$data = array();
	$pageno = 1;  
	$no_of_records_per_page = 10; 
	$page_query = ''; 
	if(!empty($contact_form_type)){
		$contact_form_type="&type=$contact_form_type";
	}
	if (isset($GET['pageno']) && $GET['pageno'] > 0) {
		$pageno = $GET['pageno'];
	} 
	if (isset($GET['query']) && !empty($GET['query'])) {
		$page_query = "&query=".urlencode($GET['query']);
	} 
	if (isset($GET['per_page'])) {
		$no_of_records_per_page = $GET['per_page'];
	}
	$data['offset'] = ($pageno-1) * $no_of_records_per_page;
	$data['no_of_records_per_page'] = $no_of_records_per_page;
	$data['pageno'] = $pageno;

	$query="select count(*) from ".@$table.@$where;
	// echo $query;die;

	$result = $link->query($query);
	$data['total_rows'] = $total_rows = mysqli_fetch_array(@$result)[0];
	$data['total_pages'] = $total_pages = ceil($total_rows / $no_of_records_per_page);


	// PAGINATION LINKS
	$per_page = "?per_page=$no_of_records_per_page";

	$data['first_page'] = "$per_page&pageno=1".$page_query.$contact_form_type;
	$data['last_page'] = "$per_page&pageno=".$total_pages.$page_query.$contact_form_type;
	$data['next_page'] = "$per_page&pageno=".($pageno + 1).$page_query.$contact_form_type;
	$data['prev_page'] = "$per_page&pageno=".($pageno - 1).$page_query.$contact_form_type;
	return $data;
}
function getStatus($id, $status, $STATUS = array(), $checked, $table, $to_be_changed){
	$status_res = '';
	if(@$id > 0 && @$status >= 0 && !empty($table)){
		$status_res.='<td>';
		$status_res.='<label data-toggle="tooltip" title="'.@$STATUS[@$status].'" class="switch" onchange="updateStatus('.$id.','.@$status.','.@$to_be_changed.','.@$table.')">';
		$status_res.='<input id="status_'.$id.'"  type="checkbox" '.@$checked.'>';
		$status_res.='<span class="slider round"></span>';
		$status_res.='</label>';
		$status_res.='</td>';
	}
	return $status_res;
}
 
function getArchiveStatus($id, $archive_status, $ARCHIVE_STATUS = array(), $checked, $table, $to_be_changed){
	$archive_status_res = '';
	if(@$id > 0 && @$archive_status >= 0 && !empty($table)){
		$archive_status_res.='<td>';
		$archive_status_res.='<label data-toggle="tooltip" title="'.@$ARCHIVE_STATUS[@$archive_status].'" class="switch" onchange="updateStatus('.$id.','.@$archive_status.','.@$to_be_changed.','.@$table.')">';
		$archive_status_res.='<input id="archive_status_'.$id.'"  type="checkbox" '.@$checked.'>';
		$archive_status_res.='<span class="slider round"></span>';
		$archive_status_res.='</label>';
		$archive_status_res.='</td>';
	}
	return $archive_status_res;
}
function recognize($header=array()){
	//echo '<pre>'; print_r($header);
	//echo '<pre>'; print_r($GLOBALS['COLUMNS_NAME']);exit;
	$cn = $GLOBALS['COLUMNS_NAME'];
	$tmp = array();
	if(is_array($header) && !empty($header)){
		foreach($header as $k => $v){
			$v = str_replace(array('?','?'),'',trim($v));
			$v = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $v);
			$tmp[$k] = 'skip';
			foreach($cn as $cnk => $cnv){
				if (in_array(trim(strtolower($v)), $cnv)){
					$tmp[$k] = (in_array($cnk, $tmp)) ? $cnk.'_1' : $cnk;
				}
			}
		}
	}
	//$new = array_merge($header,$tmp);
	// echo '<pre>'; print_r($tmp);exit;
	return $tmp;
}

$CUSTOM_CSV_REPORT_HEADER = array(
	"" => 'Select One',
	'ReportTitle' => 'ReportTitle',
	'PublisherID' => 'PublisherID',
	'CategoryID' => 'CategoryID',
	'Pages' => 'Pages',
	'Date' => 'Date',
	'Content' => 'Content',
	'TOC' => 'TOC',
	'TOF' => 'TOF',
	'PageTitle' => 'PageTitle',
	'MetaTitle' => 'MetaTitle',
	'SingleUserLicense' => 'SingleUserLicense',
	'EnterpriseUserLicense' => 'EnterpriseUserLicense',
	'CorporateUserLicense' => 'CorporateUserLicense',
	'skip' => 'Skip Column',
);

$CUSTOM_LEAD_HEADER = array(
	"" => 'Select One',
	'Name' => 'Name',
	'Email' => 'Email',
	'Phone' => 'Phone',
	'Company' => 'Company',
	'Country' => 'Country',
	'LeadType' => 'LeadType',
	'LeadDate' => 'LeadDate',
	'isFollowed' => 'isFollowed',
	'FollowupId' => 'FollowupId',
	'IntervalDate' => 'IntervalDate',
	'Interval' => 'Interval',
	'IntervalValue' => 'IntervalValue',
	'ReportTitle' => 'ReportTitle',
	'skip' => 'Skip Column',
);


// -------------------------- DROPDOWN   ----------------------------------------------
 
if ( ! function_exists('form_dropdown'))
{
	/**
	 * Drop-down Menu
	 *
	 * 
	 * @param	mixed	$data
	 * @param	mixed	$options
	 * @param	mixed	$selected
	 * @param	mixed	$extra
	 * @return	string
	 */
	function form_dropdown($data = '', $options = array(), $selected = array(), $extra = '')
	{
		$defaults = array();

		if (is_array($data))
		{
			if (isset($data['selected']))
			{
				$selected = $data['selected'];
				unset($data['selected']); // select tags don't have a selected attribute
			}

			if (isset($data['options']))
			{
				$options = $data['options'];
				unset($data['options']); // select tags don't use an options attribute
			}
		}
		else
		{
			$defaults = array('name' => $data);
		}

		is_array($selected) OR $selected = array($selected);
		is_array($options) OR $options = array($options);

		// If no selected state was submitted we will attempt to set it automatically
		if (empty($selected))
		{
			if (is_array($data))
			{
				if (isset($data['name'], $_POST[$data['name']]))
				{
					$selected = array($_POST[$data['name']]);
				}
			}
			elseif (isset($_POST[$data]))
			{
				$selected = array($_POST[$data]);
			}
		}

		$extra = _attributes_to_string($extra);

		$multiple = (count($selected) > 1 && stripos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

		$form = '<select '.rtrim(_parse_form_attributes($data, $defaults)).$extra.$multiple.">\n";

		foreach ($options as $key => $val)
		{
			$key = (string) $key;

			if (is_array($val))
			{
				if (empty($val))
				{
					continue;
				}

				$form .= '<optgroup label="'.$key."\">\n";

				foreach ($val as $optgroup_key => $optgroup_val)
				{
					$sel = in_array($optgroup_key, $selected) ? ' selected="selected"' : '';
					$form .= '<option value="'.htmlspecialchars($optgroup_key).'"'.$sel.'>'
						.(string) htmlentities($optgroup_val, ENT_QUOTES)."</option>\n";
				}

				$form .= "</optgroup>\n";
			}
			else
			{
				$form .= '<option value="'.htmlspecialchars($key).'"'
					.(in_array($key, $selected) ? ' selected="selected"' : '').'>'
					.(string) htmlentities($val, ENT_QUOTES)."</option>\n";
			}
		}

		return $form."</select>\n";
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('_parse_form_attributes'))
{
	/**
	 * Parse the form attributes
	 *
	 * Helper function used by some of the form helpers
	 *
	 * @param	array	$attributes	List of attributes
	 * @param	array	$default	Default values
	 * @return	string
	 */
	function _parse_form_attributes($attributes, $default)
	{
		if (is_array($attributes))
		{
			foreach ($default as $key => $val)
			{
				if (isset($attributes[$key]))
				{
					$default[$key] = $attributes[$key];
					unset($attributes[$key]);
				}
			}

			if (count($attributes) > 0)
			{
				$default = array_merge($default, $attributes);
			}
		}

		$att = '';

		foreach ($default as $key => $val)
		{
			if ($key === 'value')
			{
				$val = htmlspecialchars($val);
			}
			elseif ($key === 'name' && ! strlen($default['name']))
			{
				continue;
			}

			$att .= $key.'="'.$val.'" ';
		}

		return $att;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('_attributes_to_string'))
{
	/**
	 * Attributes To String
	 *
	 * Helper function used by some of the form helpers
	 *
	 * @param	mixed
	 * @return	string
	 */
	function _attributes_to_string($attributes)
	{
		if (empty($attributes))
		{
			return '';
		}

		if (is_object($attributes))
		{
			$attributes = (array) $attributes;
		}

		if (is_array($attributes))
		{
			$atts = '';

			foreach ($attributes as $key => $val)
			{
				$atts .= ' '.$key.'="'.$val.'"';
			}

			return $atts;
		}

		if (is_string($attributes))
		{
			return ' '.$attributes;
		}

		return FALSE;
	}
}