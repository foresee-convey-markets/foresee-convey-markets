<?php require_once('config.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Register | Foresee Convey Markets</title>
	<link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/foresee-convery-markets-logo.png" type="image/x-icon" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/validator.min.js"></script>
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" type="text/css"/>
	<link href="<?php echo BASE_URL; ?>assets/css/all.min.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/all.min.js"></script>    
    <link href="<?php echo BASE_URL; ?>assets/css/home.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
        $(document).ready(function(){
            $("#dob").datepicker({
            format:'Y-m-d',
            });

	        $("#form").validator();
        });
        
</script>

</head>
<style type="text/css">
	span{
		font-size: 1.1em;
		text-transform: capitalize;
		font-weight: bold;
		color: #b1051d;
	}
    iframe{
        display: none;
    }
    button.btn-register,button.btn-register:hover,button.btn-register:focus{
        background: #d05107;
        border: 0px;
    }
	button.btn-login{
		background: #0465ac;
		border: 0px;
	}
    .form-group{
        margin-bottom: 0px;
    }
	.container-fluid .jumbotron{	    
        border-radius: 4px;
        background: #cfd7d5;
        padding-top: 15px;
        padding-bottom: 15px;
	}
    .container-fluid{
        background-color: #f3f3f3;
    }
    @media screen and (min-width: 767px){
        .form-group{
            margin-bottom: 5px;
        }
        .sticky {
            position: -webkit-sticky; /* SAFARI */
            position: -moz-sticky; /* MOZILLA */
            position: -o-sticky; /* OPERA */
            position: -ms-sticky; /* IE */
            position: sticky;
            top: 0px;
        }
    }
    @media screen and (max-width: 767px){
        .form-group{
            margin-bottom: 0px;
        }
        .form-horizontal .form-group{
            margin-left: 0px;
            margin-right: 0px
        }
        button.btn-register{
            margin-top: 5%;
        }
        span{
            font-size: 1em;
        }
        button.btn-login{
            min-width: 120px;
            float: right;
            transform: translate(15px,-26px);
        }
    }
    button.btn-login > .fa,button.btn-register > .fa{
        color: #fff;
    }
    .img-banner{
        margin-top: 15%;
    }
</style>
<body>
	<div class="container-fluid">
        <div class="row login-block">
            <div class="col-md-12">
                <div class="logo" align='center'>
                    <img src="<?php echo BASE_URL.'assets/images/foresee-convery-markets-logo.png' ?>"/>
                </div><br/>
            </div>
        </div>
		<div class="jumbotron">
            <div class="row">
                <div class="col-md-6 sticky">
                    <img class="img img-responsive img-banner" src="<?php echo BASE_URL.'assets/images/login-banner.jpg' ?>" alt='Login Banner' />
                </div>
                <div class="col-md-6">
                    <div align='center'>
                        <h3 class="login-header">New Registration</h3>
                        <p>*Please fill the following details to register</p>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-2">
                            <div class="message-block"></div>
			
                                <form class="form-horizontal" id="form" role="form"  onsubmit="return false;"  method="post" data-toggle="validator" >
                                    <input type="hidden" name="register" value="register" />

                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label class="control-label">Name <span class="star">*</span> :</label>
                                            <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="name" type="text" data-error="Please enter your name" required/>
                                            <div class="help-block with-errors"></div>                                     
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label class="control-label">Email Address <span class="star">*</span> :</label>
                                            <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" data-error="Please enter valid email id" required/>
                                            <div class="help-block with-errors"></div>                                     
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label class="control-label">Password <span class="star">*</span> :</label>
                                            <input class="form-control" name="password" id="password"  placeholder="Password" data-bv-field="password" type="password" data-error="Please enter Password"  data-minlength="8" required/>
                                            <div class="help-block with-errors"></div>                                     
                                        </div>
                                    </div>                    
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label class="control-label">Confirm Password <span class="star">*</span> :</label>
                                            <input class="form-control" name="con_pass" id="con_pass"  placeholder="Re-enter Your Password" data-bv-field="con_pass" type="password" data-error="Please enter Password"  data-minlength="8" data-match="#password" data-match-error="Whoops, Passwords don't match"  required/>
                                            <div class="help-block with-errors"></div>                                     
                                        </div>
                                    </div>                       
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label class="control-label">Role <span class="star">*</span> :</label>
                                            <select class="form-control" name="user_type" id="user_type" data-bv-field="user_type" data-error="Please select your role" required>
                                                <option value="">Select</option> 
                                                <option value="2">Digital Marketing</option>
                                                <option value="3">Sales Manager</option>
                                                <option value="4">PHP Developer</option>
                                            </select>
                                            <div class="help-block with-errors"></div>                                     
                                        </div>
                                    </div>  
                                    <div>
                                        <button type="submit" class="btn btn-primary btn-submit btn-register" onclick="registerUser()"><span class="fa fa-user"></span>&nbsp; Register</button><br class="md-hidden"/><br class="md-hidden" />
                                        &nbsp;&nbsp;<span>If already registered </span>&nbsp;&nbsp;
                                        <button type="button" class="btn btn-primary btn-submit btn-login" onclick="window.location.href='<?php echo BASE_URL; ?>'"><span class="fa fa-sign-in-alt"></span>&nbsp; Login</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</body>
<script type="text/javascript">	

    function registerUser(){

        var form = $('#form');

        $.ajax({
            beforeSend:function() { 
                $(".btn-register").html("<span class='fa fa-spinner fa-spin'></span>");
            },
            complete:function() {
                $(".fa fa-spinner fa-spin").remove();
            },
            type:'post',
            url:'ajaxoperations.php',
            data: form.serialize(),

            success:function(data){
                $(".message-block").html(data);
                $(".alert-success").append("<meta http-equiv='refresh' content='2,url=<?php echo BASE_URL; ?>' />");
                $(".fa fa-spinner fa-spin").remove();

                if($('.message-block').children('h4').hasClass('alert-success')){
                    $(".btn-register").css({'background-color':'green'}).html("<span class='fa fa-check-circle'></span> Registration Success");
                }else{
                    $(".btn-register").css({'background-color':'#d43f3a'}).html("<span class='fa fa-exclamation-triangle'></span> Registration Failed");
                }
            }
        });
        
    }
</script>
</html>