<?php

$STATUS = array(
	'0' => 'Inactive',
	'1' => 'Active'
);

$ARCHIVE_STATUS = array(
	'0' => 'Not Achived',
	'1' => 'Archived'
);

$USER_TYPES = array(
	'1' => 'Admin',
	'2' => 'Digital Marketing',
	'3' => 'Sales Manager', 
	'4' => 'PHP Developer'
);
$PAYMENT_MODES = array(
	'1' => 'Paypal',
	'2' => 'EazyPay',
	'3' => 'Bank Transfer',
	'0' => 'NA',
	'' => 'NA'
);
$RECORDS_PER_PAGE = array(
	'10' => '10',
	'15' => '15', 
	'25' => '25', 
	'50' => '50',
	'100' => '100',
	'250' => '250', 
	'500' => '500'	
);
$FORM_TYPES = array(
	'1' => 'Contact Query', 
	'2' => 'Custom Research Query',
	'3' => 'Search Query', 
	'4' => 'Request Sample Query', 
	'5' => 'Request Customization Query', 
	'6' => 'Enquire Before Purchase Query', 
	'7' => 'Request Discount Query', 
	'8' => 'Checkout Query',	 
	'9' => 'Report Details Query',	
);
$STATUS_TABLES = array(
	'1' => 'mr_blogs', 
	'2' => 'mr_press',
	'3' => 'mr_publisher', 
	'4' => 'mr_report', 
	'5' => 'mr_sub_cat_1', 
	'6' => 'mr_login', 
	'7' => 'mr_bannedwords',
	'8' => 'mr_form_contact',
	'9' => 'mr_global_checkout'	
);
$STATUS_TYPES = array(
	'1' => 'status', 
	'2' => 'archive_status'	
);

$COLUMNS_NAME = array(
    'ReportTitle' => array(
                        'report_title',
                        'reportTitle',
                        'rep_title',
                        'repTitle',
                        'reportTitle',
                        'ReportTitle',
                        'report_Title',
                        'Report_Title',
                        'ReportTitle',
                        'report title',
                        'reporttitle',
                        'title',
                        'report and title',
                        'report_and_title'
    ),
    'PublisherID' => array(
                        'Publisher ID',
                        'Publisher id',
                        'publisher id',
                        'Publisher_ID',
                        'Publisher_id',
                        'publisher_id',
                        'publisherid',
                        'publisherID',
    ),
    'CategoryID' => array(
                        'category id',
                        'Category_ID',
                        'category_id',
                        'Category_id',
                        'Categoryid',
                        'catalog',
    ),
    'Pages' => array( 
                        'Pages',
                        'Page',
                        'pages',
                        'page',
    ),
    'Date' => array( 
                        'date',
                        'published date',
                        'published_date'
    ),
    'Content' => array( 
                        'content',
                        'scope of the report',
                        'scope of report',
                        'scope_of_the_report',
                        'scope_of_report',
                        'report_content',
                        'report content'
    ),
    'TOC' => array(
                        'report table of contents',
                        'toc',
                        'report toc',
                        'table of contents',
                        'table of content',
                        'table and contents',
                        'table and content',
                        'table_of_contents',
                        'table_of_content',
                        'table_and_contents',
                        'table_and_content',
                        
    ),
    'TOF' => array(
						'report toc fig',
						'report toc figure',
                        'tof',
                        'report tof',
                        'table and figure',
                        'tables and figures',
                        'tables and figure',
                        'table and figures'
    ),
    'PageTitle' => array(
						'page title',
						'page_title',
						'pagetitle',
    ),
    'MetaTitle' => array(
						'meta title',
						'meta_title',
						'metatitle',
    ),
    'SingleUserLicense' => array(
						'single user license',
						'single_user_license',
						'singleuserlicense',
                        'single user price',
                        'single_user_price'
    ),
    'EnterpriseUserLicense' => array(
						'enterprise user license',
						'enterprise_user_license',
						'enterpriseuserlicense',
                        'multi user price',
                        'multi_user_price'
    ),
    'CorporateUserLicense' => array(
						'corporate user license',
						'corporate_user_license',
						'corporateuserlicense',
                        'enterprise price',
                        'enterprise_price'
    ),
    'LeadDate' => array( 
						'lead date',
						'leaddate',
						'lead_date',
    ),
    'Name' => array( 
                        'name',
						'client name',
						'client_name',
						'clientname',
						'fullname',
						'full_name',
						'full name',
    ),
    'Email' => array( 
                        'email',
						'client email',
						'client_email',
						'clientemail',
    ),
    'Company' => array( 
                        'company',
						'client company',
						'client_company',
						'clientcompany',
    ),
    'Country' => array( 
                        'country',
						'client country',
						'client_country',
						'clientcountry',
    ),
    'Phone' => array( 
                        'phone',
						'client phone',
						'client_phone',
						'clientphone',
    ),
    'LeadType' => array( 
                        'leadtype',
						'lead_type',
						'lead type',
    ),
    'isFollowed' => array( 
                        'isfollowed',
						'is_followed',
						'is followed',
    ),
    'FollowupId' => array( 
                        'followupid',
						'followup_id',
						'followup id',
    ),
    'IntervalDate' => array( 
                        'intervaldate',
						'interval_date',
						'interval date',
    ),
    'Interval' => array( 
                        'interval',
						'followup interval',
						'followupinterval',
						'followup_nterval ',
    ),
    'IntervalValue' => array( 
                        'intervalvalue',
						'interval_value',
						'interval value'
    ),
);