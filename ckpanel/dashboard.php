
<!DOCTYPE html>
<html>
<head>
    <title>Dashboard | Foresee Convey Markets</title>

    <!--*****************************  HEADER  ************************************** -->

    <?php require_once 'header.php'; ?>

    <!--*****************************  HEADER  ************************************** -->
    <style type="text/css">
        .bg-warning{
            background: #dfb924;
        }
    </style>
    <?php 

//*********************** CHECK FOR SESSION ****************************//


//*********************** PUBLISHERS ****************************//
    $getPublishers=$link->query("select count(id) from mr_publisher where status='1' ");
    $publishers=$getPublishers->fetch_row()[0];

//*********************** PRESS RELEASES ****************************//
    $getPressReleases=$link->query("select count(id) from mr_press where status='1' ");
    $press=$getPressReleases->fetch_row()[0];

//*********************** BLOGS ****************************//
    $getBlogs=$link->query("select count(id) from mr_blogs where status='1' ");
    $blogs=$getBlogs->fetch_row()[0];

//*********************** CATEGORIES ****************************//
    $getCategories=$link->query("select count(id) from mr_sub_cat_1 where status='1' ");
    $categories=$getCategories->fetch_row()[0];

//*********************** REPORTS ****************************//
    $getReports=$link->query("select count(id) from mr_report where status='1' ");
    $reports=$getReports->fetch_row()[0];

//*********************** LEADS ****************************//
    $getLeads=$link->query("select count(contact_id) from mr_form_contact");
    $leads=$getLeads->fetch_row()[0];

//*********************** Contact And Custom Research Queries ****************************//
    $getContactQueries=$link->query("select count(contact_id) from mr_form_contact where contact_form_type in (1,2)");
    $contact_queries=$getContactQueries->fetch_row()[0];

//*********************** Search Queries ****************************//
    $getSearchQueries=$link->query("select count(contact_id) from mr_form_contact where contact_form_type=3");
    $search_queries=$getSearchQueries->fetch_row()[0];

//*********************** LEADS ****************************//
    $getLeads=$link->query("select count(contact_id) from mr_form_contact where contact_form_type in (4,5,6,7,9)");
    $leads=$getLeads->fetch_row()[0];

//*********************** Checkouts ****************************//
    $getCheckouts=$link->query("select count(contact_id) from mr_form_contact where contact_form_type = 8");
    $checkouts=$getCheckouts->fetch_row()[0];

//*********************** USERS ****************************//
    $getUsers=$link->query("select count(id) from mr_login where user_type!='1'");
    $users=$getUsers->fetch_row()[0];

//*********************** CHECKOUTS ****************************//
    $getGlobalCheckouts=$link->query("select count(id) from mr_global_checkout");
    $global_checkouts=$getGlobalCheckouts->fetch_row()[0];

//*********************** BANNED WORDS ****************************//
    $getBannedWords=$link->query("select count(id) from mr_bannedwords");
    $bannedwords=$getBannedWords->fetch_row()[0];

    if(@$_SESSION["user_type"]=='1'){

    ?>

    <h1 class="stats"><span class="fa fa-database"></span> STATISTICS</h1><br/>

    <div class="row statistics">
        <div class="col-md-4">
            <h3>USERS</h3>
            <p><?php echo $users; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'login-approval'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PRESS RELEASES</h3>
            <p><?php echo $press; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'press-release-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>BLOGS</h3>
            <p><?php echo $blogs; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'blog-list'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics">
        <div class="col-md-4">
            <h3>CATEGORIES</h3>
            <p><?php echo $categories; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'category-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PUBLISHERS</h3>
            <p><?php echo $publishers; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'publisher-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>REPORTS</h3>
            <p><?php echo $reports; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'report-list'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics statistics-last">
        <div class="col-md-4">
            <h3>BANNEDWORDS</h3>
            <p><?php echo $bannedwords; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'bannedword-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>CONTACT QUERIES</h3>
            <p><?php echo $contact_queries; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'lead-list?type=1'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>SEARCH QUERIES</h3>
            <p><?php echo $search_queries; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'lead-list?type=3'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics statistics-last">
        <div class="col-md-4">
            <h3>CHECKOUTS</h3>
            <p><?php echo $checkouts; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'lead-list?type=8'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>LEADS</h3>
            <p><?php echo $leads; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'lead-list?type=4'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>GLOBAL CHECKOUTS</h3>
            <p><?php echo $global_checkouts; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'global-checkout-list'; ?>'">View All</button></p>
        </div>
    </div>

<?php } 
if(in_array(@$_SESSION["user_type"],array('2','3','4'))){
?>

    <h1 class="stats"><span class="fa fa-database"></span> STATISTICS</h1><br/>

    <div class="row statistics">
        <div class="col-md-4">
            <h3>CATEGORIES</h3>
            <p><?php echo $categories; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'category-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PUBLISHERS</h3>
            <p><?php echo $publishers; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'publisher-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PRESS RELEASES</h3>
            <p><?php echo $press; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'press-release-list'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics statistics-last">
        <div class="col-md-4">
            <h3>BLOGS</h3>
            <p><?php echo $blogs; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'blog-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>REPORTS</h3>
            <p><?php echo $reports; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'report-list'; ?>'">View All</button></p>
        </div>
    </div><br/>

<?php } ?>
   
    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
