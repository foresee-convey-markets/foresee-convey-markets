<?php
include_once ('../include/config.php');
include_once("../include/authenticate-admin.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"/> 
        <meta name="viewport" content="width=device-width, initial-scale=1"/>    
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="language" content="France, French"/>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" type="text/css" />
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <link href="admin-styles.css" rel="stylesheet" type="text/css" />
        <title>
            <?= $adm_panel_title ?>
        </title>
        <style type="text/css">
            .tab
            {
                float: left;
                margin-bottom: 10%;
            }
            .tab li
            {
                padding: 10px;
                text-align: left;
            }
            @media screen and (min-width:767px) and (max-width:992px)
            {
                .tab-content 
                {
                    margin-top: 30%;
                }
            }
            @media screen and (max-width:767px)
            {
                .tab-content 
                {
                    margin-top: 60%;
                }
            }
            .tab li a
            {
                text-decoration: none;
            }
            .tab li a:hover
            {
                color:black;
            }
            .navbar-inverse .navbar-nav>li>a{
                color:#fff;
            }
            .navbar-inverse .navbar-nav>li:hover >a{
                background: lightgrey;
                color: #000;
            }
        </style>
        <script type="text/javascript">
        $(document).ready(function(){
            $(".datepicker").datepicker({dateFormat:"yy-mm-dd"});
        })
        
        </script>
    </head>
    <?php
    $pid=$_GET["pid"];
    $link=new mysqli("localhost","root","","qqmarket_research");
    if(!$link)
    {
        die("Error");
    }

    $sql=$link->query("select * from mr_press where p_cat_id=$pid");
    $data=mysqli_fetch_array($sql);
    $radio=$data["p_status"];
    $cat=$data["p_category"];
   
       
    
    ?>
     <?php
    if(isset($_POST["pressUpdate"]))
    {
        $link=mysqli_connect("localhost","root","","qqmarket_research");
        if(!$link)
        {
         echo "Error";   
        }
        $title=$_POST["presstitle"];
        $domain=$_POST["domain"];
        $pcat=$_POST["pcategory"];
        $desc=$_POST["pressdesc"];
        $url=$_POST["pressurl"];
        $date=$_POST["pressdate"];
        $status=$_POST["pressStatus"];
        $cur_date=date("Y-m-d");
        
        $image=$_FILES["pressimage"]["name"];
       
		if($image !== ""){
			$image_dir="../assets/img/pressRelease-logos/";	
			move_uploaded_file($_FILES["pressimage"]["tmp_name"], $image_dir.$image);
		}
		if($image === ""){
			$image=mysqli_real_escape_string($link,$_POST["primage"]);
			//echo "<script>alert('".$image."')</script>";
		}
        
        $sql=mysqli_query($link,"update mr_press set p_category='".$pcat."', p_title='".$title."',domain='".$domain."',p_status='$status',p_content='".$desc."',p_date='".$date."',p_img='".$image."',p_url='".$url."',p_added_on='".$cur_date."' where p_cat_id=$pid ");
        
            if($sql===TRUE )
            {
                $message="<b style='color:green'>Press release Updated successfully !</b>";
                echo "<meta http-equiv='refresh' content='3,url=press-release.php' />";
            }
            else
            {
                $message="<b style='color:red'>Error Updating Press release !</b>";
                echo mysqli_error($link);
            }
        
        
        
    }
    $link->close();
    
    ?>
<body class="container-fluid" style="font-size: 1.4em">
    <?php
    include_once('admin-header.php');
    ?>
    <nav class="navbar navbar-inverse"  style='background:green;'>
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="admin-home.php">Home</a></li>
                <?php /* ?> <li><a href="admin-general.php" >General</a></li><?php */ ?>
                <?php /* ?> <li><a href="admin-cms.php">CMS</a></li><?php */ ?>
                <li><a href="admin-category.php">Category / Sub Category</a></li>
                <li><a href="publisher-add.php" >Publishers</a></li>
                <li><a href="admin-report.php" >Report</a></li>
                <?php /* ?> <li><a href="admin-promo.php" >Promo</a></li><?php */ ?>
                <li><a href="csv_upload.php" >CSV Upload</a></li>
                <li><a href="press-release.php" >Press Release</a></li>
                <li><a href="blogs.php" >Blogs</a></li>
                <li><a href="list-leads.php" >Leads</a></li>
                <li><a href="global-checkout-list.php" >Global Checkout</a></li>
            </ul>
        </div>
    </nav>
       <div class="container">
        <div class="col-md-3">
            <ul style="display:inline-block;list-style-type: none" class="tab">
                <li><a  href="press-release.php#home">Add New Press Release</a></li>
                <li><a  href="press-release.php#menu1">PR LIST</a></li>
                <li><a  href="press-release.php#menu2">PR Archive List</a></li>
            </ul><br/><br/>
        </div>
        <div class="col-md-9">
            <h4 style="height:30px;background: black;color: white;text-align: center"><strong>Edit Press Release </strong></h4>
            <h4 style="text-align:center"><?php echo $message; ?></h4>
            <div  style="border:1px solid gray;box-shadow: 1px 1px 2px 1px gray;">
                <div style="margin:10px">
                    <form class="form-horizontal" name="pressForm"  id="pr_frm" role="form" action="<?php echo $_PHP_SELF; ?>" method="post" enctype="multipart/form-data" >
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="pcategory">Press Release Category*</label>
                            <div class="col-md-9">
                                <select class="form-control" name="pcategory" id="pcategory" title="Press Release Categrory"  required>
                                    <?php
                                    $link = mysqli_connect("localhost", "root", "", "qqmarket_research");
                                    if (!$link) {
                                        echo "Error";
                                    }
                                    $pressCat = "SELECT sc1_id, sc1_name FROM mr_sub_cat_1 WHERE sc1_status='Y' ORDER BY sc1_name ASC";
                                    $pcatQuery = mysqli_query($link, $pressCat);
                                    
                                    echo '<option value="0">Select Category</option>';		
                                    
                                        if (mysqli_num_rows($pcatQuery) > 0)
                                        {
                                            while ($pressrow = mysqli_fetch_array($pcatQuery)) 
                                            {
                                            ?>
                                            <option value="<?php echo $pressrow['sc1_id']; ?>" <?php if($data["p_category"]==$pressrow["sc1_id"]) { ?> selected <?php } else { } ?> ><?php echo $pressrow["sc1_name"]; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                    
                                </select>             
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="presstitle">Title *</label>
                            <div class="col-md-9">
                                <input class="form-control" name="presstitle" id="presstitle"  placeholder="Press Release Title" title="Press Release Title" type="text" value="<?php echo $data["p_title"] ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="domain">Domain  *</label>
                            <div class="col-md-9">
                                <select class="form-control" name="domain" id="domain"   title="Press Release Title" type="text" required>
                                    <option value="">Select</option>
                                    <option value="qandq" <?php if($data["domain"] == 'qandq'){ echo 'selected' ;} ?> >www.qandqmarketresearch.com</option>
                                    <option value="qq" <?php if($data["domain"] == 'qq') { echo 'selected' ;} ?> >www.qqmarketresearch.com</option>
                                    <option value="statistic" <?php if($data["domain"] == 'statistic') { echo 'selected' ;} ?>>www.statisticreports.com</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="pressdesc">Description *</label>
                            <div class="col-md-9">
                                <?php
                                include_once 'ckeditor_old/ckeditor.php';
                                $sBasePath = "ckeditor/";
                                $CKEditor = new CKEditor();
                                $CKEditor->editor("pressdesc",$data["p_content"]);
                                ?>   
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="pressurl">URL *</label>
                            <div class="col-md-9">
                                <input class="form-control" name="pressurl" id="pressurl" placeholder="URL"  type="text" title="URL" value="<?php echo $data["p_url"] ?>" />
                            </div>
                        </div>
						<div class="form-group">
                            <input type="hidden" class="form-control" name="primage" value="<?php echo $data["p_img"] ?>" />
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Image *</label>
                            <div class="col-md-9">
                                <img  src="../assets/img/pressRelease-logos/<?php echo $data["p_img"] ?>"  title="<?php echo $data["p_img"] ?>" height="100px" width="200px" /><br/><br/>
                                <input class="form-control" name="pressimage" id="pressimage"   type="file" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="pressdate">Date *</label>
                            <div class="col-md-9">
                                <input class="form-control datepicker" name="pressdate" id="pressdate"  type="text" value="<?php echo $data["p_date"] ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="pull-left control-label col-md-offset-2">Status  *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                            <div class=" pull-left">
                                <label class="radio-inline"><input name="pressStatus" id="pressStatus" value="Y" <?php echo ($radio=="Y") ? "checked" : "" ?>  type="radio"/>Active</label>
                                <label class="radio-inline"><input name="pressStatus" id="pressStatus" value="N" <?php echo ($radio=="N") ? "checked" : "" ?> type="radio" />Inactive</label>
                            </div>
                        </div>
                        <div class="pull-left col-md-offset-3">
                            <button type="submit" class="btn btn-info btn-md" name="pressUpdate" style="width:100px">Update</button>&nbsp;&nbsp;
                            <a href="press-release.php">Back</a>
                        </div><br/><br/>
                    </form>
                </div>
            </div><br/>
        </div>
    </div>
    <br/>
    <?php include_once('admin-footer.php'); ?>
    <br/>
    </body>

