<?php require_once('config.php');  
    
    //*********************** CHECK FOR SESSION ****************************//

 if(!isset($_SESSION["email"]) && !isset($_SESSION["user_type"]) && !isset($_SESSION["name"])){

?>
<!DOCTYPE html>
<html>
<head>
	<title>Login | Foresee Convey Markets</title>
	<link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/foresee-convery-markets-logo.png" type="image/x-icon" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/validator.min.js"></script>
	<link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" type="text/css"/>
	<link href="<?php echo BASE_URL; ?>assets/css/all.min.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/all.min.js"></script>
	<link href="<?php echo BASE_URL; ?>assets/css/home.css" rel="stylesheet" type="text/css"/>

</head>
<script type="text/javascript">
	$("#form").validator();
</script>
<style type="text/css">
    button.btn-register,button.btn-register:hover,button.btn-register:focus{
        background: #d05107;
        border: 0px;
    }
    .container-fluid .jumbotron{	    
        border-radius: 4px;
        background: #cfd7d5;
        padding-top: 15px;
        padding-bottom: 15px;        
    }
    @media screen and (min-width : 767px) {        
        .sticky {
            position: -webkit-sticky; /* SAFARI */
            position: -moz-sticky; /* MOZILLA */
            position: -o-sticky; /* OPERA */
            position: -ms-sticky; /* IE */
            position: sticky;
            top: 200px;
        }
    }
    @media screen and (max-width: 767px){
        .form-horizontal .form-group{
            margin-left: 0px;
            margin-right: 0px
        }
    }
    .container-fluid{
        background-color: #f3f3f3;
    }
</style>
<body>
	<div class="container-fluid">
        <div class="row login-block">
            <div class="col-md-12">
                <div class="logo" align='center'>
                    <img src="<?php echo BASE_URL.'assets/images/foresee-convery-markets-logo.png' ?>"/>
                </div><br/>
            </div>
        </div>
        <div class="jumbotron">
            <div class="row">
                <div class="col-md-6 sticky">
                    <img class="img img-responsive img-banner" src="<?php echo BASE_URL.'assets/images/login-banner.jpg' ?>" alt='Login Banner' />
                </div>
                <div class="col-md-6">
                    <div align='center'>
                        <h3 class="login-header">Sign in to your account</h3>
                        <p>*Please Login To Continue</p><br/>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-2">	
                            <div class="login-message-block"></div> 		
                            <form class="form-horizontal" id="form" role="form"  onsubmit="return false;"  method="post" data-toggle="validator" >
                                <input type="hidden" name="login" value="login" />
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <label class="control-label">Email Address <span class="star">*</span> :</label>
                                        <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" data-error="Please enter valid email id" required/>
                                        <div class="help-block with-errors"></div>                                     
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <label class="control-label">Password <span class="star">*</span> :</label>
                                        <input class="form-control" name="password" id="password"  placeholder="Password" data-bv-field="password" type="password" data-error="Please enter Password" required/>
                                        <div class="help-block with-errors"></div>                                     
                                    </div>
                                </div><br/>                    
                                <div>
                                    <button type="submit" class="btn btn-primary btn-submit btn-login" onclick="loginUser()"><span class="fa fa-sign-in-alt"></span>&nbsp; Login</button>

                                    <button type="button" class="btn btn-primary btn-submit btn-register" onclick="window.location.href='<?php echo BASE_URL.'register'; ?>'"><span class="fa fa-user"></span>&nbsp; Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</body>
<script type="text/javascript">	

    function loginUser(){

        var form = $('#form');
        $.ajax({
            beforeSend:function() { 
                $(".btn-login").html("<span class='fa fa-spinner fa-spin'></span>");
            },
            complete:function() {
                $(".fa fa-spinner fa-spin").remove();
            },
            type:'post',
            url:'ajaxoperations.php',
            data: form.serialize(),
            
            success:function(data){
                $(".login-message-block").html(data);
                $(".alert-success").append("<meta http-equiv='refresh' content='0,url=<?php echo BASE_URL.'dashboard'; ?>' />");
                $(".fa fa-spinner fa-spin").remove();
                
                if($('.login-message-block').children('h4').hasClass('alert-success')){
                    $(".btn-login").css({'background-color':'green'}).html("<span class='fa fa-check-circle'></span> Success");
                }else{
                    $(".btn-login").css({'background-color':'#d43f3a'}).html("<span class='fa fa-exclamation-triangle'></span> Login Failed").addClass(' wow wobble');
                }
            }
        });
        
    }
</script>
</html>
<?php 
}else{
    echo "<meta http-equiv='refresh' content='0,url=".BASE_URL."dashboard' />";
}

?>