<?php require_once('config.php');

function get_shortDescription($rep_descrip, $limit = 20, $condi = 20) 

{
$cond = $limit+$condi; 
$descrip = $rep_descrip;
$sentence=$rep_descrip; 
$words=count(explode(' ', $sentence));

	if($words > $cond) 
	{
	
	$descrip=implode(' ', array_slice(explode(' ', $sentence), 0, $limit));
	$descrip .= " ...";
	  }
	$descrip = str_replace('&Acirc;','',$descrip);
$descrip=str_replace('&prime;','&lsquo;',$descrip);
$descrip=str_replace('&Prime;','&rdquo;',$descrip);
$descrip=str_replace('&bprime;','&rsquo;',$descrip);
$descrip=str_replace('&ndash;','&dash;',$descrip);
$descrip=str_replace('&mdash;','&dash;',$descrip);
//$string=str_replace('’;',"'",$string);

$specials = array('&Acirc;','&acirc;','&Aacute;','&Atilde;','&aacute;','&Ecirc;','&Egrave;','&Eacute;','“','�');
$descrip=str_replace($specials,'',$descrip);
return $descrip;	
}
function get_pure_words_url($string) 
  {
$specials = array("Global","Africa","South & Southeast Asia","Asia-Pacific","North Africa","Europe","Sub-Saharan Africa","North & Cental America","Antarctica","Caribbean Islands","Asia","Mesoamerica","East Asia","North America","North Asia","Oceania","West & Central Asia","South America","Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe","Latin America",'Middle East','and','USA','&amp;','global','2017','2022','(',')','-',',','Industry','Research','Report','Market','Company','Profile','Deals','Ltd','Alliances',': ','Country','Sector',' in ','Prospects','Survey','Chinese','Regional','Regional ','Production', 'Sales' ,'Consumption','Status','Prospects','Professional','Outlook','By', 'Players', 'Regions', 'Product' ,'Types','Applications','Analysis',' to ');
$string=str_replace($specials,'',$string);
return $string;
}
function get_pure_words($string) 
{
$specials = array('(',')',"'");
$string=str_replace($specials,'',$string);
return $string;
}

//*********************** CHECK FOR SESSION ****************************//



if(isset($_POST['submit']))
{
$file = $_FILES['file']['tmp_name'];
$handle = fopen($file, 'r');

	while(($data = fgetcsv($handle, 1000, ',')) !== false)
	{
	    date_default_timezone_set('Asia/kolkata');
	$insert_csv = array();

	$insert_csv['rep_title'] = $data[0];

	$rep_title= $data[0];

	$url = str_replace(array('\r','\n','\r\n'),'',($insert_csv['rep_title']));
	//$url=strtolower($insert_csv['rep_title']);
	$key= (explode('Market',$url)) ? explode('Market',$url) : array($url);
	$url = get_pure_words_url($key[0]);
	$url1=str_replace(" ","-",trim($url));
	$url1 = str_replace("%5Cr",'', $url1);
	$url1=str_replace("(","",$url1);
	$url2=str_replace(")","",$url1);
	$url3=str_replace("%","",$url2);
	$url4=str_replace(".","",$url3);
	$url5=str_replace(",","",$url4);
	$url6=str_replace("$","USD",$url5);
	$url7=str_replace(":","",$url6);
	$url7=str_replace("--","",$url7);
	$url8=urlencode(str_replace(" ","-",trim($url7)));
	$unique_url=strtolower($url8)."-market-".rand(0,111);

	$publisher_id=$insert_csv['publisher_id']=$data[1];
	$sub_cat_1=$insert_csv['rep_sub_cat_1_id'] = $data[2];
	$rep_page=$insert_csv['rep_page'] = $data[3];
	$rep_toc_no=$insert_csv['rep_toc_no'] = $data[4];
	//$rep_date=$insert_csv['rep_date'] = $data[5];
	//$originalDate = $insert_csv['rep_date'];
	//$rep_date=$insert_csv['rep_date'] = date("Y-m-d H:i:s",strtotime($originalDate));

	$rep_contents=$insert_csv['rep_contents'] =get_pure_words($data[6]);
	$rep_table_of_contents=$insert_csv['rep_table_of_contents'] = get_pure_words($data[7]);
	$rep_toc_fig=$insert_csv['rep_toc_fig'] = get_pure_words($data[8]);

	$rep_descrip=get_shortDescription($rep_contents);

	$rep_page_title=$insert_csv['rep_page_title'] = get_pure_words($data[9]);
	$rep_meta_title=$insert_csv['rep_meta_title'] = get_pure_words($data[10]);

	$li_key1=$insert_csv['li_key1'] = "Single User Price";
	$li_value1=$insert_csv['li_value1'] = $data[11];
	$li_key2=$insert_csv['li_key2'] = "Enterprise User License";
	$li_value2=$insert_csv['li_value2'] = $data[12];

	$li_key3=$insert_csv['li_key3'] = "Corporate User License";
	$li_value3=$insert_csv['li_value3'] = $data[13];
	$insert_csv['rep_cat_id']=1;

	$entry_date=$insert_csv['rep_date'] = $data[5];
	$originalDate = $insert_csv['rep_date'];
	$entry_date=$insert_csv['rep_date'] = date("Y-m-d H:i:s",strtotime($originalDate));
	//$entry_date=getCurDateTime();
	ini_set("date.timezone", "Asia/Kolkata");

	date_default_timezone_set('Asia/Kolkata');
	$rep_date=date('Y-m-d H:i:s');

	//$unique=rand(1,1000);
	//$unique_url=$url8.'-'.$unique;
		if($rep_title === "Title" || $rep_descrip === "Content" || $rep_contents === "" || $publisher_id ==="" || $li_key1 ==="" || $rep_page === 'Pages'){
		    echo "<script>alert('INVALID')</script>";
		}else{
		    
		    $sql="INSERT INTO mr_report (rep_cat_id,rep_sub_cat_1_id,publisher_id,rep_url,rep_title,rep_descrip,rep_page,rep_toc_no,rep_date,rep_toc_fig,rep_entry_date,rep_page_title,rep_meta_title)  
		    VALUES ('1','$sub_cat_1','$publisher_id','$unique_url','".$rep_title."','$rep_descrip','$rep_page','$rep_toc_no','$rep_date','$rep_toc_fig','$entry_date','$rep_page_title','$rep_meta_title')";


		    if($link->query($sql)){
				$last_id = $link->insert_id;
				   // $last_id = SELECT LAST_INSERT_ID();

				$sql="INSERT INTO mr_report_metadata (rep_contents,rep_table_of_contents,meta_rep_id)  
				VALUES ('$rep_contents','$rep_table_of_contents','$last_id')";
				$link->query($sql);	

				if($li_value1 > 0){	
				$sql1="INSERT INTO mr_report_license (li_pub_id,li_rep_id,li_key,li_value)
				VALUES ('$publisher_id','$last_id','$li_key1','$li_value1')";
				$link->query($sql1); 
				}
				if($li_value2 > 0){
				$sql2="INSERT INTO mr_report_license (li_pub_id,li_rep_id,li_key,li_value)
				VALUES ('$publisher_id','$last_id','$li_key2','$li_value2')";
				$link->query($sql2);
				}
				if($li_value3 > 0){
				$sql3="INSERT INTO mr_report_license (li_pub_id,li_rep_id,li_key,li_value)
				VALUES ('$publisher_id','$last_id','$li_key3','$li_value3')";
				$link->query($sql3);
				}
		    }else{
				echo mysqli_error($link); 
		    }
		}

	}


echo '<h3><b style="color:#479143">Reports have been imported successfully into database</b></h3>
<meta http-equiv="refresh" content="1,url='.BASE_URL.'csv-upload" />';
exit;


}


?>