
<html>
<head>
<title>Category List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->
<script src="assets/js/status_toggle.js"></script>

    <h1 class="stats"><span class="fas fa-list-ol"></span> Category List</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.location.href='<?php echo BASE_URL.'category-add';?>'">
        <a href="<?php echo BASE_URL.'category-add';?>"><i class="fa fa-plus"></i> Add New Category</a> 
    </button>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover text-center" >
    		<thead class="text-primary">
    			<tr>
    				<th style="max-width: 40px;">Category ID</th>
    				<th>Category</th>

                <?php if($_SESSION["user_type"]=='1'){ ?>
                    <th>Status</th>
    				<th>Options</th>
                <?php } ?>

    			</tr>
    		</thead>
    		<tbody id='deleteCount'>
    			<?php
    			$getCategories=$link->query("select * from mr_sub_cat_1 order by id");
    			if($getCategories->num_rows > 0){
	    			while($row=$getCategories->fetch_assoc()){
                        $id = $row['id'];
                        $status = @$row['status'];
	    				echo "<tr id='categoryDelete".$row['id']."'><td>".$id."</td>";
	    				echo "<td  style='max-width: 100px;text-align:justify'>".$row["sc1_name"]."</td>";

                    if($_SESSION["user_type"]=='1'){                         
                        $checked = @$status == '1' ? 'checked' : '';
                        echo getStatus($id, @$status, $STATUS, $checked, 5, 1);                        

	    				echo "<td colspan='2'  align='center'><a href='category-edit?id=".$id."'><i class='far fa-edit text-primary'></i></a>

                            <button data-toggle='modal' data-target='#removeMe".$id."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                                <div id='removeMe".$id."' class='modal fade' role='dialog'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button class='btn btn-primary btn-confirm' type='submit' onclick='deleteData(".$id.", 5)' >Confirm</button>
                                                <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div></td>";
                        }
                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='4' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO CATEGORIES FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
