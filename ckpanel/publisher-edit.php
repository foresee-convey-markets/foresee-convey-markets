
<html>
<head>
<title>Edit Publisher  | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php 
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getPublisherDetails=$link->query("select * from mr_publisher where id='$id'");
$row=$getPublisherDetails->fetch_assoc();
?>
<?php
    if(isset($_POST["submit"])){ 
		$publisher_id = $_POST['id'];       
        $publisher=$_POST["publisher"];
        $publisher_code=$_POST["publisher_code"];
        $url=$_POST["url"];
		$url=generateReportURL($url);
        $status=$_POST["status"];        
        $archive_status=$_POST["archive_status"];
        
        $sql=mysqli_query($link,"update mr_publisher set publisher_name='".$publisher."',publisher_code='".$publisher_code."',publisher_url='".$url."',status='".$status."',archive_status='".$archive_status."' where id='$publisher_id' ");
        
		if($sql===TRUE ){
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Publisher Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'publisher-list "/>'; 
		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Publisher !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Edit Publisher</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="edit_publisher" id="edit_publisher" enctype="application/x-www-form-urlencoded">
	    	
			<input type="hidden" name="id" value="<?=@$id?>" />
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher<span class="star">*</span> :</label>
		    		<input  name="publisher" class="form-control" value="<?=@$publisher ? @$publisher : (@$row["publisher_name"] ? $row["publisher_name"] : ''); ?>" required="" />
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher Code<span class="star">*</span> :</label>
		    		<input  name="publisher_code" class="form-control" value="<?=@$publisher_code ? @$publisher_code :  ($row["publisher_code"] ? $row["publisher_code"] : ''); ?>" required="" />
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Publisher URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" value="<?=@$url ? @$url :  ($row["publisher_url"] ? $row["publisher_url"] : ''); ?>" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?= @$status && @$status == '1' ? 'checked' : ($row["status"]=='1' ? 'checked' : ''); ?> value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?= @$status && @$status == '0' ? 'checked' : ($row["status"]=='0' ? 'checked' : ''); ?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>


	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Archive Status <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" <?= @$archive_status && @$archive_status == '1' ? 'checked' : ($row["archive_status"]=='1' ? 'checked' : ''); ?> value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="archive_status" <?= @$archive_status && @$archive_status == '0' ? 'checked' : ($row["archive_status"]=='0' ? 'checked' : ''); ?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->