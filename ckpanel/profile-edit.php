
<!DOCTYPE html>
<html>
<head>
    <title>Edit Profile | Foresee Convey Markets</title>

    <!--*****************************  HEADER  ************************************** -->

    <?php require_once 'header.php'; ?>

    <!--*****************************  HEADER  ************************************** -->

    <?php 

if(isset($_GET["id"])){


//*********************** CATEGORIES ****************************//
    $id=base64_decode($_GET["id"]);
    
    $getUserDetails=$link->query("select * from mr_login where email='".$_SESSION["email"]."'  and  user_type ='".$_SESSION["user_type"]."' and id='".$id."'");	
	$user=$getUserDetails->fetch_assoc();    

    if(isset($_POST["submit"]))
    {
        $id=base64_decode(mysqli_real_escape_string($link,$_POST["id"]));
        
        $name=mysqli_real_escape_string($link,$_POST["name"]);
        $email=mysqli_real_escape_string($link,$_POST["email"]);
        $password=$user["password"];
        $orig_password=$user["orig_password"];
        if(!empty($_POST["password"])){
            $password=md5(mysqli_real_escape_string($link,$_POST["password"]));
            $orig_password=mysqli_real_escape_string($link,$_POST["password"]);
        }
        $user_type=$_SESSION["user_type"];
        $date=date("Y-m-d H:i:s");
        
        $sql=mysqli_query($link,"update mr_login set name='".$name."',email='".$email."',password='".$password."',orig_password='".$orig_password."',created_at='".$date."' where id='".$id."'  ");
    
        if($sql===TRUE ){
            $_SESSION["name"]=$_POST["name"];
            $_SESSION["email"]=$_POST["email"];
            if(!empty($_POST["password"])){
                session_destroy();
                $_SESSION["password"]=$_POST["password"];
            }		
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Profile Details Updated  successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'profile "/>';
        }
        else{                
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Profile !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
        }
    }
    ?>
    <h1 class="stats"><span class="fa fa-user"></span> Edit Profile</h1><br/>
    <div class="form-container">
        <?php 
            if(isset($success)){
                echo "<br/>".$success;
            }else{
                if(isset($error)){
                    echo "<br/>".$error;
                }
        ?>    
        <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."?id=". $_GET["id"]; ?>" method='post' name="upload" id="upload" >
            
            <input type="hidden" name="id" class="form-control" required="" value="<?php echo $_GET["id"];  ?>" />

            <div class="form-group">
                <div class="col-md-10">
                    <label class="control-label">Name<span class="star">*</span> :</label>
                    <input  name="name" placeholder="Your Name" value="<?php echo isset($name) ? $name : $user["name"] ?>" class="form-control" required="" />
                </div>
            </div>           
            <div class="form-group">
                <div class="col-md-10">
                    <label class="control-label">Email Address <span class="star">*</span> :</label>
                    <input class="form-control" name="email" id="email" value="<?php echo isset($email) ? $email : $user["email"]; ?>"   placeholder="Business Email" type="email" required/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10">
                    <label class="control-label">Password:</label>
                    <input type="password" autocomplete="off" value="" name="password" class="form-control"/>
                    <!-- <span class="fa fa-eye"></span> -->
                </div>
            </div>  

            <div class="form-group">
                <div class="col-md-10">
                    <label class="control-label">Role<span class="star">*</span> :</label>
                    <input type="text" readonly="" name="user_type" class="form-control" required=""  value="<?php echo @$USER_TYPES[$user["user_type"]];  ?>" />
                </div>
            </div>

            
            <div>
                <button type="submit" name="submit" class="btn btn-upload btn-update"><span class="far fa-edit"></span> Update</button>
                <button type="button"  class="btn btn-upload" onclick="window.history.back()"><span class="fa fa-times"></span> Cancel</button>
            </div>
        </form>
        <?php } ?>
    </div>

   
    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->

<?php
}else{
    header("location:dashboard");
}
?>