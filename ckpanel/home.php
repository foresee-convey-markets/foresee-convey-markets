
<!DOCTYPE html>
<html>
<head>
    <title>Home | Foresee Convey Markets</title>

    <!--*****************************  HEADER  ************************************** -->

    <?php require_once 'header.php'; ?>

    <!--*****************************  HEADER  ************************************** -->
    <style type="text/css">
        .bg-warning{
            background: #dfb924;
        }
    </style>
    <?php 

//*********************** CHECK FOR SESSION ****************************//
if(isset($_SESSION["email"])  && isset($_SESSION["name"])){


//*********************** PUBLISHERS ****************************//
    $getPublishers=$link->query("select publisher_id from mr_publisher where publisher_status='Y' ");
    $publishers=$getPublishers->num_rows;

//*********************** PRESS RELEASES ****************************//
    $getPressReleases=$link->query("select id from mr_press where status='Y' ");
    $press=$getPressReleases->num_rows;

//*********************** BLOGS ****************************//
    $getBlogs=$link->query("select id from mr_blogs where blog_status='Y' ");
    $blogs=$getBlogs->num_rows;

//*********************** CATEGORIES ****************************//
    $getCategories=$link->query("select sc1_id from mr_sub_cat_1 where sc1_status='Y' ");
    $categories=$getCategories->num_rows;

//*********************** REPORTS ****************************//
    $getReports=$link->query("select rep_id from mr_report where rep_status='Y' ");
    $reports=$getReports->num_rows;

//*********************** LEADS ****************************//
    $getLeads=$link->query("select contact_id from mr_form_contact");
    $leads=$getLeads->num_rows;


    //*********************** USERS ****************************//
    $getUsers=$link->query("select id from mr_login where user_type!='1'");
    $users=$getUsers->num_rows;

    //*********************** CHECKOUTS ****************************//
    $getCheckouts=$link->query("select id from mr_global_checkout");
    $checkouts=$getCheckouts->num_rows;


    //*********************** BANNED WORDS ****************************//
    $getBannedWords=$link->query("select id from mr_bannedwords");
    $bannedwords=$getBannedWords->num_rows;

    if($_SESSION["user_type"]=='1'){

    ?>

    <h1 class="stats"><span class="fa fa-database"></span> STATISTICS</h1><br/>

    <div class="row statistics">
        <div class="col-md-4">
            <h3>USERS</h3>
            <p><?php echo $users; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'login-approval'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PRESS RELEASES</h3>
            <p><?php echo $press; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'press-release-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>BLOGS</h3>
            <p><?php echo $blogs; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'blog-list'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics">
        <div class="col-md-4">
            <h3>CATEGORIES</h3>
            <p><?php echo $categories; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'category-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PUBLISHERS</h3>
            <p><?php echo $publishers; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'publisher-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>REPORTS</h3>
            <p><?php echo $reports; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'report-list'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics">
        <div class="col-md-4">
            <h3>LEADS</h3>
            <p><?php echo $leads; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'lead-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>CHECKOUTS</h3>
            <p><?php echo $checkouts; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'global-checkout-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>BANNEDWORDS</h3>
            <p><?php echo $bannedwords; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'bannedword-list'; ?>'">View All</button></p>
        </div>
    </div>

<?php } 
if(in_array(@$_SESSION["user_type"],array('2','3','4'))){
?>

    <h1 class="stats"><span class="fa fa-database"></span> STATISTICS</h1><br/>

    <div class="row statistics">
        <div class="col-md-4">
            <h3>CATEGORIES</h3>
            <p><?php echo $categories; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'category-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PUBLISHERS</h3>
            <p><?php echo $publishers; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'publisher-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>PRESS RELEASES</h3>
            <p><?php echo $press; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'press-release-list'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics">
        <div class="col-md-4">
            <h3>BLOGS</h3>
            <p><?php echo $blogs; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'blog-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>REPORTS</h3>
            <p><?php echo $reports; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'report-list'; ?>'">View All</button></p>
        </div>
    </div><br/>

<?php } ?>
   
    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->

<?php 
}
?>