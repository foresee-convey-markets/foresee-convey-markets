
function updateStatus(id, status, to_be_changed,table){
    if(id > 0){
        var alert_status = ''
        var alert_name = ''
        if(to_be_changed == '1'){
            alert_status = 'Status'
            alert_name = 'status'
        }
        if(to_be_changed == '2'){
            alert_status = 'Archive Status'
            alert_name = 'archive_status'
        } 
        if(to_be_changed > 0 && status >= 0){         
            var status_value = (status == '1') ? 0 : 1
            $.ajax({
                url : 'ajaxoperations.php',
                type : 'post',
                data : { id : id, status_value : status_value, table : table, to_be_changed : to_be_changed },
                async : true,
                success : function(data){
                    let response = data.trim();
                    if(response.length > 0 && response === 'Success'){
                        window.location.reload();
                    }else{          
                        alert('Status Update Request Failed');
                    }
                }
            })
        }else{          
            alert('Invalid '+ alert_status +' Type');
        }
    }
}
function deleteData(id, table){
    if(id > 0){
        $.ajax({
            url : 'ajaxoperations.php',
            type : 'post',
            data : { id : id,  table : table, 'type' : 'delete'},
            async : true,
            success : function(data){
                let response = data.trim();
                if(response.length > 0 && response === 'Success'){
                    window.location.reload();
                }else{          
                    alert('Delete Request Failed');
                }
            }
        })
    }
}