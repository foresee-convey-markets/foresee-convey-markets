<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>QandQ Market Research - Global Leader in Strategic Market Research Reports</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="QandQ Market Research,Industry Reports, Business Research Process, Market News,  Research Report, Business Research, Industrial Analysis, Market Research Analysis,  USA Marketing Research Process, China Research Report, Japan Business Market Research Report, Europe Industrial Analysis,Industry Analysis Report" />
<meta name="description" content="QandQ Market Research is a Global Leader In Strategic Research Reports  "/>
<meta name="author" content="QandQ market research"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
<?php //$this->output->enable_profiler(true); ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slideshow.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>
<style>
    body{
		background-color: #fff;
	}
	

</style>
<script type="text/javascript" src="assets/js/prefunctions.js"></script>
<script type="text/javascript" src="assets/js/sliker.js"></script>


<script>
$(document).ready(function(){
    $('.slider_4').sliker({
        'nbr_li': 1,
        'liquid': 0,
        'vitesse_auto': 3000,
        'vitesse': 1,
        'auto': 1,
        'loop': 0,
        'arrows': 2,
        'drag': 1
  
    });
});
</script>
<?php
		
$reportImageArray=array('1'=>'ICT Media.jpg','2'=>'Medical Devices.jpg','3'=>'Pharmaceutical Healthcare.png','4'=>'Semiconductor Electronics.png','5'=>'Materials Chemicals.jpg','6'=>'Consumer Goods And Services.jpg','7'=>'Agriculture.jpg','8'=>'Energy and Power.jpg','9'=>'Machinery and equipments.jpg','10'=>'Automotive and Transportation.jpg','11'=>'Aerospace and Defense.jpg','12'=>'Food and Beverage.jpg','13'=>'Construction and Manufacturing.jpg','14'=>'BFSI.jpg','15'=>'Company Reports.jpg','16'=>'other.png');

?>
<div class="image-banner-block">
    <br/><br/><h1 class="banner-title">Global Leader In <big>Strategic Research Reports</big></h1>
    <div class="container">
        <div class="row banner-block">
            <div class="col-md-3">
                <h2>1.5M+</h2>
                <p>Market <span class="gold">Research Reports</span></p>
            </div>
             <div class="col-md-3">
                <h2>500+</h2>
                <p><span class="gold">Industry Sectors</span> Analyzed</p>
            </div> 
            <div class="col-md-3">
                <h2>24/7</h2>
                <p>Support in <span class="gold">365 days</span></p>
            </div>
             <div class="col-md-3">
                <h2>200+</h2>
                <p>Satisfied Clients <span class="gold">Worldwide</span></p>
            </div>

        </div>
    </div>
</div>


<div class="container">
    <div class="content-heading">
        <h3>LATEST PUBLICATIONS</h3>
        <hr class="hr-line" />
    </div><br/>


    <div class="report-block-main">
        <?php foreach($report->result() as $row):
             $date = date_create($row->rep_entry_date); 
			 
			 foreach($reportImageArray as $key=>$value){
				if($key == $row->rep_sub_cat_1_id){
					$category_image=$value;
				}		
				
			}
			$report_url=str_ireplace('%2f','-', $row->rep_url);	

		
			$specials=array("Global and Chinese","Global","Southeast","European Union","South America","Oceania","North America","Europe","Asia","Antarctica","Africa","Zimbabwe","Zealand","Zambia","Yemen","Walli sutuna","Vietnam","Venezuela","Vatican","Vanuatu","Uzbekistan","USA","America","United States","Uruguay","Ukraine","Uganda","Tuvalu","Turks Caicos","Turkmenistan","Turkey","Tunisia","Trinidad","Tobago","Tonga","Tokelau","Togo","Timor-Leste","Thailand","Tanzania","Tajikistan","Taiwan","Syrian","Syria","Switzerland","Sweden","Swaziland","Svalbard","Mayen","Suriname","Sri Lanka","Spain","Somalia","Solomon","Slovenia","Slovakia","Singapore","Sierra Leone","Seychelles","Serbia and Montenegro","Senegal","Saudi Arabia","Samoa","Samoa","Salvador"," Saint Vincent","Grenadines","Saint Kitts Nevis","Saint Helena","Sahara","Rwanda","Romania","Reunion","Qatar","Puerto Rico","Portugal","Poland","Pitcairn","Pierre Miquelon","Philippines","Peru","Paraguay","Papua","Panama","Palestinian","Palau","Pakistan","Oman","Norway","Norfolk","Niue","Nigeria","Niger","Nicaragua","Netherlands Antilles","Netherlands","Nepal","Nauru","Namibia","Myanmar","Mozambique","Morocco","Montserrat","Mongolia","Monaco","Moldova","Micronesia","Mexico","Mayotte","Mauritius","Mauritania","Martinique","Marshall","Marino","Mariana","Malta","Mali","Maldives","Malaysia","Malawi","Madagascar","Macedonia","Macao","Luxembourg","Lucia","Lithuania","125Liechtenstein","Libyan Jamahiriya","Liberia","Lesotho","Lebanon","Latvia","Latin","Lao","Kyrgyzstan","Kuwait","Korea","Korea","Kiribati","Kingdom","London","Kenya","Kazakhstan","Jordan","Japan","Jamaica","Italy","Israel","Ireland","Iraq","Iran","Indonesia","India","Iceland","Hungary","Hong Kong","Honduras","Haiti","Guyana","Guinea-Bissau","Guinea","Guiana","Guatemala","Guam","Guadeloupe","Grenada","Greenland","Greece","Gibraltar","Ghana","Germany","Georgia","Georgia","Gambia","Gabon","France","Finland","Fiji","Faroe","Falkland","Malvinas","Ethiopia","Estonia","Eritrea","Egypt","Ecuador","Dominican","Dominica","Djibouti","Denmark","Czech","Cyprus","Cuba","Croatia","CoteDIvoire","Costa Rica","ongo","Congo","Comoros","Colombia","Cocos","China","Chile","Chad","Cayman","CapeVerde","Canada","Cameroon","Cambodia","Caledonia","Burundi","Burkina Faso","Bulgaria","Brunei Darussalam","Brazil","Bouvet","Botswana","Bosnia","Herzegovina","Bolivia","Bhutan","Bermuda","Benin","Belize","Belgium","Belarus","Barbados","Bangladesh","Bahrain","Bahamas","Azerbaijan","Austria","Australia","Aruba","Armenia","Argentina","ArabEmirates","Antigua","Barbuda","Antarctica","Anguilla","Angola","Andorra","Algeria","Albania","Africa","Afghanistan","Polynesia","United States",",","  ");
			
			//*************************************        REPORT TITLE        *************************************************
					
					
			$report_title=$row->rep_title;
			if($row->publisher_id == 11){
				$title=$row->rep_title;
			}else{
				//$report_year=explode('20', $report_title);
				if(stristr($report_title,"forecast")){
					$report_title_split=explode('Forecast', $report_title);
				}else{
					$report_title_split=explode('Market', $report_title);
				}
				$report_year=preg_replace('/[^0-9]/','', $report_title_split[1]);
				if($report_year == NULL){
					$report_title_split=explode('Market', $report_title);
					$report_year=preg_replace('/[^0-9]/','', $report_title_split[1]);
					if($report_year==NULL){
						$report_year=preg_replace('/[^0-9]/','', $report_title);
					}
				}
				if(strlen($report_year) < 9){
					$sub_string=strlen($report_year)+1;
				}else{
					$sub_string=9;
				}	
				
				if(strlen($report_year) > 4){
					$data['report_year'] =substr(chunk_split($report_year,4,'-'),0,$sub_string);
				}else if($report_year==='2019'){
					$data["report_year"]='2019-2025';
				}else{
					$data['report_year'] =$report_year;
				}
				
				// $specials = array("Global","Africa","South & Southeast Asia","Asia-Pacific","North Africa","Europe","Sub-Saharan Africa","North & Cental America","Antarctica","Caribbean Islands","Asia","Mesoamerica","East Asia","North America","North Asia","Oceania","West & Central Asia","South America","Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe","Latin America",'Middle East','and','USA','&amp;','global','2017','2022','(',')','-',',','Industry','Research','Report','Market','Company','Profile','Deals','Ltd','Alliances',': ','Country','Sector',' in ','Prospects','Survey','Chinese','Regional','Regional ','Production', 'Sales' ,'Consumption','Status','Prospects','Professional','Outlook','By', 'Players', 'Regions', 'Product' ,'Types','Applications','Analysis',' to ');


				// $report_title=str_ireplace($specials,'',$report_title);
				$year_string=['2017','2018','2019','2020','2021','2022','2023','2024','2025','2026'];
				$report_title=str_ireplace('-',' ',$report_title);
				$report_title=str_ireplace($year_string,'',$report_title);
				$report_title=str_ireplace('(United States, European Union and China)','',$report_title);
				
				if($row->publisher_id != 10 &&  $row->publisher_id != 3){
					$report_title=str_ireplace($specials,' ',$report_title);
				}

				if(stristr($report_title,'market')){
					//echo "<script>alert('Market')</script>"; 
					//$report_title=str_ireplace('industry','',$report_title);
					$meta_title=explode('Market',$report_title);
				}
				else if(stristr($report_title,'industry')){
					//echo "<script>alert('Industry')</script>";
					$meta_title=explode('Industry',$report_title);
				}
						
				$title=$meta_title[0].' Market by Type, Top Key Players, Size, Share, Growth, Trends - Opportunity, Analysis, Forecast '.$data['report_year'];			
			}
			 ?>

            <div class="report-sub-content" onclick="window.location.href = '<?php echo base_url(); ?>reports/<?php echo $row->rep_id; ?>/<?php echo $report_url."-market"; ?>'">
                <div class="col-md-12" align="center">
                    <img  class="img-responsive report-abs-image lazy" src='<?php echo base_url(); ?>assets/images/report/<?php echo $category_image; ?>' alt="<?php echo $title ?>"  />
					<div class="company-name-block">
                        <span>QandQ Market Research</span><br/><br/>
                    </div>
				</div>
                <div class="col-md-12">
                    <h4><a class="report-title" href="reports/<?php echo $row->rep_id; ?>/<?php echo $report_url."-market"; ?>"><?php echo (strlen($title)>55) ? substr($title, 0, 58) . ".." : $title; ?></a>
                    </h4>
                </div>
                <div class="col-md-12">
                    <p class="date-format"><i class="fa fa-calendar text-danger"></i> <?php echo date_format($date, 'M Y'); ?>
                        <button onclick="window.location.href = '<?php echo base_url(); ?>reports/<?php echo $row->rep_id; ?>/<?php echo $report_url."-market"; ?>'" class="btn btn-primary btn-price" title="Click here to view report"><i class="fa fa-eye"></i> View Report</button>
                    </p>
                </div>
            </div>

        <?php endforeach ?>
    </div><br/>
     
</div>

<div class="container">
	<div class="content-heading">
        <h3>CATEGORIES</h3>
        <hr style="border-bottom: 1px solid #0465ac;width: 25%" />
    </div><br/><br/>

    <div class="jumbotron">
        <div class="slider_4 sliker">
            <div class="sliker__window">
                <ul class="sliker__track">
                    <?php foreach($categories->result() as $row): ?>

                    <li class="sliker__item" align="center">
                        <img src='<?php echo base_url(); ?>assets/images/category/<?php echo $row->sc1_image; ?>' alt='<?php echo $row->sc1_name; ?>' />
                        
                        <h5><a href='<?php echo base_url(); ?>category/<?php echo $row->sc1_url; ?>'><?php echo $row->sc1_name; ?></a></h5>
                    </li>

                <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div><br/>
</div>


<div class="content-heading">
    <h3>OUR SUPPORT TEAM</h3>
    <hr class="hr-line"/>
</div><br/><br/>
<div class="jumbotron">
    <div class="container">
        <div class="block-container">
            <div class='row home-row-container' >
                <div class="col-sm-3" align="center">
                    <img class="img img-circle img-thumbnail" src="assets/images/avatar-female.png" alt="Female Avatar"/> 
                    <p><b>Prashanti R</b></p>
                    <h5>Global Key Account Manager<br/>at QandQ Market Research </h5>
                </div>
                <div class="col-sm-3" align="center">
                    <img class="img img-circle img-thumbnail" src="assets/images/avatar-male.png" alt="Male Avatar"/> 
                    <p><b>Anil</b></p>
                    <h5>Global Key Account Manager<br/>at QandQ Market Research </h5>
                </div>
                <div class="col-sm-3" align="center">
                    <img class="img img-circle img-thumbnail" src="assets/images/avatar-male-2.png" alt="Male Avatar"/> 
                    <p><b>Subhash</b></p>
                    <h5>Global Key Account Manager<br/>at QandQ Market Research </h5>
                </div>
                <div class="col-sm-3" align="center">
                    <img class="img img-circle img-thumbnail" src="assets/images/avatar-male-3.png" alt="Male Avatar"/> 
                    <p><b>Sai Ram</b></p>
                    <h5>Global Key Account Manager<br/>at QandQ Market Research </h5>
                </div>
            </div><hr style="border-bottom: 1px solid #ccc"  />
            <div class='row home-row-container-2'>
                <div class="col-sm-6 col-sm-offset-3" align="center"><br/>
                    <h3>24/7 PHONE AND EMAIL SUPPORT</h3><br/>
                    <p>We're here for you 24 hours a day, 365 days a year</p>
                    <br/>
                    <span onclick="window.location.href='<?php echo base_url(); ?>contact-us'">CONTACT US</span><br/>
                </div> 
            </div><br/>
        </div>
    </div>
</div>

<hr style="width:100px"/>
<div class="container">
    <div class="content-heading">
        <h3>MEDIA RELEASES</h3>
        <hr class="hr-line" />
    </div><br/><br/>
    <div class="row home-row-container-2">
        <?php foreach($press_list->result() as $row):
             $date = date_create($row->p_date) ?>
        <div class="col-md-4 press-list wow fadeInRight delay-2s " >
            <div align='center'>
                <img class="img-responsive lazy"  style="height:100px;width:auto;" src="<?php echo base_url(); ?>assets/images/press/<?php echo $row->p_img ?>" alt="<?php echo $row->p_title; ?>"  />
            </div>
            <h4><a class="press-title" href="<?php echo base_url(); ?>press/<?php echo $row->p_url; ?>"><?php echo substr($row->p_title, 0, 55) . ".."; ?></a></h4>
            <p class="date-format"><i class="fa fa-calendar text-danger"></i> <?php echo date_format($date, 'M Y'); ?></p>
        </div>
        <?php endforeach ?>
    </div>
</div>

<br/><br/>



<div style='background:#f9f9f9'>
    <div class="container">
        <div class="row">
            <div class='col-md-6 custom-research'>
                <h4><b>DISCOVER OUR CUSTOM RESEARCH SERVICE</b></h4>
                <p style='font-size:1.2em;' class="sm-hidden">Tailored to your specific research requirements</p>
            </div>
            <div class='col-md-6 custom-button' style="padding:15px">
                <div>
                    <p class='home-custom-research' onclick="window.location.href='<?php echo base_url(); ?>custom-research'"><strong>CUSTOM RESEARCH SERVICE</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="company-block">
    <div class='container'><br/>
        <div class="row info-block">
            <div class="col-md-3">
                <h2 class='fa fa-globe icon-home'></h2>
                <h4>WORLD'S LARGEST SELECTION</h4>
                <p>Access to over <strong>1.5M+ Strategic</strong> <br/>  <strong class='blue'>Research Reports</strong></p>
            </div>
            <div class="col-md-3">
                <h2 class='fa fa-handshake-o icon-home'></h2>
                <h4>TRUSTED BY THE BEST</h4>
                <p><strong class='blue'>200+ Satisfied Clients </strong> worldwide.</p>
            </div>
            <div class="col-md-3">
                <h2 class='fa fa-users icon-home'></h2>
                <h4>CUSTOMER FOCUSSED</h4>
                <p>We are here for you <strong class='blue'>24 hours</strong> a day<br/>and <strong class='blue'>365 days</strong> a year</p>
            </div>
            <div class="col-md-3">
                <h2 class='fa fa-lock icon-home'></h2>
                <h4>SAFE AND SECURE</h4>
                <p>Your payment details <strong class='blue'>are <br/>protected</strong> by us.  </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/js/postfunctions.js"></script>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->





