
<html>
<head>
<title>Edit Banned Word  | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getBannedwordDetails=$link->query("select * from mr_bannedwords where id=".@$id);
$row=$getBannedwordDetails->fetch_assoc();
// echo "<pre>";print_r($row);die;
?>
<?php
    if(isset($_POST["submit"])){
		$word_id = $_POST['id'];        
        $word=$_POST["word"];
        $status=$_POST["status"];
		$modified = date('Y-m-d H:i:s');

        $sql=mysqli_query($link,"update mr_bannedwords set word='".$word."',status='".$status."',modified='".@$modified."' where id='$word_id' ");
        
		if($sql===TRUE ){
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Bannedword Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'bannedword-list "/>'; 
		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Bannedword !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		} 
    }
    
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Edit Banned Word</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="word_add" id="word_add" enctype="application/x-www-form-urlencoded">
			
			<input type="hidden" name="id" value="<?=@$id?>" />
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">BannedWord<span class="star">*</span> :</label>
		    		<input name="word" class="form-control"  value="<?=@$word ? @$word : @$row['word']?>" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?php echo ($row["status"]=='1') ? 'checked' : ''; ?> value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?php echo ($row["status"]=='0') ? 'checked' : ''; ?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
