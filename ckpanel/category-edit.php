
<html>
<head>
<title>Edit Category  | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php
$id=isset($_GET["id"]) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : '');
$getCategoryDetails=$link->query("select * from mr_sub_cat_1 where id='$id'");
$row=$getCategoryDetails->fetch_assoc();
?>
<?php
    if(isset($_POST["submit"])){
		$sc1_id  = $_POST['id'];        
        $category=$_POST["category"];
        $url=$_POST["url"];
		$url=generateReportURL($url);
        $status=$_POST["status"];
        if(!empty($_FILES["image"]["name"])){
	        $image=$_FILES["image"]["name"];
	        $tmp_name=$_FILES["image"]["tmp_name"];
	        $dir="../assets/images/category/";
        	unlink($dir.$row["sc1_image"]);
	        move_uploaded_file($tmp_name,$dir.$image);  
        }else{
        	$image=mysqli_real_escape_string($link,$_POST["image_edit"]);
        }

        $sql=mysqli_query($link,"update mr_sub_cat_1 set sc1_name='".$category."',sc1_url='".$url."',status='".$status."',sc1_image='".$image."' where id='$sc1_id' ");
        
		if($sql===TRUE ){
			$success='<div class="alert alert-success alert-form"><span class="fa fa-check-circle"></span>&nbsp;Category Updated successfully !&nbsp;&nbsp; <mark>Redirecting......</mark></div> 
			<meta http-equiv="refresh" content="2,url='.BASE_URL.'category-list "/>'; 
		}else{
			$error='<div class="alert alert-danger alert-form"><span class="fa fa-times-circle"></span>&nbsp;Error Updating Category !'.(ENVIRONMENT == 'development' ? mysqli_error($link) : '').'</div>';
		}  
    }
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Edit Category</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
	<?php 
		if(isset($success)){
			echo "<br/>".$success;
		}else{
			if(isset($error)){
				echo "<br/>".$error;
			}
	?>    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="edit_category" id="edit_category" enctype='multipart/form-data'>
	    	
			<input type="hidden" name="id" value="<?=@$id?>" />
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Category<span class="star">*</span> :</label>
		    		<input  name="category" class="form-control" value="<?=@$category ? @$category :  (@$row["sc1_name"] ? $row["sc1_name"] : ''); ?>" required="" />
		    	</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Category URL<span class="star">*</span> :</label>
		    		<input  name="url" class="form-control" value="<?=@$url ? @$url :  (@$row["sc1_url"] ? $row["sc1_url"] : ''); ?>" required="" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
	    			<?php if(!empty($row["sc1_image"])){?>
		    			<img src="../assets/images/category/<?=$row["sc1_image"];?>" height='100px' width='100px' alt='<?=$row["sc1_image"];?>'/><br/>
		    		<?php } ?>
		    		<label class="control-label">Category Image :</label>
		    		<input type="file"  name="image" class="form-control"  />
		    		<input type="hidden"  name="image_edit" value="<?php echo (isset($row["sc1_image"])) ? $row["sc1_image"] : ''; ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?= @$status && @$status == '1' ? 'checked' : ($row["status"]=='1' ? 'checked' : ''); ?> value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?= @$status && @$status == '0' ? 'checked' : ($row["status"]=='0' ? 'checked' : ''); ?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fas fa-times"></span> Cancel</button>
		    </div>
	    </form>
		<?php } ?>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
