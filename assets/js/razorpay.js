        

var SITEURL = "<?= base_url() ?>";
$('body').on('click', '.btn-checkout', function(e){
  if(validate_captcha()){
  if(!$("button.btn-checkout").hasClass("disabled")){
      var radio = $("input[name='radio-inline']:checked"). val();
      if(radio !== 'Paypal'){
          // alert(radio);
          var name=$("#name").val();
          var email=$("#email").val();
          var company=$("#company").val();
          var country=$("#country").val();
          var zip=$("#zip").val();
          var address=$("#address").val();

          var title=$("#rep_title").val();
          var price=parseInt($("#rep_price").val());
          var license=$("#rep_license").val();
          var currency=$("#currency").val();

          // alert('working'+price);
          var options = {
          "key": "rzp_test_JUDBARtU4m5S4f",
          "currency": 'USD',
          "amount": (price*100), // 2000 paise = INR 20
          "name": "Foresee Convey Markets",
          "description": title,
          "image": "<?php echo base_url(); ?>assets/images/prospect-research-reports-logo.jpg",
          "handler": function (response){
                 $.ajax({
                  url: '<?php echo base_url(); ?>payment/eazypay-success',
                   type: 'post',
                   data: {name:name,email:email,company:company,country:country,zip:zip,address:address,title:title,price:price,license:license,response:response,
                   }, 
                   success: function () {
                      //alert("PAID");
                      window.location.href = '<?php echo base_url(); ?>payment/eazypay-thanks';
                   }
               });
            
           },
       
           "theme": {
               "color": "#528FF0"
           }
         };
         var rzp1 = new Razorpay(options); 
                           
          rzp1.on('payment.failed', function (response){
                  $.ajax({
                  url: '<?php echo base_url(); ?>payment/eazypay-cancel',
                   type: 'post',
                   data: {name:name,email:email,company:company,country:country,zip:zip,address:address,title:title,price:price,license:license,response:response,
                   }, 
                   success: function () {
                      //alert("PAID");
                      window.location.href = '<?php echo base_url(); ?>eazypay/payment-failed';
                   }
               });
          });

         rzp1.open();
         e.preventDefault();
     }
  }
      
  }
});   