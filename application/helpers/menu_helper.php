<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * CodeIgniter
	 *
	 * An open source application development framework for PHP 4.3.2 or newer
	 *
	 * @package		CodeIgniter
	 * @author		ExpressionEngine Dev Team
	 * @copyright	Copyright (c) 2008 - 2009, EllisLab, Inc.
	 * @license		http://codeigniter.com/user_guide/license.html
	 * @link		http://codeigniter.com
	 * @since		Version 1.0
	 * @filesource
	 */
	
	// ------------------------------------------------------------------------
	
	/**
	 * CodeIgniter Functions Helpers
	 *
	 * @package		CodeIgniter
	 * @subpackage	Helpers
	 * @category	Helpers
	 * @author		ExpressionEngine Dev Team
	 * @link		http://codeigniter.com/user_guide/helpers/array_helper.html
	 */
	
	// ------------------------------------------------------------------------
	
	/**
	 * Element 
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns FALSE (or whatever you specify as the default value.)
	 *
	 * @access	public
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	
	/*
	*$name : Any name you want to pass
	*$form_action : This is the url you want to pass
	*$a_id : id for anchot tag
	*$a_class : clss for anchot tag
	*$a_title : title for anchot tag
	*$a_style : style for anchot tag
	*$a_href : href for anchot tag
	*$a_no_href : if you want to put like "javascript:void(0);" then pass 1
	*$onclick : onclick for anchot tag
	*/
	
	
	/*
	* SEND SMTP EMAIL via CI email Library...
	*/
	
	function getDetailsFromIP(){

        $ip = $_SERVER['REMOTE_ADDR'];

        $url = "http://api.ipinfodb.com/v3/ip-city/?key=632be3af0e8a0aec7bcfe4cbca591a9bc9aa140a92cad8df575dab4e9587b0e0&ip=$ip&format=json";

        $d = file_get_contents($url);

        $data = json_decode($d, true);

        // $info = array(
        //     'ip' => $data['ipAddress'],
        //     'country_code' => $data['countryCode'],
        //     'country_name' => $data['countryName'],
        //     'region_name' => $data['regionName'],
        //     'city' => $data['cityName'],
        //     'zip_code' => $data['zipCode'],
        //     'latitude' => $data['latitude'],
        //     'longitude' => $data['longitude'],
        //     'time_zone' => $data['timeZone'],
        // );

        return $data;
	}
	function getFormTypes() {
		$FORM_TYPES = array(
			'1' => 'Contact Form', 
			'2' => 'Custom Research',
			'3' => 'Search Form', 
			'4' => 'Request Sample', 
			'5' => 'Request Customization', 
			'6' => 'Enquire Before Purchase', 
			'7' => 'Request Discount', 
			'8' => 'Checkout Form',	 
			'9' => 'Report Details',	
		);
		return $FORM_TYPES;
	}
	
	function getBannedwords(){
		$CI =& get_instance();
		$CI->load->database();
		$CI->db->select('id,word');
        $CI->db->from('mr_bannedwords');
        $CI->db->where(array('status' => '1'));
        $CI->db->order_by("word","asc");
		$banned_words = $CI->db->get();
		$BANNEDWORDS= array();
		foreach($banned_words->result() as $row){
			$BANNEDWORDS[$row->id]=$row->word;
		}
		return $BANNEDWORDS;
	}
	function get_categories(){
		$CI =& get_instance();
		$CI->load->database();
		$CI->db->select('sc1_url,sc1_name,mr_sub_cat_1.id as sc1_id,sc1_image');
        $CI->db->from('mr_sub_cat_1');
        $CI->db->where(array('status' => 1));
        $CI->db->order_by("sc1_name","asc");
        return $CI->db->get();
	}
	function categories(){
		$categories=get_categories();
		$category_list=array();
		foreach($categories->result() as $row){
			$category_list[$row->sc1_id]=$row->sc1_name;
		}
		return $category_list;
	}
	function category_details(){
		$categories=get_categories();
		$category_list=array();
		foreach($categories->result() as $row){
			$category_list[$row->sc1_id]['sc1_name']=$row->sc1_name;
			$category_list[$row->sc1_id]['sc1_url']=$row->sc1_url;
		}
		return $category_list;
	}	
	function get_publishers($publisher_id){
		if($publisher_id){
			$CI =& get_instance();
			$CI->load->database();
			$CI->db->select('publisher_code,publisher_name');
			$CI->db->from('mr_publisher');
			$CI->db->where(array("id"=>$publisher_id, 'status' => 1, 'archive_status' => 0));
			// $CI->db->order_by("publisher_name","ASC");
			return $CI->db->get()->row();
		}else{
			return '';
		}
	}
	function encrypt_decrypt($type,$text){

		$auth_iv = '1234567891011123';  // encryption/decryption iv (Initialization Vector) 
		$auth_key = "prospectresearchreports";    // encryption/decryption key 
		$ciphering = "AES-256-CBC";    // Store the cipher method 
		$iv_length = openssl_cipher_iv_length($ciphering);
		$options = 0;    // Store the cipher method 
		$key = hash('sha256', $auth_key);
		$iv = substr(hash('sha256', $auth_iv), 0, 16);	
	
		$string='';
		if(@$type === 'encrypt'){
			$string=openssl_encrypt($text, $ciphering, 
				$key, $options, $iv); 
			$string = base64_encode($string);
	
		}elseif (@$type === 'decrypt') {
			$string=openssl_decrypt(base64_decode($text), $ciphering, 
				$key, $options, $iv); 
		}
		return $string;
	}

	function getCategoryIcon(){
		$ICONS = array(
			'1' => 'ICT.jpg',
			'2' => 'Medical Devices.jpg',
			'3' => 'Pharmaceutical and Healthcare.jpg',
			'4' => 'Semiconductor and Electronics.jpg',
			'5' => 'Materials and Chemicals.jpg',
			'6' => 'Consumer goods and services.jpg',
			'7' => 'Agriculture.jpg',
			'8' => 'Energy and Power.jpg',
			'9' => 'Machinery and Equipments.jpg',
			'10' => 'Automotive and Transportation.jpg',
			'11' => 'Aerospace and Defense.jpg',
			'12' => 'Food and Beverages.jpg',
			'13' => 'Construction and Manufacturing.jpg',
			'14' => 'BFSI.jpg',
			'15' => 'Company Reports.jpg',
			'16' => 'Other.jpg'
		);
		return $ICONS;
	}

	function getCountryDropdown(){
		$html='';
		$html.='<option value="">Select Country</option>
	            <option value="+(93)-Afghanistan">Afghanistan (+93)</option>
	            <option value="+(355)-Albania">Albania (+355)</option>
	            <option value="+(213)-Algeria">Algeria (+213)</option>
	            <option value="+(1684)-American Samoa">American Samoa (+1684)</option>
	            <option value="+(376)-Andorra">Andorra (+376)</option>
	            <option value="+(244)-Angola">Angola (+244)</option>
	            <option value="+(1264)-Anguilla">Anguilla (+1264)</option>
	            <option value="+(0)-Antarctica">Antarctica (+0)</option>
	            <option value="+(1268)-Antigua and Barbuda">Antigua and Barbuda (+1268)</option>
	            <option value="+(54)-Argentina">Argentina (+54)</option>
	            <option value="+(374)-Armenia">Armenia (+374)</option>
	            <option value="+(297)-Aruba">Aruba (+297)</option>
	            <option value="+(61)-Australia">Australia (+61)</option>
	            <option value="+(43)-Austria">Austria (+43)</option>
	            <option value="+(994)-Azerbaijan">Azerbaijan (+994)</option>
	            <option value="+(1242)-Bahamas">Bahamas (+1242)</option>
	            <option value="+(973)-Bahrain">Bahrain (+973)</option>
	            <option value="+(880)-Bangladesh">Bangladesh (+880)</option>
	            <option value="+(1246)-Barbados">Barbados (+1246)</option>
	            <option value="+(375)-Belarus">Belarus (+375)</option>
	            <option value="+(32)-Belgium">Belgium (+32)</option>
	            <option value="+(501)-Belize">Belize (+501)</option>
	            <option value="+(229)-Benin">Benin (+229)</option>
	            <option value="+(1441)-Bermuda">Bermuda (+1441)</option>
	            <option value="+(975)-Bhutan">Bhutan (+975)</option>
	            <option value="+(591)-Bolivia">Bolivia (+591)</option>
	            <option value="+(387)-Bosnia and Herzegovina">Bosnia and Herzegovina (+387)</option>
	            <option value="+(267)-Botswana">Botswana (+267)</option>
	            <option value="+(0)-Bouvet Island">Bouvet Island (+0)</option>
	            <option value="+(55)-Brazil">Brazil (+55)</option>
	            <option value="+(246)-British Indian Ocean Territory">British Indian Ocean Territory (+246)</option>
	            <option value="+(673)-Brunei Darussalam">Brunei Darussalam (+673)</option>
	            <option value="+(359)-Bulgaria">Bulgaria (+359)</option>
	            <option value="+(226)-Burkina Faso">Burkina Faso (+226)</option>
	            <option value="+(257)-Burundi">Burundi (+257)</option>
	            <option value="+(855)-Cambodia">Cambodia (+855)</option>
	            <option value="+(237)-Cameroon">Cameroon (+237)</option>
	            <option value="+(1)-Canada">Canada (+1)</option>
	            <option value="+(238)-Cape Verde">Cape Verde (+238)</option>
	            <option value="+(1345)-Cayman Islands">Cayman Islands (+1345)</option>
	            <option value="+(236)-Central African Republic">Central African Republic (+236)</option>
	            <option value="+(235)-Chad">Chad (+235)</option>
	            <option value="+(56)-Chile">Chile (+56)</option>
	            <option value="+(86)-China">China (+86)</option>
	            <option value="+(61)-Christmas Island">Christmas Island (+61)</option>
	            <option value="+(672)-Cocos (Keeling) Islands">Cocos (Keeling) Islands (+672)</option>
	            <option value="+(57)-Colombia">Colombia (+57)</option>
	            <option value="+(269)-Comoros">Comoros (+269)</option>
	            <option value="+(242)-Congo">Congo (+242)</option>
	            <option value="+(242)-Congo, the Democratic Republic of the">Congo, the Democratic Republic of the (+242)</option>
	            <option value="+(682)-Cook Islands">Cook Islands (+682)</option>
	            <option value="+(506)-Costa Rica">Costa Rica (+506)</option>';

            $html.=addslashes("<option value='+(225)-Cote D'Ivoire'>Cote D'Ivoire (+225)</option>");
            $html.='<option value="+(385)-Croatia">Croatia (+385)</option>
	            <option value="+(53)-Cuba">Cuba (+53)</option>
	            <option value="+(357)-Cyprus">Cyprus (+357)</option>
	            <option value="+(420)-Czech Republic">Czech Republic (+420)</option>
	            <option value="+(45)-Denmark">Denmark (+45)</option>
	            <option value="+(253)-Djibouti">Djibouti (+253)</option>
	            <option value="+(1767)-Dominica">Dominica (+1767)</option>
	            <option value="+(1809)-Dominican Republic">Dominican Republic (+1809)</option>
	            <option value="+(593)-Ecuador">Ecuador (+593)</option>
	            <option value="+(20)-Egypt">Egypt (+20)</option>
	            <option value="+(503)-El Salvador">El Salvador (+503)</option>
	            <option value="+(240)-Equatorial Guinea">Equatorial Guinea (+240)</option>
	            <option value="+(291)-Eritrea">Eritrea (+291)</option>
	            <option value="+(372)-Estonia">Estonia (+372)</option>
	            <option value="+(251)-Ethiopia">Ethiopia (+251)</option>
	            <option value="+(500)-Falkland Islands (Malvinas)">Falkland Islands (Malvinas) (+500)</option>
	            <option value="+(298)-Faroe Islands">Faroe Islands (+298)</option>
	            <option value="+(679)-Fiji">Fiji (+679)</option>
	            <option value="+(358)-Finland">Finland (+358)</option>
	            <option value="+(33)-France">France (+33)</option>
	            <option value="+(594)-French Guiana">French Guiana (+594)</option>
	            <option value="+(689)-French Polynesia">French Polynesia (+689)</option>
	            <option value="+(0)-French Southern Territories">French Southern Territories (+0)</option>
	            <option value="+(241)-Gabon">Gabon (+241)</option>
	            <option value="+(220)-Gambia">Gambia (+220)</option>
	            <option value="+(995)-Georgia">Georgia (+995)</option>
	            <option value="+(49)-Germany">Germany (+49)</option>
	            <option value="+(233)-Ghana">Ghana (+233)</option>
	            <option value="+(350)-Gibraltar">Gibraltar (+350)</option>
	            <option value="+(30)-Greece">Greece (+30)</option>
	            <option value="+(299)-Greenland">Greenland (+299)</option>
	            <option value="+(1473)-Grenada">Grenada (+1473)</option>
	            <option value="+(590)-Guadeloupe">Guadeloupe (+590)</option>
	            <option value="+(1671)-Guam">Guam (+1671)</option>
	            <option value="+(502)-Guatemala">Guatemala (+502)</option>
	            <option value="+(224)-Guinea">Guinea (+224)</option>
	            <option value="+(245)-Guinea-Bissau">Guinea-Bissau (+245)</option>
	            <option value="+(592)-Guyana">Guyana (+592)</option>
	            <option value="+(509)-Haiti">Haiti (+509)</option>
	            <option value="+(0)-Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands (+0)</option>
	            <option value="+(39)-Holy See (Vatican City State)">Holy See (Vatican City State) (+39)</option>
	            <option value="+(504)-Honduras">Honduras (+504)</option>
	            <option value="+(852)-Hong Kong">Hong Kong (+852)</option>
	            <option value="+(36)-Hungary">Hungary (+36)</option>
	            <option value="+(354)-Iceland">Iceland (+354)</option>
	            <option value="+(91)-India">India (+91)</option>
	            <option value="+(62)-Indonesia">Indonesia (+62)</option>
	            <option value="+(98)-Iran, Islamic Republic of">Iran, Islamic Republic of (+98)</option>
	            <option value="+(964)-Iraq">Iraq (+964)</option>
	            <option value="+(353)-Ireland">Ireland (+353)</option>
	            <option value="+(972)-Israel">Israel (+972)</option>
	            <option value="+(39)-Italy">Italy (+39)</option>
	            <option value="+(1876)-Jamaica">Jamaica (+1876)</option>
	            <option value="+(81)-Japan">Japan (+81)</option>
	            <option value="+(962)-Jordan">Jordan (+962)</option>
	            <option value="+(7)-Kazakhstan">Kazakhstan (+7)</option>
	            <option value="+(254)-Kenya">Kenya (+254)</option>
	            <option value="+(686)-Kiribati">Kiribati (+686)</option>';
            $html.="<option value='+(850)-Korea, Democratic People's Republic of'>Korea, Democratic People's Republic of (+850)</option>";
            $html.='<option value="+(82)-Korea, Republic of">Korea, Republic of (+82)</option>
	            <option value="+(965)-Kuwait">Kuwait (+965)</option>
	            <option value="+(996)-Kyrgyzstan">Kyrgyzstan (+996)</option>';
	            $html.="<option value='+(856)-Lao People's Democratic Republic'>Lao People's Democratic Republic (+856)</option>";
            $html.='<option value="+(371)-Latvia">Latvia (+371)</option>
	            <option value="+(961)-Lebanon">Lebanon (+961)</option>
	            <option value="+(266)-Lesotho">Lesotho (+266)</option>
	            <option value="+(231)-Liberia">Liberia (+231)</option>
	            <option value="+(218)-Libyan Arab Jamahiriya">Libyan Arab Jamahiriya (+218)</option>
	            <option value="+(423)-Liechtenstein">Liechtenstein (+423)</option>
	            <option value="+(370)-Lithuania">Lithuania (+370)</option>
	            <option value="+(352)-Luxembourg">Luxembourg (+352)</option>
	            <option value="+(853)-Macao">Macao (+853)</option>
	            <option value="+(389)-Macedonia, the Former Yugoslav Republic of">Macedonia, the Former Yugoslav Republic of (+389)</option>
	            <option value="+(261)-Madagascar">Madagascar (+261)</option>
	            <option value="+(265)-Malawi">Malawi (+265)</option>
	            <option value="+(60)-Malaysia">Malaysia (+60)</option>
	            <option value="+(960)-Maldives">Maldives (+960)</option>
	            <option value="+(223)-Mali">Mali (+223)</option>
	            <option value="+(356)-Malta">Malta (+356)</option>
	            <option value="+(692)-Marshall Islands">Marshall Islands (+692)</option>
	            <option value="+(596)-Martinique">Martinique (+596)</option>
	            <option value="+(222)-Mauritania">Mauritania (+222)</option>
	            <option value="+(230)-Mauritius">Mauritius (+230)</option>
	            <option value="+(269)-Mayotte">Mayotte (+269)</option>
	            <option value="+(52)-Mexico">Mexico (+52)</option>
	            <option value="+(691)-Micronesia, Federated States of">Micronesia, Federated States of (+691)</option>
	            <option value="+(373)-Moldova, Republic of">Moldova, Republic of (+373)</option>
	            <option value="+(377)-Monaco">Monaco (+377)</option>
	            <option value="+(976)-Mongolia">Mongolia (+976)</option>
	            <option value="+(1664)-Montserrat">Montserrat (+1664)</option>
	            <option value="+(212)-Morocco">Morocco (+212)</option>
	            <option value="+(258)-Mozambique">Mozambique (+258)</option>
	            <option value="+(95)-Myanmar">Myanmar (+95)</option>
	            <option value="+(264)-Namibia">Namibia (+264)</option>
	            <option value="+(674)-Nauru">Nauru (+674)</option>
	            <option value="+(977)-Nepal">Nepal (+977)</option>
	            <option value="+(31)-Netherlands">Netherlands (+31)</option>
	            <option value="+(599)-Netherlands Antilles">Netherlands Antilles (+599)</option>
	            <option value="+(687)-New Caledonia">New Caledonia (+687)</option>
	            <option value="+(64)-New Zealand">New Zealand (+64)</option>
	            <option value="+(505)-Nicaragua">Nicaragua (+505)</option>
	            <option value="+(227)-Niger">Niger (+227)</option>
	            <option value="+(234)-Nigeria">Nigeria (+234)</option>
	            <option value="+(683)-Niue">Niue (+683)</option>
	            <option value="+(672)-Norfolk Island">Norfolk Island (+672)</option>
	            <option value="+(1670)-Northern Mariana Islands">Northern Mariana Islands (+1670)</option>
	            <option value="+(47)-Norway">Norway (+47)</option>
	            <option value="+(968)-Oman">Oman (+968)</option>
	            <option value="+(92)-Pakistan">Pakistan (+92)</option>
	            <option value="+(680)-Palau">Palau (+680)</option>
	            <option value="+(970)-Palestinian Territory, Occupied">Palestinian Territory, Occupied (+970)</option>
	            <option value="+(507)-Panama">Panama (+507)</option>
	            <option value="+(675)-Papua New Guinea">Papua New Guinea (+675)</option>
	            <option value="+(595)-Paraguay">Paraguay (+595)</option>
	            <option value="+(51)-Peru">Peru (+51)</option>
	            <option value="+(63)-Philippines">Philippines (+63)</option>
	            <option value="+(0)-Pitcairn">Pitcairn (+0)</option>
	            <option value="+(48)-Poland">Poland (+48)</option>
	            <option value="+(351)-Portugal">Portugal (+351)</option>
	            <option value="+(1787)-Puerto Rico">Puerto Rico (+1787)</option>
	            <option value="+(974)-Qatar">Qatar (+974)</option>
	            <option value="+(262)-Reunion">Reunion (+262)</option>
	            <option value="+(40)-Romania">Romania (+40)</option>
	            <option value="+(70)-Russian Federation">Russian Federation (+70)</option>
	            <option value="+(250)-Rwanda">Rwanda (+250)</option>
	            <option value="+(290)-Saint Helena">Saint Helena (+290)</option>
	            <option value="+(1869)-Saint Kitts and Nevis">Saint Kitts and Nevis (+1869)</option>
	            <option value="+(1758)-Saint Lucia">Saint Lucia (+1758)</option>
	            <option value="+(508)-Saint Pierre and Miquelon">Saint Pierre and Miquelon (+508)</option>
	            <option value="+(1784)-Saint Vincent and the Grenadines">Saint Vincent and the Grenadines (+1784)</option>
	            <option value="+(684)-Samoa">Samoa (+684)</option>
	            <option value="+(378)-San Marino">San Marino (+378)</option>
	            <option value="+(239)-Sao Tome and Principe">Sao Tome and Principe (+239)</option>
	            <option value="+(966)-Saudi Arabia">Saudi Arabia (+966)</option>
	            <option value="+(221)-Senegal">Senegal (+221)</option>
	            <option value="+(381)-Serbia and Montenegro">Serbia and Montenegro (+381)</option>
	            <option value="+(248)-Seychelles">Seychelles (+248)</option>
	            <option value="+(232)-Sierra Leone">Sierra Leone (+232)</option>
	            <option value="+(65)-Singapore">Singapore (+65)</option>
	            <option value="+(421)-Slovakia">Slovakia (+421)</option>
	            <option value="+(386)-Slovenia">Slovenia (+386)</option>
	            <option value="+(677)-Solomon Islands">Solomon Islands (+677)</option>
	            <option value="+(252)-Somalia">Somalia (+252)</option>
	            <option value="+(27)-South Africa">South Africa (+27)</option>
	            <option value="+(0)-South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands (+0)</option>
	            <option value="+(34)-Spain">Spain (+34)</option>
	            <option value="+(94)-Sri Lanka">Sri Lanka (+94)</option>
	            <option value="+(249)-Sudan">Sudan (+249)</option>
	            <option value="+(597)-Suriname">Suriname (+597)</option>
	            <option value="+(47)-Svalbard and Jan Mayen">Svalbard and Jan Mayen (+47)</option>
	            <option value="+(268)-Swaziland">Swaziland (+268)</option>
	            <option value="+(46)-Sweden">Sweden (+46)</option>
	            <option value="+(41)-Switzerland">Switzerland (+41)</option>
	            <option value="+(963)-Syrian Arab Republic">Syrian Arab Republic (+963)</option>
	            <option value="+(886)-Taiwan, Province of China">Taiwan, Province of China (+886)</option>
	            <option value="+(992)-Tajikistan">Tajikistan (+992)</option>
	            <option value="+(255)-Tanzania, United Republic of">Tanzania, United Republic of (+255)</option>
	            <option value="+(66)-Thailand">Thailand (+66)</option>
	            <option value="+(670)-Timor Leste">Timor-Leste (+670)</option>
	            <option value="+(228)-Togo">Togo (+228)</option>
	            <option value="+(690)-Tokelau">Tokelau (+690)</option>
	            <option value="+(676)-Tonga">Tonga (+676)</option>
	            <option value="+(1868)-Trinidad and Tobago">Trinidad and Tobago (+1868)</option>
	            <option value="+(216)-Tunisia">Tunisia (+216)</option>
	            <option value="+(90)-Turkey">Turkey (+90)</option>
	            <option value="+(7370)-Turkmenistan">Turkmenistan (+7370)</option>
	            <option value="+(1649)-Turks and Caicos Islands">Turks and Caicos Islands (+1649)</option>
	            <option value="+(688)-Tuvalu">Tuvalu (+688)</option>
	            <option value="+(256)-Uganda">Uganda (+256)</option>
	            <option value="+(380)-Ukraine">Ukraine (+380)</option>
	            <option value="+(971)-United Arab Emirates">United Arab Emirates (+971)</option>
	            <option value="+(44)-United Kingdom">United Kingdom (+44)</option>
	            <option value="+(1)-United States">United States (+1)</option>
	            <option value="+(1)-United States Minor Outlying Islands">United States Minor Outlying Islands (+1)</option>
	            <option value="+(598)-Uruguay">Uruguay (+598)</option>
	            <option value="+(998)-Uzbekistan">Uzbekistan (+998)</option>
	            <option value="+(678)-Vanuatu">Vanuatu (+678)</option>
	            <option value="+(58)-Venezuela">Venezuela (+58)</option>
	            <option value="+(84)-Viet Nam">Viet Nam (+84)</option>
	            <option value="+(1284)-Virgin Islands, British">Virgin Islands, British (+1284)</option>
	            <option value="+(1340)-Virgin Islands, U.s.">Virgin Islands, U.s. (+1340)</option>
	            <option value="+(681)-Wallis and Futuna">Wallis and Futuna (+681)</option>
	            <option value="+(212)-Western Sahara">Western Sahara (+212)</option>
	            <option value="+(967)-Yemen">Yemen (+967)</option>
	            <option value="+(260)-Zambia">Zambia (+260)</option>
	            <option value="+(263)-Zimbabwe">Zimbabwe (+263)</option>';
	    // $html=addslashes($html);    
        return $html;
	}


	function initializePagination($rows,$base_url='',$uri_segment='',$first_url=''){
		$config['base_url'] = $base_url;
		$config['total_rows'] = $rows;
        $config['per_page'] = 15;
        $config["uri_segment"] = $uri_segment;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = FALSE;
        $config["num_links"] = 2; //ceil($config["total_rows"] / $config["per_page"]);
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_url'] = (!empty($first_url)) ? $first_url : 1;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li rel=prev >';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li rel="next">';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void()">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['suffix'] = ($_GET) ? '?' . http_build_query($_GET, '', "&") : NULL;
        $config['attributes']['rel'] = true;
        return $config;
	}

	function getPressContent($press_content){
		// $press_content = strip_tags($press_content, "blockquote") ;
		// $press_content = strip_tags($press_content, "b") ;
		// $press_content = str_ireplace('<blockquote>','<q>',$press_content);
		// $press_content = str_ireplace( array('\n','\r\n','\r','<br/>'),'<br/>',$press_content);
		return $press_content;
	}
	function highlightSearchText($report_title, $search){
		if(stristr($report_title,$search)){
			$highlighted = "<mark class='highlight'>".@$search."</mark>";
			$report_title = str_ireplace($search,$highlighted,$report_title);
		}
		return $report_title;
	}
	
	