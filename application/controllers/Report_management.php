<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Report_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Reports_model');
        $this->load->database("");
        // $this->output->cache(60);
    }
    
    public function country_list(){
        $data['publishers']=$this->Reports_model->get_publishers();
        $region=$this->input->post("region");
        $data["country"]=$this->Reports_model->get_country($region);
        echo "<ul class='country'>";
        foreach($data["country"]->result() as $row){
            $country=$row->country;
            $id=$row->id;
            echo "<li><input class='country$id' onclick='getReports(".$id.")' type='checkbox' name='country[]' value='$country'><span>".$country."</span></li>";
        }
        echo "</ul>";
        
    }    
    public function latest_reports() {
        $where = array();
        $where_between = array();
        $region_data=array();
        $orderarray=null;

        $data['year']= $year = $this->input->get('year') ? $this->input->get('year') : '';
        $data['current_region']= $region = $this->input->get('region') ? $this->input->get('region') : '';
        $data['date_order']= $date = $this->input->get('date') ? $this->input->get('date') : '';
        $data['price']= $price = $this->input->get('price') ? $this->input->get('price') : '';
        $data['current_url']=str_ireplace('/index.php','',current_url());
        
        if(!empty($year)){
            $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
        }
        if(!empty($region)){
            // $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
            $data["region"]=$this->Reports_model->get_country($region);
            $country="";
            foreach($data["region"]->result() as $countryName){
                 $country=$countryName->country.",".$country;
            }
            $country=rtrim(',',$country);
            $region_data=array('country' => $country, 'region' => $region);
        }
        if(!empty($price)){
            if(stristr($price,'-')){
                $report_price=explode('-',$price);
                $where_between[] ="li_value BETWEEN '".@$report_price[0]."' and '".@$report_price[1]."' ";
            }elseif(stristr($price,'<')){
                $where['li_value <']=str_ireplace('<','',$price);
            }elseif(stristr($price,'>')){
                $where['li_value > ']=str_ireplace('>','',$price);
            }
        }
        if(!empty($date)){
            $orderarray ="rep_entry_date ".$date;
        }else{
            $orderarray="rep_id DESC";
        }
        $base_url = base_url('reports/page/');

        $countrep = $this->Reports_model->get_latest_reports($where, $where_between, $orderarray, $region_data, NULL, NULL);
        $rows=$countrep->num_rows();
        $uri_segment=3;
        $first_url = '';
        if(!empty($_GET)){
            $first_url .='1?year='.@$year.'&region='.@$region.'&date='.@$date.'&price='.@$price;
        }
        $config=array();
        $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
        // echo "<pre>";print_r($config);die();
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
        $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
        $data['uri_suffix'] = $config['suffix'];

        $data['report_list'] = $this->Reports_model->get_latest_reports($where, $where_between , $orderarray, $region_data , $config["per_page"], $data['page'] - 1);
        
        if(!empty($_GET) || $config['total_rows'] > 0){
            $data['total_reports'] = $config['total_rows'];
        }

        $data['pagination'] = $this->pagination->create_links();
        $data["region_data"]=$this->Reports_model->get_region();
        // $data["country"]=$this->Reports_model->get_country();

        $this->load->view('latest-reports', $data);
    }

   
    public function report_details() {
        $regionName=array("South America","Oceania","North America","Global","Europe","Asia","Antarctica","Africa","Zimbabwe","Zealand","Zambia","Yemen","Walli sutuna","Vietnam","Venezuela","Vatican","Vanuatu","Uzbekistan","US","America","United States","Uruguay","Ukraine","Uganda","Tuvalu","Turks Caicos","Turkmenistan","Turkey","Tunisia","Trinidad","Tobago","Tonga","Tokelau","Togo","Timor-Leste","Thailand","Tanzania","Tajikistan","Taiwan","Syrian","Syria","Switzerland","Sweden","Swaziland","Svalbard","Mayen","Suriname","Sudan","Sri Lanka","Spain","Somalia","Solomon","Slovenia","Slovakia","Singapore","Sierra Leone","Seychelles","Serbia and Montenegro","Senegal","Saudi Arabia","Samoa","Samoa","Salvador"," Saint Vincent","Grenadines","Saint Kitts Nevis","Saint Helena","Sahara","Rwanda","Romania","Reunion","Qatar","Puerto Rico","Portugal","Poland","Pitcairn","Pierre Miquelon","Philippines","Peru","Paraguay","Papua","Panama","Palestinian","Palau","Pakistan","Oman","Norway","Norfolk","Niue","Nigeria","Niger","Nicaragua","Netherlands Antilles","Netherlands","Nepal","Nauru","Namibia","Myanmar","Mozambique","Morocco","Montserrat","Mongolia","Monaco","Moldova","Micronesia","Mexico","Mayotte","Mauritius","Mauritania","Martinique","Marshall","Marino","Mariana","Malta","Mali","Maldives","Malaysia","Malawi","Madagascar","Macedonia","Macao","Luxembourg","Lucia","Lithuania","125Liechtenstein","Libyan Jamahiriya","Liberia","Lesotho","Lebanon","Latvia","Latin","Lao","Kyrgyzstan","Kuwait","Korea","Korea","Kiribati","Kingdom","London","Kenya","Kazakhstan","Jordan","Japan","Jamaica","Italy","Israel","Ireland","Iraq","Iran","Indonesia","India","Iceland","Hungary","Hong Kong","Honduras","Haiti","Guyana","Guinea-Bissau","Guinea","Guiana","Guatemala","Guam","Guadeloupe","Grenada","Greenland","Greece","Gibraltar","Ghana","Germany","Georgia","Georgia","Gambia","Gabon","France","Finland","Fiji","Faroe","Falkland","Malvinas","Ethiopia","Estonia","Eritrea","Egypt","Ecuador","Dominican","Dominica","Djibouti","Denmark","Czech","Cyprus","Cuba","Croatia","CoteDIvoire","Costa Rica","ongo","Congo","Comoros","Colombia","Cocos","China","Chile","Chad","Cayman","CapeVerde","Canada","Cameroon","Cambodia","Caledonia","Burundi","Burkina Faso","Bulgaria","Brunei Darussalam","Brazil","Bouvet","Botswana","Bosnia","Herzegovina","Bolivia","Bhutan","Bermuda","Benin","Belize","Belgium","Belarus","Barbados","Bangladesh","Bahrain","Bahamas","Azerbaijan","Austria","Australia","Aruba","Armenia","Argentina","ArabEmirates","Antigua","Barbuda","Antarctica","Anguilla","Angola","Andorra","Algeria","Albania","Africa","Afghanistan","Polynesia","United States","European Union","and",")",",","(");
        $rep_id=$this->uri->segment(2);
        $data['report_data'] = $this->Reports_model->get_report_data($rep_id);
        if($data['report_data']){
            $data['report_license'] = $this->Reports_model->get_report_license($rep_id);
            $related_product_category=$data["report_data"]->rep_sub_cat_1_id;
            $data["related_reports"]= $this->Reports_model->get_related_reports($related_product_category,$rep_id,10);


            $report_title=$data["report_data"]->rep_title;

            //$report_year=explode('20', $report_title);
            $report_year=str_ireplace('|','',preg_replace('/[^0-9]/','', $report_title));
            $sub_string=strlen($report_year)+1;
            if(strlen($report_year) > 4){
                $data['report_year'] =substr(chunk_split($report_year,4,'-'),0,$sub_string);
            }else{
                $data['report_year'] =$report_year;
            }

            $report_title=str_ireplace('global','',$report_title);
            $start_year='2015';
            $end_year=date('Y')+10;
            $year_array=array();
            for($i=$start_year;$i<=$end_year;$i++){
                array_push($year_array,$i);
            }
            
            // $year_string=['2017','2018','2019','2020','2021','2022','2023','2024','2025','2026'];
            $report_title=str_ireplace($year_array,'',$report_title);
            //$report_title=str_ireplace($regionName,'',$report_title);


            if(stristr($report_title,'market')){
                $meta_title=explode('Market',$report_title);
            }
            if(stristr($report_title,'industry')){
                $meta_title=explode('Industry',$report_title);
            }

            $meta_title[0]=str_ireplace($regionName,'',$meta_title[0]);

            $data['meta_report_title']=$meta_title[0];        

            $this->load->view('report-details',$data);
        }else{
            $this->load->view('404');
        }
    }

    public function report_details_amp() {
        $data['publishers']=$this->Reports_model->get_publishers();

        if ($this->session->has_userdata("badge_count")):
            $data["badge"] = $this->session->userdata("badge_count");
        endif;
       $rep_id=$this->uri->segment(3);
        $data['report_data'] = $this->Reports_model->get_report_data($rep_id);
        $data['report_license'] = $this->Reports_model->get_report_license($rep_id);
        $related_product_category=$data["report_data"]->rep_sub_cat_1_id;
        $data["related_reports"]= $this->Reports_model->get_related_reports($related_product_category,10);

        $this->load->view('report-details-amp',$data);
    }
    public function process_form_request() {
        $rep_id=$this->uri->segment(2);
        $type=$this->input->get('type');
        if(!in_array($type, array('request_sample','request_customization','request_discount','enquire_before_purchase'))){
            redirect('../latest-reports');
        }
        $data['form_type']=ucwords(str_ireplace('_',' ',$type));
        $data['report_data'] = $this->Reports_model->get_report_form_data($rep_id);
        $this->load->view('report-sample',$data);
    }
}
