<?php

class Form_management extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $this->load->helper(array('html','menu'));
        $this->load->model("Search_model");
        ini_set("date_default_timezone_set", "Asia/Kolkata");
        date_default_timezone_set('Asia/Kolkata');
    }
     public function thanks() {
        $this->load->view('thanks');
    } 
    public function cancel() {        
        $this->load->view('cancel');
    }
    public function request_discount_email(){
        redirect("../contact-us");
        // $email=$this->input->post("email");
        // $report_title=$this->input->post("title");

        // $config['charset'] = 'utf-8';
        // $config['wordwrap'] = TRUE;
        // $config['mailtype'] = 'html';
        // $this->email->initialize($config);
        // $this->email->from('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
        // $this->email->to(array('sairam@foreseeconvey-markets.com'));
        // $this->email->cc(array('prospectresearchreports@gmail.com'));
        // $this->email->subject('New Request to Avail 10% Discount');
        
        // $this->email->message('
        // Dear Admin,<br><br>       
        // You have a New Request to Avail 10% Discount..<br><br>
        // <font size=2><b>Email :- </b></font> ' . $email. '<br><br>
        // <font size=2><b>Report Title :- </b></font> ' . $report_title. '<br><br>
        // // <font size=2><b>IP Address:- </b></font>' . $ip . '<br /><br />
        // Thanks.<br><br>
        // ');

        // if($this->email->send()){
        //     echo "You have successfully applied to avail discount . One of our executives will get back to you soon.";
        // }
        
    }
    
    public function search_form_process()
    {
        if(!empty($_POST)){      
            $data=getDetailsFromIP();

            $ip = $data['ipAddress'];
            $countryCode = $data['countryCode'];
            $countryName = $data['countryName'];
            $regionName = $data['regionName'];
            $cityName = $data['cityName'];

            $this->load->library("email");
            $rep_title=addslashes(trim($this->input->post("rep_title")));
            $contact_person=$this->input->post("name");
            $contact_email=$this->input->post("email");
            $contact_phone=$this->input->post("phone");
            $contact_msg=addslashes(trim($this->input->post("message")));
            $contact_country=$this->input->post("country");            
            $country= explode('-', str_ireplace('\"',"",$contact_country));            
            $country_code =str_ireplace(array('(',')'),'',$country[0]);
            $contact_form_type = array_search('Search Form',getFormTypes()) ? array_search('Search Form',getFormTypes()) :  0;
                       
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
            $this->email->to('sairam@foreseeconvey-markets.com');
            // $this->email->cc(array('prospectresearchreports@gmail.com'));
            $this->email->subject('New Search Query');
            $this->email->message('
                Dear Admin,<br><br>       
                You have a new Search Query..<br><br>
                <font size=2><b>Report Title :- </b></font>' . $rep_title . '<br><br>
                <font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                <font size=2><b>Contact Number :- </b></font> ' .$country_code."-".$contact_phone . '<br><br>
                <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                <hr>
                <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                Thanks.<br><br>
                ');

           if($this->email->send())
           {
                $data= array(
                    "contact_rep_title"=>$rep_title,
                    "contact_person"=>$contact_person,
                    "contact_email"=>$contact_email,
                    "contact_phone"=>$country_code.'-'.$contact_phone,
                    "contact_msg"=>$contact_msg,
                    'contact_country' => $country[1],
                    'contact_exact_region' => $regionName,
                    'contact_real_country' => $countryName,
                    'contact_city' => $cityName,
                    'contact_ip' => $ip,
                    "contact_datetime" => date("Y-m-d H:i:s"),
                    'contact_form_type' => @$contact_form_type,
                    'query_source' => 'web'
                );
                $this->Search_model->insert($data);
                redirect("../search-form/thanks");
            }
        }else{
            redirect("../latest-reports");            
        }              
    }

    public function contact_form_process()
    {
        if(!empty($_POST)){ 
            $data=getDetailsFromIP();

            $ip = $data['ipAddress'];
            $countryCode = $data['countryCode'];
            $countryName = $data['countryName'];
            $regionName = $data['regionName'];
            $cityName = $data['cityName'];
            
            $data['name']=$contact_person=$this->input->post("name");
            $data['email']=$contact_email=$this->input->post("email");
            $data['phone']=$contact_phone=$this->input->post("phone");
            $data['contact_msg']=$contact_msg=addslashes(trim($this->input->post("message")));
            $data['job_role']=$contact_job_role=$this->input->post("job_role");
            $contact_country=$this->input->post("country");
            $country= explode('-', str_ireplace('\"',"",$contact_country));            
            $country_code =str_ireplace(array('(',')'),'',$country[0]);
            $data['company']=$contact_company=addslashes(trim($this->input->post("company")));

            $contact_rep_title=($this->input->post("rep_title")) ? addslashes(trim($this->input->post("rep_title"))) : "";
            $invalid_message='';
            if(@$contact_rep_title === ''){
                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                // Check if there is a url in the text
                if(preg_match($reg_exUrl, $contact_msg, $url)) {
                   // echo preg_replace($reg_exUrl, "<a href="{$url[0]}">{$url[0]}</a> ", $text);
                    $invalid_message='Sorry ! Your request could not be processed due to inclusion of Link(s) in Message';
                }else{
                    $BANNEDWORDS=getBannedWords();
                    // echo '<pre>';print_r($BANNEDWORDS);die();

                    $explode_msg=explode(' ',$contact_msg);
                    foreach ($explode_msg as $key => $value) {
                        if(in_array(strtolower($value),$BANNEDWORDS)){
                            $invalid_message='Sorry ! Your request could not be processed due to invalid characters in a Message';
                        }
                    }
                }
            }
            if(!empty(@$invalid_message)){
                $data['message']=@$invalid_message;
                $this->load->view('contact-us',$data);
            }else{
                $mail_subject = '';
                $contact_form_type = 0;
                if($this->input->post("rep_title")){
                    $mail_subject = 'New Report Details Contact Query';
                    $contact_form_type = array_search('Report Details',getFormTypes());
                }else{
                    $mail_subject = 'New Contact Query';
                    $contact_form_type = array_search('Contact Form',getFormTypes());
                }
                $mail_message='';
                $mail_message.="Dear Admin,<br><br>       
                    You have a ".$mail_subject."..<br><br>";
                $mail_message.=($this->input->post("rep_title")) ? '<font size=2><b>Report Title :- </b></font>' . $contact_rep_title . '<br><br>' : '';
                    
                $mail_message.='
                    <font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                    <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                    <font size=2><b>Contact Number :- </b></font> ' . $country_code.'-'.$contact_phone . '<br><br>
                    <font size=2><b>Contact Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                    <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                    <hr>
                    <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                    <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                    <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                    Thanks.<br><br>';
                
                            
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
                $this->email->to('sairam@foreseeconvey-markets.com');
                // $this->email->cc(array('prospectresearchreports@gmail.com')); 
                $this->email->subject($mail_subject);
                $this->email->message($mail_message);                

                if($this->email->send())
                {
                    $db_data= array(
                        "contact_rep_title"=>$contact_rep_title,
                        "contact_person"=>$contact_person,
                        "contact_email"=>$contact_email,
                        "contact_phone"=>$country_code.'-'.$contact_phone,
                        "contact_msg"=>$contact_msg,
                        "contact_job_role"=>$contact_job_role,
                        'contact_country' => $country[1],
                        "contact_company" => $contact_company,
                        'contact_exact_region' => $regionName,
                        'contact_real_country' => $countryName,
                        'contact_city' => $cityName,
                        'contact_ip' => $ip,
                        "contact_datetime" => date("Y-m-d H:i:s"),
                        'contact_form_type' => @$contact_form_type,
                        'query_source' => 'web'
                    );
                    $this->Search_model->insert($db_data);

                    if($this->input->post("rep_title")){
                        redirect("../report-request/thanks");
                    }else{
                        redirect("../contact-form/thanks");
                    }
                }
            }
        }else{
            redirect("../contact-us");            
        }                        
    }


    public function custom_research_process()
    { 
        if(!empty($_POST)){     
            $data=getDetailsFromIP();

            $ip = $data['ipAddress'];
            $countryCode = $data['countryCode'];
            $countryName = $data['countryName'];
            $regionName = $data['regionName'];
            $cityName = $data['cityName'];
            
            $contact_person=$this->input->post("name");
            $contact_email=$this->input->post("email");
            $contact_phone=$this->input->post("phone");
            $contact_msg=addslashes(trim($this->input->post("message")));
            $contact_budget=$this->input->post("budget");
            $contact_job_role=$this->input->post("job_role");
            $contact_country=$this->input->post("country");            
            $country= explode('-', str_ireplace('\"',"",$contact_country));            
            $country_code =str_ireplace(array('(',')'),'',$country[0]);
            $contact_company=addslashes(trim($this->input->post("company")));
            $contact_form_type = array_search('Custom Research',getFormTypes()) ? array_search('Custom Research',getFormTypes()) :  0;
                        
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
            $this->email->to('sairam@foreseeconvey-markets.com');
            // $this->email->cc(array('prospectresearchreports@gmail.com'));
            $this->email->subject('New Custom Research Request');
            
            $this->email->message('
                Dear Admin,<br><br>       
                You have a New Custom Research Request..<br><br>
                <font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                <font size=2><b>Contact Number :- </b></font> ' . $country_code.'-'.$contact_phone . '<br><br>
                <font size=2><b>Contact Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                <font size=2><b>Budget :- </b></font>' . "$".$contact_budget . '<br><br>
                <hr>
                <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                Thanks.<br><br>'
            );

           if($this->email->send())
           {
            $db_data= array(
                "contact_person"=>$contact_person,
                "contact_email"=>$contact_email,
                "contact_phone"=>$country_code.'-'.$contact_phone,
                "contact_msg"=>$contact_msg,
                "contact_job_role"=>$contact_job_role,
                'contact_country' => $country[1],
                "contact_company" => $contact_company,
                'contact_exact_region' => $regionName,
                'contact_real_country' => $countryName,
                'contact_city' => $cityName,
                'contact_ip' => $ip,
                "contact_datetime" => date("Y-m-d H:i:s"),
                'contact_form_type' => @$contact_form_type,
                'query_source' => 'web'
            );
            $this->Search_model->insert($db_data);
            redirect("../custom-research/thanks");
           }
        }else{
            redirect("../custom-research");            
        }
                        
    }
    public function report_form_process(){
        if(!empty($_POST)){          
            $data=getDetailsFromIP();

            $ip = $data['ipAddress'];
            $countryCode = $data['countryCode'];
            $countryName = $data['countryName'];
            $regionName = $data['regionName'];
            $cityName = $data['cityName'];

            $form_type=$this->input->post("form_type");
            $contact_person=$this->input->post("name");
            $contact_report_title=addslashes(trim($this->input->post("rep_title")));
            $contact_email=$this->input->post("email");
            $contact_phone=$this->input->post("phone");
            $contact_msg=addslashes(trim($this->input->post("message")));
            $contact_job_role=$this->input->post("job_role");
            $contact_country=$this->input->post("country");            
            $country= explode('-', str_ireplace('\"',"",$contact_country));            
            $country_code =str_ireplace(array('(',')'),'',$country[0]);
            $contact_company=addslashes(trim($this->input->post("company")));
            $contact_form_type = array_search($form_type,getFormTypes()) ? array_search($form_type,getFormTypes()) :  0;
                        
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
            $this->email->to('sairam@foreseeconvey-markets.com');
            // $this->email->cc(array('prospectresearchreports@gmail.com'));
            $this->email->subject('New '.@$form_type.'  Query');
            $this->email->message('
                Dear Admin,<br><br>       
                You have a new '.@$form_type.' Query..<br><br>
                <font size=2><b>Report Title:- </b></font>' . $contact_report_title . '<br><br>
                <font size=2><b>Contact Person :- </b></font>' . $contact_person . '<br><br>
                <font size=2><b>Email :- </b></font> ' . $contact_email. '<br><br>
                <font size=2><b>Country Name :- </b></font> ' . $country[1] . '<br><br>
                <font size=2><b>Contact Number :- </b></font> ' . $country_code.'-'.$contact_phone . '<br><br>
                <font size=2><b>Contact Job Role :- </b></font> ' . $contact_job_role . '<br><br>
                <font size=2><b>Message :- </b></font>' . $contact_msg . '<br><br>
                <hr>
                <font size=2><b>Country:- </b></font> ' . $countryName . '<br /><br />
                <font size=2><b>State:- </b></font> ' . $regionName . '<br /><br />
                <font size=2><b>City:- </b></font>' . $cityName . '<br /><br />
                Thanks.<br><br>
            ');

           if($this->email->send())
           {
                $db_data= array(
                    "contact_rep_title"=>$contact_report_title,
                    "contact_person"=>$contact_person,
                    "contact_email"=>$contact_email,
                    "contact_phone"=>$country_code.'-'.$contact_phone,
                    "contact_msg"=>$contact_msg,
                    "contact_job_role"=>$contact_job_role,
                    'contact_country' => $country[1],
                    "contact_company" => $contact_company,
                    'contact_exact_region' => $regionName,
                    'contact_real_country' => $countryName,
                    'contact_city' => $cityName,
                    'contact_ip' => $ip,
                    "contact_datetime" => date("Y-m-d H:i:s"),
                    'contact_form_type' => @$contact_form_type,
                    'query_source' => 'web'
                );
                $this->Search_model->insert($db_data);
                $redirect=str_ireplace(' ','-',strtolower(@$form_type));
                redirect('../'.@$redirect.'/thanks');
            }
        }else{
            redirect("../latest-reports");            
        }                        
    }
    public function do_upload()
    {
        if(!empty($_POST)){
            $config['upload_path']    = './uploads/careers/';
            $config['allowed_types']  = 'doc|docx|xls|pdf';
            $config['max_size']       = 5000;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('resume')){
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('careers', $error);
            }else{
                $file=$this->upload->data();
                 $values= array(
                    "candidate_name"=>$this->input->post("name"),
                    "candidate_email"=>$this->input->post("email"),
                    "candidate_skill_info"=>$this->input->post("message"),
                    "candidate_resume"=>$file["file_name"],
                    "position_applied"=>"NULL",
                    "criteria_meet"=>"N",
                );
                 
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
                $this->email->to('hr@foreseeconvey-markets.com');
                
                $this->email->subject('New Job Application');
                $this->email->attach(base_url().'/uploads/careers/'.$file["file_name"]);
                $this->email->message('
                    Dear HR Team,<br><br>       
                    You have a new Job Application.<br><br>
                    <font size=2><b>Applicant Name :- </b></font>' . $this->input->post("name") . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $this->input->post("email"). '<br><br>
                    <font size=2><b>Skills :- </b></font> ' . $this->input->post("message") . '<br><br>
                    Thanks.<br><br>
                ');
               if($this->email->send()){
                    if($this->Search_model->insert_values($values)){
                        $data["successText"] ="<div class='alert alert-success career-success'><span class='fa fa-graduation-cap'></span> &nbsp;Congratulations ! We have received your application. We will get back to you soon !</div>";
                        $this->load->view('careers', $data);
                    }
                }
            }
        }else{
            redirect("../careers");            
        }
        
    }
    public function file_upload()
    {
        if(!empty($_POST)){
            $config['upload_path']          = './uploads/careers/';
            $config['allowed_types']        = 'doc|docx|xls|pdf';
            $config['max_size']             = 5000;

            $data["role"]=$this->input->post("role");

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('resume')){
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('careers-apply', $error);
            }else{
                $file=$this->upload->data();
                 $values= array("candidate_name"=>$this->input->post("name"),
                "candidate_email"=>$this->input->post("email"),
                "candidate_skill_info"=>$this->input->post("message"),
                "candidate_resume"=>$file["file_name"],
                "position_applied"=>$this->input->post("role"),
                "criteria_meet"=>"Y",
                     );
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
                $this->email->to('hr@foreseeconvey-markets.com');
                
                $this->email->subject('New Job Application');
                $this->email->attach(base_url().'/uploads/careers/'.$file["file_name"]);
                $this->email->message('
                    Dear HR Team,<br><br>       
                    You have a new Job Application.<br><br>
                    <font size=2><b>Position Applied For :- </b></font>' . $this->input->post("role") . '<br><br>
                    <font size=2><b>Applicant Name :- </b></font>' . $this->input->post("name") . '<br><br>
                    <font size=2><b>Email :- </b></font> ' . $this->input->post("email"). '<br><br>
                    <font size=2><b>Skills :- </b></font> ' . $this->input->post("message") . '<br><br>
                    Thanks.<br><br>
                ');
                
                if($this->email->send()){
                    if($this->Search_model->insert_values($values)){
                        $data["successText"] ="<div class='alert alert-success career-success'><span class='fa fa-graduation-cap'></span> &nbsp;Congratulations ! We have received your application. We will get back to you soon !</div>";
                        // $data["role"]=$this->input->post("role");
                        $this->load->view('careers', $data);
                    }
                }
            }
        }else{
            redirect("../careers");            
        }
    }
}
?>