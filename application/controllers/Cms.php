<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array("Reports_model","Category_model"));
        $this->load->database("");
        $this->load->helper(array('xml','text'));
        // $this->output->cache(60);
    }

    public function cookie_update(){
        $cookie_value=$this->input->post("cookie_value");
        set_cookie("cookie_consent",$cookie_value,24*60*60);        
    }

    public function about_us() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);        
		$this->load->view('about-us',$data);		
    }

    public function contact_us() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
        $this->load->view('contact-us',$data);    
    }
    public function custom_research() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
        $this->load->view('custom-research',$data);        
    }
    public function terms_conditions() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
		$this->load->view('terms-conditions',$data);
    }    
    
    public function error_404() {
		$this->load->view('404');
    }

    public function privacy_policy() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
		$this->load->view('privacy-policy',$data);
    }

    public function return_policy() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
		$this->load->view('return-policy',$data);
    }

    public function cookie_policy() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
		$this->load->view('cookie-policy',$data);
    }
    public function payment_mode() { 
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
		$this->load->view('payment-mode',$data);
		
    }   
    public function careers() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
		$this->load->view('careers',$data);
    }

    public function careers_apply() {
		$this->load->view('careers-apply');		
    }

    public function rss() {

        $data['feed_name'] = 'Foresee Convey Markets';
        $data['encoding'] = 'utf-8';
        $data['feed_url'] = base_url() . 'rss-feeds';
        $data['page_description'] = 'This page gives you information about latest reports as rss feeds';
        $data['page_language'] = 'en-us';
        $data['report_data'] = $this->Reports_model->get_top_list_rss(5000);
        header("Content-Type: application/rss+xml");

        $this->load->view('rss', $data);
    }
    
/****************     SITEMAP   **********************/
    
     public function sitemap_web() {
        header("Content-Type: application/xml");
        $data['categories'] = $this->Category_model->get_cat_list();
        $this->load->view('sitemap-web.xml',$data);
    }
    public function sitemap() {
        header("Content-Type: application/xml");
        $data['reports']=$this->Reports_model->count_reports();
        $count=$data['reports']->num_rows();
        $data['sitemaps']=ceil($count/20000);
        $this->load->view('sitemap.xml',$data);
    }
	
	public function sitemap_monthwise(){

		header("Content-Type: application/xml");

		$sitemap_no=$this->uri->segment(1);		
		$no=explode(".",substr($sitemap_no,8));		
		$limit=20000;
		$offset=($no[0]-1)*$limit;
		$data['report_data'] = $this->Reports_model->get_sitemap_reports($offset,$limit);
		
		$this->load->view('sitemap-content.xml',$data);
	}

    public function sitemap_html() {
        $url=$this->uri->segment(1);
        $data['meta']=$this->Reports_model->get_meta_info($url);
		$this->load->view('sitemap',$data);	
    }
}
