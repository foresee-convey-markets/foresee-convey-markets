<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Reports_model','Press_model'));
        // $this->output->cache(60);
    }

    public function index() {
        $data['report'] = $this->Reports_model->get_top_latest_reports(12); 
        $data['press_list'] = $this->Press_model->get_press_list(5);
        $this->load->view('index',$data);
    }

}
