<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_management extends CI_Controller {

    public function __construct() {

        parent:: __construct();
        $this->load->model(array("Reports_model","Search_model","Category_model"));
        $this->load->helper(array('form'));
        // $this->output->cache(60);
    }

    public function search_suggestions() {

        $data = array();
        $search=$this->input->post('search');
        $data['report_search'] = $this->Search_model->get_report_search_ajax(trim($search));
        //echo $data['report_search']->num_rows();
        if($data['report_search']->num_rows() > 0){
            foreach ($data['report_search']->result() as $row) {
                echo '<a href="'.base_url().'reports/'.$row->rep_id.'" style="color:#000;font-family:asul,text-decoration:none;font-size:0.8em">'.substr($row->rep_title,0,50).'..'.'</a><br/>';
            }
        }else{
            echo 'Sorry ! No results found !';
        } 
    }
    public function search_form() {
        $data['search']= $search = $this->input->get('search') ? $this->input->get('search') : '';
        $data['year']= $this->input->get('year') ? $this->input->get('year') : '';
        $data['current_region']= $this->input->get('region') ? $this->input->get('region') : '';
        $data['date_order']= $this->input->get('date') ? $this->input->get('date') : '';
        $data['price']= $this->input->get('price') ? $this->input->get('price') : '';        

        if(!empty(@$search)){
            $data["helpText"] = "Report Search Request For <strong>" . $search ."</strong>";
        }

        $this->load->view("search-form",$data);
    }
    
     public function search_result() {

        $data = array();
	    $data['search']=$search=$this->input->get('search');

        $data['category'] = $this->Category_model->get_cat_list_id();

        $data['year']= $year = $this->input->get('year') ? $this->input->get('year') : '';
        $data['current_region']= $region = $this->input->get('region') ? $this->input->get('region') : '';
        $data['date_order']= $date = $this->input->get('date') ? $this->input->get('date') : '';
        $data['price']= $price = $this->input->get('price') ? $this->input->get('price') : '';
        $data['category']= $category = $this->input->get('category') ? $this->input->get('category') : '';

        
        $where = array();
        $where_like = null;
        $where_between = array();
        $region_data=array();
        $orderarray=null;

        if(!empty($search)){
            $where_like['rep_title'] = explode(' ',$search);
        }
        if(!empty($category)){
            $where['rep_sub_cat_1_id'] = $category;
        }
        if(!empty($year)){
            $where_between[] ="rep_entry_date BETWEEN '".$year."-01-01' AND '".$year."-12-31'";
        }
        if(!empty($region)){
            $data["region"]=$this->Reports_model->get_country($region);
            $country="";
            foreach($data["region"]->result() as $countryName){
                 $country=$countryName->country.",".$country;
            }
            $country=rtrim(',',$country);
            $region_data=array('country' => $country, 'region' => $region);
        }
        if(!empty($price)){
            if(stristr($price,'-')){
                $report_price=explode('-',$price);
                $where_between[] ="li_value BETWEEN '".@$report_price[0]."' and '".@$report_price[1]."' ";
            }elseif(stristr($price,'<')){
                $where['li_value <']=str_ireplace('<','',$price);
            }elseif(stristr($price,'>')){
                $where['li_value > ']=str_ireplace('>','',$price);
            }
        }
        if(!empty($date)){
            $orderarray ="rep_entry_date ".$date;
        }else{
            $orderarray="rep_id DESC";
        }

        
        $base_url = base_url('search-result/page/');
        $query = $data['report_search'] = $this->Search_model->get_report_search_result($where, $where_between, $where_like, $orderarray, $region_data,null,null);

        $first_url = @$search ? '1?search='.@$search : '';
        $data['current_url']=str_ireplace('/index.php','',current_url()).'?search='.@$search;

        if(!empty($_GET)){
            $temp_get=$_GET;
            if(!empty(array_intersect_key($temp_get,['search' => @$search]))){
                unset($temp_get['search']);
                if(!empty($temp_get)){
                    $first_url ='1?search='.@$search.'&year='.@$year.'&region='.@$region.'&date='.@$date.'&price='.@$price;
                    $data['current_url']=str_ireplace('/index.php','',current_url()).'?search='.@$search.'&year='.@$year.'&region='.@$region.'&date='.@$date.'&price='.@$price;
                }
            }
        }
        $rows=$query->num_rows(); 
        $uri_segment=3; 
        $config=array();
        $config=initializePagination($rows,$base_url,$uri_segment,$first_url); // Initialize Pagination
        // echo "<pre>";print_r($config);die();
        $this->pagination->initialize($config);

        $data['total_pages'] = ceil($config["total_rows"] / $config["per_page"]); 
        $data['page'] = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 1;
        $data['uri_suffix'] = $config['suffix'];

        $data['report_search'] = $this->Search_model->get_report_search_result($where, $where_between , $where_like , $orderarray, $region_data ,$config["per_page"], $data['page'] - 1);
 
        //echo $this->db->last_query();

        //$data['total_reports'] = $config['total_rows'];
        $data['total_reports']= $count = $query->num_rows();
        $data['pagination'] = $this->pagination->create_links();

        $data['cat_list'] = $data['cat_1_menu'] = $data['cat_1_list_id'] = $this->Category_model->get_cat_list_id();
              
        $data['cat_1_menu'] = $data['cat_1_list_id'] = $this->Category_model->get_cat_list_id();

        $data['SHOW'] = false;

        if(@$count > 0 && !empty(@$search)){
            $data['SHOW'] = true;
            $data["helpText"] = "Showing <strong>".$data["total_reports"]."</strong> result/s for <strong>" . $search ."</strong>";
        }
        if(!empty($_GET)){
            $temp_get=$_GET;
            if(!empty(array_intersect_key($temp_get,['search' => @$search]))){
                unset($temp_get['search']);
                if(!empty($temp_get)){
                    $data['SHOW'] = true;
                }
            }
        }
        $data['action_url']=str_ireplace('/index.php','',current_url()).'?search='.@$search;

        // $data['publishers']=$this->Reports_model->get_publishers();
        $data["region_data"]=$this->Reports_model->get_region();
        if(@$count == 0){
            $data['helpText']="Sorry ! No Results Found For Your Search Query";
            $this->load->view('search-form',$data);
        }else{
            $this->load->view('search-result', $data);
        }
    }
    function __destruct() 
    {
        $this->db->close();
    }

}
