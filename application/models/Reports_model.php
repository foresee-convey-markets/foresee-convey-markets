<?php

class Reports_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        ini_set("memory_limit", "500M");
        $this->load->database("");
    }    
    public function get_top_latest_reports($limit) {
        $this->db->select('rep_sub_cat_1_id,mr_report.id as rep_id,rep_url,rep_title,rep_entry_date');
        $this->db->from('mr_report');
        // $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array("status" => 1,"archive_status" => 0));
        $this->db->order_by("rep_entry_date","DESC");
        $this->db->limit($limit);
        return $this->db->get();
    }
   
    public function get_publishers() {
        $this->db->select('*');
        $this->db->from('mr_publisher');
        $this->db->where(array("status" => 1,"archive_status" => 0));
        $this->db->order_by("publisher_name","ASC");
        return $this->db->get();
    }

    /*************** FILTER REPORTS ********************/
    
    public function get_country($region){
        $this->db->select("*");
        $this->db->from("mr_country");
        $this->db->where(array("continent"=>$region));
        //$this->db->order_by("country","desc");
        return $this->db->get();
    }

    public function get_all_country(){
        $this->db->select("*");
        $this->db->from("mr_country");
        $this->db->order_by("mr_country.country","asc");
        return $this->db->get();
    }
    
    public function get_region(){
        $this->db->select("*");
        $this->db->from("mr_regions");
        $this->db->order_by("region","asc");
        return $this->db->get();
    }

    /* --------------- CATEGORY REPORTS & FILTER --------------- */

    public function get_category_reports($where = NULL, $where_between = null , $orderarray = null,$region_data = array() , $limit = NULL, $start = NULL) {
        $this->db->select('mr_report.id as rep_id,rep_price,rep_entry_date,rep_url,rep_descrip,rep_sub_cat_1_id,rep_title,rep_date,sc1_url,mr_sub_cat_1.id as sc1_id,sc1_name,publisher_name,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = mr_report.rep_sub_cat_1_id', 'left');
        $this->db->join('mr_publisher', 'mr_publisher.id = mr_report.publisher_id','left');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        $this->db->where("mr_report_license.li_key = 'Single User Price' ");
        if(!empty($region_data)){
            $this->db->where('match(rep_title) against("'.$region_data['country'].','.$region_data['region'].'")',null,false);
        }

        if ($where):
            $this->db->where($where);
        endif;
		
		if($where_between){
			foreach($where_between as $k=>$value){
				$this->db->where($value, NULL, FALSE );
			}
		}
        $this->db->where(array('mr_report.status' => 1, 'mr_report.archive_status' => 0));
        $this->db->order_by($orderarray);
        if ($limit)
        {
            $this->db->limit($limit, $start * $limit);
        }
        return $this->db->get();
    }

    /* ------------------ LATEST REPORTS & FILTER  --------------- */
    
    public function get_latest_reports($where = NULL, $where_between = null , $orderarray = null,$region_data = array() , $limit = NULL, $start = NULL) 
    {
        $this->db->select('mr_report.id as rep_id,rep_entry_date,rep_url,rep_descrip,rep_title,rep_date,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        $this->db->where("mr_report_license.li_key = 'Single User Price' ");
        if(!empty(@$region_data)){
            $this->db->where('match(rep_title) against("'.$region_data['country'].','.$region_data['region'].'")',null,false);
        }       
        $this->db->where(array('status' => 1, 'archive_status' => 0));

        if(@$where):
            $this->db->where($where);
        endif;
		
		if(@$where_between){
			foreach($where_between as $k=>$value){
				$this->db->where($value, NULL, FALSE );
			}
		}

        $this->db->where(array('mr_report.status' => 1, 'mr_report.archive_status' => 0));
        $this->db->order_by(@$orderarray);
        if (@$limit)
        {
            $this->db->limit(@$limit, @$start * @$limit);
        }
        return $this->db->get();
    }
    
    /* --------------------------------- REPORT DETAILS ------------------------------------- */


    public function get_report_data($rep_id){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_entry_date,rep_sub_cat_1_id,publisher_id,rep_title,rep_page,rep_toc_fig,rep_contents,rep_table_of_contents,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_metadata', 'mr_report_metadata.meta_rep_id = mr_report.id','left');
        // $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id =mr_report.rep_sub_cat_1_id','left');
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array('mr_report.id' => $rep_id ,'status' => 1, 'archive_status' => 0));
        return $this->db->get()->row();
    }   

    public function get_report_license($id){
        $this->db->select('*');
        $this->db->from('mr_report_license');
        $this->db->where(array('li_rep_id' => $id));
        return $this->db->get();
    }
    
    public function get_related_reports($category_id='',$rep_id=0,$limit=0){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_sub_cat_1_id,rep_title,rep_date,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->where("mr_report_license.li_key = 'Single User Price' and mr_report.rep_sub_cat_1_id='$category_id'");
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where("mr_report.id != ".$rep_id);        
        $this->db->where(array('status' => 1, 'archive_status' => 0));
        $this->db->order_by("rep_date","desc");
        $this->db->limit($limit);
        
        return $this->db->get();
    }

    /* ---------------- REQUEST SAMPLE FORM ------------------- */

    public function get_report_form_data($rep_id){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_date,rep_sub_cat_1_id,mr_report.publisher_id,rep_title,mr_report.rep_page,mr_report.rep_toc_fig,category_image');
        $this->db->from('mr_report');
        // $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id =mr_report.rep_sub_cat_1_id','left');
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array('mr_report.id' => $rep_id, 'status' => 1, 'archive_status' => 0));
        return $this->db->get()->row();
    }

    public function get_report_checkout($id){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_sub_cat_1_id,rep_title,publisher_id,rep_page,category_image');
        $this->db->from('mr_report');
        // $this->db->join('mr_sub_cat_1', 'mr_report.rep_sub_cat_1_id=mr_sub_cat_1.id','left');
        $this->db->join("mr_report_images","mr_report.rep_sub_cat_1_id=mr_report_images.category_id","left");
        $this->db->where(array('mr_report.id' => $id));
        $this->db->where(array('status' => 1, 'archive_status' => 0));
        return $this->db->get()->row();
    }

    /* ---------------- RSS & SITEMAP ---------------------- */  
    
    public function get_top_list_rss($limit){
        $this->db->select('mr_report.id as rep_id,rep_url,rep_descrip,rep_title,rep_date,sc1_name,li_value');
        $this->db->from('mr_report');
        $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = mr_report.rep_sub_cat_1_id', 'left');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->where("mr_report_license.li_key = 'Single User Price'");
        $this->db->where(array("mr_report.status" => 1,"mr_report.archive_status" => 0));
        $this->db->order_by("rep_date","DESC");
        $this->db->limit($limit);
        return $this->db->get();
    }
    
	public function count_reports(){
        $this->db->select("id");
        $this->db->from("mr_report");        
        $this->db->where(array('status' => 1, 'archive_status' => 0));
        return $this->db->get();
    }
	public function get_sitemap_reports($offset,$limit) {
        $this->db->select('id as rep_id,rep_url,rep_date');
        $this->db->from('mr_report');
        $this->db->where(array("status" => 1,"archive_status" => 0));				
        $this->db->order_by("id","DESC");
        if($limit !== NULL):
        $this->db->limit($limit,$offset);
        endif;
        return $this->db->get();
    }    

    public function get_meta_info($url){
        $this->db->select("*");
        $this->db->from("mr_seo");
        $this->db->where(array("page_url"=>$url));
        return $this->db->get()->row();
    }
       
    //*******************   GLOBAL CHECKOUT     **************//
    
    public function get_checkout_details($id){
        $this->db->select('*');
        $this->db->from('mr_global_checkout');
        $this->db->where(array('id'=>$id,'status' => '1'));
        return $this->db->get()->row();
    }
}

?>