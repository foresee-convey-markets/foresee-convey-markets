<?php

class Press_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        ini_set("memory_limit", "500M");
        $this->load->database("");
    }

    //*********************      PRESS RELEASE          ***********************************//

    public function get_press_list($limit)
    {
        $this->db->select('title,content,published_on,image,url,cat_id');
        $this->db->from('mr_press');
        $this->db->where(array('status' => 1,"archive_status"=> 0));

        if ($limit) {
            $this->db->limit($limit);
        }
        return $this->db->get();
    }

    public function get_press_releases($where = NULL, $where_between = null , $orderarray = null, $limit = NULL, $start = NULL)
    {
        $this->db->select('title,content,published_on,image,url,cat_id');
        $this->db->from('mr_press');        

        if ($where):
            $this->db->where($where);
        endif;
		
		if($where_between){
			foreach($where_between as $k=>$value){
				$this->db->where($value, NULL, FALSE );
			}
		}
        $this->db->order_by($orderarray);
        $this->db->where(array('status' => 1,"archive_status"=> 0));

        if ($limit) {
            $this->db->limit($limit, $start * $limit);
        }
        return $this->db->get();
    }

    public function press_details($press_url){
        $this->db->select('title,content,published_on,image,url,cat_id');
        $this->db->from('mr_press');
        $this->db->where(array('status' => 1,"archive_status"=> 0, 'url' => $press_url));
        return $this->db->get()->row();
    }
    

    //*********************      BLOGS          ***********************************//
    
    public function get_blogs($where = NULL, $orderarray = 'blog_date DESC', $limit = NULL, $start = NULL)
    {
        $this->db->select('blog_title,blog_content,blog_date,blog_img,blog_url,blog_cat_id');
        $this->db->from('mr_blogs');
        if ($where):
            $this->db->where($where);
        endif;
        if ($orderarray):
            $this->db->order_by($orderarray);
        endif;
        $this->db->where(array("status" => 1,"archive_status" => 0));

        if ($limit) {
            $this->db->limit($limit, $start * $limit);
        }
        return $this->db->get();
    }

    public function blog_details($url) 
    {
        $this->db->select('blog_title,blog_content,blog_date,blog_img,blog_url,blog_cat_id');
        $this->db->from('mr_blogs');
        $this->db->where(array("status" => 1,"archive_status" => 0, 'blog_url' => $url));
        return $this->db->get()->row();
    }
    
}

?>