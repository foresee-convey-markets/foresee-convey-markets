<?php

class Category_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database('');
    }

    /* ------------------------------------------------------------------------------------ */

    public function get_cat_list($limit='') {
        $this->db->select('sc1_url,sc1_name,id as sc1_id,sc1_image');
        $this->db->from('mr_sub_cat_1');
        $this->db->where(array('status' => 1));
        if(!empty($limit)){
            $this->db->limit($limit);
        }
        $this->db->order_by("sc1_name","asc");
        return $this->db->get();
    }

    /* ------------------------------------------------------------------------------------ */

    public function get_cat_byurl($url) {
        $this->db->select('id as sc1_id,sc1_url,sc1_name,sc1_image');
        $this->db->from('mr_sub_cat_1');
        if (is_numeric($url)) {
            $this->db->where(array('status' => 1, 'id' => $url));
        } else {
            $this->db->where(array('status' => 1, 'sc1_url' => $url));
        }
        return $this->db->get()->row();
    }
    public function get_cat_byurl_filter($url) {
        $this->db->select('id as sc1_id,sc1_url,sc1_name,sc1_image');
        $this->db->from('sub_cat_1');
        $this->db->where(array('status' => 1, 'sc1_url' => $url));
        return $this->db->get()->row();
    }

    /* ------------------------------------------------------------------------------------ */

    public function get_cat_list_id() {
        $this->db->select('*');
        $this->db->from('mr_sub_cat_1');
        $this->db->where(array('status' => 1));
        $this->db->order_by('id', 'ASC');

        return $this->db->get();
    }
     
    public function get_category_list_id($category) {
        $this->db->select('sc1_url,sc1_name,id as sc1_id');
        $this->db->from('mr_sub_cat_1');
        $this->db->where(array('sc1_name'=>$category, 'status'=>1 ));

        return $this->db->get()->row();
    }
    
    /* ------------------------------------------------------------------------------------ */

    public function get_report_cat_id($id) {
        $this->db->select('*');
        $this->db->from('mr_sub_cat_1');
        $this->db->where(array('id'=> $id , 'status' => 1));

        return $this->db->get()->row();
    }

}

?>