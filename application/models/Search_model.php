<?php

class Search_model extends CI_Model {

    public function __construct() {
        parent::__Construct();
        $this->load->database('');
    }

    public function insert($data){
        if($this->db->insert("mr_form_contact",$data)){
            return true;
        }
    }
   public function insert_values($values){
        if($this->db->insert("mr_careers",$values)){
            return true;
        }
    }
    
    // search query   
    
    public function get_report_search_result($where = NULL, $where_between = null, $where_like = null , $orderarray = null,$region_data = array() ,$limit, $start) {
        $this->db->select('mr_report.id as rep_id,rep_entry_date,rep_title,rep_url,rep_sub_cat_1_id,rep_descrip,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->where("mr_report_license.li_key = 'Single User Price'");    
        
        if(!empty($region_data)){
            $this->db->where('match(rep_title) against("'.$region_data['country'].','.$region_data['region'].'")',null,false);
        }

        if ($where):
            $this->db->where($where);
        endif;
		
		if($where_between){
			foreach($where_between as $k=>$value){
				$this->db->where($value, NULL, FALSE );
			}
		}
		
		if($where_like){
			foreach($where_like as $key=>$value){
                if(is_array(@$value) && !empty(@$value)){
                    foreach($value as $kk => $vv){
                        $this->db->like($key, $vv, 'both' );
                    }
                }
			}
		}

        $this->db->where(array('mr_report.status' => 1, 'mr_report.archive_status' => 0));
        $this->db->order_by($orderarray);
        $this->db->limit($limit,$start);
        
        return $this->db->get();
    }
    public function get_report_search_ajax($search) {
        $str = explode(' ', $search);

        $this->db->select('mr_report.id as rep_id,rep_title,rep_url');
        $this->db->from('mr_report');

        foreach ($str as $string):
            $this->db->like("rep_title",$string);
        endforeach;
        
        $this->db->order_by("rep_date ","desc");
        $this->db->limit(10);
        
        return $this->db->get();
    }

    public function get_search_category_report($limit=NULL, $start=NULL,$cat_id,$search) {
        $str = explode(' ', $search);

        $this->db->select('mr_report.id,rep_entry_date,rep_sub_cat_1_id,rep_url,rep_descrip,rep_title,rep_status,rep_archive_status,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        $this->db->where(' mr_report_license.li_key="Single User Price"');
        $this->db->where(array("mr_report.rep_sub_cat_1_id"=>$cat_id));
        $this->db->order_by("mr_report.id","DESC");

        foreach ($str as $string):
            $this->db->like("rep_title",$string,'both');
        endforeach;

        if ($limit){
            $this->db->limit($limit, $start * $limit);
        } 
        return $this->db->get();
        
    }

    public function get_search_reports($limit=NULL, $start=NULL,$country,$region,$search) {
        $str = explode(' ', $search);

        $this->db->select('mr_report.id,rep_entry_date,rep_sub_cat_1_id,rep_url,rep_descrip,rep_title,rep_status,rep_archive_status,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        $this->db->where('match(rep_title) against("'.$country.','.$region.'")',null,false);
        $this->db->where(' mr_report_license.li_key="Single User Price"');
        $this->db->order_by("mr_report.id","DESC");

        foreach ($str as $string):
            $this->db->like("rep_title",$string);
        endforeach;

        if ($limit){
            $this->db->limit($limit, $start * $limit);
        } 
        return $this->db->get();
        
    }

    public function get_search_report($limit,$start,$year1,$year2,$price1,$price2,$search) {
        $str = explode(' ', $search);

         $this->db->select('mr_report.id as rep_id,rep_entry_date,rep_url,rep_descrip,rep_sub_cat_1_id,rep_title,sc1_url,mr_sub_cat_1.id as sc1_id,sc1_name,rep_status,rep_archive_status,li_value,category_image');
        $this->db->from('mr_report');
        $this->db->join('mr_sub_cat_1', 'mr_sub_cat_1.id = mr_report.rep_sub_cat_1_id', 'left');
        $this->db->join('mr_report_license', 'mr_report_license.li_rep_id = mr_report.id','left');
        $this->db->join("mr_report_images","mr_report_images.category_id=mr_report.rep_sub_cat_1_id","left");
        if($year1!==NULL && $year2!==NULL){
            $this->db->where('rep_entry_date between "' .$year1.'" and "' .$year2.'" and mr_report_license.li_key = "Single User Price" ');
        }else{
            $this->db->where('li_value between "' .$price1.'" and "' .$price2.'" and mr_report_license.li_key = "Single User Price" ');
        }        
        $this->db->where(array('status' => 1, 'archive_status' => 0));

        //foreach ($str as $string):
            $this->db->like("rep_title",$search);
        //endforeach;

        $this->db->order_by("mr_report.id","DESC");

        if ($limit){
            $this->db->limit($limit, $start * $limit);
        }
        return $this->db->get();
    }

}

?>