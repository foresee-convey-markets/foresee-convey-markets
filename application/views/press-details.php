<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?=$records->title?> | Foresee Convey Markets</title>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="Foresee Convey Markets Press Release, Press Release " />
        <meta name="description" content="Press Release- <?php echo $records->title; ?>"/>
        <meta name="author" content="Foresee Convey Markets"/>


        <!--**********************    HEADER OPEN      ***************************-->

        <?php require_once 'layouts/header.php'; ?>

        <!--**********************    HEADER CLOSE     ***************************-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>
        

        <style type="text/css">
            @media screen and (max-width:767px){
                .press-title-block p{
                    font-size: 1.2em;
                    line-height: 1.5;
                }
            }  
            @media screen and (min-width:767px) {
                .press-title-block p{
                    font-size: 2.5vw;
                    line-height: 1.6;
                    text-transform: uppercase;
                }        
            }
        </style>

        <ul class="breadcrumb">
            <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
            <li><a href="<?php echo base_url();?>press-release">Press Release</a></li>
            <li><?php echo $records->title ?></li>
        </ul>

        <div class="latest-report page-banner" align='center'>  
            <h2><span class="fa fa-newspaper fa-2x"></span></h2>
            <h1>PRESS RELEASE</h1><br/>
        </div>

        <div class="press-title-block" align='center'>
            <p><?=@$records->title?></p>
            <div class="press-category-block">
                <h3><strong>Category : </strong> <?=categories()[@$records->cat_id]?> <span class="sm-hidden">| &nbsp;</span>
                    <strong>Published On : </strong> 
                    <?php $date = date_create($records->published_on);
                        echo date_format($date, 'M Y'); 
                    ?>
                </h3>
            </div><br/>
        </div>
        
        <div class="row" style='margin : 0px'>
            <div class="col-md-9">
                <div class="col-md-12">
                    <div class="press-desc">                        
                        <img class="img img-responsive" src="<?php echo base_url(); ?>assets/images/press/<?php echo $records->image ?>"  alt="<?php echo $records->image ?>" />
                        <div><?=getPressContent($records->content); ?></div>
                    </div> 
                </div>
            </div>
            <div class="col-md-3 sticky">
                <div class='content-block-shadow'>
                    <div class="row info-block press-info-block">
                        <div class="col-md-12" align='center'>
                            <h3 class='report-heading'>Contact Us </h3>
                            <div class="info-block-row">
                                <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                                <h4>LOCATION</h4>
                                <p>12-23 ABC Street, Convey Garden,
Foresee, XY2Z 8PQ</p><br/>
                            
                                <h1 class='fa fa-phone fa-2x icon-home'></h1>
                                <h4>CALL US</h4>
                                <p>International : <a href='tel:+18442677928'>+1-844-267-7928</a> </p><br/>
                            
                                <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                                <h4>EMAIL US</h4>
                                <p><a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p><br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(function(){
                var right_side_height=$('.home-contact-block').height();
                var col_9_height=$('.col-9').height();
                if(col_9_height < right_side_height){
                    $('.col-9').css({ height : right_side_height});
                    $('.press-desc').css({  'line-height' : '2' });
                }                    

                $('.press-desc').children('blockquote').attr('class','blockquote')
                $(".press-desc").each(function(){
                   console.log($(this).children('p').text().length);
                })
                if($('.press-desc p').text() === '&nbsp;'){
                    console.log($('.press-desc p').text())
                }
            })
        </script>

        

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->



