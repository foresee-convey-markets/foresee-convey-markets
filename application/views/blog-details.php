<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Blogs | Foresee Convey Markets</title>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="Foresee Convey Markets Press Release, Press Release " />
        <meta name="description" content="Press Release- <?php echo $records->p_title; ?>"/>
        <meta name="author" content="Foresee Convey Markets"/>

        <style type="text/css">
           
            @media screen and (min-width:991px){                
                .press-block{
                    margin-top: 3%;
                }
            }
            
            @media screen and (max-width:767px){
                .container,.row,.col-md-12{
                    margin:0;
                    padding:0;
                }
                .col-md-12  p  strong  a{
                    float: left;
                    overflow: scroll;
                    width: 100%;
                }
            }           
            .press-title{
                color: #000;
                
                font-size: 1.3em;
                line-height: 26px;
                font-weight: bold;
            }
            .press-desc{
                text-align:justify;
                padding: 25px;
                font-size:1.2em;
                font-family:Poppins;
                width: 100%;
            }
            .calendar{
                border: 2px solid #fff;
                width: 95px;
                height: 70px;
                border-radius: 12px;
                font-size: 1.1em;
            }
            .calendar-top{
                height: 28px;
                background: #090d2b;
                border-radius: 12px 12px 0px 0px;
                padding: 5px;

            }
            .calendar-body{
                border-style: solid;
                border-width: 0px 5px 5px 5px;
                border-color: #090d2b;
                height:55px; 
                border-radius:  0px 0px 12px 12px;
                background: lightgrey;
                padding: 7px;
                
            }
            
           
        </style>


        <!--**********************    HEADER OPEN      ***************************-->

        <?php require_once 'layouts/header.php'; ?>

        <!--**********************    HEADER CLOSE     ***************************-->
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>" style="font-size:20px"><img src="<?php echo base_url();?>assets/images/home.jpg" height="20px" width="30px"/></a></li>
            <li><a href="<?php echo base_url();?>blogs">Blog</a></li>
            <li><?php echo $records->p_title ?></li>
        </ul>
        
        <div style="margin-bottom:20px;text-align: center;color: #000;">
            <h1>BLOG</h1>
            <hr style="border-bottom: 2px dotted chocolate;width:330px"/>            
        </div> 
        <br/>
        
        <div class="container-fluid">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive img-thumbnail" src="<?php echo base_url(); ?>assets/images/press/<?php echo $records->p_img ?>" style="height:200px;width:200px;" alt="<?php echo $records->p_img ?>" />
                    </div>
                    <div class="press-block">
                        <div class="col-md-7">
                            <h4 class="press-title"><big><?php echo ucfirst($records->blog_title); ?></big></h4>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="calendar">
                            <div class="calendar-top">
                                <span class="year" style="color:white"><?php $date=date_create($records->blog_date);  echo date_format($date, 'Y') ?></span>
                            </div>
                            <div class="calendar-body">                                    
                                <span><?php $date=date_create($records->blog_date);  echo date_format($date, 'd') ?></span>
                                <span><?php $date=date_create($records->blog_date);  echo date_format($date, 'F') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="content-block-shadow">
                        <div class="press-desc">
                            <?php echo $records->blog_content; ?>
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->



