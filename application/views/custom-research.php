
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Custom Research | Foresee Convey Markets';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Custom Research Service Foresee Convey Markets, Custom Research Method in Market,  Custom Research';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'We,at Foresee Convey Markets,are here to listen to all customers queries and offer them satisfactory outputs.Fill the form here and avail reports of your choice and budget from our custom research service.';?>"/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/captcha.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>

<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
    });
</script>
<script type="text/javascript">    
// Form validation     
    function validate_captcha(){
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code){
           CaptchaError.innerHTML="Invalid Captcha Code";
           return false;
        }else{
            CaptchaError.innerHTML="";
            return true;
        }
    }
</script>
<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Custom Research Service</li>
</ul>

<div class="page-banner" align='center'>
    <h2><span class="fa fa-search-dollar fa-2x"></span></h2>
    <h1>Custom Research Service</h1><br/>
</div>    

<div class="page-contact-block"><br/>
    <div class="row" style='margin: 0px'>
        <div class="col-md-6 col-contact">
            <div class="content-block-shadow">
                <div class="panel-body">
                    <p class="form-text">*Please fill the form below to customize your search !</p>
                    
                    <form  id="form" role="form" action="<?php echo base_url(); ?>custom-research-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()" >
                        <div class="form-group">
                            <label class="control-label">Name <span class="star">*</span> :</label>
                            <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" value="<?=@$name?>" data-error="Please enter full name" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Business Email <span class="star">*</span> :</label>
                            <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" value="<?=@$email?>" data-error="Please enter valid email id" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Company <span class="star">*</span> :</label>
                            <input class="form-control" name="company" id="company" placeholder="Company Name" data-bv-field="company" type="text" value="<?=@$company?>" data-error="Please enter company / organization name"   required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Job Role <span class="star">*</span> :</label>
                            <input class="form-control" name="job_role" id="job_role" value="<?=@$job_role?>" placeholder="Job Role" data-bv-field="job_role" type="text" data-error="Please mention job role"  required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Country <span class="star">*</span> :</label>
                            <select name="country" class="form-control required selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="This field can not be blank"  required>
                                <?=getCountryDropdown()?>
                            </select>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Contact Number <span class="star">*</span> :</label>
                            <input class="form-control" pattern="^[0-9]{10}$"  name="phone" id="phone" value="<?=@$phone?>"  maxlength="10" placeholder="Contact Number without country code"  data-bv-field="mobile_no" type="text" data-error="Please enter valid contact number" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Requirements <span class="star">*</span> :</label>
                            <textarea data-error="Please mention your custom research requirements" placeholder="Your Requirements" rows="5" value="<?=@$contact_msg?>"  class="form-control" name="message" id="message" data-bv-field="message"  required><?=@$contact_msg?></textarea>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Budget <span class="star">*</span> :</label>
                            <select data-error="Please specify Your Budget" rows="5"  class="form-control" name="budget" id="budget" data-bv-field="budget"  required>
                                <option value=''>Select</option>
                                <option value='0-5000'>$0 - $5000</option>
                                <option value='5000-10000'>$5000 - $10000</option>
                                <option value='10000-20000'>$10000 - $20000</option>
                            </select> 
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="security_code">Captcha Code: <span class="star">*</span>: </label><br/>
                            <div class="input-group" style="padding-right:0;">
                                <input data-error="Please enter captcha code" onkeyup="validate_captcha()"  type="text" name="captcha_code" maxlength="6" placeholder="Captcha Code" class="form-control" id="security_code" required/>
                                <input type="text" id="txtCaptcha" readonly class="form-control" />
                                <span class="input-group-btn">
                                    <button class="btn btn-secondary btn-captcha" type="button" onClick="DrawCaptcha();"> <span class="fa fa-sync"></span> </button>
                                </span> 
                            </div><br/>
                            <p style="color:#f00" id="CaptchaError"></p>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div>
                            <button type="submit" class="btn btn-submit">SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 sticky col-contact">
            <div class='content-block-shadow'>
                <div class="row info-block">
                    <div class="col-md-12" align='center'>            
                        <h3 class='report-heading'>Why FCM?</h3>
                    
                        <div class="info-block-row">
                            <h1 class='fa fa-lock fa-2x icon-home'></h1>
                            <h4>RELIABLE AND PROTECTED</h4>
                            <p>Your sensitive information and data  <strong>are <br/>secure</strong> with us.</p>
                        
                            <h1 class='fa fa-globe fa-2x icon-home'></h1>
                            <h4>GLOBAL CHOICE</h4>
                            <p>Get access to over <strong>1M+ Forecast</strong><br/><strong>Research Reports</strong></p>

                            <h1 class='fa fa-users fa-2x icon-home'></h1>
                            <h4>CUSTOMER ORIENTED</h4>
                            <p>We are available <strong>24*7</strong> <br/>for <strong class='blue'>365 days</strong> in a year</p>
                        
                            <h1 class='fa fa-handshake fa-2x icon-home'></h1>
                            <h4>TRUSTED BY ALL</h4>
                            <p><strong>500+ Contented Clients </strong> Globally.</p>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='home-contact-block'>
    <div class="row info-block static-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Contact</h3>
            <img class="img img-responsive logo contact-logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
        </div>
        <div class="col-md-9">
            <div class="info-block-row"><br/>
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                        <h4>LOCATION</h4>
                        <p>1445 Woodmont Ln NW, Atlanta, Ga 30318</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-phone fa-2x icon-home'></h1>
                        <h4>CALL / EMAIL US</h4>
                        <p><a href='tel:+18442677928'>+1-844-267-7928</a> <br/>  <a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                        <h4>CONTACT</h4>
                        <a class='btn-report' href="<?=base_url()?>contact-us" title='Contact Us'>Contact Us</a>
                    </div>
                </div><br/>
            </div>
        </div>
    </div>
</div>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
