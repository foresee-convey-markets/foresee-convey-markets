<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo isset($meta->title) ? $meta->title : 'Thanks | Bank Transfer';?></title>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : '' ;?> " />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Thank you for contacting Foresee Convey Markets . We will get Back to you soon.';?>">
<meta name="author" content="Foresee Convey Markets"/>

<link href='https://fonts.googleapis.com/css?family=Asul' rel='stylesheet'/>


<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<style>
    a.btn-home{
        border-radius: 0px;
        font-size:1em;
        background: #000;
        text-transform: uppercase;
    }
    h1.thank-you{
        color: #0465ac;
        font-size: 3.5em;
        font-weight: bold;
    }
    .container .jumbotron{
        border-radius: 0px;
        background: #eee;
    }
</style>

<br/>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <article>
                <div class="jumbotron text-xs-center">
                    <h1 class="thank-you">ORDER RECEIVED !</h1>
                    <p class="lead">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your Order will not be shipped until the funds have cleared in our account.</p>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <tr class="text-primary">
                                <th>Order ID</th>
                                <th>Report Title</th>
                                <th>Total</th>
                                <th>Date</th>
                                <th>Payment Mode</th>
                            </tr>
                            <tr>
                                <td><?php echo $order_data->order_id;  ?></td>
                                <td><?php echo $order_data->report_title;  ?></td>
                                <td><?php echo $order_data->amount;  ?></td>
                                <td><?php echo $order_data->date;  ?></td>
                                <td>Bank Transfer</td>
                            </tr>
                        </table>
                    </div><br/><br/>

                    <div style="padding: 10px;">
                        <h3 class="text-primary">Our Bank Details</h3><br/>
                        <h4 class="text-info">Vatt Enterprise</h4>
                        <br/>
                        <p>Bank : MAH</p>
                        <p>Account : 60322886289</p>
                        <p>IFSC : MAHB000615</p>
                    </div><br/><br/>

                    <p>
                        Having trouble? <a href="<?php echo base_url(); ?>contact-us">Contact us</a>
                    </p>
                    <p class="lead">
                        <a class="btn btn-primary btn-md btn-home" href="<?php echo base_url(); ?>" role="button">Continue To Homepage</a>
                    </p>
                </div>
            </article>
        </div>
    </div>
</div>
    

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->