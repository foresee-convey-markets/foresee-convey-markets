<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Terms and conditions | Foresee Convey Markets';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Terms and conditions Foresee Convey Markets';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Terms and conditions Foresee Convey Markets';?>"/>
<meta name="author" content="Foresee Convey Markets"/>
  
<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>
  
 
<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Terms &amp; Conditions</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-file-contract fa-2x"></span></h2>
    <h1>TERMS &amp; CONDITIONS</h1><br/>
</div> 

<div class="container-fluid page-container">
    <div class="content-block col-content">
        <div class='row' style='margin : 0px'>
            <div class='col-md-12'>
                <!-- <p class="terms"><strong>Terms Of Use</strong></p>
                <h4>Subject to those Terms of Use (this "Agreement"), our website and/or its subsidiaries, as applicable, (collectively and individually, "we", "our" or "us") make available certain on-line information and services on various websites ("Our Service") to registered and/or authorized users ("you" or "your"). Our Service presents information, data, content, news, reports, programs, video, audio and other materials and services, communications, transmissions and other items, tangible or intangible, which are brought up as "Material." Your use of Our Service constitutes your acknowledgment of and assent to be bound by this Agreement.</h4> -->
                
                <p class="terms"><strong>TERMS OF USE:</strong></p>
                
                <h4>Subject to those Terms of Use (this "Agreement"), foreseeconvey-markets.com and/or its subsidiaries, as applicable, (collectively and individually, "we", "our" or "us") make available certain on-line information and services on various websites ("Our Service") to registered and/or authorized users ("you" or "your"). Our Service presents information, data, content, news, reports, programs, video, audio and other materials and services, communications, transmissions and other items, tangible or intangible, which are mentioned as "Material." Your use of Our Service constitutes your acknowledgment of and assent to be bound by this Agreement.</h4>
                <h4>Unless there's another agreement between you and us that covers your use of part or all of Our Service, this can be the whole agreement between you and us. If there's another legal document between you and us that covers your use of a part of Our Service, this Agreement covers all other use of Our Service by you. Whenever new products or services become available, your use of them are going to be under this Agreement unless we notify you otherwise or another instrument covers your use of these new products or services.</h4>
                <h4>If upon reading our terms and conditions, you discover you're ineligible or unable to adjust to them through circumstances outside your control, then you're hereby required to contact foreseeconvey-markets.com within twenty-four (24) hours of purchase (in the case of Instant Online Delivery content) or receipt (in the case of other content) to elucidate your ineligibility. An appropriate solution are discovered at that point. If a call isn't received within the allotted time, you're deemed to possess approved and to be in compliance with the extra terms and conditions.</h4>
                <h4>We may change this Agreement at any time and you'll read a current copy of this Agreement at any time by selecting the "Legal Notice" link on Our Service. If any change isn't acceptable, you want to discontinue your use immediately; using Our Service after the date that this Agreement changes means you accept the changes. No change to the current Agreement that's not posted on the Service is valid unless it's in writing and signed by both you and us.</h4>

                <p class="terms"><strong>Permitted Use, Limitations on Use:</strong></p>
                <h4>You may access and download the Material as required to view the Material on your web browser for your individual use, keeping all copyright and other notices on the Material. you will print one copy of Material for your use. you'll not republish or distribute any Material or do anything with the Material, which isn't specifically permitted during this Agreement. You conform to accommodates all notices and requirements accompanying Third-Party Content (see paragraph 3 under Terms of Use).</h4>
                
                <p class="terms"><strong>Anti-hacking Provision:</strong></p>
                <h4>You may not, nor may you permit others to, directly or indirectly: (a) try to or actually disrupt, impair or interfere with, alter or modify Our Service or any Material; or (b) collect or try and collect any information of others, including passwords, account or other information.</h4>
                

                <p class="terms"><strong>No Advice:</strong></p>
                <h4>The Material available on Our Service is for informational purposes only.</h4>

                <p class="terms"><strong>Links to 3rd Party Sites:</strong></p>
                <h4>Various links on Our Service will take you out of Our Service. These linked sites don't seem to be necessarily under our control. We aren't to blame for the contents of any linked page or the other page not under our control. we offer these links only as a convenience; the inclusion of a link doesn't imply endorsement of that linked site.</h4>

                <p class="terms"><strong>Limitation of Liability:</strong></p>
                <h4>You are entirely responsible for activities conducted by you or anyone else in reference to your browsing and use of Our Service. If you're dissatisfied with the Material or Our Service or with these Terms of Use, your sole and exclusive remedy is to prevent using the Material and Our Service. we are going to not pay you any damages.</h4>

                <h4>We don't warrant the accuracy, completeness or other characteristics of any Material available on or through Our Service. we are going to not be chargeable for any loss or injury resulting directly or indirectly from Our Service, whether or not caused in whole or partially by our negligence or by contingencies within or beyond our control. Neither we, nor suppliers of Third-Party Content, are responsible or liable, directly or indirectly, for any loss or damage caused by use of or reliance on or inability to use or access Our Service or the Material.</h4>

                <h4>YOUR ACCESS TO AND USE OF OUR SERVICE ARE AT YOUR SOLE RISK. OUR SERVICE IS PROVIDED "AS IS" AND "AS AVAILABLE." OUR SERVICE IS FOR YOUR PERSONAL USE ONLY and that we MAKE NO REPRESENTATION OR WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. WE EXPRESSLY DISCLAIM ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR a selected PURPOSE OR USE.</h4>

                <h4>WE don't seem to be and can NOT BE a celebration TO ANY TRANSACTION BETWEEN YOU AND ANY THIRD PARTY, WHETHER OR NOT THAT THIRD PARTY'S WEBSITE IS LINKED FROM OUR SERVICE.</h4>

                <p class="terms"><strong>Governing Law:</strong></p>
                <h4>The laws of the State of latest York govern this Agreement and your use of Our Service. You conform to go with all laws, regulations, obligations and restrictions, which apply to you. You agree that the courts located in big apple have exclusive jurisdiction for any claim, action or dispute under this Agreement. you furthermore may agree and expressly consent to the exercise of private jurisdiction within the State of recent York. No failure or delay in enforcing any right shall be a waiver of that or the other right. If any term of this Agreement is held invalid, illegal or unenforceable, the remaining portions shall not be affected.</h4>

                <p class="terms"><strong>Copyright:</strong></p>
                <h4>Unless specifically stated in conjunction with particular Material, all Material is copyrighted by us. you have got no rights in or to the Material and you will not use any Material apart from as permitted under this Agreement.</h4>

                <p class="terms"><strong>Trademark:</strong></p>
                <h4>All trade names, trademarks, service marks and other product and repair names and logos on Our Service or within the Material are the proprietary trademarks of their respective owners and are protected by applicable trademark and copyright laws.</h4>

                <p class="terms"><strong>Additional Agreements:</strong></p>
                <h4>In the event that you simply or your company have signed a right away contract with foreseeconvey-markets.com ("Agreement") and there exist terms which conflict between this Terms and Conditions Document and your signed Agreement for this service, the terms of your signed Agreement shall supersede this Terms and Conditions Document.</h4>
                
                <p class="terms"><strong>Return Policy:</strong></p>
                <h4>Due to the character of the knowledge being sold, we unfortunately cannot accept returns of products once they need been delivered.<br/>
                    Please make sure to read all available information a few report before you place your order.<br/>
                    If you've got any questions about a report's coverage or relevance, simply contact us for expert assistance from a look Specialist.</h4>

                <p class="terms"><strong>Contact  Information</strong></p>
                <h4>Questions about the Terms of Service should be sent to us at <a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></h4>
            </div>
        </div>
    </div>
</div>
<div class='home-contact-block'>
    <div class="row info-block static-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Contact</h3>
            <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
        </div>
        <div class="col-md-9">
            <div class="info-block-row"><br/>
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                        <h4>LOCATION</h4>
                        <p>1445 Woodmont Ln NW, Atlanta, Ga 30318</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-phone fa-2x icon-home'></h1>
                        <h4>CALL / EMAIL US</h4>
                        <p><a href='tel:+18442677928'>+1-844-267-7928</a> <br/>  <a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                        <h4>CONTACT US</h4>
                        <a class='btn-report' href="<?=base_url()?>contact-us" title='Contact Us'>Contact Us</a>
                    </div>
                    <div class="col-md-6 cr-block" align="center">
                        <h1 class='fa fa-search fa-2x icon-home'></h1>
                        <h4>CUSTOM RESEARCH</h4>
                        <a class='btn-report' href="<?=base_url()?>custom-research" title='Custom Research'>Custom Research</a>
                    </div>
                </div><br/>
            </div>
        </div>
    </div>
</div>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
