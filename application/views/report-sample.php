<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=@$form_type?> - <?php echo $report_data->rep_title; ?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?=@$form_type?> Foresee Convey Markets,<?=@$form_type?> " />
<meta name="description" content="<?=@$form_type?> Request for Report - <?php echo $report_data->rep_title; ?>"/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/RD-structure.css"/>
<script src="<?php echo base_url(); ?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/captcha.js"></script>

<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>


<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
    
        var w=$(window).width();
        if(w>767){
            $('html,body').animate({
                scrollTop: $('.page-banner').offset().top        
            }, 'slow');
        }
    });
        
    function validate_captcha(){
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code){
            CaptchaError.innerHTML="Invalid Captcha Code";
            return false;
        }else{
            CaptchaError.innerHTML="";
            return true;
        }
    }
</script>
<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li><a href="<?php echo base_url(); ?>reports/<?php echo $report_data->rep_id; ?>/<?php echo $report_data->rep_url; ?>" title="Go Back"><span class="fa icon fa-backward"></span></a></li>
    <li title="<?php echo $report_data->rep_title; ?>"><?=@$form_type?></li>
</ul>
    
<div class="page-banner"> 
    <div class="row">
        <div class="col-md-8 rd-block">
            <h2 class="request-sample"><?=@$form_type?></h2>
            <h2 class="report-title <?=strlen($report_data->rep_title) <= 200 ? 'report-sm-title' : '' ?>"><?php echo $report_data->rep_title; ?></h2>
        </div>
        <div class="col-md-4 feature-block" align='center'>
            <div class="row">
                <div class="col-md-5 col-feature">
                    <img class="img img-responsive img-rounded report-img" style='height: 150px' src="<?php echo base_url(); ?>assets/images/category/<?php echo $report_data->category_image; ?>" alt="Report Details"/>
                </div>
                <div class="col-md-7 report-details">
                    <p><strong>ID</strong> : FCM-<?=$report_data->rep_id?></p>
                    <p><strong>Pages</strong> : <?=$report_data->rep_page?></p>
                    <p><strong>Publisher</strong> : <?=get_publishers($report_data->publisher_id)->publisher_code?></p>
                    <p title="Report is available in Excel, PDF, Powerpoint and Word Formats">
                        <strong>Format</strong> : 
                        <span class="fas fa-file-excel"></span>&nbsp;
                        <span class="fas fa-file-pdf"></span>&nbsp;
                        <span class="fas fa-file-powerpoint"></span>&nbsp;
                        <span class="fas fa-file-word"></span>&nbsp;
                    </p>
                    <p><strong>Industry</strong> : <a href="<?=base_url()?>category/<?=category_details()[@$report_data->rep_sub_cat_1_id]['sc1_url']?>"><?=categories()[@$report_data->rep_sub_cat_1_id]?></a></p>
                </div>
            </div><br/>
        </div>
    </div>
</div> 
<div class="row">
    <div class="col-md-12">
        <div class="report-tab-empty">                 
            <p class='contact-heading' align='center'>*Please fill the form below to <?=strtolower(@$form_type)?> !</p>
            <div class="col-md-6 col-md-offset-1 col-contact">                       
                <div class="content-block-shadow">
                    <div class="panel-body">
                        <!-- <p class="form-text">*Please fill the form below to <?=strtolower(@$form_type)?> !</p> -->
                        <form class="form-horizontal request-form" id="form" role="form" action="<?php echo base_url(); ?>report-form-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()" >
                            <input name="rep_title" value="<?=$report_data->rep_title?>" type="hidden" />
                            <input name="form_type" value="<?=@$form_type?>" type="hidden" />

                            <div class="form-group">
                                <label class="control-label">Name <span class="star">*</span> :</label>
                                <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" value="<?=@$name?>" data-error="Please enter your name" required/>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Business Email <span class="star">*</span> :</label>
                                <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" value="<?=@$email?>" data-error="Please enter valid email address" required/>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Company <span class="star">*</span> :</label>
                                <input class="form-control" name="company" id="company" placeholder="Company Name" data-bv-field="company" type="text" value="<?=@$company?>" data-error="Please enter organization name"   required/>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Job Role <span class="star">*</span> :</label>
                                <input class="form-control" name="job_role" id="job_role" value="<?=@$job_role?>" placeholder="Job Role" data-bv-field="job_role" type="text" data-error="Please enter your job role"  required/>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Country <span class="star">*</span> :</label>
                                <select name="country" class="form-control required selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="Please select your country"  required>
                                    <?=getCountryDropdown()?>
                                </select>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Contact Number <span class="star">*</span> :</label>
                                <input class="form-control" pattern="^[0-9]{10}$"  name="phone" id="phone" value="<?=@$phone?>"  maxlength="10" placeholder="Contact Number without country code"  data-bv-field="mobile_no" type="text" data-error="Please enter valid contact number" required/>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Your Message <span class="star">*</span> :</label>
                                <textarea data-error="Please enter your message" placeholder="Your Message" rows="5" value="<?=@$contact_msg?>"  class="form-control" name="message" id="message" data-bv-field="message"  required><?=@$contact_msg?></textarea>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="security_code">Captcha Code: <span class="star">*</span>: </label><br/>
                                <div class="input-group" style="padding-right:0;">
                                    <input data-error="Please enter captcha code" onkeyup="validate_captcha()"  type="text" name="captcha_code" maxlength="6" placeholder="Captcha Code" class="form-control" id="security_code"  required/>
                                    <input type="text" id="txtCaptcha" readonly class="form-control" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary btn-captcha" type="button"  onClick="DrawCaptcha();"> <span class="fa fa-sync"></span> </button>
                                    </span> 
                                </div><br/>
                                <p style="color:#f00" id="CaptchaError"></p>
                                <div class="help-block with-errors"></div> 
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-submit">SUBMIT</button>
                            </div>
                            <div class="form-group" align="center">
                                <p class="form-note">NOTE: Your personal data is secure with us. Please read our <a href="<?php echo base_url(); ?>privacy-policy" target="_blank">privacy policy</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 sticky col-contact">
                <div class='content-block-shadow'>
                    <div class="row info-block press-info-block">
                        <div class="col-md-12" align='center'>
                            <h3 class='report-heading'>Contact Us </h3>
                            <div class="info-block-row">
                                <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                                <h4>LOCATION</h4>
                                <p>1445 Woodmont Ln NW, Atlanta, Ga 30318</p><br/>
                            
                                <h1 class='fa fa-phone fa-2x icon-home'></h1>
                                <h4>CALL US</h4>
                                <p>International : <a href='tel:+18442677928'>+1-844-267-7928</a> </p><br/>
                            
                                <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                                <h4>EMAIL US</h4>
                                <p><a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p><br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
       