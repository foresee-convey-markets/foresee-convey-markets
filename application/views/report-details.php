<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $meta_report_title; ?>Market Share Industry Size Growth Forecast Report <?php echo $report_year; ?></title>
    <!-- Basic -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="robots" content="index,follow"/>
    <meta name="keywords" content="<?php echo $meta_report_title; ?>Market - Global <?php echo $meta_report_title; ?>Market Industry Size, Share, Price, Growth Trends, Application, Research Report, Technology, Forecast, Competitive Analysis, PDF Report" />
    <meta name="description" content="Foresee Convey Markets Brings Premium Market Research Report of <?php echo $meta_report_title; ?>Market with Industry Share, Size, Future Trends and Competitive Analysis"/>
    <meta name="author" content="Foresee Convey Markets"/>
    
    <!--**********************    HEADER OPEN      ***************************-->

    <?php require_once 'layouts/header.php'; ?>

    <!--**********************    HEADER CLOSE     ***************************--> 
    <link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/RD-structure.css" />
    <script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/captcha.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/prefunctions.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/sliker.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slideshow.css"/>

    <!-- *****************   BOOTSTRAP SELECT          ******************  -->
    <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
    <script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".selectpicker").selectpicker();
        })
    </script>
    <style type="text/css">
        blockquote{
            border-left: 0px;
        }    
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".icon").mouseover(function(){
                $(".icon").removeClass("fa-arrow-right").addClass("fa-arrow-left");
            });
            $(".icon").mouseleave(function(){
                $(".icon").removeClass("fa-arrow-left").addClass("fa-arrow-right");
            })
            
        })
        
        function validate_captcha(){
            var text=txtCaptcha.value;
            str = text.replace(/ +/g, "");
            var code=security_code.value;

            if(str !== code){
                CaptchaError.innerHTML="Invalid Captcha Code";
                return false;
            }else{
                CaptchaError.innerHTML="";
                return true;
            }
        }
        
    </script>     
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
        <li><a onclick="window.history.back()" title="Go Back"><span class="fa icon fa-backward"></span></a></li>
        <li><a href="<?=base_url();?>latest-reports">Industry Reports</a></li>
        <li title="<?php echo $report_data->rep_title; ?>"><?php echo strlen($report_data->rep_title) > 150 ? substr($report_data->rep_title,0,150).'....' : $report_data->rep_title; ?></li>
    </ul>
        

    <div class="page-banner"> 
        <div class="row">
            <div class="col-md-8 rd-block">
                <div class="row rd-row">
                    <div class="col-md-9">
                        <h2 class="report-title <?=strlen($report_data->rep_title) <= 200 ? 'report-sm-title' : '' ?>"><?php echo $report_data->rep_title; ?></h2>
                    </div>
                    <div class="col-md-3">          
                        <div class="col-md-12 rd-button">
                            <a target="_blank" href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=request_customization' class="btn btn-md btn-tab btn-customize" >
                                <i class="fa fa-pencil-alt" aria-hidden="true"></i>&nbsp;&nbsp;Request Customization       
                            </a>
                            <a target="_blank"  href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=request_sample' class="btn btn-md btn-tab" >
                                <i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;Request Sample       
                            </a>
                            <a target="_blank"  href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=request_discount' class="btn btn-md btn-tab btn-discount" >
                                <i class="fa fa-money-check-alt" aria-hidden="true"></i>&nbsp;&nbsp;Request Discount       
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 feature-block" align='center'>
                <div class="row">
                    <div class="col-md-5 col-feature">
                        <img class="img img-responsive img-rounded report-img" style='height: 150px' src="<?php echo base_url(); ?>assets/images/category/<?php echo $report_data->category_image; ?>" alt="Report Details"/>
                    </div>
                    <div class="col-md-7 report-details">
                        <p><strong>ID</strong> : FCM-<?=$report_data->rep_id?></p>
                        <p><strong>Date</strong> : <?php $date=date_create($report_data->rep_entry_date);echo date_format($date,"Y-m-d");?></p>
                        <p><strong>Pages</strong> : <?=$report_data->rep_page?></p>
                        <p><strong>Publisher</strong> : <?=get_publishers($report_data->publisher_id)->publisher_code?></p>
                        <p><strong>Industry</strong> : <a href="<?=base_url()?>category/<?=category_details()[@$report_data->rep_sub_cat_1_id]['sc1_url']?>"><?=categories()[@$report_data->rep_sub_cat_1_id]?></a></p>
                    </div>
                </div><br/>
            </div>
        </div>
    </div> 
        
    <div class="row">
        <div class="col-md-8"> 
            <div class="tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a href="#desc" data-toggle="tab" class="text-center">Description</a> </li>
                    <li> <a href="#toc" data-toggle="tab" class="text-center"> Table Of Content</a> </li>
                    <li> <a href="#tof" data-toggle="tab" class="text-center"> Table Of Figures</a> </li>
                    <!--<li> <a href="#summary" data-toggle="tab" class="text-center">  Summary</a> </li>-->
                </ul>
                <div class="tab-content">
                    <div id="desc" class="tab-pane fade in active"><?=nl2br(trim($report_data->rep_contents))?></div>
                    <div id="toc" class="tab-pane fade in"><?=nl2br(trim($report_data->rep_table_of_contents))?></div>
                    <div id="tof" class="tab-pane fade in <?= strlen($report_data->rep_toc_fig) == '0' ? 'tab-empty' : '' ?>">
                        <?php if($report_data->rep_toc_fig !== ""){ 
                                echo nl2br(trim($report_data->rep_toc_fig));
                        }else{ ?>
                        <p class='contact-heading'>Please fill the following form to know more details about this Report</p>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="content-block-shadow" >
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <form class="form-horizontal" id="form" role="form" action="<?php echo base_url(); ?>contact-form-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()" >
                                            <input type='hidden' name='rep_title' value='<?php echo $report_data->rep_title; ?>' />
                                            <div class="form-group">
                                                <label class="control-label">Name <span class="star">*</span> :</label>
                                                <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" data-error="Please enter your name" required/>
                                                <div class="help-block with-errors"></div> 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Business Email <span class="star">*</span> :</label>
                                                <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" data-error="Please enter valid email address" required/>
                                                <div class="help-block with-errors"></div> 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Company <span class="star">*</span> :</label>
                                                <input class="form-control" name="company" id="company" value="" placeholder="Company Name" data-bv-field="company" type="text" data-error="Please enter company / organization name"   required/>
                                                <div class="help-block with-errors"></div> 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Job Role <span class="star">*</span> :</label>
                                                <input class="form-control" name="job_role" id="job_role" value="" placeholder="Job Role" data-bv-field="job_role" type="text" data-error="Please mention job role"  required/>
                                                <div class="help-block with-errors"></div>                                 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Country <span class="star">*</span> :</label>
                                                <select name="country" class="form-control required selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="This field can not be blank"  required>
                                                    <?=getCountryDropdown()?>
                                                </select>
                                                <div class="help-block with-errors"></div> 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Contact Number <span class="star">*</span> :</label>
                                                <input class="form-control"  pattern="^[0-9]{10}$" name="phone" id="phone" value="" maxlength="10" placeholder="Contact Number" data-bv-field="mobile_no" type="text" data-error="Please enter valid contact number" required/>
                                                <div class="help-block with-errors"></div> 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Message <span class="star">*</span> :</label>
                                                <textarea data-error="Please enter your message" placeholder="Your message" rows="5"  class="form-control" name="message" id="message" data-bv-field="message"  required></textarea>
                                                <div class="help-block with-errors"></div> 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="security_code">Captcha Code: <span class="star">*</span>: </label>
                                                <div class="input-group" style="padding-right:0;">
                                                    <input data-error="Please enter captcha code" type="text" name="captcha_code" maxlength="6"   placeholder="Captcha Code" class="form-control" id="security_code" required/>
                                                    <input type="text" id="txtCaptcha" readonly class="form-control" />
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-secondary btn-captcha" type="button" onClick="DrawCaptcha();"> <span class="fa fa-sync"></span> </button>
                                                    </span> 
                                                </div><br/>
                                                <p style="color:#f00" id="CaptchaError"></p>
                                                <div class="help-block with-errors"></div> 
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>                        
                    </div>
                </div>
            </div><br/>            
        </div>
        <div class="col-md-4 license-block"><br class="mdx-hidden"/>
            <div class="panel-purchase">
                <h3>Choose License Type</h3>
                <form class="rd-form" action="<?php echo base_url(); ?>checkout/<?php echo $report_data->rep_id; ?>" name="" method="post">
                    <input type="hidden" name="rep_title" value="<?php echo $report_data->rep_title; ?>" />
                    <input type="hidden" name="rep_id" value="<?php echo $report_data->rep_id; ?>"/>
                    <input type="hidden" name="publisher_id" value="<?php echo $report_data->publisher_id; ?>"/>
                    <div class="row">

                        <div class="col-md-4 col-4 md-hidden" align="center">
                            <br/>
                            <span class="display"></span>
                            <br/><br/>
                        </div>
                        <div class="col-md-8">
                            <ul class="list-group">
                                
                                <?php $i=0; foreach($report_license->result() as $row): ?>
                                <li class="list-group-item">
                                    <?php if($i==0){?>
                                    <label class="container-radio"><?php echo $row->li_key;?> 
                                        <input type="radio" name="report_price" value="<?php echo $row->li_key;?>-<?php echo $row->li_value;?>" checked /><span class="checkmark"></span>
                                    </label>
                                    <?php  }else{ ?>
                                    <label class="container-radio"><?php echo $row->li_key;?> 
                                        <input type="radio" name="report_price"  value="<?php echo $row->li_key;?>-<?php echo $row->li_value;?>" /><span class="checkmark"></span>
                                    </label>
                                    <?php } ?>
                                </li>
                                <?php $i++; endforeach;?>
                            </ul>
                        </div>
                        <div class="col-md-4 col-4 sm-hidden" align="center">
                            <br/>
                            <span class="display"></span>
                            <br/><br/>
                        </div>  
                    </div> 
                    <div align='center'>
                        <button type="submit" name="submit_todo"  class="btn btn-md btn-tab btn-buynow"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;&nbsp;Add To Basket</button> 
                    </div>
                </form>
            </div> 

            <hr class="hr-line"/> 

            <div class="question-block" align='center'>
                <h5><strong>Have A  Question ?</strong></h5><br/>
                
                <a target="_blank" href='<?php echo base_url() ?>contact-us' class="btn btn-md btn-tab btn-email" >
                    <strong><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;Email Us</strong>       
                </a> &nbsp;&nbsp;
                <a target="_blank" href='<?php echo base_url() ?>report/<?=$report_data->rep_id?>?type=enquire_before_purchase' class="btn btn-md btn-tab btn-purchase" >
                    <strong><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;Enquire Before Purchase</strong>       
                </a>
            </div> 
            
            <hr class="hr-line"/>
            
            <div class="testimonial-block"><br/>
                <h5 align="center"><strong>Testimonials</strong></h5>
                <div class="slideshow-container">
                    <div class="mySlides">
                        <blockquote>
                            <q>I am completely satisfied with the information given in the sample pages of the report and I am waiting for the affirmative response from my peers and colleagues to purchase it.hey answered to all my queries promptly. I am highly satisfied!!</q>
                            <footer class="author"> John Keats</footer>
                        </blockquote>
                    </div>
                    <div class="mySlides">
                        <blockquote>
                            <q>We received a very quick response to all our queries on the RTLS report and we were also given a free consult with analyst for 15 minutes. It is refreshing (and increasingly rare) to have a new company go the extra mile to win the confidence of the customer</q>
                            <footer class="author"> Ernest Hemingway</footer>
                        </blockquote>
                    </div>
                    <div class="mySlides">
                        <blockquote>
                            <q>I would like to express my gratitude for the excellent service received this morning from the support team. They acknowledged my concern of adding me to the secondary user's list immediately. Thank you for this excellent, speedy and thoughtful service</q>
                            <footer class="author"> Thomas A. Edison</footer>
                        </blockquote>
                    </div>
                    <a class="prev" onclick="plusSlides(-1)"><i class="fa fa-chevron-circle-left" ></i></a>
                    <a class="next" onclick="plusSlides(1)"><i class="fa fa-chevron-circle-right"></i></a>
                </div>
            </div> 
            
            <hr class="hr-line"/>
            
            <div class="custom-research" align='center'><br/>
                <h5><strong>Custom Research</strong></h5>
                <p>Couldn't find that you're looking for ?<br/> Talk to our Custom Research Team</p>
                
                <a target="_blank" href='<?php echo base_url() ?>custom-research' class="btn btn-md btn-tab btn-email" >
                    <strong><i class="fa fa-search-dollar" aria-hidden="true"></i>&nbsp;&nbsp;Learn More</strong>       
                </a>
            </div> 
            
            <hr class="hr-line"/>
            
            <div class="why-us-block">
                <h3>Why Foresee Convey Markets</h3>
                <ul>
                    <li>Access to over 1M+ Forecast Research Reports</li>
                    <li>500+ Contented Clients Worldwide.</li>
                    <li>24/7 for 365 days a year support on both call and emails.</li>
                    <li>Your sensitive information and data are secure with us.</li>
                    <li>Get unlimited support for your custom research needs.</li>
                    <li>Report Delivery: Email</li>
                    <li>Delivery Time:
                        <ul>
                            <li>Upto 24 hrs - working days</li>
                            <li>Upto 48 hrs max - weekends and public holidays</li>
                        </ul>
                    </li>
                </ul>
            </div> 
            
            <hr class="hr-line"/>

            <div class="why-us-block related-reports">
                <h3>Related Forecast Reports</h3>
                <ul>
                <?php
                if(@$related_reports->result()){
                    foreach($related_reports->result() as $row): ?>
                    <li>
                        <a href='<?php echo base_url(); ?>reports/<?php echo $row->rep_id; ?>/<?php echo $row->rep_url; ?>'><?=substr($row->rep_title,0,100)."..."; ?></a>
                    </li>
                <?php endforeach;
                }else{ 
                ?>                
                    <li style="list-style: none;font-size:1.25em" class="text-center">
                        <span class="text-danger"><strong><span class="fa fa-exclamation-triangle"></span> No Results Found !</strong></span>
                    </li>
                <?php  }  ?>
                </ul>
            </div><br/>        
        </div>
    </div>        
    <!--**********************    FOOTER OPEN      ***************************-->

    <?php require_once 'layouts/footer.php'; ?>

    <!--**********************    FOOTER CLOSE     ***************************-->

    <script type="text/javascript">

    $(document).ready(function(){
        var defaultPrice=$("input[name='report_price']:checked").val();
        defaultPrice=defaultPrice.split('-');
        var display=$('.display').html(defaultPrice[1]+"<br/><span class='currency'>USD</span>");

        $("input[name='report_price']").change(function(){
            var price=$(this).attr('checked',true).val();
            price=price.split('-');
            $('.display').html(price[1]+"<br/><span class='currency'>USD</span>");
        })
    })

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        if (n > slides.length) {
            slideIndex = 1;
        }
        if (n < 1) {
            slideIndex = slides.length;
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slides[slideIndex - 1].style.display = "block";
    }
    var rdh = $('.rd-block .row').height()
    var w = $(window).width()
    if(w > 992){
        $('.feature-block .row').css({'height' : rdh})
        if(rdh > 190){
            $('.rd-block .row a.btn').css({'margin-bottom' : '15%'})
            $('.feature-block .row div').css({'margin-top' : '10%'})
        }
    }else{
        if(rdh > 190){
            $('.rd-block .row a.btn').css({'margin-bottom' : '3%'})
            $('.feature-block .row div').css({'margin-top' : '1.5%'})
        }
    }
    // console.log(rdh)
    </script>



    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/postfunctions.js"></script>
