<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Checkout- <?php if(isset($report_data)) { echo $report_data->rep_title;}else{ echo "Order Confirmation"; } ?></title>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/captcha.js"></script>

<!-- *****************   BOOTSTRAP SELECT          ******************  -->

<link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>
<link href="<?php echo base_url();?>assets/css/checkout.css" rel='stylesheet' />
<link href="<?php echo base_url();?>assets/css/RD-structure.css" rel='stylesheet' />

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
    
        var w=$(window).width();
        if(w>767){
            $('html,body').animate({
                scrollTop: $('.page-banner').offset().top        
            }, 'slow');
        }
    });        
    function validate_captcha(){
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code){
            CaptchaError.innerHTML="Invalid Captcha Code";
            return false;
        }else{
            CaptchaError.innerHTML="";
            return true;
        }
    }
</script>


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var SITEURL = "<?= base_url() ?>";
    $('body').on('click', '.btn-checkout', function(e){ 
        if(validate_captcha()){
            var country = $("select#country"). val();
            // country=country.replace(/\"/gi,'');
            // country=country.replace(/\\/gi,'');
            if(country !=='' ){
                if(!$("button.btn-checkout").hasClass("disabled")){
                    var radio = $("input[name='radio-inline']:checked"). val();
                    if(radio == 'EazyPay'){
                        // alert(radio);
                        var name=$("#name").val();
                        var email=$("#email").val();
                        var phone=$("#phone").val();
                        var company=$("#company").val();
                        var country=$("#country").val();
                        var zip=$("#zip").val();
                        var address=$("#address").val();

                        var title=$("#rep_title").val();
                        title = (title.length > 250) ? title.substr(0,250) : title
                        var price=parseInt($("#rep_price").val());
                        var license=$("#rep_license").val();
                        var currency=$("#currency").val();
                        var is_global=$("#is_global").val();
                        $.ajax({
                            url: '<?php echo base_url(); ?>payment/eazypay-success',
                            type: 'post',
                            data: {name:name,email:email,phone:phone,company:company,country:country,zip:zip,address:address,title:title,price:price,currency:currency,license:license,is_global:is_global}, 
                            success: function () {
                                window.location.href = '<?php echo base_url(); ?>payment/eazypay-thanks';
                            }
                        }); 
                        

                        // alert('working'+price);
                        // var options = {
                        //     "key": "rzp_live_xLDXJ4EQ7XkGSP",
                        //     "currency": currency,
                        //     "amount": (price*100), // 2000 paise = INR 20
                        //     "name": "Foresee Convey Markets",
                        //     "description": title,
                        //     "image": "<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png",
                        //     "handler": function (response){
                        //         $.ajax({
                        //             url: '<?php echo base_url(); ?>payment/eazypay-success',
                        //             type: 'post',
                        //             data: {name:name,email:email,phone:phone,company:company,country:country,zip:zip,address:address,title:title,price:price,currency:currency,license:license,is_global:is_global,response:response}, 
                        //             success: function () {
                        //                 window.location.href = '<?php echo base_url(); ?>payment/eazypay-thanks';
                        //             }
                        //         });                            
                        //     },                        
                        //     "theme": {
                        //         "color": "#528FF0"
                        //     }
                        // };
                        // var rzp1 = new Razorpay(options); 
                                            
                        // rzp1.on('payment.failed', function (response){
                        //     $.ajax({
                        //     url: '<?php echo base_url(); ?>payment/eazypay-cancel',
                        //         type: 'post',
                        //         data: {name:name,email:email,phone:phone,company:company,country:country,zip:zip,address:address,title:title,price:price,currency:currency,license:license,is_global:is_global,response:response,
                        //         }, 
                        //         success: function () {
                        //         //alert("PAID");
                        //             window.location.href = '<?php echo base_url(); ?>eazypay/payment-failed';
                        //         }
                        //     });
                        // });
                        // rzp1.open();
                        e.preventDefault();
                    }
                }
            }else{
                alert('Please select country from dropdown');
                e.preventDefault();
            }            
        }
    });         
</script>
<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li><a href="<?=base_url();?>latest-reports">Industry Reports</a></li>
    <li title="<?php echo $report_data->rep_title; ?>">Checkout</li>
</ul>

<div class="page-banner"> 
    <div class="row">
        <div class="col-md-8 rd-block">
            <h2 class="request-sample">Checkout</h2>
            <h2 class="report-title <?=strlen($report_data->rep_title) <= 200 ? 'report-sm-title' : '' ?>"><?php echo $report_data->rep_title; ?></h2>
        </div>
        <div class="col-md-4 feature-block" align='center'>
            <div class="row">
                <div class="col-md-5 col-feature">
                    <img class="img img-responsive img-rounded report-img" style='height: 150px' src="<?php echo base_url(); ?>assets/images/category/<?php echo $report_data->category_image; ?>" alt="Report Details"/>
                </div>
                <div class="col-md-7 report-details">
                    <p><strong>ID</strong> : FCM-<?=$report_data->rep_id?></p>
                    <p><strong>Pages</strong> : <?=$report_data->rep_page?></p>
                    <p><strong>Publisher</strong> : <?=get_publishers($report_data->publisher_id)->publisher_code?></p>
                    <p title="Report is available in Excel, PDF, Powerpoint and Word Formats">
                        <strong>Format</strong> : 
                        <span class="fas fa-file-excel"></span>&nbsp;
                        <span class="fas fa-file-pdf"></span>&nbsp;
                        <span class="fas fa-file-powerpoint"></span>&nbsp;
                        <span class="fas fa-file-word"></span>&nbsp;
                    </p>
                    <p><strong>Industry</strong> : <a href="<?=base_url()?>category/<?=category_details()[@$report_data->rep_sub_cat_1_id]['sc1_url']?>"><?=categories()[@$report_data->rep_sub_cat_1_id]?></a></p>
                </div>
            </div><br/>
        </div>
    </div>
</div> 
<div class="row">
    <div class="col-md-8">
        <div class="report-tab-empty">                 
            <p class='contact-heading' align='center'>*Please fill the form below to proceed further for Checkout</p>
            <div class="row">
                <div class="col-md-6 cart-block md-hidden col-contact">
                    <div class="content-block-shadow">
                        <div class="panel-body">                    
                            <h3 class='cart-title'>Cart Total</h3><br/>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Report Title:</b>
                                </div>
                                <div class="col-md-9">
                                    <a class="cart-report-title" title='<?=$report_data->rep_title?>' href="<?=base_url()?>reports/<?=$report_data->rep_id?>/<?=$report_data->rep_url?>"><span><?=$report_data->rep_title?></span></a>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-2">
                                    <b>License:</b>
                                </div>
                                <div class="col-md-10">
                                    <span class="pull-right"><?php  echo $report_license;  ?></span>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Sub Total:</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="pull-right price-quote">
                                        <span class="fa fa-dollar-sign"></span><?=@$report_price?>
                                    </span>
                                </div>                       
                                
                            </div><hr/>
                            <br/>
                            <div class="row">
                                <div class="grand-total">
                                    <div class="col-md-8">
                                        <b>GRAND TOTAL :</b>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="total-right">
                                            <span class="fa fa-dollar-sign"></span>
                                            <span id='grand_total'>
                                                <?php  if (isset($report_price)) {  echo $report_price; } if (isset($grand_total)) {  echo $grand_total; } ?>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-contact">
                    <div class="content-block-shadow">
                        <div class="panel-body">
                            <form class="form-horizontal" id="checkout-form" role="form" action="<?php echo base_url(); ?>checkout-process/<?=$report_data->rep_id?>" method="post" data-toggle="validator" onsubmit="return validate_captcha()">
                                
                            <!--*************************    HIDDEN FIELDS    *****************************-->
                                
                                <input type="hidden" name="rep_title" id="rep_title" value="<?php if(isset($report_data)) {  echo $report_data->rep_title;} if(isset($report_title)) {  echo $report_title;} ?>" />
                                <input type="hidden" id='rep_license' name="rep_license" value="<?php if (isset($report_license)) {  echo $report_license; }else{ echo "Single User Price"; } ?>"/>
                                <input type="hidden" id='rep_price' name="rep_price" value="<?php if (isset($report_price)) {  echo $report_price; } if (isset($grand_total)) {  echo $grand_total; } ?>" />
                                <input type="hidden" id="currency" name="currency" value="USD"/>
                                <input type="hidden" id='is_global' name="is_global" value="0"/>
                                    
                            <!--**************************    HIDDEN FIELDS     ***************************-->

                                <div class="form-group">
                                    <label class="control-label">Name <span class="star">*</span> :</label>
                                    <input class="form-control" name="name" id="name"  placeholder="Your Name" data-bv-field="full_name" type="text" value="<?=@$name?>" data-error="Please enter your name" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Business Email <span class="star">*</span> :</label>
                                    <input class="form-control" name="email" id="email"  placeholder="Business Email" data-bv-field="email" type="email" value="<?=@$email?>" data-error="Please enter valid email address" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Phone <span class="star">*</span> :</label>
                                    <input class="form-control" pattern="^[0-9]{10}$"  name="phone" id="phone" maxlength="10" placeholder="Contact Number without country code"  data-bv-field="phone" type="text" value="<?=@$report_data->phone?>" data-error="Please enter valid phone number" required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Company <span class="star">*</span> :</label>
                                    <input class="form-control" name="company" id="company" placeholder="Company Name" data-bv-field="company" type="text" value="<?=@$company?>" data-error="Please enter company / organization name"   required/>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Country <span class="star">*</span> :</label>
                                    <select name="country" id="country" class="form-control required selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="This field can not be blank"  required>
                                        <?=getCountryDropdown()?>
                                    </select>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Zip Code <span class="star">*</span> :</label>
                                    <input type="text" name="zip" placeholder="Enter your Zip code" data-error="Please enter your zip code" id="zip" class="form-control" required/>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Adderss <span class="star">*</span> :</label>
                                    <textarea data-error="Please provide your address" rows="5"  class="form-control" name="address" id="address" data-bv-field="address"  required></textarea>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                
                                <div class="form-group">
                                    <strong>Payment: <span class="star">*</span>:</strong>
                                    <label class="container-radio">
                                        <img src="<?php echo base_url(); ?>assets/images/paypal.png" class="img img-responsive img-paypal"/> 
                                        <input type="radio" class="mode radio-inline pull-left" name="radio-inline" required="" value="Paypal"/><span class="checkmark"></span>
                                    </label>
                                    <label class="container-radio">
                                        <img src="<?php echo base_url(); ?>assets/images/bank-transfer.jpg" class="img img-responsive img-bank" alt="bank transfer" title="Bank Transfer"/> 
                                        <input type="radio" class="mode radio-inline pull-left" name="radio-inline" required=""  value="Bank Transfer"/><span class="checkmark"></span>
                                    </label>
                                    <!-- <label class="container-radio">
                                        <img src="<?php echo base_url(); ?>assets/images/eazypay.jpg" class="img img-responsive img-bank" alt="EazyPay" title="EazyPay"/> 
                                        <input type="radio" class="mode radio-inline pull-left" name="radio-inline"  required="" value="EazyPay"/><span class="checkmark"></span>
                                    </label>  -->
                                    <div class="help-block with-errors"></div> 

                                </div>                               

                                <div class="form-group">
                                    <label class="control-label" for="security_code">Captcha: <span class="star">*</span>: </label><br/>
                                    <div class="input-group" style="padding-right:0;">
                                        <input data-error="Please enter security code" onkeyup="validate_captcha()"  type="text" name="captcha_code" maxlength="6" placeholder="Security Code" class="form-control" id="security_code"  required/>
                                        <input type="text" id="txtCaptcha" readonly class="form-control" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary btn-captcha" type="button"  onClick="DrawCaptcha();"> <span class="fa fa-sync"></span> </button>
                                        </span> 
                                    </div><br/>
                                    <p style="color:#f00" id="CaptchaError"></p>
                                    <div class="help-block with-errors"></div> 
                                </div>
                                                                
                                <div class="col-md-12 button-group-1">
                                    <a href='<?php echo base_url(); ?>reports/<?= @$report_data->rep_id;  ?>/<?= @$report_data->rep_url;  ?>' class="btn btn-md btn-tab" >
                                        <strong><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;CONTINUE SHOPPING</strong>       
                                    </a>
                                    <button type="submit" class="btn btn-md btn-tab btn-checkout" >
                                        <strong>PROCEED TO CHECKOUT&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></strong>       
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 cart-block sticky sm-hidden col-contact">
                    <div class="content-block-shadow">
                        <div class="panel-body">                    
                            <h3 class='cart-title'>Cart Total</h3><br/>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Report Title:</b>
                                </div>
                                <div class="col-md-9">
                                    <a class="cart-report-title" title='<?=$report_data->rep_title?>' href="<?=base_url()?>reports/<?=$report_data->rep_id?>/<?=$report_data->rep_url?>"><span><?=$report_data->rep_title?></span></a>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-2">
                                    <b>License:</b>
                                </div>
                                <div class="col-md-10">
                                    <span class="pull-right"><?php  echo $report_license;  ?></span>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Sub Total:</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="pull-right price-quote">
                                        <span class="fa fa-dollar-sign"></span><?=@$report_price?>
                                    </span>
                                </div>                       
                                
                            </div><hr/>
                            <br/>
                            <div class="row">
                                <div class="grand-total">
                                    <div class="col-md-8">
                                        <b>GRAND TOTAL :</b>
                                    </div>
                                    <div class="col-md-4">
                                        <span class="total-right">
                                            <span class="fa fa-dollar-sign"></span>
                                            <span id='grand_total'>
                                                <?php  if (isset($report_price)) {  echo $report_price; } if (isset($grand_total)) {  echo $grand_total; } ?>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class='content-block-shadow'>
            <div class="row info-block press-info-block">
                <div class="col-md-12" align='center'>
                    <h3 class='report-heading'>Contact Us </h3>
                    <div class="info-block-row">
                        <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                        <h4>LOCATION</h4>
                        <p>1445 Woodmont Ln NW, Atlanta, Ga 30318</p><br/>
                    
                        <h1 class='fa fa-phone fa-2x icon-home'></h1>
                        <h4>CALL US</h4>
                        <p>International : <a href='tel:+18442677928'>+1-844-267-7928</a> </p><br/>
                    
                        <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                        <h4>EMAIL US</h4>
                        <p><a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p><br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
