<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************--> 

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css"/>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script>
    $(function(){
        new WOW().init();
    })
</script>

<style type="text/css">
    .logo p span{
        color:lightgreen;
    }
    .wrap{
        position: absolute;
        background-color: rgb(0,0,0,0.5);
    }
    .wrap-block{
        padding: 30px;
    }
    .wrap h1,.wrap h2{
        color: rgb(233, 97, 7);
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-style: italic;
        font-weight: 700;
    }
    @media screen and (min-width:767px){
        .error-block  img{
            height: 600px;
            width: 100%;
        }
        .wrap h1{ 
            font-size: 5vw;
            margin-bottom: 15%;
        }
        .wrap h2{ 
            font-size: 3.5vw;
            margin-bottom: 15%;
        }
        .wrap{
            top: 30%;
            left: 36%;
        }
    }
    @media screen and (max-width:767px){
        .error-block  img{
            height: 400px;
            width: 100%;
        }
        .wrap{
            top: 23%;
            left: 6%;
        }
    }
</style>


<!---******************************  ERROR MESSAGE  **************************** -->

<div class="error-block">
    <img class="img img-responsive" src="<?php echo base_url(); ?>assets/images/track-bg.jpg" alt="error-404"/>
    <div class="wrap">
        <div class="wrap-block" align='center'>
            <h1 class="wow bounceInDown"><i style="color: #fff;" class="fa fa-spinner fa-exclamation-triangle wow flip infinite 2s"></i> 404</h1>
            <h2 class="wow bounceInUp">PAGE NOT FOUND</h2>
            <p class="lead">
                <a class="btn btn-primary btn-md btn-home" href="<?php echo base_url(); ?>" role="button">Continue To Homepage</a>
            </p>
        </div>
    </div>
</div>

<!---******************************  ERROR MESSAGE  **************************** -->



<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
