<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Latest Reports | Foresee Convey Markets</title>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="Foresee Convey Markets reports,  reports" />
        <meta name="description" content="Company Reports,  company reports"/>
        <meta name="author" content="Foresee Convey Markets"/>
        
        <!--**********************    HEADER OPEN      ***************************-->
        
        <?php require_once 'layouts/header.php'; ?>
        
        <!--**********************    HEADER CLOSE     ***************************-->
        <link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
        <script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $(".selectpicker").selectpicker();
            });
        </script>
        <script>
            $(document).ready(function(){
                $(".report-data-3").addClass("sticky");
            })
        </script>
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
            <li>Industry Reports</li>
        </ul>

        <div class="page-banner" align='center'> 
            <h2><span class="fa fa-chart-bar fa-2x"></span></h2>
            <h1>LATEST REPORTS</h1><br/>
        </div>
    
        <div class="row banner-row">
            <?php if (isset($total_reports)) {?>
            <div class="col-md-3 mdx-hidden"><br/>
                <div class="filter-block">
                    <h3 align='center' class="btn-filter accordion">Search By Filters </h3>
                    <div class="panel-info panel-accordion">
                        <form method="get" id='filterForm' action="<?=@$current_url?>" enctype="application/x-www-form-urlencoded">
                            <div class="panel-body">
                                <select class="form-control selectpicker" name="year" id="year" placeholder='Filter By Year'>
                                    <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                                    <?php for($index=2019;$index<=date('Y');$index++){ 
                                        $selected=(@$index == @$year) ? 'selected' : '';
                                        echo "<option value='".$index."' ".$selected.">".$index."</option>";
                                    } ?>
                                </select>
                            </div><br/>
                            <div class="panel-body">
                                <select class="form-control selectpicker" data-live-search="true" data-actions-box="true" name="region" id="region" placeholder='Filter By Region'>
                                    <option value='' <?=(@$current_region == '') ? 'selected' : ''?> >Filter By Region</option>
                                    <?php foreach(@$region_data->result() as $row){
                                        $selected=(@$row->region == @$current_region) ? 'selected' : '';
                                        echo "<option value='".@$row->region."' ".@$selected." >".ucfirst(@$row->region)."</option>";
                                    } ?>
                                </select>
                            </div><br/>
                            <div class="panel-body">
                                <select class="form-control selectpicker" name="date" id="date" placeholder='Filter By Date Order'> 
                                    <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                                    <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                                    <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                                </select>
                            </div><br/>
                            <div class="panel-body">
                                <select class="form-control selectpicker" name="price" id="price" placeholder='Filter By Price'>
                                    <option value=''<?= (@$price == '') ? 'selected' : ''?>>Filter By Price</option>
                                    <option value='<1000' <?= (@$price == '<1000') ? 'selected' : ''?> >&lt;1000 USD</option>
                                    <option value='1000-2000' <?= (@$price == '1000-2000') ? 'selected' : ''?>>1000-2000 USD</option>  
                                    <option value='2000-3000' <?= (@$price == '2000-3000') ? 'selected' : ''?>>2000-3000 USD</option>  
                                    <option value='3000-4000' <?= (@$price == '3000-4000') ? 'selected' : ''?>>3000-4000 USD</option>   
                                    <option value='>4000' <?= (@$price == '>4000') ? 'selected' : ''?>>&gt;4000 USD</option>                                
                                </select>
                            </div><br/>

                            <div class="panel-body">
                                <button type="submit" class="btn btn-search-filter btn-apply" title="Apply Filter">Apply <span class="fa fa-hourglass"></span></button>
                                <button type="reset" class="btn pull-right btn-search-filter btn-clear" title="Clear Filter" onclick="window.location.href='<?=@$current_url?>'">Clear <span class="fa fa-times"></span></button> 
                            </div><br/>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div align="center">
                    <?php
                    if (isset($total_reports)) {
                        if ($total_reports == 0) {
                            echo "<br/><div class='col-md-12 no-data'>                                                
                                        <div class='alert alert-info'>
                                            <h3><i class='fa fa-spinner fa-exclamation-triangle'></i>&nbsp;Sorry ! No Reports Found. Try Other Search Filter</span></h3>
                                        </div>
                                    </div>";
                        }
                    }elseif (isset($errorText)) {
                        echo "<div class='col-md-12 no-data'>                                                
                                <div class='alert alert-info'>
                                    <h3><i class='fa fa-spinner fa-exclamation-triangle'></i>&nbsp;".@$errorText."</span></h3>
                                </div>
                            </div>";
                    }
                    ?>
                </div><br/> 
                <?php if (@$report_list){
                    $i=1;
                    foreach ($report_list->result() as $row) { 
                        // $report_url=str_ireplace('%2f','-', $row->rep_url);
                        $report_title = explode('Market',$row->rep_title);
                        if(isset($report_title[0]) && strlen($report_title[0]) == 0){
                        $report_title_array = explode(" ", $row->rep_title);
                        $first_word_length = strlen($report_title_array[0]);
                        $report_title = explode('Market',substr($row->rep_title, $first_word_length));
                        }
                        $report_title = ucwords(str_ireplace(array(' -','- ','-'),' - ',$report_title[0]).' market');
                        $report_title = strlen($report_title) > 130 ? substr($report_title , 0 ,130)."..." : $report_title;
                    ?>
                <div class="report-details-block <?= ($i%2 === 0) ? 'report-block-even' : 'report-block-odd'?>"><br/>
                    <div class="row ">
                        <div class="col-md-4" align="center">
                            <img class="img img-responsive" onclick="window.location.href='<?=base_url();?>reports/<?=$row->rep_id; ?>/<?=urlencode($row->rep_url); ?>'" style="height:150px;" src="<?php echo base_url(); ?>assets/images/category/<?php echo $row->category_image; ?>" alt="<?php echo $row->rep_title; ?>"  />
                        </div>
                        <div class="col-md-8">
                            <p class='rep-title' title="<?=$row->rep_title?>"><a href="<?=base_url();?>reports/<?=$row->rep_id; ?>/<?=urlencode($row->rep_url); ?>"  rel="follow"><?=@$report_title; ?></a></p>
                            <div class='rep-desc'><?=substr($row->rep_descrip, 0, 200).".." ?></div>
                            <div class="report-content">
                                <h5>
                                    <span> <b>Published </b> :
                                        <?php $date = date_create($row->rep_entry_date);
                                        echo date_format($date, 'M Y'); ?>
                                    </span> <strong>|</strong>&nbsp;
                                    <span><b>From </b> : <?php if ($row->li_value > 0) {
                                            echo "$" . $row->li_value;
                                        } ?>
                                    </span>
                                </h5>
                            </div>                                    
                            <a class="btn btn-details pull-right" title="View This Report" href='<?=base_url();?>reports/<?=$row->rep_id; ?>/<?=urlencode($row->rep_url); ?>'>View Report <i class="fa fa-info-circle" style="margin-top: 3px;"></i></a>
                        </div>
                    </div>
                </div>
                <?php $i++; }  } ?>
                <div class="clearfix">                    
                    <div class="row">
                        <div class="col-md-8"><?php if (isset($pagination)) echo $pagination; ?> </div>
                        <div class="col-md-4 sm-hidden"><br/>
                            <h4 class="pull-right">
                            <?php if (isset($total_reports) && $total_reports > 15) {
                            ?> Page <?= @$page?> of <?=@$total_pages?>
                            <?php  } ?>
                            </h4>
                        </div>
                    </div>
                </div><br/>
            </div>
            <div class="col-md-3 sticky smx-hidden"><br/>
                <div class="filter-block"><br/>
                    <h3 align='center' class="btn-filter">Search By Filters </h3>
                    <div class="panel-info">
                        <form method="get" id='filterForm' action="<?=@$current_url?>" enctype="application/x-www-form-urlencoded">
                            <div class="panel-body">
                                <select class="form-control selectpicker" name="year" id="year" placeholder='Filter By Year'>
                                    <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                                    <?php for($index=2019;$index<=date('Y');$index++){ 
                                        $selected=(@$index == @$year) ? 'selected' : '';
                                        echo "<option value='".$index."' ".$selected.">".$index."</option>";
                                    } ?>
                                </select>
                            </div><br/>
                            <div class="panel-body">
                                <select class="form-control selectpicker" data-live-search="true" data-actions-box="true" name="region" id="region" placeholder='Filter By Region'>
                                    <option value='' <?=(@$current_region == '') ? 'selected' : ''?> >Filter By Region</option>
                                    <?php foreach(@$region_data->result() as $row){
                                        $selected=(@$row->region == @$current_region) ? 'selected' : '';
                                        echo "<option value='".@$row->region."' ".@$selected." >".ucfirst(@$row->region)."</option>";
                                    } ?>
                                </select>
                            </div><br/>
                            <div class="panel-body">
                                <select class="form-control selectpicker" name="date" id="date" placeholder='Filter By Date Order'> 
                                    <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                                    <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                                    <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                                </select>
                            </div><br/>
                            <div class="panel-body">
                                <select class="form-control selectpicker" name="price" id="price" placeholder='Filter By Price'>
                                    <option value=''<?= (@$price == '') ? 'selected' : ''?>>Filter By Price</option>
                                    <option value='<1000' <?= (@$price == '<1000') ? 'selected' : ''?> >&lt;1000 USD</option>
                                    <option value='1000-2000' <?= (@$price == '1000-2000') ? 'selected' : ''?>>1000-2000 USD</option>  
                                    <option value='2000-3000' <?= (@$price == '2000-3000') ? 'selected' : ''?>>2000-3000 USD</option>  
                                    <option value='3000-4000' <?= (@$price == '3000-4000') ? 'selected' : ''?>>3000-4000 USD</option>   
                                    <option value='>4000' <?= (@$price == '>4000') ? 'selected' : ''?>>&gt;4000 USD</option>                                
                                </select>
                            </div><br/>

                            <div class="panel-body">
                                <button type="submit" class="btn btn-search-filter btn-apply" title="Apply Filter">Apply <span class="fa fa-hourglass"></span></button>
                                <button type="reset" class="btn pull-right btn-search-filter btn-clear" title="Clear Filter" onclick="window.location.href='<?=@$current_url?>'">Clear <span class="fa fa-times"></span></button> 
                            </div><br/>

                        </form>
                    </div><br/>
                </div><br/>
            </div>
            <?php }else{if(!isset($helpText) && !isset($errorText)){ ?>

                <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css"/>
                <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
                <script>
                    $(function(){
                        new WOW().init();
                    })
                </script>

                <div class='col-md-12 no-data'>                                                
                    <div class='alert alert-info' align='center'>
                        <h2 class="wow pulse infinite" data-wow-duration="2s" data-wow-delay="0.5s"><i class='fa fa-clock'></i> &nbsp;<span>Coming Soon ...</span></h2>
                    </div>
                </div>               
            <?php }  } ?>
        </div>

        <script type="text/javascript">

            var data=<?php echo (isset($_GET) && !empty($_GET)) ? '1' : '0' ?>;
            if(data == '1'){
                var w=$(window).width();
                if(w>767){
                    $('html,body').animate({
                        scrollTop: ($('.banner-row').offset().top  + 60 )      
                    }, 'slow');
                }
            }
            var panel = $('.accordion').siblings('.panel-accordion')[0];
            if(data == '1'){
                $('.accordion').addClass('active')
                let maxHeight = panel.scrollHeight + "px";
                $(panel).css({'max-height' : maxHeight})
                
            }else{
                $('.accordion').removeClass('active')
                // panel.style.maxHeight = 0; 
                $(panel).css({'max-height' :0})
                // console.log(panel)               
            }
            // var acc = document.getElementsByClassName("accordion");
            // var i;
            // for (i = 0; i < acc.length; i++) {
            $('.accordion').click(function(){
                $(this).toggleClass("active");
                var panel = $('.accordion').siblings('.panel-accordion')[0];
                console.log('clicked')
                // console.log(typeof(panel.style.maxHeight))
                if (panel.style.maxHeight === '0px') {
                    let maxHeight = panel.scrollHeight + "px";
                    console.log(maxHeight)
                    $(panel).css({'max-height' : maxHeight})
                    // panel.style.maxHeight = panel.scrollHeight + "px";
                } else {
                    // panel.style.maxHeight = 0;  
                    console.log('0px')                  
                    $(panel).css({'max-height' : 0})
                } 
            })
        </script>
        
        
<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->

