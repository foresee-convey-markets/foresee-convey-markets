<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Search Result | <?php echo ucfirst($search); ?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="Search Result  Market" />
<meta name="description" content="Search Result for your query - <?php echo ucfirst($search); ?>"/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
    })
</script>

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li><a onclick="window.history.back()" title="Go Back"><span class="fa icon fa-backward"></span></a></li>
    <li>Search Result</li>
</ul>

<div class="latest-report page-banner" align='center'>  
    <h2><span class="fa fa-search-dollar fa-2x"></span></h2>
    <h1>SEARCH RESULTS</h1><br/>
</div>    
<div class="search-block" align='center'>
    <p><?=@$helpText?></p>
</div> 
        
<div class="row banner-row">
    <?php if (isset($total_reports) && @$SHOW !== false) {?>
    <div class="col-md-3 mdx-hidden"><br/>
        <div class="filter-block">
            <h3 align='center' class="btn-filter accordion">Search By Filters </h3>
            <div class="panel-info panel-accordion">
                <form method="get" id='filterForm' action="<?=@$action_url?>" enctype="application/x-www-form-urlencoded">
                    <input type="hidden" name="search" value="<?=@$search?>"/>        
                    <div class="panel-body">
                        <select class="form-control selectpicker" name="year" id="year" placeholder='Filter By Year'>
                            <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                            <?php for($index=2019;$index<=date('Y');$index++){ 
                                $selected=(@$index == @$year) ? 'selected' : '';
                                echo "<option value='".$index."' ".$selected.">".$index."</option>";
                            } ?>
                        </select>
                    </div><br/>
                    <div class="panel-body">
                        <select class="form-control selectpicker" data-live-search="true" data-actions-box="true" name="region" id="region" placeholder='Filter By Region'>
                            <option value='' <?=(@$current_region == '') ? 'selected' : ''?> >Filter By Region</option>
                            <?php foreach(@$region_data->result() as $row){
                                $selected=(@$row->region == @$current_region) ? 'selected' : '';
                                echo "<option value='".@$row->region."' ".@$selected." >".ucfirst(@$row->region)."</option>";
                            } ?>
                        </select>
                    </div><br/>
                    <div class="panel-body">
                        <select class="form-control selectpicker" name="date" id="date" placeholder='Filter By Date Order'> 
                            <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                            <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                            <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                        </select>
                    </div><br/>
                    <div class="panel-body">
                        <select class="form-control selectpicker" name="price" id="price" placeholder='Filter By Price'>
                            <option value=''<?= (@$price == '') ? 'selected' : ''?>>Filter By Price</option>
                            <option value='<1000' <?= (@$price == '<1000') ? 'selected' : ''?> >&lt;1000 USD</option>
                            <option value='1000-2000' <?= (@$price == '1000-2000') ? 'selected' : ''?>>1000-2000 USD</option>  
                            <option value='2000-3000' <?= (@$price == '2000-3000') ? 'selected' : ''?>>2000-3000 USD</option>  
                            <option value='3000-4000' <?= (@$price == '3000-4000') ? 'selected' : ''?>>3000-4000 USD</option>   
                            <option value='>4000' <?= (@$price == '>4000') ? 'selected' : ''?>>&gt;4000 USD</option>                                
                        </select>
                    </div><br/>

                    <div class="panel-body">
                        <button type="submit" class="btn btn-search-filter btn-apply" title="Apply Filter">Apply <span class="fa fa-hourglass"></span></button>
                        <button type="reset" class="btn pull-right btn-search-filter btn-clear" title="Clear Filter" onclick="window.location.href='<?=base_url()?>search-result?search=<?=@$search?>'">Clear <span class="fa fa-times"></span></button> 
                    </div><br/>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-9">

        <div align="center">
            <?php
            if (isset($total_reports)) {
                if ($total_reports == 0) {
                    $search_form=str_ireplace('search-result','search-form',@$current_url);
                    echo "<br/><div class='alert alert-danger'><span class='fa fa-exclamation-triangle'></span>&nbsp;&nbsp;Sorry ! No Reports Found. Try Other Search Filter</div>";
                    echo "<div class='alert alert-info alert-search text-center'>
                            <h4><span class='fa fa-info-circle'></span>&nbsp;&nbsp;What would you like to do? </h4><br/>
                            <a href='".@$search_form."' class='btn btn-result btn-reach-us'><span class='fa fa-envelope'></span>&nbsp;&nbsp; Reach Out To Us</a>
                            <a href='".base_url()."latest-reports' class='btn btn-result btn-browse'><span class='fa fa-bar-chart'></span>&nbsp;&nbsp;Browse Reports</a>    
                        </div>";
                }
            }
            ?>
        </div> <br/>
        <?php if (@$report_search){
            $i=1;
            foreach ($report_search->result() as $row) { 
                // $report_url=str_ireplace('%2f','-', $row->rep_url);
                $rep_title = strlen($row->rep_title) > 100 ? substr($row->rep_title,0,100)."..." : $row->rep_title;
            ?>
        <div class="report-details-block <?= ($i%2 === 0) ? 'report-block-even' : 'report-block-odd'?>"><br/>
            <div class="row ">
                <div class="col-md-4" align="center">
                    <img class="img img-responsive" onclick="window.location.href='<?=base_url();?>reports/<?=$row->rep_id; ?>/<?=urlencode($row->rep_url); ?>'" style="height:150px;" src="<?php echo base_url(); ?>assets/images/category/<?php echo $row->category_image; ?>" alt="<?php echo $row->rep_title; ?>"  />
                </div>
                <div class="col-md-8">
                    <p class='rep-title' title="<?=$row->rep_title?>"><a href="<?=base_url();?>reports/<?=$row->rep_id; ?>/<?=urlencode($row->rep_url); ?>"  rel="follow"><?= highlightSearchText(@$rep_title,@$search); ?></a></p>
                    <div class='rep-desc'><?=substr($row->rep_descrip, 0, 200).".." ?></div>
                    <div class="report-content">
                        <h5>
                            <span> <b>Published </b> :
                                <?php $date = date_create($row->rep_entry_date);
                                echo date_format($date, 'M Y'); ?>
                            </span> <strong>|</strong>&nbsp;
                            <span><b>From </b> : <?php if ($row->li_value > 0) {
                                    echo "$" . $row->li_value;
                                } ?>
                            </span>
                        </h5>
                    </div>                                    
                    <a class="btn btn-details pull-right" title="View This Report" href='<?=base_url();?>reports/<?=$row->rep_id; ?>/<?=urlencode($row->rep_url); ?>'>View Report <i class="fa fa-info-circle" style="margin-top: 3px;"></i></a>
                </div>
            </div>
        </div>
        <?php $i++; }  } ?>
        <div class="clearfix">                                
            <div class="row">
                <div class="col-md-8"><?php if (isset($pagination)) echo $pagination; ?> </div>
                <div class="col-md-4 sm-hidden"><br/>
                    <h4 class="pull-right">
                    <?php if (isset($total_reports) && @$total_reports > 15) {
                    ?> Page <?= @$page?> of <?=@$total_pages?>
                    <?php  } ?>
                    </h4>
                </div>
            </div>
        </div><br/>
    </div>
    <div class="col-md-3 sticky smx-hidden"><br/>
        <div class="filter-block"><br/>
            <h3 align='center' class="btn-filter">Search By Filters </h3>
            <div class="panel-info">
                <form method="get" id='filterForm' action="<?=@$action_url?>" enctype="application/x-www-form-urlencoded">
                    <input type="hidden" name="search" value="<?=@$search?>"/>
                    <div class="panel-body">
                        <select class="form-control selectpicker" name="year" id="year" placeholder='Filter By Year'>
                            <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                            <?php for($index=2019;$index<=date('Y');$index++){ 
                                $selected=(@$index == @$year) ? 'selected' : '';
                                echo "<option value='".$index."' ".$selected.">".$index."</option>";
                            } ?>
                        </select>
                    </div><br/>
                    <div class="panel-body">
                        <select class="form-control selectpicker" data-live-search="true" data-actions-box="true" name="region" id="region" placeholder='Filter By Region'>
                            <option value='' <?=(@$current_region == '') ? 'selected' : ''?> >Filter By Region</option>
                            <?php foreach(@$region_data->result() as $row){
                                $selected=(@$row->region == @$current_region) ? 'selected' : '';
                                echo "<option value='".@$row->region."' ".@$selected." >".ucfirst(@$row->region)."</option>";
                            } ?>
                        </select>
                    </div><br/>
                    <div class="panel-body">
                        <select class="form-control selectpicker" name="date" id="date" placeholder='Filter By Date Order'> 
                            <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                            <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                            <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                        </select>
                    </div><br/>
                    <div class="panel-body">
                        <select class="form-control selectpicker" name="price" id="price" placeholder='Filter By Price'>
                            <option value=''<?= (@$price == '') ? 'selected' : ''?>>Filter By Price</option>
                            <option value='<1000' <?= (@$price == '<1000') ? 'selected' : ''?> >&lt;1000 USD</option>
                            <option value='1000-2000' <?= (@$price == '1000-2000') ? 'selected' : ''?>>1000-2000 USD</option>  
                            <option value='2000-3000' <?= (@$price == '2000-3000') ? 'selected' : ''?>>2000-3000 USD</option>  
                            <option value='3000-4000' <?= (@$price == '3000-4000') ? 'selected' : ''?>>3000-4000 USD</option>   
                            <option value='>4000' <?= (@$price == '>4000') ? 'selected' : ''?>>&gt;4000 USD</option>                                
                        </select>
                    </div><br/>

                    <div class="panel-body">
                        <button type="submit" class="btn btn-search-filter btn-apply" title="Apply Filter">Apply <span class="fa fa-hourglass"></span></button>
                        <button type="reset" class="btn pull-right btn-search-filter btn-clear" title="Clear Filter" onclick="window.location.href='<?=base_url()?>search-result?search=<?=@$search?>'">Clear <span class="fa fa-times"></span></button> 
                    </div><br/>

                </form>
            </div><br/>
        </div><br/>
    </div>
</div>
<?php }else{   
    $search_form=str_ireplace('search-result','search-form',@$current_url);

    echo "<br/><div class='alert alert-danger text-center'><span class='fa fa-exclamation-triangle'></span>&nbsp;&nbsp;Sorry ! No Reports Found For Your Search Query.</div>";
    echo "<div class='alert alert-info text-center'>
            <h4><span class='fa fa-info-circle'></span>&nbsp;&nbsp;What would you like to do? </h4><br/>
            <a href='".@$search_form."' class='btn btn-result btn-reach-us'><span class='fa fa-envelope'></span>&nbsp;&nbsp;Reach Out To Us</a>
            <a href='".base_url()."latest-reports' class='btn btn-result btn-browse'><span class='fa fa-bar-chart'></span>&nbsp;&nbsp;Browse Reports</a>    
        </div>";
}
?>

<script type="text/javascript">

    var data=<?php echo (isset($_GET) && !empty($_GET)) ? '1' : '0' ?>;
    if(data == '1'){
        var w=$(window).width();
        if(w>767){
            $('html,body').animate({
                scrollTop: ($('.banner-row').offset().top  + 60 )       
            }, 'slow');
        }
    }
    var panel = $('.accordion').siblings('.panel-accordion')[0];
    if(data == '1'){
        $('.accordion').addClass('active')
        let maxHeight = panel.scrollHeight + "px";
        $(panel).css({'max-height' : maxHeight})
        
    }else{
        $('.accordion').removeClass('active')
        $(panel).css({'max-height' :0})
    }
    $('.accordion').click(function(){
        $(this).toggleClass("active");
        var panel = $('.accordion').siblings('.panel-accordion')[0];
        console.log('clicked')
        if (panel.style.maxHeight === '0px') {
            let maxHeight = panel.scrollHeight + "px";
            console.log(maxHeight)
            $(panel).css({'max-height' : maxHeight})
        } else {
            console.log('0px')                  
            $(panel).css({'max-height' : 0})
        } 
    })
</script>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
