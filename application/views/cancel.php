<!DOCTYPE html>
<html>
<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Cancel - Payment PayPal | Foresee Convey Markets</title>
<meta name="keywords" content="Payment Failed, Payment Cancelled  Market" />
<meta name="description" content="Your Payment has not successfully completed . Please visit our home Page.">
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<div class="container-fluid" ><br/>
    <div class="content-block">
        <div class='row' style='margin : 0px'>
            <div class='col-md-12' align='center'>
                <h2 class="text-wrong"><span class="fa fa-exclamation-triangle"></span> Oops ! Something went wrong ! </h2><br/>
                <p class="lead">
                    <a class="btn btn-primary btn-md btn-home" href="<?php echo base_url(); ?>" role="button">Continue To Homepage</a>
                </p>
            </div>
        </div>
    </div>
</div>
<div class='home-contact-block'>
    <div class="row info-block static-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Contact</h3>
            <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
        </div>
        <div class="col-md-9">
            <div class="info-block-row">
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-lock fa-2x icon-home'></h1>
                        <h4>RELIABLE AND PROTECTED</h4>
                        <p>Your sensitive information and data  <strong>are <br/>secure</strong> with us.</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-globe fa-2x icon-home'></h1>
                        <h4>GLOBAL CHOICE</h4>
                        <p>Get access to over <strong>1M+ Forecast</strong><br/><strong>Research Reports</strong></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-users fa-2x icon-home'></h1>
                        <h4>CUSTOMER ORIENTED</h4>
                        <p>We are available <strong>24*7</strong> <br/>for <strong class='blue'>365 days</strong> in a year</p>
                    </div>
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-handshake fa-2x icon-home'></h1>
                        <h4>TRUSTED BY ALL</h4>
                        <p><strong>500+ Contented Clients </strong> Globally.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->


