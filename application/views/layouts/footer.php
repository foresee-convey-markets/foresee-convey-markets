<footer class="footer">
    <div><br/>
        <div class="container-fluid"><br/>            
            <div class="row footer-menu"> 
                <div class="col-md-3 footer-menu-list">
                    <h4>COMPANY</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
                        <li><a href="<?php echo base_url(); ?>terms-and-conditions">Terms &amp; Conditions</a></li>
                        <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url(); ?>careers">Careers</a></li>
                    </ul><br class="sm-hidden"/><br/>
                    <h4>OUR PRODUCTS</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>categories">Industries</a></li>
                        <li><a href="<?php echo base_url(); ?>latest-reports">Industry Reports</a></li>
                        <li><a href="<?php echo base_url(); ?>press-release">Press Releases</a></li>
                    </ul>
                </div>               
                <div class="col-md-3 footer-menu-list">
                    <h4>INDUSTRY SECTORS</h4>
                    <ul class="list">
                        <?php $i=1;
                        foreach($categories->result() as $row):
                            if($i == 11) { continue; }
                        ?>
                            <li><a href="<?php echo base_url(); ?>category/<?=@$row->sc1_url?>"><?=@$row->sc1_name?></a></li>
                        <?php $i++; endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-3 footer-menu-list">
                    <h4>CUSTOMER SUPPORT</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
                        <li><a href="<?php echo base_url(); ?>custom-research">Custom Research</a></li>
                        <li><a href="<?php echo base_url(); ?>return-policy">Return Policy</a></li>
                        <li><a href="<?php echo base_url(); ?>payment-mode">Payment Modes</a></li>
                    </ul><br/><br/>
                    <h4>SOCIAL</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>sitemap.html">SITEMAP</a></li>
                        <ul class="social-link">
                            <li class="twitter"  title="Twitter "><a href="#" ><i class="fab fa-twitter"></i></a></li>
                            <li class="blogger"    title="Blogger"><a href="#" ><i class="fab fa-blogger"></i></a></li>
                            <li class="linkedin" title="LinkedIn"><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li class="blogger" title="Medium"><a href="#"><i class="fa fa-bullhorn"></i></a></li>
                        </ul>
                    </ul><br/>
                </div>
                <div class="col-md-3 footer-menu-list">
                    <h4>CONTACT</h4>
                    <ul class="list contact-list">
                        <li> <?php
                                $show = true;
                                if(@$checkout_id && @$checkout_id == '16' ){
                                    $show = false;
                                }
                            ?>
                            <?php if(@$show == true){ ?>
                                <a href="tel:+18442677928"><strong>International :</strong><br/> +1-844-267-7928</a><br/><br/>
                            <?php } ?>
                            <a href="mailto:help@foreseeconvey-markets.com"><strong>Mail Us :</strong><br/> help@foreseeconvey-markets.com</a><br/><br/>
                            <?php if(@$show == true){ ?>
                                <a onclick='return false;'><strong>Location :</strong><br/> 1445 Woodmont Ln NW, Atlanta, Ga 30318</a>
                            <?php } ?>
                        </li>
                        
                    </ul>
                </div>

            </div>
            <hr style="border-top:1px solid #eee"/>

            <div class="row">
                <div class="col-md-12" align="center">
                    <p class="copyright">Copyright <span class="fa fa-copyright"></span>  <?php echo date("Y"); ?> &nbsp;| &nbsp; Foresee Convey Markets&nbsp; <span class="sm-hidden">|&nbsp;</span> All Rights Reserved &nbsp; </p>
                </div>
            </div>

            <!--<h5 class="disclaimer"> <b>Legal Disclaimer : -  </b><span>We  have the right to reveal your personally identifiable information as necessary by law and when we trust that disclosure is needed to safeguard our rights and/or to comply with judicial proceedings, court order, or legal process served on our website.</span></h5>-->

        </div>

        <?php if(!isset($_COOKIE['cookie_consent'])){ ?> 
        <div class="footer-fixed-block">
            <h3>We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. Please read our <a class="blue" href="<?php echo base_url(); ?>privacy-policy" target="_blank">privacy policy</a><br class="md-hidden" />
                <button class="btn-dismiss"><span class="fa fa-times"></span></button>
                <button class="btn-accept">Accept</button>
            </h3>
        </div>
        <?php } ?>
        <p id="back-top">
            <a href="#top"><span class=" fas fa-chevron-circle-up" style="font-size:35px"></span></a>
        </p>
    </div>
    
</footer>
<link href="<?php echo base_url(); ?>assets/css/all.min.css" rel="stylesheet" type="text/css"/>
<script async type="text/javascript"  src="<?php echo base_url(); ?>assets/js/all.min.js"></script>
<script type="text/javascript">
    var path = "<?=str_ireplace("/index.php","",current_url())?>";
    var href=$(' a[href="'+path+'"]').addClass('active');


    // fade in #back-top
    $("#back-top").hide();

    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
            } else {
                    $('#back-top').fadeOut();
            }
        });
            // scroll body to 0px on click
        $('#back-top a').click(function () {
            $('body,html').animate({
                    scrollTop: 0
            }, 800);
            return false;
        });
    });
    <?php if(isset($_COOKIE["cookie_consent"])){
        echo "$('.footer-fixed-block').css({'display':'none'})";
    } ?>

    $(".btn-dismiss").click(function(){
        document.cookie = "cookie_consent=decline; path=/; max-age="+24*60*60;
        location.reload(true);           
    })
    $(".btn-accept").click(function(){
        document.cookie = "cookie_consent=accept; path=/; max-age="+365*24*60*60;
        location.reload(true);           
    })
</script>
</body>
</html>