<!---------------------------- dropdown menu --------------------------------------->

<li><a href="<?php echo base_url(); ?>about-us">About</a></li>
<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Contact
    <span class="caret"></span></a>
    <ul class="dropdown-menu contact row">
        <li class="col-md-3" align='center'>
            <a href="<?php echo base_url(); ?>contact-us">
                <h2><span class="fa fa-envelope fa-2x"></span></h2> 
                <span>Contact Us</span>
            </a>
        </li>
        <li class="col-md-3" align='center'>
            <a href="<?php echo base_url(); ?>custom-research">
                <h2><span class="fa fa-search-dollar fa-2x"></span> </h2>
                <span>Custom Research</span>
            </a>
        </li>
    </ul>
</li>
<li><a href="<?php echo base_url(); ?>press-release">Newsroom</a></li>
<li class='mdx-hidden'><a href="<?php echo base_url(); ?>latest-reports">Industry Reports</a></li>

<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Categories
    <span class="caret"></span></a>
    <ul class="dropdown-menu categories row">
        <?php $categories=get_categories();
        $i=0;
        foreach ($categories->result() as $row) {
            if($i === 12){continue;}
        ?>
            <li class="col-md-3" align='center'>
                <a href="<?php echo base_url(); ?>category/<?php echo $row->sc1_url; ?>">
                <img class="img" src="<?php echo base_url(); ?>assets/images/category/<?php echo $row->sc1_image; ?>"   alt="<?php echo $row->sc1_name; ?>" /> 
                <br class="sm-hidden"/><span><?= (@$row->sc1_id == 14) ? 'BFSI' : $row->sc1_name; ?></span>
                </a>
            </li>
        <?php $i++;
            } 
        ?>
        <li class="col-md-12" align='center'>
            <a href="<?php echo base_url(); ?>categories">
                <span>All Categories</span>
                </a>
            </li>
    </ul>
</li>

<!---------------------------- dropdown menu --------------------------------------->
