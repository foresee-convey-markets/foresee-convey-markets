<?php 
    if(!isset($_COOKIE["sid"])){
        $val=base64_encode("USER".rand(1,1000));
        setcookie('sid',$val,time()+365*24*60*60,'/');
    }
 ?>
        <link rel="shortcut icon" href="<?=base_url()?>assets/images/foresee-convery-markets-logo.png" type="image/x-icon" />
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <script type="text/javascript"  src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/>
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Poppins"/> -->
        <script type="text/javascript"  src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css"/>
    
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-X12GSCMRNG"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-X12GSCMRNG');
        </script>

        <script type='text/javascript'>
            $(document).ready(function(){                
                $('.btn-navbar').click(function(){
                    $(".nav-form .form-control").focus()
                    $('.search-div').slideDown('fast').css({'display':'block'});
                })
                $('.btn-navbar-close').click(function(){
                    $('.search-div').slideUp('fast').css({'display':'none'});
                })
            })

        </script>        
        </head>
    <?php //$this->output->enable_profiler(true); ?>
    <body>
        <!--<div class="loader"></div>-->
        <div class="container">
            <div class="top-div">
                <ul class="top-nav">
                    <li>
                        <ul class="nav-social-link">
                        <!-- <li class="twitter"  title="Twitter "><a href="#" target="_blank" ><i class="fab fa-twitter"></i></a></li> -->
                            <li class="twitter"  title="Twitter "><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li class="blogger"    title="Blogger"><a href="#"><i class="fab fa-blogger"></i></a></li>
                            <li class="linkedin" title="LinkedIn"><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li class="blogger" title="Medium"><a href="#"><i class="fa fa-bullhorn"></i></a></li>
                        </ul>
                    </li>
                    <li class="sm-hidden">
                        <div class="navbar-brand">
                            <a href="<?php echo base_url(); ?>">
                                <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
                            </a>
                        </div>
                    </li>
                    <li class="pull-right"><a href="mailto:help@foreseeconvey-markets.com"><span class="fa fa-envelope"></span> help@foreseeconvey-markets.com</a></li>
                    <li class="pull-right"><a href="tel:+18442677928"><span class="fa fa-phone"></span> +1-844-267-7928</a></li>
                </ul>
            </div>
        </div>
        <header id="nav_header">
            <div class="row" style="margin:0;">
                <div class="col-md-12 col-12" style="padding:0px">
                    <nav class="navbar navbar-default"  style="border-radius: 0px;">
                        <div class="container-fluid">                            

                            <div class="navbar-header">
                                <div class="navbar-brand md-hidden">
                                    <a href="<?php echo base_url(); ?>">
                                        <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
                                    </a>
                                </div>
                                <div class="md-hidden">                                                                       
                                    <button class="btn btn-default btn-navbar btn-search"  type="button">
                                        Search <i class="fa fa-search fa-2x"></i>
                                    </button>
                                </div>

                                <!---------------------- Mobile Friendly Menu(< 767px) ----------------------->

                                <span class="open-nav-btn" onclick="openNav()">&#9776;</span>
                                <div id="myNav" class="overlay">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                    <ul class="overlay-content">
                                        
                                        <!--------------- dropdown menu ------------------>

                                        <?php require 'dropdown-menu.php'; ?>

                                        <!--------------- dropdown menu --------------------->

                                    </ul>
                                </div>
                            </div>

                            <!---------------------- Desktop, laptop Devices Menu(> 767px) ----------------------->

                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav">
                                    <!--------------- dropdown menu ------------------>

                                    <?php require 'dropdown-menu.php'; ?>

                                    <!--------------- dropdown menu --------------------->
                                    <li class='sm-hidden'>     
                                        <form  action="<?php echo base_url(); ?>search-result" method="get" class="nav-form" enctype="application/x-www-form-urlencoded">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="search" pattern="[a-zA-Z, -.0-9()]{2,150}" placeholder="Search For Reports" required value="<?php if(isset($search))echo $search; ?>" autocomplete='off'  id='search'/>
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default btn-search" type="submit">
                                                        <span>Search <i class="fa fa-search"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>                               
                                        <!-- <button class="btn btn-default btn-navbar"  type="button">
                                            <i class="fa fa-search"></i>
                                        </button> -->
                                    </li>
                                </ul>                               
                            </div>
                        </div>
                    </nav>
                </div>
            </div>                                 
            <div class='search-div md-hidden'>
                <form  action="<?php echo base_url(); ?>search-result" method="get" class="nav-form" enctype="application/x-www-form-urlencoded">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" pattern="[a-zA-Z, -.0-9()]{2,150}" placeholder="Search For Reports" required value="<?php if(isset($search))echo $search; ?>" autocomplete='off'  id='search'/>
                        <div class="input-group-btn">
                            <button class="btn btn-default btn-search" type="submit">
                                <span>Search <i class="fa fa-search"></i></span>
                            </button>
                            <button class="btn btn-default btn-navbar-close" type="button">
                                <span><i class="fa fa-times fa-2x"></i></span>
                            </button>
                        </div>
                    </div>
                </form>
            </div> 
            <div id='suggestions'></div>
        </header> 
        
        <script type="text/javascript">
            function openNav() {
            document.getElementById("myNav").style.height = "100%";
            }

            function closeNav() {
            document.getElementById("myNav").style.height = "0%";
            }
        </script> 