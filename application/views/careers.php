<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Careers | Foresee Convey Markets'; ?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Careers Application Form ,Career Opportunities at Foresee Convey Markets' ; ?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Go through our Careers page and here you will find various Career Opportunities at Foresee Convey Markets'; ?>  "/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/captcha.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>
<script type="text/javascript">
    
// Form validation 

    function validate_captcha()
    {
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code)
        {
           CaptchaError.innerHTML="Invalid Captcha Code";
           return false;
        }
        else
        {
            CaptchaError.innerHTML="";
            return true;
        }
    }
</script>
<style type="text/css">
    @media screen and (max-width:767px){
        .form-horizontal .form-group{
           margin:0px;
        }
    }
    label.control-label{
        color: #fff;
    }
    .content-block-shadow .form-control::placeholder,input#txtCaptcha,input#resume.form-control{
        color: rgb(0, 0, 0, 0.7);
        font-size: 1em;
    }
    .content-block-shadow .form-control{
        background-color: #ccc;
    }
    .has-error .checkbox, .has-error .checkbox-inline, .has-error .control-label, .has-error .form-control-feedback, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label{
        color: #f51e2e;    
    }
    p#CaptchaError{
        color:#f51e2e;
        font-size: 14px;
    }
    .content-block {
        padding: 5px;
    }
    .content-block:hover{
        box-shadow: none;
        cursor: default;
    }
    .content-block-shadow{
        background: rgb(0, 0, 0, 0.5);
        border: none;
        box-shadow: none;
    }
    .home-contact-block{
        background: #eee;
    }
</style>
<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Careers</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-chart-line fa-2x"></span></h2>
    <h1>CAREERS</h1>
</div> 


<div class="career-block">
    <div class="career-content">
        <br/>
        <center>
            <?php
            if (isset($successText)) {
                echo "<div class='container'>".@$successText."</div>";
                echo '<meta http-equiv="refresh" content="2,url=' . base_url() . 'careers"/>';
            }
            ?>
        </center><br/>
        <div class="row">
            <div class="col-md-12">
                <div class="career-heading">
                    <div class="blockquote-block">
                        <blockquote>
                            <p>There is only one way to avoid criticism: Do nothing, say nothing, and be nothing.</p>
                            <footer>Aristotle</footer>
                        </blockquote>
                    </div>
                </div><br/>

                <div class='home-contact-block'>
                    <div class="row info-block static-block">
                        <div class="col-md-12" align='center'>            
                            <h3 class='why-join report-heading'>Why Join Us?</h3>
                        </div>
                        <div class="col-md-12">
                            <div class="info-block-row career-info-block">
                                <div class="row">
                                    <div class="col-md-6 col-6-info" align="center">
                                        <h4>Work Life Balance</h4>
                                        <p>We believe that quality time out of the office is essential to the satisfaction of our employees at work. That's why our flexible PTO plan encourages team members to take time off so they can get up to speed. </p>
                                    </div>
                                    <div class="col-md-6">
                                        <img class="img img-responsive" src='<?=base_url();?>assets/images/work-life-balance.jpg' alt='Work Life Balance At FCM' />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 smx-hidden sm-hidden">
                                        <img class="img img-responsive" src='<?=base_url();?>assets/images/motivation.jpg' alt='Stay Motivated At FCM' />
                                    </div>
                                    <div class="col-md-6 col-6-info" align="center">
                                        <h4>Stay Motivated</h4>
                                        <p>We motivate our Employees Employees and let them benefit from two free Lifetime FCM sites to pursue their creative passions and collateral commitments.</p>
                                    </div>
                                    <div class="col-md-6 mdx-hidden">
                                        <img class="img img-responsive" src='<?=base_url();?>assets/images/motivation.jpg' alt='Stay Motivated At FCM' />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" align="center">
                                        <h4>We Value Our People</h4>
                                        <p>We really want to share the successes of Foresee Convey Markets. All employees receive a share of the company as part of their total compensation.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <img class="img img-responsive" src='<?=base_url();?>assets/images/we-value-employee.jpg' alt='We Value Our Employee At FCM' />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 smx-hidden sm-hidden">
                                        <img class="img img-responsive" src='<?=base_url();?>assets/images/live-it-up.jpg' alt='Live It Up At FCM' />
                                    </div>
                                    <div class="col-md-6" align="center">
                                        <h4>Live It Up</h4>
                                        <p>One of the benefits of having great coworkers is enjoying each other's company, and we find great excuses to have fun together. Squarespace's weekly catered happy hours are a great way to connect with colleagues and unwind after the week.</p>
                                    </div>
                                    <div class="col-md-6 mdx-hidden">
                                        <img class="img img-responsive" src='<?=base_url();?>assets/images/live-it-up.jpg' alt='Live It Up At FCM' />
                                    </div>
                                </div><br/>
                            </div>
                        </div>
                    </div><br/>
                </div><br/>

                <div class="col-md-11 col-md-offset-1">
                    <button class="btn btn-danger no-jobs"><span class="fa fa-hourglass-end"></span>&nbsp;&nbsp; No Job Openings</button>
                    <br/><br/>
                    <div class='job-text'>
                        <h3>Sorry ! We do not have job openings right now !</h3>
                        <h4>Don't worry ! Drop your resume here we will get back to you !</h4>
                        <br/>
                    </div>
                </div>
            </div>
        </div><br/>
        <div class="row">
            <div class="col-md-8 col-md-offset-1 col-career">
                <div class="content-block-shadow"><br/>
                    <form class="form-horizontal" id="form" role="form" action="<?php echo base_url(); ?>career-form-process" method="post" data-toggle="validator" onsubmit="return validate_captcha()" style="text-align:left;padding: 10px" enctype="multipart/form-data">
                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <label class="control-label">Your Name <span class="star">*</span> :</label>
                                <input class="form-control" autocomplete="name" name="name" id="name"  placeholder="Please Enter Your Name" data-bv-field="name" type="text" data-error="Please enter your name" required/>
                                <div class="help-block with-errors"></div> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <label class="control-label">Your Email <span class="star">*</span> :</label>
                                <input class="form-control" autocomplete="email" name="email" id="email"  placeholder="Please Enter Your Email" data-bv-field="email" type="email" data-error="Please enter valid email address" required/>
                                <div class="help-block with-errors"></div> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <label class="control-label">Describe Yourself <span class="star">*</span> :</label>
                                <textarea data-error="Please mention your skills in brief..." rows="5"  class="form-control" name="message" id="message" data-bv-field="message"  required placeholder="Mention your skills and work experience in brief..."></textarea>
                                <div class="help-block with-errors"></div> 
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <label class="control-label">CV <span class="star">*</span> :</label>
                                <input name="resume" id="resume" class="form-control" data-bv-field="resume" type="file" data-error="Please attach resume"  onchange="return ValidateExtension();"    required/>
                                <div class="help-block with-errors"></div> 
                                <div style="color:#a94442"><span id="feedback"></span><?php if (isset($error)) { echo $error ; } ?></div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <label class="control-label" for="security_code">Captcha Code: <span class="star">*</span>: </label>
                                    <div class="input-group" style="padding-right:0;">
                                        <input data-error="Please enter security code" onkeyup="validate_captcha()"  type="text" name="captcha_code"   placeholder="Security Code" class="form-control" maxlength="6" id="security_code" required data-bv-field="security_message"/>
                                        <input type="text" id="txtCaptcha" readonly class="form-control"/>
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary btn-captcha" type="button"  onclick="DrawCaptcha();"> <span class="fa fa-sync"></span> </button>
                                        </span> 
                                    </div>
                                <br/>
                                <p id="CaptchaError"></p>
                                <div class="help-block with-errors"></div> 
                            </div>
                        </div>
                        <div class="col-md-offset-2">
                            <button type="submit" class="btn btn-submit">Apply</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->

<script>
function ValidateExtension() {
        var allowedFiles = [".doc", ".docx", ".pdf", ".xls",".xlx"];
        var fileUpload = document.getElementById("resume");
        var feedback = document.getElementById("feedback");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.value.toLowerCase())) {
            feedback.innerHTML = "Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.";
            return false;
        }
        feedback.innerHTML = "";
        return true;
    }
</script>