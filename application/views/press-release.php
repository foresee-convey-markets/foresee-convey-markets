<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<title>Press Relase |  Foresee Convey Markets</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="Foresee Convey Markets Press Release, Press Release " />
<meta name="description" content="Foresee Convey Markets&#039;s press release section features all the industry statistics cited by the global news publication sites."/>
<meta name="author" content="Foresee Convey Markets"/>


<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
    })
</script>

<link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
    });
</script>

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Press Releases</li>
</ul>

<div class="latest-report page-banner" align='center'>  
    <h2><span class="fa fa-newspaper fa-2x"></span></h2>
    <h1>PRESS RELEASES</h1><br/>
</div>

<div class="row press-row banner-row" style="margin:0px;">
    <?php if (isset($count)) {?>
        <?php if(@$count > 0){ ?>
        <div class="row">
            <div class="col-md-5 col-pagination"><?php if (isset($pagination)) echo $pagination; ?> </div>
            <div class="col-md-3"><br/>
                <h4>Total Press Releases : <strong><?= @$count; ?></strong> </h4>
            </div>
            <div  class="col-md-4"><br class="sm-hidden"/>
                <h4 class="pull-right">
                <?php if (isset($count)) {
                ?> Page <?= @$page?> of <?=@$total_pages?>
                <?php  } ?>
                </h4>
            </div>
        </div>
        <?php } ?>
        <div class="col-md-3 md-hidden"><br/>
            <div class="filter-block">
                <h3 align='center' class="btn-filter accordion">Search By Filters </h3>
                <div class="panel-info panel-accordion">
                    <form method="get" id='filterForm' action="<?=@$current_url?>" enctype="application/x-www-form-urlencoded">
                        <div class="panel-body">
                            <select class="form-control selectpicker"  name="category" id="category" placeholder='Filter By Category'>
                                <option value='' <?=(@$category == '') ? 'selected' : ''?> >Filter By Categories</option>
                                <?php foreach(categories() as $key => $value){
                                    $selected=(@$key == @$category) ? 'selected' : '';
                                    echo "<option value='".@$key."' ".@$selected." >".ucfirst(@$value)."</option>";
                                } ?>
                            </select>
                        </div><br/>
                        <div class="panel-body">
                            <select class="form-control selectpicker" name="year" id="year" placeholder='Filter By Year'>
                                <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                                <?php for($index=2019;$index<=date('Y');$index++){ 
                                    $selected=(@$index == @$year) ? 'selected' : '';
                                    echo "<option value='".$index."' ".$selected.">".$index."</option>";
                                } ?>
                            </select>
                        </div><br/>
                        <div class="panel-body">
                            <select class="form-control selectpicker" name="date" id="date" placeholder='Filter By Date Order'> 
                                <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                                <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                                <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                            </select>
                        </div><br/>

                        <div class="panel-body">
                            <button type="submit" class="btn btn-search-filter btn-apply">Apply <span class="fa fa-hourglass"></span></button>
                            <button type="reset" class="btn pull-right btn-search-filter btn-clear" onclick="window.location.href='<?=@$current_url?>'">Clear <span class="fa fa-times"></span></button> 
                        </div><br/>
                    </form>
                </div>
            </div><br/>
        </div>
        <div class="col-md-9">
            <div align="center">
                <?php
                    if ($count == 0) {
                        echo "<br/>
                        <div class='col-md-12 no-data'>                                                
                            <div class='alert alert-info'>
                                <h3><i class='fa fa-exclamation-triangle'></i> &nbsp;<span>Sorry ! No Press Releases Found. Try Other Search Filter</span></h3>
                            </div>
                        </div>";
                    }
                ?>
            </div> 
            <?php if(@$press_list){
                $i=1;
                foreach ($press_list->result() as $row) { 
                    // $press_url=str_ireplace('%2f','-', $row->url);
                ?>
                <div class="report-details-block press-block"><br/>
                    <div class="row" style="margin:0px;">
                        <div class="col-md-4" align="center">
                            <img class="img img-responsive" onclick="window.location.href='<?php echo base_url();?>press/<?=urlencode(@$row->url); ?>'" style="height:220px;" src="<?php echo base_url(); ?>assets/images/press/<?php echo $row->image; ?>" alt="<?php echo $row->title; ?>"  />
                        </div>
                        <div class="col-md-8">
                            <p class='pr-title' title="<?=$row->title?>"><a href="<?php echo base_url(); ?>press/<?=urlencode($row->url); ?>"  rel="follow"><?php echo substr($row->title,0,130)."..."; ?></a></p>
                            <div class='rep-desc press-desc'><?=substr(strip_tags($row->content), 0, 300).".." ?></div>
                            <h5 class="report-content press-content">                            
                                <span class='category-name'><?=categories()[@$row->cat_id]?></span>&nbsp;<strong class="sm-hidden">|</strong>&nbsp;
                                <span> Published :
                                    <?php $date = date_create($row->published_on);
                                    echo date_format($date, 'M Y'); ?>
                                </span>
                            </h5>                             
                            <a class="btn btn-details pull-right btn-view" title="View This Press Release" href='<?=base_url();?>press/<?=urlencode($row->url); ?>'>View More <i class="fa fa-info-circle" style="margin-top: 3px;"></i></a>
                        </div>
                    </div>
                </div>
            <?php $i++; }  } ?>
            <div class="clearfix">
                <div style="display:inline-block"><?php if (isset($pagination)) echo $pagination; ?> </div>
            </div>
        </div>
        <div class="col-md-3 sm-hidden sticky"><br/>
            <div class="filter-block"><br/>
                <h3 align='center' class="btn-filter">Search By Filters </h3>
                <div class="panel-info">
                    <form method="get" id='filterForm' action="<?=@$current_url?>" enctype="application/x-www-form-urlencoded">
                        <div class="panel-body">
                            <select class="form-control selectpicker"  name="category" id="category" placeholder='Filter By Category'>
                                <option value='' <?=(@$category == '') ? 'selected' : ''?> >Filter By Categories</option>
                                <?php foreach(categories() as $key => $value){
                                    $selected=(@$key == @$category) ? 'selected' : '';
                                    echo "<option value='".@$key."' ".@$selected." >".ucfirst(@$value)."</option>";
                                } ?>
                            </select>
                        </div><br/>
                        <div class="panel-body">
                            <select class="form-control selectpicker" name="year" id="year" placeholder='Filter By Year'>
                                <option value='' <?=(@$year == '') ? 'selected' : ''?> >Filter By Year</option>
                                <?php for($index=2019;$index<=date('Y');$index++){ 
                                    $selected=(@$index == @$year) ? 'selected' : '';
                                    echo "<option value='".$index."' ".$selected.">".$index."</option>";
                                } ?>
                            </select>
                        </div><br/>
                        <div class="panel-body">
                            <select class="form-control selectpicker" name="date" id="date" placeholder='Filter By Date Order'> 
                                <option value='' <?= (@$date_order == '') ? 'selected' : ''?>>Filter By Date</option>
                                <option value='DESC'  <?= (@$date_order == 'DESC') ? 'selected' : ''?> >Recent</option>
                                <option value='ASC' <?= (@$date_order == 'ASC') ? 'selected' : ''?> >Old</option>                                
                            </select>
                        </div><br/>

                        <div class="panel-body">
                            <button type="submit" class="btn btn-search-filter btn-apply">Apply <span class="fa fa-hourglass"></span></button>
                            <button type="reset" class="btn pull-right btn-search-filter btn-clear" onclick="window.location.href='<?=@$current_url?>'">Clear <span class="fa fa-times"></span></button> 
                        </div><br/>

                    </form>
                </div><br/>
            </div><br/>
        </div>
    <?php }else{?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css"/>
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script>
            $(function(){
                new WOW().init();
            })
        </script>

        <div class='col-md-12 no-data'>                                                
            <div class='alert alert-info' align='center'>
                <h2 class="wow pulse infinite" data-wow-duration="2s" data-wow-delay="0.5s"><i class='fa fa-clock'></i> &nbsp;<span>Coming Soon ...</span></h2>
            </div>
        </div>               
    <?php }  ?>
</div>

<script type="text/javascript">

var data=<?php echo (isset($_GET) && !empty($_GET)) ? '1' : '0' ?>;
if(data == '1'){
    var w=$(window).width();
    if(w>767){
        $('html,body').animate({
            scrollTop: ($('.press-row').offset().top + 60)       
        }, 'slow');
    }
}
var panel = $('.accordion').siblings('.panel-accordion')[0];
if(data == '1'){
    $('.accordion').addClass('active')
    let maxHeight = panel.scrollHeight + "px";
    $(panel).css({'max-height' : maxHeight})
    
}else{
    $('.accordion').removeClass('active')
    $(panel).css({'max-height' :0})
}
$('.accordion').click(function(){
    $(this).toggleClass("active");
    var panel = $('.accordion').siblings('.panel-accordion')[0];
    console.log('clicked')
    if (panel.style.maxHeight === '0px') {
        let maxHeight = panel.scrollHeight + "px";
        console.log(maxHeight)
        $(panel).css({'max-height' : maxHeight})
    } else {
        console.log('0px')                  
        $(panel).css({'max-height' : 0})
    } 
})
</script>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->





