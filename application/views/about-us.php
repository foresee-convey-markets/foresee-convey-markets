<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>About Us | Foresee Convey Markets</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="About Foresee Convey Markets" />
<meta name="description" content="At Foresee Convey Markets we tend to aim to create it easier for decision makers to search out relevant info and find right research reports which might save their time and assist in what they are doing best, i.e. take time-critical choices."/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>About Us</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-users fa-2x"></span></h2>
    <h1>About Us</h1><br/>
</div>
<div class="container-fluid page-container">
    <div class='content-block'>
        <div class='row' style='margin : 0px'>
            <div class='col-md-12'>
                <div class='about-para'>
                    <h2 class="about-para-h2">What we do ?</h2>
                    <img class="img img-responsive" style="background: #ccc;" src="<?=base_url()?>assets/images/about-us.jpg" alt="What We Do" />
                    <p>foreseeconvey-markets.com is the leading provider of industry intelligence for businesses, consultants, investors, and anyone seeking to grasp where markets, countries, or companies are headed. With over 350 publishers covering every sector of the economy similarly as emerging industries, we curate the foremost comprehensive collection of market reports and services updated daily.</p>
                    <p>Our reports are designed to want the legwork out of understanding the market and help over 10,000 customers a year get smart quickly. Hard-to-find information is sourced, vetted, and compiled in one place. But we also transcend the facts and figures to produce the crucial history, context, and dimensions that make the knowledge meaningful.</p>
                    <p>And if you've got a very specific question or unique research need, we have a full team of expert analysts at the able to facilitate your answer it in every possible aspect and according to every possible market trends. </p>
                    <p>We have been within the research business for over 20 years. After we started, it had been about providing access to data. Today it's about making that data actionable.</p>
                </div>
            </div>
        </div>
    </div>

    <div class='vision-block'>
        <div class='row' style='margin : 0px'>
            <div class='col-md-5' align='center'>
                <img class="img img-responsive" src="<?=base_url()?>assets/images/vision-mission-values.png" alt="Vision, Misson & Core Values" />
            </div>
            <div class='col-md-6 col-md-offset-1'>
                <div class='about-para'>
                    <h2 class="about-para-h2">Vision</h2>
                    <p>At Foresee Convey Markets, we envision to estimate customer needs by providing transparent and market information &amp; implement correct strategies to develop the organization with logical and valuable decisions.</p>
                </div><br/>
            </div>
        </div>
        <div class="row">
            <div class='col-md-6'>
                <div class='about-para'>
                    <h2 class="about-para-h2">Mission</h2>
                    <p>We aim to provide the best quality of Foresee Convey Markets reports from the top most publishers of the world to you at the most affordable price you can ever get.</p>
                </div><br/>
            </div>
            <div class='col-md-6'>
                <div class='about-para'>
                    <h2 class="about-para-h2">Core Values</h2>                
                    <p>Authenticity, Social Responsibility, Innovation and trustworthiness are the cornerstone of what we do here at <strong>Foresee Convey Markets</strong>.</p>
                </div><br/>
            </div>
        </div>
    </div>
</div>
<div class='home-contact-block'>
    <div class="row info-block static-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Contact</h3>
            <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
        </div>
        <div class="col-md-9">
        <div class="info-block-row">
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-lock fa-2x icon-home'></h1>
                        <h4>RELIABLE AND PROTECTED</h4>
                        <p>Your sensitive information and data  <strong>are <br/>secure</strong> with us.</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-globe fa-2x icon-home'></h1>
                        <h4>GLOBAL CHOICE</h4>
                        <p>Get access to over <strong>1M+ Forecast</strong><br/><strong>Research Reports</strong></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-users fa-2x icon-home'></h1>
                        <h4>CUSTOMER ORIENTED</h4>
                        <p>We are available <strong>24*7</strong> <br/>for <strong class='blue'>365 days</strong> in a year</p>
                    </div>
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-handshake fa-2x icon-home'></h1>
                        <h4>TRUSTED BY ALL</h4>
                        <p><strong>500+ Contented Clients </strong> Globally.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->








