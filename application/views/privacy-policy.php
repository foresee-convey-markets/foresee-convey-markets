<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Privacy Policy | Foresee Convey Markets</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content= "Privacy Policy, Foresee Convey Markets privacy policy,Privacy Policy Foresee Convey Markets" />
<meta name="description" content="Foresee Convey Markets Refund policy states that The reports provided by Foresee Convey Markets are strictly intended for in-house use by the client and should not be used for general publication or distribution to third parties. Foresee Convey Markets is not responsible for the incorrect report received from publisher."/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Privacy Policy</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-shield-virus fa-2x"></span></h2>
    <h1>PRIVACY POLICY</h1><br/>
</div>

<div class="container-fluid page-container" >
    <div class="content-block col-content">
        <div class='row' style='margin : 0px'>
            <div class='col-md-12'>
                <h4>We have recently made some changes in our policies. In accordance with its necessary business practices <a href='<?=base_url();?>'>foreseeconvey-markets.com</a>, Inc. collects and processes personal data referring to its customers. <a href='<?=base_url();?>'>foreseeconvey-markets.com</a>, is committed on being transparent about how it collects and uses the data, and to meeting its data protection obligations as defined by the law.</h4>

                <p class="terms"><strong>What information can we collect?</strong></p>

                <p class="terms"><strong>IP Address:</strong></p>
                <h4>After you visit this site, your IP address is recorded. We use your IP address to assist diagnose problems with our server and to administer our website by creating statistical reports and tracking errors. Your IP address is employed to assist identify you after you buy and to assemble broad demographic identification which will not be tied to your identity.</h4>
                
                <p class="terms"><strong>Personal Identification Information:</strong></p>
                <h4>All the information including name, address, contact details, email, phone. This information is collected</h4>
                <ul class="list-item">
                    <li>After you buy our Alert or newsletter services,</li>
                    <li>Once you want to look at detailed information about our products, and/or </li>
                    <li>After you want to submit a matter about our products/services to our customer maintenance staff</li>
                </ul>
                
                <p class="terms"><strong>Usage and get History:</strong></p>
                <h4>Name and Email Address of Colleague(s): If you choose to use our "Email to a Colleague" feature to tell others about our site or about particular products on our site, we ask you for your name and email address and your colleague's name and email address. <a href='<?=base_url();?>'>foreseeconvey-markets.com</a> will automatically send the colleague a one-time email inviting them to go to the positioning. <a href='<?=base_url();?>'>foreseeconvey-markets.com</a> doesn't store this information. it's used for the only purpose of sending this one-time email and tracking the success of the referral program.</h4>
                
                <p class="terms"><strong>Cookies and other technical or site function data:</strong></p>
                <h4>This site uses "cookies" and other technology to record usage of the web site. this offers us information about what number people visit certain pages on the positioning. <br/>
                <a href='<?=base_url();?>'>foreseeconvey-markets.com</a> may collect this information during a sort of ways. for instance, data may be contained in website registration forms, customer-initiated chat applications, payment submission forms, notes collected through phone conversations, etc. We may additionally collect personal data about you from third parties. We don't collect special categories of private data.</h4>
                <h4>Data are stored in a very range of various places, including in HR management systems, on other IT systems (including email), and marketing and sales-related software/spreadsheets.</h4>
                
                <p class="terms"><strong>Why does foreseeconvey-markets.com process personal data?</strong></p>
                <h4>We need to process data to require steps to enter in to a contract with you or to still fulfill a contract with you, which can even be one purchase. for instance, to register you as a brand new customer, process and deliver your order, manage our relationship with you, enable you to complete surveys, deliver relevant website content, make recommendations to you for goods and services, etc. In some cases, processing your data is critical to our legitimate business interests, like to check how customers use our products/services or to recover debts owed to us. In some cases, we want to process data to make sure that we are complying with legal obligations as defined by the suitable governments.</h4>
                <h4>All information we collect will help us maintain your account and supply services to you.</h4>

                <p class="terms"><strong>Who has access to data?</strong></p>
                <h4>Your information is also shared internally for legitimate business reasons, like customer support, marketing, or other client relationship building activities. This includes members of the marketing and sales teams, company analysts, Human Capital, and IT.</h4>
                <h4><a href='<?=base_url();?>'>foreseeconvey-markets.com</a> may disclose your information to 3rd parties if we determine that such disclosure is important to satisfy the contract/provide requested services to our customers or to adjust to the law. Some specific examples include:</h4>

                <p class="terms"><strong> Publishers:</strong></p>
                <h4>We may share personal information with copyright holders (i.e. publishers or authors) for any products you buy. The publisher(s) may send you marketing materials and if you are doing not wish to receive these materials you may must contact the publisher directly so as to opt-out.</h4>
                
                <p class="terms"><strong>Shipping of Mailed Purchases: </strong></p>
                <h4>Our publishers use an outdoor shipping service provider to satisfy applicable orders. These orders are shipped to you directly from the publisher via a shipping provider, like FedEx or DHL.</h4>
                
                <p class="terms"><strong>Payment Processing: </strong></p>
                <h4>Once you buy something with a mastercard, we use a mastercard processing service provider to verify your mastercard and bill you.<br/>
                    Other than those third parties indicated above, we don't share, sell, rent, or trade personal identifiable information with other third parties for his or her promotional purposes.</h4>

                <p class="terms"><strong>For how long does foreseeconvey-markets.com keep data?</strong></p>
                <h4><a href='<?=base_url();?>'>foreseeconvey-markets.com</a> will retain your information for as long because it is important to produce you with services and/or complete data analytics processes. <a href='<?=base_url();?>'>foreseeconvey-markets.com</a> might also use your information so as to fits legal obligations, resolve disputes, or enforce contracts and agreements.</h4>
                <h4> As a knowledge subject you've got variety of rights. European clients and visitors can submit a subject matter Access Request (SAR) to our Data Protection Officer to:</h4>
                <ul class="list-item">
                    <li>Access and acquire a duplicate of your data for the asking to the (right of transparency)</li>
                    <li>Request the organization to vary incorrect or incomplete data</li>
                    <li>Request the organization to delete (right of erasure) or stop processing your data, as an example where the information is not any longer necessary for the needs of processing</li>
                </ul>
                <p class="terms"><strong>Changes during this Privacy Statement:</strong></p>
                <h4>If we commit to change our privacy policy, we'll post those changes to the current privacy statement on the house page and other places we deem appropriate so you're attentive to what information we collect, how we use it, and under what circumstances, if any, we disclose it.</h4>
                <h4>We reserve the correct to switch this privacy statement at any time, so please review it frequently. If we make material changes to the current policy, we are going to notify you here, by email, or by means of a notice on our home page. </h4>

                <p class="terms"><strong>Legal Disclaimer:</strong></p>
                <h4>We reserve the right to disclose your information by law.</h4> 

            </div>
        </div>
    </div>
</div>
<div class='home-contact-block'>
    <div class="row info-block static-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Contact</h3>
            <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
        </div>
        <div class="col-md-9">
            <div class="info-block-row"><br/>
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                        <h4>LOCATION</h4>
                        <p>1445 Woodmont Ln NW, Atlanta, Ga 30318</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-phone fa-2x icon-home'></h1>
                        <h4>CALL / EMAIL US</h4>
                        <p><a href='tel:+18442677928'>+1-844-267-7928</a> <br/>  <a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                        <h4>CONTACT US</h4>
                        <a class='btn-report' href="<?=base_url()?>contact-us" title='Contact Us'>Contact Us</a>
                    </div>
                    <div class="col-md-6 cr-block" align="center">
                        <h1 class='fa fa-search fa-2x icon-home'></h1>
                        <h4>CUSTOM RESEARCH</h4>
                        <a class='btn-report' href="<?=base_url()?>custom-research" title='Custom Research'>Custom Research</a>
                    </div>
                </div><br/>
            </div>
        </div>
    </div>
</div>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
