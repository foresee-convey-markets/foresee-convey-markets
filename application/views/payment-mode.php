<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Mode of Payments | Foresee Convey Markets';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Mode of Payments Foresee Convey Markets,Payment Modes ,Transaction modes ,Transaction Modes,Payment Methods';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Foresee Convey Markets offers various payment methods like Bank Wire Transfer,Online Payment Systems like PayPal, Paytm, Google Pay and Credit Cards.';?>"/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Payment Modes</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-wallet fa-2x"></span></h2>
    <h1>PAYMENT MODES</h1><br/>
</div>    

<div class="container-fluid page-container">
    <div class="payment-mode content-block col-content">
        <div class='row' style='margin : 0px'>
            <div class='col-md-12'>
                <p>After selection of report, the client can make payment through following mediums:</p>
                <ul class="list-item">
                    <li>Bank Transfer</li>
                    <li>Net banking</li>
                    <li>Payment Wallet (Paytm/UPI/BHIM)</li>
                    <li>Online Payment via Credit Card/Debit Card (Master Card, Visa Card, American Express Cards) through <strong>Paypal</strong> and <strong>EazyPay</strong> from our website.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->

