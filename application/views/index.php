<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Home | Foresee Convey Markets</title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="Foresee Convey Markets,Industry Reports, Business Research Process, Market News,  Research Report, Business Research, Industrial Analysis, Market Research Analysis,  USA Marketing Research Process, China Research Report, Japan Business Market Research Report, Europe Industrial Analysis,Industry Analysis Report" />
<meta name="description" content="Foresee Convey Markets is a Global Leader In Strategic Research Reports and is the leading provider of market research reports which offers premium progressive statistical surveying, market research reports, analysis &amp; forecast data for industries around the globe. "/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slideshow.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>

<script type="text/javascript" src="assets/js/prefunctions.js"></script>
<script type="text/javascript" src="assets/js/sliker.js"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css"/>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script>
    $(function(){
        new WOW().init();
    })
</script>

<script>
$(document).ready(function(){
    $('#slider_4,#slider_41').sliker({
        'nbr_li': 1,
        'liquid': 0,
        'vitesse_auto': 3000,
        'vitesse': 1,
        'auto': 1,
        'loop': 0,
        'arrows': 0,
        'drag': 0
    });
});
</script>
<style>
    body{
        background: #fff;
    }
</style>


<div class="image-banner-block">
    <div class='container'>
        <div class="col-md-12">
            <h1 align='center' class="banner-title"><span>Global Leader In</span> <br class='md-hidden'/><big>Market Research</big> that <big>Forecast Growth</big></h1>
        </div>
        <!-- <div class="row banner-block">
            <div class="col-md-12">                                                 
                <div class='home-search-div'>
                    <form  action="<?php echo base_url(); ?>search-result" method="get" class="nav-form">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" pattern="[a-zA-Z, -.0-9()]{3,150}" placeholder="Search For Reports" required value="<?php if(isset($search))echo $search; ?>" autocomplete='off'  id='search'/>
                            <div class="input-group-btn">
                                <button class="btn btn-default btn-search" type="submit">
                                    <span>Search <i class="fa fa-search"></i></span>
                                </button>
                                <button class="btn btn-navbar-close" title="Clear The Search" type="reset">
                                    <span><i class="fa fa-times fa-2x"></i></span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    </div>
</div>

<div class='container-fluid home-block'>
    <div class="home-display-block">
        <h3 class='report-heading <?=@$report->num_rows() == '0' ? 'press-heading' : '';?>'><span class="fa fa-chart-line wow pulse infinite" data-wow-duration="2s" data-wow-delay="0.5s"></span> Latest Reports</h3>
        <?php if($report->num_rows() > 0){ ?>
            <div id="slider_4" class="sliker">
                <div class="sliker__window">
                    <ul class="sliker__track">
                        <?php foreach($report->result() as $row): 
                         
                        $report_title = explode('Market',$row->rep_title);
                        if(isset($report_title[0]) && strlen($report_title[0]) == 0){
                        $report_title_array = explode(" ", $row->rep_title);
                        $first_word_length = strlen($report_title_array[0]);
                        $report_title = explode('Market',substr($row->rep_title, $first_word_length));
                        }
                        $report_title = ucwords(str_ireplace(array(' -','- ','-'),' - ',$report_title[0]).' market');
                        $report_title = strlen($report_title) > 130 ? substr($report_title , 0 ,130)."..." : $report_title;
                        ?>
                        <li class="sliker__item">
                            <p class='sector-title'><?=categories()[@$row->rep_sub_cat_1_id]?></p>
                            <p><a class="category-title" href="<?php echo base_url(); ?>reports/<?php echo $row->rep_id; ?>/<?php echo $row->rep_url; ?>"><?=@$report_title?></a></p>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class='btn-block'>
                <a class='btn-report' href="<?=base_url()?>latest-reports" title='Browse All Reports'>Browse All Reports <span class='fa fa-chevron-circle-right'></span> </a>
            </div>
        <?php }else{ ?>
            <div class="row">
                <div class='col-md-12 no-data'>                                                
                    <div class='alert alert-info' align='center'>
                        <h2 class="wow pulse infinite" data-wow-duration="2s" data-wow-delay="0.5s"><i class='fa fa-clock'></i> &nbsp;<span>Coming Soon</span></h2>
                    </div>
                </div> 
            </div>
        <?php }  ?>
        <br/>
    </div>
    <br/>
    <div class="home-display-block">
        <h3 class='report-heading'><span class="fa fa-industry wow pulse infinite" data-wow-duration="2s" data-wow-delay="0.5s"></span> Industry Sectors We Serve</h3>
        <div id="slider_41" class="sliker">
            <div class="sliker__window">
                <ul class="sliker__track">
                    <?php $i=1;
                    foreach($categories->result() as $row):
                        if($row->sc1_id !== '14'){
                            if($i == 9) { continue; }
                    ?>
                    <li class="sliker__item  <?=strlen(@$row->sc1_name) > 25 ? 'sliker__item__cat' : ''?>">
                        <div align='center'><img align='center' class='img img-responsive img-category' src="assets/images/category/<?=getCategoryIcon()[@$row->sc1_id]?>" alt="<?=$row->sc1_name?>" /></div>
                        <p><a class="category-title" href="<?php echo base_url(); ?>category/<?php echo $row->sc1_url; ?>"><?=str_ireplace('and','&amp;',$row->sc1_name)?></a></p>
                    </li>
                <?php $i++;
                        } 
                    endforeach; 
                ?>
                </ul>
            </div>
        </div>
        <div class='btn-block'>
            <a class='btn-report' href="<?=base_url()?>categories" title='Browse All Sectors'>Browse All Sectors <span class='fa fa-chevron-circle-right'></span> </a>
        </div><br/>
    </div>
    <br/>
    <div class="home-display-block">
        <h3 class='report-heading <?=@$press_list->num_rows() == '0' ? 'press-heading' : '';?>'><span class="fa fa-bullhorn wow pulse infinite" data-wow-duration="2s" data-wow-delay="0.5s"></span> Press Releases</h3>
        <?php if(@$press_list && @$press_list->num_rows() > 0){
            foreach($press_list->result() as $row):
                $date = date_create($row->published_on) ?>
                <div class="col-md-12 col-press">
                    <!-- <img class="img img-responsive" style='height:120px' src="<?php echo base_url(); ?>assets/images/blogs/<?php echo $row->image ?>" alt="<?php echo $row->title; ?>" /><br/> -->
                    <a class="press-title" href="<?php echo base_url(); ?>press/<?php echo $row->url; ?>"><?=substr($row->title,0,90)?></a>
                    <p class="calendar"><i class="fa fa-calendar text-danger"></i>&nbsp;&nbsp; <?php echo date_format($date, 'M Y'); ?></p>
                </div>
                <span class="press-line"></span>
            <?php endforeach; 
        }else{ ?>
            <div class="row">
                <div class='col-md-12 no-data'>                                                
                    <div class='alert alert-info' align='center'>
                        <h2 class="wow pulse infinite" data-wow-duration="2s" data-wow-delay="0.5s"><i class='fa fa-clock'></i> &nbsp;<span>Coming Soon</span></h2>
                    </div>
                </div> 
            </div>  
        <?php }  ?>
        <?php if(@$press_list && @$press_list->num_rows() > 0){ ?>
        <div class='btn-block'>
            <a class='btn-report' href="<?=base_url()?>press-release" title='Browse All Press Releases'>Browse All Press Releases <span class='fa fa-chevron-circle-right'></span> </a>
        </div>
        <?php } ?><br/>
    </div><br/><br/>
</div>

<div class='home-contact-block'>
    <div class="row info-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Why ?</h3>
            <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo"><br/>
        </div>
        <div class="col-md-9">
            <div class="info-block-row home-info-block">
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-lock fa-2x icon-home'></h1>
                        <h4>RELIABLE AND PROTECTED</h4>
                        <p>Your sensitive information and data  <strong>are <br/>secure</strong> with us.</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-globe fa-2x icon-home'></h1>
                        <h4>GLOBAL CHOICE</h4>
                        <p>Get access to over <strong>1M+ Forecast</strong><br/><strong>Research Reports</strong></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-users fa-2x icon-home'></h1>
                        <h4>CUSTOMER ORIENTED</h4>
                        <p>We are available <strong>24*7</strong> <br/>for <strong class='blue'>365 days</strong> in a year</p>
                    </div>
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-handshake fa-2x icon-home'></h1>
                        <h4>TRUSTED BY ALL</h4>
                        <p><strong>500+ Contented Clients </strong> Globally.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->


<script type="text/javascript" src="assets/js/postfunctions.js"></script>
