<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Categories | Foresee Convey Markets';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Categories Foresee Convey Markets, Industry Verticals ,Reports ,Industry Reports, Business Research Process, Market News,  Research Report, Business Research, Industrial Analysis, Market Research Analysis,  USA Marketing Research Process, China Research Report, Japan Business Market Research Report, Europe Industrial Analysis,Industry Analysis Report';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Foresee Convey Markets Offers wide variety of various industry reports and company reports by category and sub category';?>"/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<ul class="breadcrumb" >
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>All Categories</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-industry fa-2x"></span></h2>
    <h1>INDUSTRY SECTORS</h1><br/>
</div>
<div class="category-block">
    <div class="row"> 
        <div class="col-md-12 category-container">
            <div class="col-md-3">
                <img class="img img-responsive img-rounded" src="<?php echo base_url(); ?>assets/images/category/ICT.jpg"/>
                <div class="abs-text">
                    <span>ICT Media</span>
                    <a href='<?php echo base_url(); ?>category/information-and-communication-technology-and-media-market-report' >View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive img-rounded'); ?>" src="<?php echo base_url(); ?>assets/images/category/Medical Devices.jpg" />
                <div class="abs-text">
                    <span>Medical Devices</span>
                    <a href='<?php echo base_url(); ?>category/medical-devices'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive '); ?>" src="<?php echo base_url(); ?>assets/images/category/Pharmaceutical and Healthcare.jpg"/>
                <div class="abs-text">
                    <span>Pharmaceutical and Healthcare</span>
                    <a href='<?php echo base_url(); ?>category/pharmaceutical-and-healthcare'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Semiconductor and Electronics.jpg"/>
                <div class="abs-text">
                    <span>Semiconductor and Electronics</span>
                    <a href='<?php echo base_url(); ?>category/semiconductor-and-electronics'>View Reports &raquo;</a>
                </div>
            </div>
        </div>        
        
        <div class="col-md-12 category-container">
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Materials and Chemicals.jpg"/>
                <div class="abs-text">
                    <span>Material and Chemicals</span>
                    <a href='<?php echo base_url(); ?>category/material-and-chemicals'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Consumer goods and services.jpg"/>
                <div class="abs-text">
                    <span>Consumer Goods and Services</span>
                    <a href='<?php echo base_url(); ?>category/consumer-goods-and-services'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Agriculture.jpg"/>
                <div class="abs-text">
                    <span>Agriculture</span>
                    <a href='<?php echo base_url(); ?>category/agriculture'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Energy and Power.jpg"/>
                <div class="abs-text">
                    <span>Energy and Power</span>
                    <a href='<?php echo base_url(); ?>category/energy-and-power'>View Reports &raquo;</a>
                </div>
            </div>
        </div>
        
        
        <div class="col-md-12 category-container">
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Machinery and Equipments.jpg"/>
                <div class="abs-text">
                    <span>Machinery and Equipments</span>
                    <a href='<?php echo base_url(); ?>category/machinery-and-equipment'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Automotive and Transportation.jpg"/>
                <div class="abs-text">
                    <span>Automotive &amp; Transportation</span>
                    <a href='<?php echo base_url(); ?>category/automotive-transportation'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Aerospace and Defense.jpg"/>
                <div class="abs-text">
                    <span>Aerospace and Defence</span>
                    <a href='<?php echo base_url(); ?>category/aerospace-defense'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Food and Beverages.jpg"/>                        
                <div class="abs-text">
                    <span>Food and Beverages</span>
                    <a href='<?php echo base_url(); ?>category/food-and-beverages'>View Reports &raquo;</a>
                </div>
            </div>
        </div>        
        
        <div class="col-md-12 category-container">
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Construction and Manufacturing.jpg"/>
                <div class="abs-text">
                    <span>Construction &amp; Manufacturing</span>
                    <a href='<?php echo base_url(); ?>category/construction-manufacturing'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/BFSI.jpg"/>
                <div class="abs-text">
                    <span>BFSI</span>
                    <a href='<?php echo base_url(); ?>category/banking-financial-services-and-insurance'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Company Reports.jpg"/>                        
                <div class="abs-text">
                    <span>Company Reports</span>
                    <a href='<?php echo base_url(); ?>category/company-reports'>View Reports &raquo;</a>
                </div>
            </div>
            <div class="col-md-3">
                <img class="img img-responsive " src="<?php echo base_url(); ?>assets/images/category/Other.jpg"/>
                <div class="abs-text">
                    <span>Other</span>
                    <a href='<?php echo base_url(); ?>category/other'>View Reports &raquo;</a>
                </div>
            </div>
        </div>
    </div> 
</div>
        


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
