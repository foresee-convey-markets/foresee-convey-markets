<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Page Not Found</title>
    <link rel="shortcut icon" href="../assets/images/qq-logo.png" type="image/x-icon" />
    <link href='https://fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
    <style type="text/css">
        
    .logo p span{
        color:lightgreen;
    }   
    .back-home a{
        color:white;
        background:darkgray;
        text-decoration:none;
        padding:5px 10px;
        font-size:13px;
        font-family: arial, serif;
        font-weight:bold;
    }   
    .footer{
        color:#555;
        position:absolute;
        right:10px;
        bottom:10px;
        font-weight:bold;
        font-family:arial, serif;
    }   
    .footer a{
        font-size:16px;
        color:#ff4800;
    }   
    .back-home a{
        color:white;
        background:darkgray;
        text-decoration:none;
        padding:5px 10px;
        font-size:13px;
        font-family: arial, serif;
        font-weight:bold;
    }
    a.btn-home{
        border-radius: 0px;
        font-size:1em;
        ;
        background: #182b3e;
        text-transform: uppercase;
    }
    a.btn-home:hover{
        background: #0465ac;
        color: #fff;
        text-decoration: none;
    }
    .error-block  h1{ 
        text-align:center;
        font-size: 3em;
        ;
    }
    .wrap{
        text-align: center;
        margin-top: -10%;
    }
    .wrap > h2{
        color: maroon;
        display: none;
    }
    @media screen and (min-width:767px){
        .error-block  img{
            height:550px;
            width:60%;
        }
        .error-block  h1{ 
            transform: translate(0,-300px);
        }
    }
    @media screen and (max-width:767px){
        .error-block  h1{ 
            transform: translate(0,-170px);
        }
    }
    </style>
    

<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/qq-logo.png" type="image/x-icon" />
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href='https://fonts.googleapis.com/css?family=Asul' rel='stylesheet'/>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/website-style.css"/>
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript"  src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script>
    $(window).load(function () {
        $('.loader').fadeOut(3000);
    });

</script>

</head>
<?php //$this->output->enable_profiler(true); ?>
<body>
<div class="loader"></div>
<header>
    <nav class="navbar" style="border-radius: 0px">
        <div class="row" style="margin: 0;padding: 0">
            <div class="col-md-4 logo" align="center">
                <a href='<?php echo base_url(); ?>'><img class="img img-responsive" src='<?php echo base_url(); ?>assets/images/logo-market-research.png' alt='Market Research Logo'/></a>
            </div>
            <div class="col-md-8 top-div">
                <ul style="background: rgb(242, 240, 236);" class="top-nav list-unstyled">
                    <li class="first"><a href="<?php echo base_url(); ?>about-us">ABOUT US</a></li>
                    <li><a href="<?php echo base_url(); ?>contact-us">CONTACT US</a></li>
                    <li><a href="tel:+1-833-267-4156"><img src="<?php echo base_url(); ?>assets/images/247-icon.jpg" alt='24/7' height="25px" width="25px" class="img img-circle img-responsive"/>
                        <span class='nav-top-item'>+1-833-267-4156 &nbsp;(TOLL FREE)</span></a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>login"><span class="fa fa-user-circle 247-icon"></span> LOGIN / REGISTER</a></li>
                </ul>
            </div>
        </div>
    </nav>
   <div class="row" style="margin:0;padding:0;">
        <div class="col-md-12" style="padding:0px">
            <nav class="navbar navbar-default"  style="border-radius: 0px;overflow: hidden;">
                <div class="container">
                    <div class="navbar-header">
                        <div class="md-hidden">
                            <ul class="navbar-header-group">
                                <li><a href=""><span class="fa fa-user"></span></a></li>
                                <li><a href=""><span class="fa fa-envelope"></span></a></li>
                                <li><a href=""><span class="fa fa-usd"></span></a></li>
                            </ul>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span> 
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url(); ?>latest-reports">Report Store</a></li>
                            <li><a href="<?php echo base_url(); ?>category/pharmaceutical-and-healthcare" class="word-normal">Pharmaceuticals</a></li>
                            <li><a href="<?php echo base_url(); ?>category/material-and-chemicals">Materials & <br class="sm-hidden"/>Chemicals</a></li>
                            <li><a href="<?php echo base_url(); ?>category/medical-devices">Medical <br class="sm-hidden"/>Devices</a></li>
                            <li><a href="<?php echo base_url(); ?>category/banking-financial-services-and-insurance">Business & <br class="sm-hidden"/>Finance</a></li>
                            <li><a href="<?php echo base_url(); ?>categories" style="border-bottom: 0px">More Categories</a></li>
                            <li><a href="<?php echo base_url(); ?>cart"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;<span class='currency'>USD</span></a></li>
                            <li>
                                <form  action="<?php echo base_url(); ?>search-process" method="get" class="navbar-form navbar-left">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search" placeholder="Search"/>
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-navbar" type="submit">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>  

<!---******************************  ERROR MESSAGE  **************************** -->

<br/>
<div class="jumbotron error-block">
    <div align="center">
        <img class="img img-responsive" src="<?php echo base_url(); ?>assets/images/error-404.gif" alt="error-404"/>
        <h1 style="color:maroon;letter-spacing: 2px;" title="PAGE NOT FOUND">404</h1>
    </div>
    <div class="wrap">
        <h2>PAGE NOT FOUND</h2>
        <div class="back-home">
            <h3><a class="btn-home" href="<?php echo base_url(); ?>">Go Back to Home</a></h3>
        </div>
    </div>
</div>

<!---******************************  ERROR MESSAGE  **************************** -->


<footer>
    <div style="background:#182b3e"><br/>
        <div class="container"><br/>
            <div class="row">
                <div class="col-md-6">
                    <div style="color:rgb(242, 240, 236)">
                        <form action="" class="footer-form">
                            <div class="form-group"><br/>
                                <span>Register for our mailing list</span>
                                <input class="form-control"  placeholder='your@gmail.com' type="email" required />
                                <button class="btn btn-primary">REGISTER</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 footer-contact"><br/>
                    <h4><span class="fa fa-phone"></span>  +1-833-267-4156  <strong> &nbsp; - TOLL FREE</strong></h4><br/>
                </div>
            </div><br/><br class="sm-hidden"/>
            <div class="row footer-menu">
                <div class="col-md-3 footer-menu-list">
                    <h4>THE COMPANY</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>about-us">ABOUT US</a></li>
                        <li><a href="<?php echo base_url(); ?>careers">CAREERS</a></li>
                        <li><a href="<?php echo base_url(); ?>terms-and-conditions">TERMS &amp; CONDITIONS</a></li>
                        <li><a href="<?php echo base_url(); ?>privacy-policy">PRIVACY POLICY</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-menu-list">
                    <h4>OUR PRODUCTS</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>categories">CATEGORIES</a></li>
                        <li><a href="<?php echo base_url(); ?>latest-reports">REPORT STORE</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-menu-list">
                    <h4>CUSTOMER SUPPORT</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>contact-us">CONTACT US</a></li>
                        <li><a href="">LOGIN/REGISTER</a></li>
                        <li><a href="<?php echo base_url(); ?>return-policy">RETURN POLICY</a></li>
                        <li><a href="<?php echo base_url(); ?>payment-mode">PAYMENT MODES</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-menu-list">
                    <h4>MORE INFO</h4>
                    <ul class="list">
                        <li><a href="<?php echo base_url(); ?>press-release">PRESS RELEASE</a></li>
                        <li><a href="<?php echo base_url(); ?>sitemap">SITEMAP</a></li>
                        <ul class="social-link">
                            <li class="twitter"  title="Twitter "><a href="https://twitter.com/qq_market" target="_blank" ><i class="fa fa-twitter"></i></a></li>
                            <li class="facebook" title="Facebook"><a href="https://www.facebook.com/Qandqmarketresearch-1975843195988087/" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li class="gplus"    title="Google Plus"><a href="https://plus.google.com/110270874313486181505" target="_blank" ><i class="fa fa-google-plus"></i></a></li>
                            <li class="linkedin" title="LinkeddIn"><a href="https://www.linkedin.com/company/qandqmarketresearch" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li class="rss"      title="RSS"><a href="<?php echo base_url(); ?>rss-feeds" target="_blank"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </ul>
                </div>

            </div>
            <hr style="border-top:1px solid #2d3c4b"/>

            <div class="row">
                <div class="col-md-5">
                    <p class="copyright">Copyright <span class="fa fa-copyright"></span>  <?php echo date("Y"); ?> &nbsp; Market And Research . All Rights Reserved &nbsp; </p>
                </div>
                <div class="col-md-7">
                    <ul class="payment-link">
                        <li><img class="img img-thumbnail img-responsive" src="<?php echo base_url(); ?>assets/images/visa.jpg" height="30px" width="40px"/></li>
                        <li><img class="img img-thumbnail img-responsive" src="<?php echo base_url(); ?>assets/images/MasterCard.png" height="30px" width="40px"/></li>
                        <li><img class="img img-thumbnail img-responsive" src="<?php echo base_url(); ?>assets/images/american-express.jpg" height="30px" width="40px"/></li>
                        <li><img class="img img-thumbnail img-responsive" src="<?php echo base_url(); ?>assets/images/discover.png" height="30px" width="40px"/></li>
                        <li><img class="img img-responsive paypal" src="<?php echo base_url(); ?>assets/images/paypal.png"  height="30px" width="70px"/></li>
                    </ul>
                </div>
            </div>

        </div><br/>
    </div>
    <p id="back-top">
        <a href="#top"><span class="fa fa fa-arrow-circle-up" style="font-size:40px"></span></a>
    </p>
</footer>
<script type="text/javascript">
$(document).ready(function(){
    // hide #back-top first
    $("#back-top").hide();

    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                    $('#back-top').fadeIn();
            } else {
                    $('#back-top').fadeOut();
            }
        });
            // scroll body to 0px on click
        $('#back-top a').click(function () {
            $('body,html').animate({
                    scrollTop: 0
            }, 800);
            return false;
        });
    });

});
</script>
</body>
</html>