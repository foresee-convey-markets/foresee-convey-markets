<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Search Request | <?php echo ucfirst($search); ?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="Search Result  Market" />
<meta name="description" content="Search Result for your query - <?php echo ucfirst($search); ?>"/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<script src="<?php echo base_url();?>assets/js/validator.min.js"></script>
<script src="<?php echo base_url();?>assets/js/captcha.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>

<!-- *****************   BOOTSTRAP SELECT          ******************  -->
<link href="<?php echo base_url();?>assets/bootstrap-select/css/bootstrap-select.min.css" rel='stylesheet' />
<script src="<?php echo base_url();?>assets/bootstrap-select/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker();
    })
</script>
<script type="text/javascript">
    function validate_captcha()
    {
        var text=txtCaptcha.value;
        str = text.replace(/ +/g, "");
        var code=security_code.value;

        if(str !== code)
        {
           CaptchaError.innerHTML="Invalid Captcha Code";
           return false;
        }
        else
        {
            CaptchaError.innerHTML="";
            return true;
        }
    }
</script>
<?php
$search_query = '';
if(@$search &&  !empty($search)){
    $search_query .= str_ireplace('report','',$search).' report [ ';
}
if(@$year &&  !empty($year)){
    $search_query .= ' for year '. $year.' ,';
}
if(@$current_region &&  !empty($current_region)){
    $search_query .= ' for region '. $current_region.' ,';
}
if(@$date_order &&  !empty($date_order)){
    if(strtolower($date_order) == 'desc'){
        $search_query .= ' in recent collection ,';
    }
    if(strtolower($date_order) == 'asc'){
        $search_query .= ' in old collection ,';
    }
}
if(@$price &&  !empty($price)){
    $search_query .= ' for price USD '. $price.' ,';
}
$search_query .= ' ]';
if(empty($year) && empty($current_region) && empty($date_order) && empty($price)){
    $search_query = str_replace(array('[',']'),'',$search_query);
}
$len = strlen($search_query);
if(stripos($search_query,',',$len-3)){
    $search_query = substr($search_query,0,$len-3).' ]';
}

?>

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li><a onclick="window.history.back()" title="Go Back"><span class="fa icon fa-backward"></span></a></li>
    <li>Search Request</li>
</ul>

<div class="latest-report page-banner" align='center'>  
    <h2><span class="fa fa-search-plus fa-2x"></span></h2>
    <h1>SEARCH REQUEST</h1><br/>
</div>   
<div class="search-block" align='center'>
    <p><?=@$helpText?></p>
</div>
<?php
if (isset($success)) {
    echo '<p style="font-weight: bold;font-size: 1.2em;color:green;text-align:center">' . $success . '</p>';
    echo '<meta http-equiv="refresh" content="2,url=' . base_url() . '"/>';
}
?> 


<div class="page-contact-block search-form"><br/>
    <div class="row">
        <div class="col-md-6 col-contact">
            <div class="content-block-shadow">
                <div class="panel-body"> 
                    <p class="form-text">*Please fill the form below to let us know about your query !</p>
                    <form action="<?php echo base_url(); ?>search-form-process"  role="form"  method="post" data-toggle="validator" id="search_form" name="search_form" class="form-horizontal" onsubmit="return validate_captcha()" >
                        <div class="form-group">
                            <label class="control-label">Report Title <span class="star">*</span></label>
                            <input class="form-control report-input" placeholder="Report Title" name="rep_title" id="rep_title" value="<?=trim(ucfirst($search_query)); ?>" data-bv-field="rep_title" type="text" data-error="Please enter Report Title" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Name <span class="star">*</span> :</label>
                            <input class="form-control" name="name" id="name"  placeholder="Enter Your Name" data-bv-field="full_name" type="text" data-error="Please enter full name" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email <span class="star">*</span> :</label>
                            <input class="form-control" name="email" id="email"  placeholder="Enter Your Email" data-bv-field="email" type="email" data-error="Please enter valid email id"   required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Country <span class="star">*</span> :</label>
                            <select name="country" class="form-control selectpicker" data-bv-field="country" data-live-search="true" data-actions-box="true" data-error="This field can not be blank"  required>
                                <?=getCountryDropdown()?>
                            </select>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Contact Number <span class="star">*</span> :</label>
                            <input class="form-control" pattern="^[0-9]{10}$"  name="phone" id="phone" value="" maxlength="10" placeholder="Contact Number without country code" data-bv-field="mobile_no" type="text" data-error="Please enter valid contact number" required/>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label">Search Requirements <span class="star">*</span> :</label>
                            <textarea data-error="Please mention your search requirements" placeholder="Mention Your Search Requirements"  rows="5"  class="form-control" name="message" id="message" data-bv-field="message"  required></textarea>
                            <div class="help-block with-errors"></div> 
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="security_code">Captcha Code: <span class="star">*</span>: </label>
                            <div class="input-group" style="padding-right:0;">
                                <input data-error="Please enter captcha code" onkeyup="validate_captcha()" type="text" name="captcha_code" maxlength="6" placeholder="Captcha Code" class="form-control" id="security_code" required/>
                                <input type="text" id="txtCaptcha" readonly class="form-control"/>
                                <span class="input-group-btn">
                                    <button class="btn btn-secondary btn-captcha" type="button" onClick="DrawCaptcha();"> <span class="fa fa-sync"></span> </button>
                                </span> 
                            </div>
                            <p style="color:#f00" id="CaptchaError"></p>
                            <div class="help-block with-errors"></div> 
                        </div><br/>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-submit">SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 sticky col-contact">
            <div class='content-block-shadow'>
                <div class="row info-block">
                    <div class="col-md-12" align='center'>            
                        <h3 class='report-heading'>Why FCM?</h3>
                    
                        <div class="info-block-row">
                            <h1 class='fa fa-lock fa-2x icon-home'></h1>
                            <h4>RELIABLE AND PROTECTED</h4>
                            <p>Your sensitive information and data  <strong>are <br/>secure</strong> with us.</p>
                        
                            <h1 class='fa fa-globe fa-2x icon-home'></h1>
                            <h4>GLOBAL CHOICE</h4>
                            <p>Get access to over <strong>1M+ Forecast</strong><br/><strong>Research Reports</strong></p>

                            <h1 class='fa fa-users fa-2x icon-home'></h1>
                            <h4>CUSTOMER ORIENTED</h4>
                            <p>We are available <strong>24*7</strong> <br/>for <strong class='blue'>365 days</strong> in a year</p>
                        
                            <h1 class='fa fa-handshake fa-2x icon-home'></h1>
                            <h4>TRUSTED BY ALL</h4>
                            <p><strong>500+ Contented Clients </strong> Globally.</p>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<div class='home-contact-block'>
    <div class="row info-block static-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Contact</h3>
            <img class="img img-responsive logo contact-logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
        </div>
        <div class="col-md-9">
            <div class="info-block-row"><br/>
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                        <h4>LOCATION</h4>
                        <p>1445 Woodmont Ln NW, Atlanta, Ga 30318</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-phone fa-2x icon-home'></h1>
                        <h4>CALL / EMAIL US</h4>
                        <p><a href='tel:+18442677928'>+1-844-267-7928</a> <br/>  <a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                        <h4>CONTACT US</h4>
                        <a class='btn-report' href="<?=base_url()?>contact-us" title='Contact Us'>Contact Us</a>
                    </div>
                    <div class="col-md-6 cr-block" align="center">
                        <h1 class='fa fa-search fa-2x icon-home'></h1>
                        <h4>CUSTOM RESEARCH</h4>
                        <a class='btn-report' href="<?=base_url()?>custom-research" title='Custom Research'>Custom Research</a>
                    </div>
                </div><br/>
            </div>
        </div>
    </div>
</div>
                

<script type="text/javascript">

    var data=<?php echo (isset($_GET) && !empty($_GET)) ? '1' : '0' ?>;
    if(data == '1'){
        var w=$(window).width();
        if(w>767){
            $('html,body').animate({
                scrollTop: ($('.banner-row').offset().top  + 60 )        
            }, 'slow');
        }
    }
</script>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
