<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Return Policy | Foresee Convey Markets';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Refund Policy,  return policy,Refund Policy Foresee Convey Markets';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Foresee Convey Markets Refund policy states that Due to the nature of the information being sold, we unfortunately cannot accept returns of products once they have been delivered and no refund will be processed under any circumstances.';?>"/>
<meta name="author" content="Foresee Convey Markets"/>

<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/home.css"/>

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Return Policy</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-money-check-alt fa-2x"></span></h2>
    <h1>RETURN POLICY</h1><br/>
</div>    

<div class="container-fluid page-container" >
    <div class="return-policy content-block col-content">
        <div class='row' style='margin : 0px'>
            <div class='col-md-12'>
                <h4>We take payments in line with the procedure mentioned on our website only. we are going to not be to blame for sales, usage, and related taxes & customs. Once the payment is completed, it'll not be refunded. any quries regarding payment/ problems, contact the sales representative. However, if the user believes that there are some errors occurring during transactions, in this case, he/she should immediately contact our customer service team.</h4>
                <h4>We don’t accept returns after delivery of products because of format & nature of knowledge and knowledge contained within the reports. However, the customization are provided through us just in case there's missing or incomplete data within the reports.</h4>
                <h4>If you have any questions about a report&#39;s coverage or relevance, simply
                <a href="<?php echo base_url(); ?>contact-us">contact us.</a></h4>
            </div>
        </div>
    </div>
</div>
<div class='home-contact-block'>
    <div class="row info-block static-block">
        <div class="col-md-3" align='center'>            
            <h3 class='report-heading'>Contact</h3>
            <img class="img img-responsive logo" src="<?php echo base_url(); ?>assets/images/foresee-convery-markets-logo.png" alt="Foresee Convey Markets Logo">
        </div>
        <div class="col-md-9">
        <div class="info-block-row"><br/>
                <div class="row">
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-map-marker fa-2x icon-home'></h1>
                        <h4>LOCATION</h4>
                        <p>1445 Woodmont Ln NW, Atlanta, Ga 30318</p>
                    </div>
                    <div class="col-md-6 col-6-info" align="center">
                        <h1 class='fa fa-phone fa-2x icon-home'></h1>
                        <h4>CALL / EMAIL US</h4>
                        <p><a href='tel:+18442677928'>+1-844-267-7928</a> <br/>  <a href='mailto:help@foreseeconvey-markets.com'>help@foreseeconvey-markets.com</a></p>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <h1 class='fa fa-envelope fa-2x icon-home'></h1>
                        <h4>CONTACT US</h4>
                        <a class='btn-report' href="<?=base_url()?>contact-us" title='Contact Us'>Contact Us</a>
                    </div>
                    <div class="col-md-6 cr-block" align="center">
                        <h1 class='fa fa-search fa-2x icon-home'></h1>
                        <h4>CUSTOM RESEARCH</h4>
                        <a class='btn-report' href="<?=base_url()?>custom-research" title='Custom Research'>Custom Research</a>
                    </div>
                </div><br/>
            </div>
        </div>
    </div>
</div>


<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->
