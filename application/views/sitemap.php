<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo isset($meta->title) ? $meta->title : 'Sitemap | Foresee Convey Markets';?></title>
<!-- Basic -->
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="<?php echo isset($meta->keywords) ? $meta->keywords : 'Foresee Convey Markets Sitemap, Sitemap Foresee Convey Markets';?>" />
<meta name="description" content="<?php echo isset($meta->description) ? $meta->description : 'Sitemap for Foresee Convey Markets';?>"/>
<meta name="author" content="Foresee Convey Markets"/>


<!--**********************    HEADER OPEN      ***************************-->

<?php require_once 'layouts/header.php'; ?>

<!--**********************    HEADER CLOSE     ***************************-->

<ul class="breadcrumb">
    <li><a href="<?php echo base_url();?>" class="breadcrumb-home"><span class="fa fa-home"></span></a></li>
    <li>Sitemap</li>
</ul>

<div class="page-banner" align='center'> 
    <h2><span class="fa fa-link fa-2x"></span></h2>
    <h1>SITEMAP</h1><br/>
</div> 

<div class="container-fluid page-container">
    <div class='sitemap-block content-block col-content'>
        <div class='row' style='margin : 0px'>
            <div class='col-md-12'>
                <h3 class="sitemap-content">
                    <a href="<?php echo base_url(); ?>"> Home</a>
                </h3>     

                <h3 class="sitemap-content">
                    <a href="<?php echo base_url(); ?>latest-reports"> Industry Reports </a>
                </h3>
                <div class="row" style="margin:0px;">
                    <h3 class="sitemap-content">About</h3>
                    <div class="col-md-4">
                        <ul class="sitemap-category-list">
                            <li><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
                            <li><a href="<?php echo base_url(); ?>careers">Careers</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row" style="margin:0px;">
                    <h3 class="sitemap-content">Contact</h3>
                    <div class="col-md-4">
                        <ul class="sitemap-category-list">
                            <li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
                            <li><a href="<?php echo base_url(); ?>custom-research">Custom Research Service</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row" style="margin:0px;">
                    <h3 class="sitemap-content"> 
                        <a href="<?php echo base_url(); ?>categories"> Industry Sectors</a>
                    </h3>
                    <div class="col-md-4">
                        <ul class="sitemap-category-list">
                            <li><a href="<?php echo base_url(); ?>category/information-and-communication-technology-and-media-market-report">ICT Media</a></li>
                            <li><a href="<?php echo base_url(); ?>category/semiconductor-and-electronics">Semiconductor and Electronics</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/agriculture">Agriculture</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/energy-and-power">Energy and Power</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/machinery-and-equipment">Machinery and Equipments</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="sitemap-category-list">
                            <li><a href="<?php echo base_url(); ?>category/medical-devices">Medical Devices</a></li>
                            <li><a href="<?php echo base_url(); ?>category/material-and-chemicals">Material and Chemicals</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/automotive-transportation">Automotive &amp; Transportation</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/aerospace-Defence">Aerospace and Defence</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/food-and-beverages">Food and Beverages</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="sitemap-category-list">
                            <li> <a href="<?php echo base_url(); ?>category/pharmaceutical-and-healthcare">Pharmaceutical and Healthcare</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/consumer-goods-and-services">Consumer Goods and Services</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/company-reports">Company Reports</a></li> <li> <a href="<?php echo base_url(); ?>category/construction- manufacturing">Construction and Manufacturing</a></li>
                            <li> <a href="<?php echo base_url(); ?>category/banking-financial-services-and-insurance">Banking, Financial Services &amp; Insurance</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!--**********************    FOOTER OPEN      ***************************-->

<?php require_once 'layouts/footer.php'; ?>

<!--**********************    FOOTER CLOSE     ***************************-->


