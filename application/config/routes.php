<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']   = 'Index';
$route['404_override']         = 'error_404/404';
$route['translate_uri_dashes'] = FALSE;
$route["welcome"]              ="Welcome/index";
$route["404"]                  ="Cms/error_404";

//******************************    REPORTS         ****************************************//

$route["latest-reports"]         = "Report_management/latest_reports";
$route["reports/page/(:num)"]    = "Report_management/latest_reports";
$route["reports/(:num)/(:any)"]  = "Report_management/report_details";
$route["report/(:num)"]          = "Report_management/process_form_request";

//******************************    Cms          ****************************************//

$route["contact-us"]          ="Cms/contact_us";
$route["about-us"]            ="Cms/about_us";
$route["terms-and-conditions"]="Cms/terms_conditions";
$route["privacy-policy"]      ="Cms/privacy_policy";
$route["return-policy"]       ="Cms/return_policy";
$route["payment-mode"]        ="Cms/payment_mode";
$route["careers"]             ="Cms/careers";
$route["careers/apply"]       ="Cms/careers_apply";
$route["custom-research"]     ="Cms/custom_research";
$route["cookie-update"]       ="Cms/cookie_update";

//*********   sitemap  *******************//

$route["rss-feeds"]              ="Cms/rss";
$route["sitemap.html"]           ="Cms/sitemap_html";
$route["sitemap-web.xml"]        ="Cms/sitemap_web";
$route["sitemap.xml"]            ="Cms/sitemap";
$route["sitemap-(:any).xml"]     ="Cms/sitemap_monthwise";

//******************************    CATEGORIES         ****************************************//

$route["categories"]                   ="Category/categories";
$route["category/(:any)"]              ="Category/category_details";
$route["category/(:any)/page/(:any)"]  ="Category/category_details";

//******************************   FORMS         ****************************************//

    /*************************    REGISTER FOR MAILING LIST   **********************/ 

    // $route['register-mail']          = "Form_management/register_mail";
    // $route['report/(:any)']          = "Report_management/process_form_request";
    // $route['report-form-process']    = "Form_management/report_form_process";

   //*************************    CONTACT US         **********************//  

    $route['contact-form-process']    = "Form_management/contact_form_process";
    $route["contact-form/thanks"]     = "Form_management/thanks";
    $route["report-request/thanks"]   = "Form_management/thanks";

    //*************************    CUSTOM RESEARCH         **********************//  

    $route['custom-research-process']    = "Form_management/custom_research_process";
    $route["custom-research/thanks"]     = "Form_management/thanks";
                  
    //************************ REQUEST SAMPLES  **********************//

    $route["report-form-process"]             = "Form_management/report_form_process";
    $route["request-customization/thanks"]    = "Form_management/thanks";
    $route["enquire-before-purchase/thanks"]  = "Form_management/thanks";
    $route["request-discount/thanks"]         = "Form_management/thanks";
    $route["request-sample/thanks"]           = "Form_management/thanks";
    
    //*************************    CAREEERS       **************************//  
                                                                                      
    $route['career-form-process']     = "Form_management/do_upload";
    $route['career-apply-process']    = "Form_management/file_upload";
                                                                             

//******************************    SEARCH         ****************************************//

$route["search-result"]                 = "Search_management/search_result";
$route["search-result/page/(:any)"]     = "Search_management/search_result";
$route["search-form-process"]           = "Form_management/search_form_process";
$route["search-form/thanks"]            = "Form_management/thanks";

// $route["ajax-search"]                              ="Search_management/search_suggestions";

//******************************    Checkout        ****************************************//

$route["checkout/(:any)"]          = "Checkout/checkout";
$route["checkout-process/(:num)"]  = "Checkout/checkout_process";

$route['paypal/thanks']            = "Form_management/thanks";
$route['bank-transfer/thanks']     = "Form_management/thanks";
$route['cancel']                   = "Form_management/cancel";

$route["payment/eazypay-success"]  = "Checkout/razorpay_success";
$route["payment/eazypay-thanks"]   = "Form_management/thanks";
$route["payment/eazypay-cancel"]   = "Checkout/razorpay_cancel";
$route["eazypay/payment-failed"]   = "Checkout/payment_failed";
$route["link-error"]               = "Checkout/link_error";
// $route["payment/bank-transfer"] = "Checkout/bank_transfer";

//******************************    GLOBAL Checkout        ****************//

$route['global-checkout/(:any)']         = "Checkout/global_checkout";
// $route['global-Checkout-process/(:any)'] = "Checkout/Checkout_process";


//******************************   PRESS RELEASE         *******************//

$route["press-release"]             = "Press/press_release";
$route["press-release/page/(:num)"] = "Press/press_release";
$route["press/(:any)"]              = "Press/press_details";

//******************************   BLOGS                 *******************//

// $route["blogs"] ="Cms/blogs";
// $route["blog/(:any)"]  ="Cms/blog_details";


