<br/><br/><br/>
   <div class="row footer">               
        <div class="col-md-12">
        <p class="copyright">Copyright <span class="fa fa-copyright"></span>  <?php echo date("Y"); ?>&nbsp;<span class="sm-hidden"> | </span>&nbsp;<br class="md-hidden"/> Foresee Convey Markets</p>
        </div>              
    </div>
</div><!-- ********** MAIN BLOCK CLOSE FROM HEADER    ******** -->

<script type="text/javascript">

    //******************       SIDENAV DROPDOWN  ********************//


    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";

            }
        });
    }
    var path = $(location).attr('href');
    path = path.replace('.php','')
    var extra = path.indexOf("?");
    if(path.indexOf('lead-list') < 0 && extra > 0){
        path=path.substr(0,extra)
    }
    else if(path.indexOf('lead-list') > 0){
        path=path.substr(0,extra)
        var para = $(location).attr('search')
        var lead_para = para.substr(para.indexOf('type'),6)
        path = path + "?" + lead_para
        // console.log(path)
    }
    // console.log(path)
    var href=$(' a[href*="'+ path +'"]').addClass('active');

    </script>
</body>
</html>
