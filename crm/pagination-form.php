
<ul class="pagination">
<?=pagination($data);?>
</ul>

<form class="navbar-form-left form-nav-search" id='search_form'  action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" method="get" enctype="application/x-www-form-urlencoded">
    <input type='hidden' value='<?=@$data['no_of_records_per_page']?>' name='per_page'>
    <input type='hidden' value='<?=@$data['pageno']; ?>' name='pageno'>    
    <input type='hidden' value='<?=@$_GET['type']?>' name='type'>
    <div class="input-group">
        <input class="form-control" id='search' name="query" placeholder="<?=@$input_placeholder?>"  value='<?=@$input_query?>'/>
        <div class="input-group-btn">
            <button class="btn btn-default btn-search" type="button" onclick="document.getElementById('search_form').submit()">
                <i class="fa fa-search"></i>
            </button>
            <?php if(isset($is_followed_list) && $is_followed_list){ ?>&nbsp;
                <a title='Click Here To Clear Search' href='<?=BASE_URL?>followed-list' class="btn btn-danger btn-clear">
                    <i class="fa fa-times"></i>
                </a>
            <?php } ?>
        </div>
    </div>
</form>

<form class="per-page-form" id="per_page"  action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>"  method="get" >
    <input type='hidden' value='<?=@$_GET['type']?>' name='type'>
    <input type='hidden' value='<?=@$input_query?>' name='query'>
    <input type='hidden' value='<?=@$data['pageno']; ?>' name='pageno'>
    <div class="form-group">
        <label class="control-labe">Per Page</label>
        <select class="form-control" id='per_page_input' onchange="document.getElementById('per_page').submit()" name="per_page" placeholder="per_page" enctype="application/x-www-form-urlencoded">
        <?php
        foreach($RECORDS_PER_PAGE as $per_page => $value){                
            $selected =  (isset($data['no_of_records_per_page']) && $data['no_of_records_per_page'] == $value ) ? 'selected' : ''  ; 

            if(isset($is_followed_list) && $is_followed_list){									
                echo "<option value='".@$value."' ".@$selected.">".$value."</option>";
            }else{
                if(in_array($value,array('1','5'))){continue;}
                echo "<option value='".@$value."' ".@$selected.">".$value."</option>";
            }
        }
        ?>
        </select>
    </div>
</form>