
<html>
<head>
<title>Add Followup Type | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->


<?php
    if(isset($_POST["submit"]))
    {
        
        $followup_name=$_POST["followup_name"];
        $followup_no=$_POST["followup_no"];
        $followup=$_POST["followup"];
        $status=$_POST["status"];
        $created = date('Y-m-d H:i:s');
		
		
        $qyr_part ="insert ignore into mr_followups(followup_name,followup,followup_no,status,created) values ";		
		$sql_chunk = sprintf("('%s','%s', '%d','%d', '%s'),", addslashes(trim(@$followup_name)),addslashes(trim(@$followup)),@$followup_no,@$status,@$created);
		$sql_insert = substr($sql_chunk, 0,-1);
		$first_ins_id = $link->query($qyr_part.$sql_insert);

		if(@$first_ins_id > 0 )
		{
			$message="<b style='color:#479143'>Followup added successfully !</b>";
			echo '<meta http-equiv="refresh" content="2,url='.BASE_URL.'followup-list "/>'; 
		}
		else
		{                
			$message="<b style='color:#dc470d'>Error adding Followup !</b>";                
			
		}
    }
    
    
    ?>

    <h1 class="stats"><span class="fa fa-plus-circle"></span> Add New Followup Type</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
		<?php 
			if(isset($message)){
				echo "<br/><h3 align='center'>".$message."</h3>";
			}
		?>
	    
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method='post' name="add" id="add">
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Followup Name<span class="star">*</span> :</label>
		    		<input  name="followup_name" class="form-control" placeholder="Followup Name" required="" />
		    	</div>
	    	</div>
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Followup<span class="star">*</span> :</label>
		    		<textarea rows='5' name="followup" class="form-control" placeholder="Followup" required="" ></textarea>
					<p class="note">(NOTE : Please add tags in curly braces. eg.{{rep_title}},{{name}},{{email}})</p>
					<p><strong>Available System Tags : {{rep_title}} , {{name}} , {{email}} </strong></p>
				</div>
	    	</div>
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Followup Number<span class="star">*</span> :</label>
		    		<select name="followup_no" class="form-control" placeholder="Followup No" required="" >
						<option value="">Select</option>
						<?php foreach($FOLLOWUPS as $followup_no => $followup){ ?>
							<option value="<?=@$followup_no?>"><?=@$followup?></option>
						<?php } ?>
					</select>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" value="0" required="" />Inactive</label>		    		
		    	</div>
	    	</div>
	    	
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload"><span class="fa fa-plus"></span> Add</button>
		    </div>
	    </form>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
