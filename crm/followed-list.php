
<html>
<head>
<title>Followed Leads List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';

    function getFollowupDetails($contact_id,$link){
        $followup_data = array();
        $followup_qyr = $link->query("select A.id as crm_id,A.followup_id as followup_id, A.date as followup_date,message,followup_name from mr_crm_followups A inner join mr_followups C on A.followup_id = C.id where A.contact_id = '".@$contact_id."' order by A.contact_id desc");
        if($followup_qyr && $followup_qyr->num_rows > 0){
            // array_push($followup_data , array(
            //     'rows' => $followup_qyr->num_rows
            // ));
            while($row = mysqli_fetch_assoc($followup_qyr)){
                $followup_data[]=array(
                    'followup_id' => $row['followup_id'],
                    'crm_id' => $row['crm_id'],
                    'followup_date' => $row['followup_date'],
                    'message' => $row['message'],
                    'followup_name' => $row['followup_name'],
                );
            }
        }
        // echo "<pre>";print_r($followup_data);die;
        return $followup_data;
    }
?>
<script type='text/javascript' src='<?=BASE_URL?>assets/js/interval.js'></script>

<!--*****************************  HEADER  ************************************** -->
<?php
//********************  PAGINATION  ****************************//
$where = '';
$input_query = '';
$is_followed_list = true;
if (isset($_GET['query']) && !empty($_GET['query'])) {
    $page_query = "&query=".trim($_GET['query']);
    $input_query = trim($_GET['query']);
    $where.= "  B.contact_person like '%".$input_query."%' or B.contact_email   like '%".$input_query."%' ";
}  
if(!empty($where)){
    $where = " where ".$where;
}

$data = create_links($_GET, $link , 'mr_form_contact', $where, '', $is_followed_list);

if(@$data['total_rows'] > 0 && @$data['pageno'] > @$data['total_pages']){
    $url = BASE_URL.'followed-list'.@$data['last_page'];
    echo "<meta http-equiv='refresh' content='0, url=".@$url."'/>";
}
$input_placeholder = 'Search here for Lead Name, Lead Email Address';
?>

    <h1 class="stats"><span class="fa fa-list-ol"></span>Followed Leads List<span class="text-danger">[</span><span class='count text-danger'><?=@$data['total_rows']?></span><span class="text-danger">]</span></h1><br/><br/>
    
    <div class="pagination-form">
        <?php require_once 'pagination-form.php' ?>
    </div>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
                    <th>Lead ID</th>
                    <th>Followup Status</th>
                    <th style= "min-width:160px">Followup Interval</th>
                    <th>Lead Name</th>
                    <th>Lead Email Address</th>
                    <th style = "min-width:250px">Report Title</th>
                    <th style = "min-width:400px">Followed Details</th>
                    <th style = "min-width:150px">Type</th>
                    <th>Company</th>
                    <th>Region</th>
                    <th>Country</th>
                    <th style= "min-width:150px">Current Country</th>
                    <th style= "min-width:160px">Interval Date Set</th>
                    <th style= "min-width:160px">Last Run At</th>

                <?php if($_SESSION["user_type"]=='1'){ ?>
    				<!-- <th style='width:100px'>Options</th> -->
                <?php } ?>

    			</tr>
    		</thead>
    		<tbody id="deleteCount">
    			<?php
                //Followup Intervals
                $INTERVALS = array();
                $interval_qyr = $link->query("select id,type from mr_followup_intervals");
                if($interval_qyr->num_rows > 0){
                    while($row = $interval_qyr->fetch_assoc()){
                        $INTERVALS[$row['id']] = $row['type'];
                    }
                }

                $fields = "contact_rep_title,contact_person, contact_email,contact_country, 
                contact_exact_region,contact_real_country,B.contact_id as lead_contact_id, 
                contact_form_type,contact_company, last_run_at,interval_set_date, followup_status,interval_type,followup_interval";

                $getFollowups=$link->query("select ".$fields." from mr_form_contact B inner join mr_crm_followups A on A.contact_id=B.contact_id  ".@$where." group by B.contact_id  desc limit ". @$data['offset']. "," . @$data['no_of_records_per_page']);
    			if($getFollowups->num_rows > 0){
	    			while($row=$getFollowups->fetch_assoc()){
                        // $id=$row["crm_id"];
                        $lead_contact_id = $row['lead_contact_id'];
                        $checked = @$row['followup_status'] == '1' ? 'checked' : '';
                        $followup_value=@$row['followup_interval'] > 0 ? @$row['followup_interval'] : '';
                        $followup_info = getFollowupDetails($lead_contact_id,$link);
                        // $less = nl2br(substr(@$row["message"],0,200))."... <br/><span class='text-primary content' id='view".@$id."' title='View More'  onclick='getMoreContent(".$id.")'>View More</span>";
                        echo "<tr id='leadDelete".$row['lead_contact_id']."'>";
                        // echo "<td >".@$id."</td>";
                        echo "<td>".@$lead_contact_id."</td>";
                        echo '<td> 
                                <input type="hidden" id="is_interval_set_'.$lead_contact_id.'" value="'.@$row["interval_type"].'" />
                                <label data-toggle="tooltip" title="'.@$FOLLOWUP_STATUS[@$row['followup_status']].'" class="switch" onchange="updateLeadFollowupStatus('.$lead_contact_id.','.@$row['followup_status'].')">
                                    <input id="followup_'.$lead_contact_id.'"  type="checkbox" '.@$checked.'>
                                    <span class="slider round"></span>
                                </label>
                            </td>';
                        if($row['interval_type'] > 0){
                            echo "<td align='center'>
                                    <div class='alert alert-info interval-alert'><span class='fa fa-clock-o'></span> &nbsp;Every ".$row["followup_interval"].' '.$INTERVALS[$row["interval_type"]].
                                "   </div>
                                    <button title='Change Followup Interval' class='btn btn-interval btn-interval-update' data-toggle='modal' data-target='#interval".$lead_contact_id."'><span class='fa fa-pencil'></span> change interval</button>
                                ";
                        }else{
                                echo "<td align='center'>
                                <button title='Set Followup Interval' class='btn btn-interval' data-toggle='modal' data-target='#interval".$lead_contact_id."'><span class='fa fa-clock-o'></span> set interval</button>
                                ";
                        }

                            echo"<div id='interval".$lead_contact_id."' class='modal fade' role='dialog'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'> <span class='fa fa-clock-o'></span> Set Followup Interval  </h4>
                                            </div>
                                            <div class='modal-body'>
                                                <form class='form-horizontal interval-form' onsubmit='return false;' method='post' name='interval_form' id='interval_form_".$lead_contact_id."'>
                                                    <p class='followup_error' id='followup_error_".$lead_contact_id."'></p>    
                                                    <p class='followup_success' id='followup_success_".$lead_contact_id."'></p>    
                                                    <input type='hidden' name='contact_id' value='".$lead_contact_id."'>
                                                
                                                    <div class='form-group'>
                                                        <div class='col-md-10'>
                                                            <label class='control-label'>Followup Interval <span class='star'>*</span> :</label>
                                                            <select name='interval_type' id='interval_type_".$lead_contact_id."' class='form-control' placeholder='Followup Interval' required='' >
                                                                <option value=''>Select</option>";
                                                                foreach($INTERVALS as $interval_id => $interval){ 
                                                                    $selected = (@$row['interval_type'] == @$interval_id) ? 'selected' : '';
                                                                    echo "<option value='".@$interval_id."' ".$selected.">".@$interval."</option>";
                                                                }
                                        echo "              </select>
                                                        </div>
                                                    </div>
                                                    <div class='form-group'>
                                                        <div class='col-md-10'>
                                                            <label class='control-label'>Followup Interval Value<span class='star'>*</span> :</label>
                                                            <input type='number' min='1' max = '24' id='followup_interval_".$lead_contact_id."' value='".@$followup_value."' name='followup_interval' class='form-control' placeholder='Followup Interval Value' required='' />
                                                        </div>
                                                    </div>
                                                    <div class='form-group'>";?>
                                                        <button class='btn btn-primary btn-confirm' id='interval_set' onclick= "setFollowupInterval('<?=$lead_contact_id?>','<?=BASE_URL?>')" type='button'>Confirm</button>
                                    <?php echo"          <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>";
                        
                        echo "<td>".$row["contact_person"]."</td>";
                        echo "<td>".$row["contact_email"]."</td>";
                        
                        echo "<td width='200px'>".@$row["contact_rep_title"]."</td>";
                        echo "<td  style='background : #eee;border-bottom: 1px solid #000'>";
                        $inc=1;
                        $count_followups = count($followup_info);
                        foreach($followup_info as $followup){
                            echo "<details>";
                            echo"<summary>&nbsp;<mark class='mark-follow'><span class='fa fa-chevron-right'></span><strong> Followup No : </strong>".$followup["followup_id"]."</mark></summary>";                            
                            echo "<p><strong> Followed ID : </strong>".$followup["crm_id"]."</p>";                            
                            echo "<p><strong> Followup Type : </strong>".$followup["followup_name"]."</p>";                            
                            echo "<p><strong> Followup Date : </strong>".$followup["followup_date"]."</p>";
                            $msg = preg_replace ("/[\r\n]+/", "",nl2br($followup["message"],false));
                            echo "<p><strong> Message  : </strong><span id='msg".$lead_contact_id."'>".nl2br(@$msg)."</span></p>";                            
                            echo "</details><br/>";
                            if($count_followups > 1 && $count_followups > $inc){
                                echo "<hr style='border-top: 1px solid #000'/>";
                            }
                            $inc++;
                        }
                        echo "</td>";

                        echo "<td>".$FORM_TYPES[$row["contact_form_type"]]."</td>";
                        echo "<td>".$row["contact_company"]."</td>";
                        echo "<td>".$row["contact_exact_region"]."</td>";
                        echo "<td>".$row["contact_country"]."</td>";
                        echo "<td>".$row["contact_real_country"]."</td>";
                        echo "<td>".$row["interval_set_date"]."</td>";
                        echo "<td>".$row["last_run_at"]."</td>";

                    if($_SESSION["user_type"]=='1'){
	    				// echo "<td colspan='3'><button data-toggle='modal' data-target='#removeMe".$id."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                        //         <div id='removeMe".$id."' class='modal fade' role='dialog'>
                        //             <div class='modal-dialog'>
                        //                 <div class='modal-content'>
                        //                     <div class='modal-header'>
                        //                         <button type='button' class='close' data-dismiss='modal'>&times;</button>
                        //                         <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                        //                     </div>
                        //                     <div class='modal-body'>
                        //                         <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                        //                     </div>
                        //                     <div class='modal-footer'>
                        //                         <button class='btn btn-primary btn-confirm' type='submit' onclick='confirmDelete(".$id.")' >Confirm</button>
                        //                         <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                        //                     </div>
                        //                 </div>
                        //             </div>
                        //         </div>
                        //     </td>";
                        }
                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='14' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO FOLLOWED LEADS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>
    <ul class="pagination">
        <?=pagination(@$data);?>
    </ul>
    <br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
    