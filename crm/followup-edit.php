
<html>
<head>
<title>Edit Followup Type | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php 


$id=@$_GET["id"];
$getFollowupDetails=$link->query("select * from mr_followups where id=".@$id);
$row=$getFollowupDetails->fetch_assoc();
// echo "<pre>";print_r($row);die;

?>
<?php
    if(isset($_POST["submit"]))
    {
        
        $followup_name=$_POST["followup_name"];
        $followup_no=$_POST["followup_no"];
        $followup=$_POST["followup"];
        $status=$_POST["status"];
		$modified = date('Y-m-d H:i:s');
		if(strpos($followup,"'")){
			if(strrpos($followup,"'") && strrpos($followup,"'") > strpos($followup,"'")){
				$followup = substr_replace($followup,'&lsquo;',strpos($followup,"'"), 1 );
				$followup = substr_replace($followup,'&rsquo;',strrpos($followup,"'"), 1 );

			}else{
				$followup = substr_replace($followup,'&rsquo;',strpos($followup,"'"), 1 );
			}
		}
		if(strpos($followup,'"')){
			if(strrpos($followup,'"') && strrpos($followup,'"') > strpos($followup,'"')){
				$followup = substr_replace($followup,'&ldquo;',strpos($followup,'"'), 1 );
				$followup = substr_replace($followup,'&rdquo;',strrpos($followup,'"'), 1 );

			}else{
				$followup = substr_replace($followup,'&rdquo;',strpos($followup,'"'), 1 );
			}
		}
		// echo $followup;die;
		// $followup=str_ireplace(array("’","'"),'&rsquo;',$followup);
		// $followup=str_ireplace(array('"','”'),'&rdquo;',$followup);

        $sql=mysqli_query($link,"update mr_followups set followup_name='".$followup_name."',followup='".addslashes($followup)."',followup_no='".$followup_no."',status='".$status."',modified='".@$modified."' where id='$id' ");
        
            if($sql===TRUE )
            {
                $message='<b style="color:#479143">Followup Updated successfully !</b>
                <meta http-equiv="refresh" content="2,url='.BASE_URL.'followup-list "/>'; 
            }
            else
            {                
                $message="<b style='color:#dc470d'>Error updating Followup !</b>";                
                
            }
    }
    
    
    ?>

    <h1 class="stats"><span class="fa fa-edit"></span> Edit Followup Type</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#"><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
		<?php 
			if(isset($message)){
				echo "<br/><h3 align='center'>".$message."</h3>";
			}
		?>
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."?id=".$_GET["id"]; ?>" method='post' name="upload" id="upload">
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Followup Name<span class="star">*</span> :</label>
		    		<input name="followup_name" class="form-control" placeholder="Followup Name" value="<?=@$row['followup_name']?>" required="" />
		    	</div>
	    	</div>
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Followup<span class="star">*</span> :</label>
		    		<textarea rows='5' name="followup" class="form-control" placeholder="Followup" required="" ><?=@$row['followup']?></textarea>
					<p class="note">(NOTE : Please add tags in curly braces. eg.{{rep_title}},{{name}},{{email}})</p>
					<p><strong>Available System Tags : {{rep_title}} , {{name}} , {{email}} </strong></p>
				</div>
	    	</div>
			<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Followup Number<span class="star">*</span> :</label>
		    		<select name="followup_no" class="form-control" placeholder="Followup No" required="" >
						<option value="">Select</option>
						<?php foreach($FOLLOWUPS as $followup_no => $followup){ ?>
							<option value="<?=@$followup_no?>" <?=@$row['followup_no'] == $followup_no ? 'selected' : '' ?>><?=@$followup?></option>
						<?php } ?>
					</select>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">STATUS <span class="star">*</span> :</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?php echo ($row["status"]=='1') ? 'checked' : ''; ?> value="1" required="" />Active</label>
		    		<label class="radio-inline"><input type="radio" name="status" <?php echo ($row["status"]=='0') ? 'checked' : ''; ?> value="0" required="" />Inactive</label>
		    		
		    	</div>
	    	</div>

	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fa fa-times"></span> Cancel</button>
		    </div>
	    </form>
	</div>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
