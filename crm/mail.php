<?php

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader

require 'vendor/autoload.php';

function send_mail($to = array(), $subject = '', $mail_message = '',$extra = array(),$reply_to = array()){
    if(!empty($to) && !empty($subject) && !empty($mail_message)){
        // echo $mail_message;die;

        $mail = new PHPMailer(true);         //Create an instance; passing `true` enables exceptions

        try {
        //**************** Server settings ******************//
        
            // $mail->SMTPDebug = ENVIRONMENT === 'development' ? SMTP::DEBUG_SERVER : SMTP::DEBUG_OFF;                      //  Debug level to show client -> server and server -> client messages
            $mail->SMTPDebug = SMTP::DEBUG_SERVER ;                         //  Debug level to show client -> server and server -> client messages
             
            $mail->isSMTP();            
            if(ENVIRONMENT === 'production'){  
                // $mail->Host       = 'sg2plcpnl0009.prod.sin2.secureserver.net';    //  Set the SMTP server to send through
                $mail->Host       = 'localhost';    //  Set the SMTP server to send through
                $mail->Username   = 'query@foreseeconvey-markets.com';           //  SMTP username
                $mail->Password   = 'rx13HchJAQv';
                $mail->SMTPAuth = false;
                $mail->SMTPAutoTLS = false; 
                $mail->Port = 25;                              //  SMTP password
            }
            //**** DEVELOPMENT  *** //
            if(ENVIRONMENT === 'development'){            
                $mail->Host       = 'smtp.gmail.com';                        //  Set the Gmail SMTP server to send through
                $mail->Username   = 'suraj.bongale99@gmail.com';             //  SMTP username
                $mail->Password   = '(vz(K^rz2Vb~v6EN';                      //  SMTP password
                $mail->SMTPAuth   = true;                                        //  Enable SMTP authentication
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;              //  Enable implicit TLS encryption
                $mail->Port       = 587;                                         //  TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                
            }                 
            //Recipients
            $mail->setFrom('query@foreseeconvey-markets.com', 'Foresee Convey Markets');
            $mail->addAddress($to['email'], $to['name']);                //  Add a recipient
            if(!empty(@$reply_to)){
                if(isset($reply_to['email']) && !empty($extra['email']) && !empty($extra['name'] )){
                    $mail->addReplyTo($reply_to['email'],$reply_to['name']);
                }
            }if(!empty(@$extra)){
                if(isset($extra['cc']) && !empty($extra['cc'])){
                    $mail->addCC($extra['cc']);
                }
                if(isset($extra['bcc']) && !empty($extra['bcc'])){
                    $mail->addBCC($extra['bcc']);
                }
            }
            //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         //  Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //  Optional name

            //Content
            $mail->isHTML(true);                                      //  Set email format to HTML
            $mail->Subject = @$subject;
            $mail->Body    = @$mail_message;
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();

            // echo 'Message has been sent';
            return True;
        } catch (Exception $e) {
            // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            return False;
        }
    }else{
        return False;
    }
}
