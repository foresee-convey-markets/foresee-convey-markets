<?php  
require_once 'config.php'; 
require 'mail.php'; 
date_default_timezone_set('Asia/Kolkata');
ini_set("date.timezone", "Asia/Kolkata");
// echo "<pre>";print_r($link);exit;


// GET FOLLOWUP INFO TO PREPARE EMAIL MESSAGE

function getFollowupInfo($followup_no, $link){    
    $followup_result = array();
    $followup_sql="Select id as followup_id,followup from mr_followups where followup_no = ".$followup_no;        
    $followup_query=$link->query($followup_sql);
    if($followup_query->num_rows > 0){
        @$followup_result = $followup_query->fetch_assoc();        
    }
    return $followup_result;
}

// GET ALL FOLLOWUP TYPES eg. Seconds, Minutes, Hours etc...

function getFollowupIntervals($link){
    $INTERVALS = array();
    $interval_qyr = $link->query("select id,type from mr_followup_intervals");
    if($interval_qyr->num_rows > 0){
        while($row = $interval_qyr->fetch_assoc()){
            $INTERVALS[$row['id']] = $row['type'];
        }
    }
    return $INTERVALS;
}

// GET ALL FOLLOWUP TYPES eg. Seconds, Minutes, Hours etc...

// function getExistingContactIDs($contact_id,$followup_id,$link){
//     $followup_qyr = $link->query("select * from mr_crm_followups where contact_id = '".@$contact_id."' and followup_id = '".@$followup_id."' ");
//     if($followup_qyr->num_rows > 0){
//         return True;
//     }
//     return False;
// }

// GET SEND EMAIL AND PERFORM OPERATIONS BASED ON EMAIL RESPONSE

function send_mail_and_insert_data($lead_data, $link){  
    $Tags = setTags($lead_data);
    $message = replaceTags($lead_data['followup'], $Tags);
    $mail_message = nl2br($message);
    $to = array('name' => @$lead_data['name'], 'email' => @$lead_data['email']);
    $subject = ucwords($lead_data['rep_title']);
    $is_mail_sent = send_mail($to, $subject, $mail_message); 
    if(@$is_mail_sent){
        echo 'Mail Sent<br/>';
        $date = date('Y-m-d H:i:s');
        $followup_qyr ="insert ignore into mr_crm_followups(contact_id,followup_id,message,status,date) values (".@$lead_data['contact_id'].", ".@$lead_data['followup_id'].",'".addslashes(@$message)."',1,'".@$date."') on duplicate key update message = '".addslashes(@$message)."' ";
        // $qyr = substr(@$followup_qyr,0,-1);
        if($link->query($followup_qyr)){
            $update_interval = '';
            if(isset($lead_data['is_interval_set']) && @$lead_data['is_interval_set'] === True){                
                $INTERVALS = getFollowupIntervals($link);
                $last_run_at = date("Y-m-d H:i:s");
                $interval_value = @$lead_data['interval_value'];
                $interval_type = @$INTERVALS[@$lead_data['interval_type']];
                $interval_after = "+".@$interval_value." ".@$interval_type;
                $next_interval_datetime = date("Y-m-d H:i:s", strtotime("$last_run_at $interval_after"));
                $update_interval.=",last_run_at='".@$last_run_at."', interval_set_date='".@$next_interval_datetime."' ";
            }
            mysqli_query($link,"update mr_form_contact set is_followed = 1 ,followup_id = ".@$lead_data['followup_id']." , error_msg = '' ".$update_interval." where contact_id=".@$lead_data['contact_id']);
            echo 'Inserted followup and updated status <br/>';
        }else{
            echo mysqli_error($link); 
            $db_err = "Database Error : ".addslashes(mysqli_error($link));                   
            mysqli_query($link,"Update mr_form_contact set error_msg = '".$db_err."' where contact_id=".@$lead_data['contact_id']);
        }
    }else{
        echo 'Failed to send mail';
        mysqli_query($link,"Update mr_form_contact set error_msg = 'Mailer Error' where contact_id=".@$lead_data['contact_id']);
    } 
}
// Get Current Date And Time for Timezone => Asia/Kolkata
$current_datetime = date('Y-m-d H:i:s');
// Followup Query Part
$followup_qyr_part = "Select * from mr_form_contact where contact_form_type in (4,5,6,7,9) and query_source = 'admin' ";
 
// CRON JOB FOR INITIAL LEAD FOLLOWUP   # STAGE I

$sql = $followup_qyr_part." and is_followed = 0 and followup_id = 0";        
        
$query=mysqli_query($link,$sql);
$lead_data = array();
if(@$query){
    if(mysqli_num_rows(@$query) > 0){
        while($row=mysqli_fetch_object($query)){
            $lead_data = array(
                'contact_id' => $row->contact_id,
                'rep_title' => $row->contact_rep_title,
                'email' => $row->contact_email,
                'name' => $row->contact_person,
                'is_interval_set' => False,
            );
            $followup_result = getFollowupInfo(1 , $link); 
            if(!empty(@$followup_result)){
                $lead_followup_data = array_merge($lead_data,$followup_result);            
                send_mail_and_insert_data($lead_followup_data,$link);
            }else{
                echo "NO FOLLOWUP SET FOR FOLLOWUP ID : 1 <br/>";
            }
        }
    }
}


// CRON JOBS FOR SECOND TO EIGHTH LEAD FOLLOWUPS   # STAGE II to STAGE VIII

$followups = array(1,2,3,4,5,6,7);
foreach($followups as $followup_id){
    echo "FOLLOWUP : ".@$followup_id.'<br/>'; 

    $sql = $followup_qyr_part." and is_followed = 1 and followup_id = '".@$followup_id."' and followup_status = 1 and interval_set_date <= '".$current_datetime."'  ";        
            
    $query=mysqli_query($link,$sql);
    $lead_data = array();
    if(@$query){
        if(mysqli_num_rows(@$query) > 0){
            while($row=mysqli_fetch_object($query)){
                $next_followup_id = $followup_id + 1;
                    $lead_data = array(
                        'contact_id' => $row->contact_id,
                        'rep_title' => $row->contact_rep_title,
                        'email' => $row->contact_email,
                        'name' => $row->contact_person,
                        'is_interval_set' => True,
                        'interval_value' => $row->followup_interval,
                        'interval_type' => $row->interval_type,
                    );
                    $followup_result = getFollowupInfo(@$next_followup_id , $link);
                    if(!empty(@$followup_result)){
                        // echo "<pre>";print_r($followup_result); print_r($lead_data);
                        $lead_followup_data = array_merge($lead_data,$followup_result);
                        // echo "<pre>";print_r($lead_followup_data);
                        send_mail_and_insert_data($lead_followup_data,$link);
                    }else{
                        echo "NO FOLLOWUP SET FOR FOLLOWUP ID : ".@$next_followup_id.'<br/>';
                    }
            }
        }
    }
}
?>