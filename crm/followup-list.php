
<html>
<head>
<title>FollowUps List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->

<?php require_once 'header.php'; ?>

<!--*****************************  HEADER  ************************************** -->

    <h1 class="stats"><span class="fa fa-list-ol"></span> FollowUp Type List</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.location.href='<?php echo BASE_URL.'followup-add';?>'">
        <a href="<?php echo BASE_URL.'followup-add';?>"><i class="fa fa-plus"></i> Add New Followup</a> 
    </button>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
    				<th>ID</th>
    				<th>FollowUp Name</th>
    				<th>FollowUp</th>
    				<th>FollowUp Number</th>

                <?php if($_SESSION["user_type"]=='1'){ ?>
                    <th>Status</th>
    				<th>Options</th>
                <?php } ?>

    			</tr>
    		</thead>
    		<tbody id='deleteCount'>
    			<?php

            //********************  PAGINATION  ****************************//

                if (isset($_GET['pageno'])) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;                                        
                }
                $no_of_records_per_page = 10;
                $offset = ($pageno-1) * $no_of_records_per_page;

                $query='select count(*) from mr_followups';

                $result = $link->query($query);
                $total_rows = mysqli_fetch_array($result)[0];
                $total_pages = ceil($total_rows / $no_of_records_per_page);


    			$getCategories=$link->query("select * from mr_followups limit $offset, $no_of_records_per_page");
    			if($getCategories->num_rows > 0){
	    			while($row=$getCategories->fetch_assoc()){
	    				echo "<tr id='categoryDelete".$row['id']."'><td>".$row["id"]."</td>";
	    				echo "<td>".ucfirst($row["followup_name"])."</td>";
	    				echo "<td>".nl2br(ucfirst($row["followup"]))."</td>";
	    				echo "<td>".@$FOLLOWUPS[$row["followup_no"]]."</td>";

                    if($_SESSION["user_type"]=='1'){ 
                        

                        if($row['status'] == '1'){
                            echo "<td align='center'><a href='#' onclick='changeStatus(".$row["id"].")' id='status".$row["id"]."' class='btn-link btn-active'>Active<a></td>";
                        }else{
                            echo "<td align='center'><a href='#' onclick='changeStatus(".$row["id"].")' id='status".$row["id"]."' class='btn-link btn-inactive'>Inactive<a></td>";
                        }
                        
	    				echo "<td colspan='2'  align='center'><a href='followup-edit?id=".$row["id"]."'><i class='fa fa-edit text-primary'></i></a>

                            <button data-toggle='modal' data-target='#removeMe".$row["id"]."' style='border:0;background:none'><i class='fa fa-trash text-danger'></i></button>
                                <div id='removeMe".$row["id"]."' class='modal fade' role='dialog'>
                                    <div class='modal-dialog'>
                                        <div class='modal-content'>
                                            <div class='modal-header'>
                                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                <h4 class='modal-title'> <span class='fa fa-trash'></span> Delete Confirmation  </h4>
                                            </div>
                                            <div class='modal-body'>
                                                <p style='font-size: 1.2em'><strong>Are you sure you want to remove this item ?</strong></p>
                                            </div>
                                            <div class='modal-footer'>
                                                <button class='btn btn-primary btn-confirm' type='submit' onclick='confirmDelete(".$row["id"].")' >Confirm</button>
                                                <button type='button' class='btn btn-warning btn-cancel' data-dismiss='modal'>Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div></td>";
                        }
                        echo "</tr>";
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='6' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO FOLLOWUPS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>
    <ul class="pagination">
        <li><a href="?pageno=1">First</a></li>
        <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
            <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
        </li>
        <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
            <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
        </li>
        <li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
    </ul>

    <br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
<script type="text/javascript">
        function changeStatus(id)
            {
                var stat=document.getElementById("status"+id).innerHTML;
                var xhr=new XMLHttpRequest();
                xhr.onreadystatechange=function()
                {
                    if(this.readyState===4 && this.status===200)
                    {
                        if(this.responseText=='Active'){
                             document.getElementById("status"+id).style.color='#479143';
                        }else{
                             document.getElementById("status"+id).style.color='#dc470d';
                        }
                        document.getElementById("status"+id).innerHTML=this.responseText;

                    }
                }
                xhr.open("get","ajaxoperations.php?id="+id+"&bannewordStat="+stat,true);
                xhr.send();
            }
            function confirmDelete(id)
            {
                 var xhr=new XMLHttpRequest();
                xhr.onreadystatechange=function()
                {
                    if(this.readyState===4 && this.status===200)
                    {
                        var response=this.responseText;
                        if(response.trim()!==""){
                            document.getElementById("deleteCount").innerHTML=this.responseText;
                        }else{
                            $("#categoryDelete"+id).css({display:'none'});
                        }
                    }
                }
                xhr.open("get","ajaxoperations.php?bannewordDeleteId="+id,true);
                xhr.send();
            }
    </script>