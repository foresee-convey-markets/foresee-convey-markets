
<html>
<head>
<title>Add Lead  | Foresee Convey Markets</title>



 <!--*****************************  HEADER  ************************************** -->

<?php 
    require_once 'header.php';
    require_once 'auth.php';
?>

<!--*****************************  HEADER  ************************************** -->

<?php 


$id=@$_GET["id"];
$getLeadDetails=$link->query("select * from mr_form_contact where contact_id='@$id'");
$row=$getLeadDetails->fetch_assoc();


?>
<?php
    if(isset($_POST["submit"]))
    {
        
        $name=$_POST["name"];
        $email=$_POST["email"];
        $phone=$_POST["phone"];
        $company=$_POST["company"];
        $country=$_POST["country"];
        if(isset($_POST["title"])){
	        $title=$_POST["title"];
        }else{
        	$title=null;
        }
        if(isset($_POST["message"])){
	        $message=$_POST["message"];
        }else{
        	$message=null;
        }
        
        $date=$_POST["date"];
        
        
        
        $sql=mysqli_query($link,"update mr_form_contact set contact_person='".$name."',contact_email='".$email."',contact_phone='".$phone."',contact_company='".$company."',contact_country='".$country."', contact_rep_title='".$title."',contact_msg='".$message."',contact_datetime='".$date."' where contact_id='$id' ");
        
            if($sql===TRUE )
            {
                $message='<b style="color:#479143">Lead Updated successfully !</b>
                <meta http-equiv="refresh" content="2,url='.BASE_URL.'lead-list "/>'; 
            }
            else
            {                
                $message="<b style='color:#dc470d'>Error Updating Lead !".mysqli_error($link)."</b>";                
                
            }
    }
    
    
    ?>

    <h1 class="stats"><span class="far fa-edit"></span> Add Lead</h1><br/><br/><br/>

    <button  class='btn btn-primary helper-item' onclick="window.history.back()">
        <a href="#" ><i class="fa fa-arrow-left"></i> Back</a> 
    </button>
	<div class="form-container">
		<?php 
			if(isset($message)){
				echo "<br/><h3 align='center'>".$message."</h3>";
			}
		?>
	    <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."?id=".$_GET["id"]; ?>" method='post' name="upload" id="upload" enctype='multipart/form-data'>

	    	
	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Name<span class="star">*</span> :</label>
		    		<input  name="name" class="form-control" required=""   value="<?php echo (isset($row["contact_person"])) ? $row["contact_person"] : ''; ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Email<span class="star">*</span> :</label>
		    		<input type="email" name="email" class="form-control" required=""  value="<?php echo (isset($row["contact_email"])) ? $row["contact_email"] : ''; ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Phone<span class="star">*</span> :</label>
		    		<input  name="phone" class="form-control" required=""   value="<?php echo (isset($row["contact_phone"])) ? $row["contact_phone"] : ''; ?>"/>
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Company<span class="star">*</span> :</label>
		    		<input  name="company" class="form-control" required=""  value="<?php echo (isset($row["contact_company"])) ? $row["contact_company"] : ''; ?>" />
		    	</div>
	    	</div>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Country :</label>
		    		<input  name="country" class="form-control"   value="<?php echo (isset($row["contact_country"])) ? $row["contact_country"] : ''; ?>" />
		    	</div>
	    	</div>

	    	<?php if(!empty($row["contact_rep_title"])){ ?>
	    		<div class="form-group">
		    		<div class="col-md-10">
			    		<label class="control-label">Report title<span class="star">*</span> :</label>
			    		<input  name="title" class="form-control" required=""  value="<?php echo (isset($row["contact_rep_title"])) ? $row["contact_rep_title"] : ''; ?>" />
			    	</div>
		    	</div>
	    	<?php } ?>

	    	

	    	<?php if(!empty($row["contact_msg"])){ ?>
	    		<div class="form-group">
		    		<div class="col-md-10">
			    		<label class="control-label">Message :</label>
			    		<input  name="message" class="form-control"  value="<?php echo (isset($row["contact_msg"])) ? $row["contact_msg"] : ''; ?>" />
			    	</div>
		    	</div>
	    	<?php } ?>

	    	<div class="form-group">
	    		<div class="col-md-10">
		    		<label class="control-label">Date<span class="star">*</span> :</label>
		    		<input  name="date" class="form-control datepicker" required=""  value="<?php echo (isset($row["contact_datetime"])) ? $row["contact_datetime"] : ''; ?>" />
		    	</div>
	    	</div>
	    	<div>
		    	<button type="submit" name="submit" class="btn btn-upload btn-update"><span class="fa fa-check-square"></span> Update</button>
		    	<button type="button" name="cancel" class="btn btn-upload" onclick="window.history.back()"><span class="fa fa-times"></span> Cancel</button>
		    </div>
	    </form>
	</div><br/><br/>



    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
