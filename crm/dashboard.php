
<!DOCTYPE html>
<html>
<head>
    <title>Dashboard | Foresee Convey Markets</title>

    <!--*****************************  HEADER  ************************************** -->

    <?php require_once 'header.php'; ?>

    <!--*****************************  HEADER  ************************************** -->
    <style type="text/css">
        .bg-warning{
            background: #dfb924;
        }
    </style>
    <?php 

//*********************** CHECK FOR SESSION ****************************//


//*********************** LEADS ****************************//
    $getLeads=$link->query("select count(contact_id) from mr_form_contact where contact_form_type in (4,5,6,7,9)");
    $leads=$getLeads->fetch_row()[0];

//*********************** Checkouts ****************************//
    $getCheckouts=$link->query("select count(contact_id) from mr_form_contact where contact_form_type = 8");
    $checkouts=$getCheckouts->fetch_row()[0];

//*********************** CHECKOUTS ****************************//
    $getGlobalCheckouts=$link->query("select count(id) from mr_global_checkout");
    $global_checkouts=$getGlobalCheckouts->fetch_row()[0];

//*********************** FollowUps ****************************//
    $getFollowups=$link->query("select count(id) from mr_followups");
    $followups=$getFollowups->fetch_row()[0];

//*********************** FollowedUps ****************************//
    $getFollowedups=$link->query("select count(id) from mr_crm_followups");
    $followedups=$getFollowedups->fetch_row()[0];


    if(@$_SESSION["user_type"]=='1'){

    ?>

    <h1 class="stats"><span class="fa fa-database"></span> STATISTICS</h1><br/>

    <div class="row statistics">
        <div class="col-md-4">
            <h3>CHECKOUTS</h3>
            <p><?php echo $checkouts; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'lead-list?type=8'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>LEADS</h3>
            <p><?php echo $leads; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'lead-list?type=4'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>GLOBAL CHECKOUTS</h3>
            <p><?php echo $checkouts; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'global-checkout-list'; ?>'">View All</button></p>
        </div>
    </div><br/>
    <div class="row statistics statistics-last">
        <div class="col-md-4">
            <h3>FollowUp Types</h3>
            <p><?php echo $followups; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'followup-list'; ?>'">View All</button></p>
        </div>
        <div class="col-md-4">
            <h3>Followed Leads</h3>
            <p><?php echo $followedups; ?></p>
            <p><button onclick="window.location.href='<?php echo BASE_URL.'followed-list'; ?>'">View All</button></p>
        </div>
    </div>

<?php } ?>
   
    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
