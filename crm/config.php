<?php
require_once 'helper.php';
if(!session_id()){
	session_start();
}
ini_set('date.timezone','Asia/Kolkata');
date_default_timezone_set('Asia/Kolkata');

//****************************  DATABASE CONNECTION   *************************************//

$host = 'foreseeconvey-markets.com';
if(isset($_SERVER['HTTP_HOST'])){
	$host=$_SERVER['HTTP_HOST'];
}
$LOCAL_ARRAY = array('192.168.1.197', '127.0.0.1' , 'localhost');

$SHOW = false;
	
//****************************  DEVELOPMENT   *************************************//

if(in_array($host,$LOCAL_ARRAY)){	
	error_reporting(-1);
	ini_set('display_errors', 1);

	//****************************  BASE URL   *************************************//

	define('BASE_URL','http://localhost/marketresearch.com/crm/');
	define('ADMIN_URL','http://localhost/marketresearch.com/ckpanel/');
	define('ENVIRONMENT','development');
	define('SHOW_DEBUG_BACKTRACE',true);

	$db_server    = "localhost";
	$db_user       ="root";
	$db_pass       ="root";
	$db_name      ="market_research";
	@$link = new mysqli($db_server, $db_user, $db_pass,$db_name);

	if(@$link->connect_error){
		$SHOW = true;
	} 

}else{	
	
//****************************  PRODUCTION   *************************************//

	ini_set('display_errors', 0);
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
		
	// error_reporting(-1);
	// ini_set('display_errors', 1);

	//****************************  BASE URL   *************************************//

	define('BASE_URL','https://foreseeconvey-markets.com/crm/'); 
	define('ADMIN_URL','https://foreseeconvey-markets.com/ckpanel/');
	define('ENVIRONMENT','production');
	define('SHOW_DEBUG_BACKTRACE',false);

	
	$db_server    ="localhost";
	$db_user       ="foresee_convey_markets";
	$db_pass       ="@giY+N^vL^id)lA]Kv";
	$db_name      ="foresee_convey_markets";
	@$link = new mysqli($db_server, $db_user, $db_pass,$db_name);
	if(@$link->connect_error){
		$SHOW = true;
	} 

}
