
<html>
<head>
<title>Global Checkout List | Foresee Convey Markets</title>


 <!--*****************************  HEADER  ************************************** -->


<?php 
    require_once 'header.php';
    require_once 'auth.php';

    $query='';
    if(@$_GET['query']){
        $query=$_GET['query'];
    }
?>
<!--*****************************  HEADER  ************************************** -->

<?php

//********************  PAGINATION  ****************************//
$where = '';
$input_query = '';
if (isset($_GET['query']) && !empty($_GET['query'])) {
    $page_query = "&query=".trim($_GET['query']);
    $input_query = trim($_GET['query']);
    $where.= " name like '%".$input_query."%' or email like '%".$input_query."%' ";
}    
if(!empty($where)){
    $where = " where ".$where;
}

$data = create_links($_GET, $link , 'mr_global_checkout', $where);

if(@$data['total_rows'] > 0 && @$data['pageno'] > @$data['total_pages']){
    $url = BASE_URL.'global-checkout-list'.@$data['last_page'];
    echo "<meta http-equiv='refresh' content='0, url=".@$url."'/>";
}
$input_placeholder = 'Search here for Client Name, Client Email Address';

?>
    <h1 class="stats"><span class="fa fa-list-ol"></span> Global Checkout List <span class="text-danger">[</span><span class='count text-danger'><?=@$data['total_rows']?></span><span class="text-danger">]</span></h1><br/><br/>

    <div class="pagination-form">
        <?php require_once 'pagination-form.php' ?>
    </div>

	<div class="table-responsive">
    	<table class="table table-bordered table-hover">
    		<thead class="text-primary">
    			<tr>
                    <th>Sr.No</th>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th style="min-width: 200px;">Report Title</th>
                    <th>Company</th>
                    <th>Report Price</th>
                    <th>Report License</th>
                    <th>Date</th>
                    <th>Expiry Date</th>
                    <th>Link Status</th>
    			</tr>
    		</thead>
    		<tbody id="deleteCount">
    			<?php

    			$getGlobalCheckouts=$link->query("select * from mr_global_checkout ".@$where." order by id desc limit ". @$data['offset']. "," . @$data['no_of_records_per_page']);
    			if($getGlobalCheckouts->num_rows > 0){
                    $i=1;
	    			while($row=$getGlobalCheckouts->fetch_assoc()){                        
                        $sr_no = ( @$data['no_of_records_per_page'] * (@$data['pageno'] == '1' ? 0 : @$data['pageno']-1) ) + $i;

                        $name="PRR".rand(1,1000);
                        $encrypted_id=encrypt_decrypt('encrypt',@$name.'-'.@$row['id']);
                        // $encrypted_id=str_ireplace(array(',','-','.',';',':','/','"',"'",')','(','{','}','[',']','!','@','#','$','%','^','&','*','&','*','+','_','=','|',' '),'',@$encrypted_id);
	    				$status=($row["status"] === '1')  ? '<b style="color:green">Active</b>' : '<b style="color:#f00">Expired</b>';
                        $onclick= ($row['status'] !== '1') ? 'return false;' : '';
                        echo "<tr id='globalCheckoutDelete".$row['id']."'><td>".@$sr_no."</td>";
                        echo "<td>".$row["name"]."</td>";
                        echo "<td>".$row["email"]."</td>";
                        echo "<td>".$row["title"]."</td>";
                        echo "<td>".$row["company"]."</td>";
                        echo "<td>$".$row["price"]."</td>";
                        echo "<td>".$row["license"]."</td>";
                        echo "<td>".$row["date"]."</td>";
                        echo "<td>".$row["expiry_date"]."</td>";
                        echo "<td>".@$status."</td>";
                        echo "</tr>";
                        $i++;
	    			}
	    		}else{
					echo "<tr style='height:60px'><td colspan='10' align='center'  style='padding:25px;font-size:1.8em'><span class='fa fa-exclamation-triangle text-danger'></span> <strong>NO GLOBAL CHECKOUTS FOUND !</strong></td></tr>";
	    		}
    			?>
    		</tbody>
    	</table>
    </div><br/><br/>
    <ul class="pagination">
        <?=pagination(@$data);?>
    </ul>
    <br/><br/>


    <!--*****************************  FOOTER  ************************************** -->

    <?php require_once 'footer.php'; ?>

    <!--*****************************  FOOTER  ************************************** -->
    